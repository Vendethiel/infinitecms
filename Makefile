conf: config/
	@lsc -jco . config/

doctrine:
	@php bin/doctrine.php $(filter-out $@,$(MAKECMDGOALS))

build-assets:
	@php bin/assets.php

autoload:
	@"bin/c" dump-autoload -o

assets: cc build-assets

cc:
	@rm -rf cache/
	@rm -rf vendor/php/Nami-Doc/Sprockets-PHP/cache

.PHONY: conf doctrine build-assets autoload assets