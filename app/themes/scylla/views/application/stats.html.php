<li>
	<?php echo lang('server_state') ?> :&nbsp;
	<span class="badge badge-<?php if (server_state() == 'ON') echo 'success'; else echo 'important'; ?> "><?php echo server_state() ?>
</li>
<?php
if ($cache = Cache::start('layout_stats_scylla', '+1 hour')):
//I don't see any reason for caching when logged? since we already loaded one table
// (the biggest memory "blow" is done at the first Doctrine_Core::getTable())

	$created = array();
	$qC = Query::create()
			->select('count(guid) as count')
				->from('Account');
	$c = $qC->fetchOne(Doctrine_Core::HYDRATE_NONE);
	$qC->free();
	$created['acc'] = $c['count'];

	$qC = Query::create()
			->select('count(guid) as count')
				->from('Character');
	$c = $qC->fetchOne(Doctrine_Core::HYDRATE_NONE);
	$qC->free();
	$created['char'] = $c['count'];
?>
<li>
	<?php echo pluralize(lang('acc'), $created['acc']) . ' ' . pluralize(lcfirst(lang('created_')), $created['acc']) ?> :
	<span class="badge"><?php echo $created['acc'] ?></span>
</li>
<li>
	<?php echo pluralize(lang('character'), $created['char']) . ' ' . pluralize(lcfirst(lang('created_')), $created['char']) ?> :
	<span class="badge"><?php echo $created['char'] ?></span>
</li>
<li>
	<?php echo make_link('@stats.game', '<span class="label label-info">' . lang('stats.all') . '</span>', array(), array('data-unless-selector' => '#stats')) ?>
</li>
<?php
	$cache->save();
endif;
?>