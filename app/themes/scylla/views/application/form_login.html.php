<?php
//Username
//%input.input{:placeholder => "Nom de compte", :type => "text"}
//	%br
//	Password
//	%input.input{:placeholder => "Mot de passe", :type => "password"}
//	br
//	center
//		a.btn.btn-info.btn-large{:href => "#"} Connexion

echo tag('form', array('method' => 'POST', 'action' => replace_url('@sign_in'), 'id' => 'login_form'),
 input(Member::CHAMP_PSEUDO, lang('account'), null, '', array('class' => 'input')) .
 input(Member::CHAMP_PASS, lang('acc.register.password'), 'password', array('class' => 'input')) .
 input('send_login', null, 'hidden') .
 '<center><input type="submit" class="button btn-info btn-large" value="' . lang('acc.log') . '"></center>' .
 input_csrf_token());