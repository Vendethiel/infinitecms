BaseMenu = require './base_menu'

module.exports = class Menu extends BaseMenu
	show: [0 1]

	(selector) ~>
		@menus-div = $ selector

		@ensure-cookie!

		@titles = @menus-div.find '.titleright'
		@menus = @menus-div.find '.textright'

		for menu, i in @menus
			@titles[i]dataset.id = menu.dataset.id = i

			if i not in @show
				menu.style.display = 'none'

		@titles.click @toggle

	toggle: !~>
		menu = it.current-target.next-element-sibling
		$menu = $ menu
		id = menu.dataset.id

		@[if $menu.is ':visible' then 'hide' else 'display'] id

		$menu.slide-toggle!