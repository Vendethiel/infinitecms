login-force = login-processing = false
login-elem = $ '#login_form'
login-f =
	pseudo: login-elem.find "\#form_#{CHAMP_PSEUDO}" #the {} aren't required but added for clarity
	pass: login-elem.find "\#form_#{CHAMP_PASS}"

login-elem.submit ->
	return if login-force or login-processing

	msg = ''
	/*
	for k, lang of pseudo: 'pa' pass: 'ps'
		unless login-f[k]val!
			msg += lang.form"must_specifiy-#lang" + '<br/>'
	*/
	unless login-f.pseudo.val!
		msg += lang.form.must_specify_pa + '<br/>'
	unless login-f.pass.val!
		msg += lang.form.must_specify_ps + '<br/>'

	if msg
		error-message.html msg
		error-div.dialog 'open'
		return false

	$this = $ @
	login-processing := true
	$.post url-login, $this.serialize!, (status) ->
		login-processing := false

		if status is 'ok'
			login-force := true
			show-note error-messages.ok
			$this.submit!
			
			true
		else
			error-message.html error-messages[status] or status
			error-div.dialog 'open'
			
			false
	.error ->
		login-processing := false
		alert error-messages.page

	it.prevent-default!