var event-participants, events

export !function show-event(id)
	event-participants
		.dialog 'open'
		.find '.event'
		.hide!

	let @ = events[id]show!
		event-participants
			.dialog 'option' 'title' @data 'name'

export !function register-event(id)
	events[id] = $ "\#event-#id"
		.add-class 'event'
		.hide!
		.append-to event-participants

#might be real tricky to move this to use component(1) and probably won't
# since it has parameters and may be used in other places
export !function bootstrap-events(date)
	return if events?

	event-participants ?:= $ '#eventParticipants'
	events ?:= []

	if date
		for type in <[month year]>
			let
				$ "\#form_#type" .change !->
						return if it is date[type]

						@form.submit!

	<-! page-bind
	event-participants := events := null #dialog auto-disposed