require! './loaders'
loader = new loaders[switch LOAD_TYPE
	| LOAD_NONE    => 'NoneLoader'
	| LOAD_CONTENT => 'ContentLoader'
	| LOAD_MDIALOG => 'DialogLoader']

@follow-link = export loader.follow-link # @-exported for AJaX redirecting


# I don't like that.
# Currently I have no event bound BEFORE php.jQ() is executed
#  but after DOM is loaded so yeah... I gotta call it explicitely
#  the first time.
export bind-dialogs = !->
	dialogs = middle.find '.dialog-base'
		.add-class 'dialog-bound'

	page-bind ->
		dialogs.each !->
			$this = $ @

			$this.dialog 'close'
			$this.parent!remove!

		dialogs := null #clear ref

export locations = []

export page-binds = loader.create-page-binder!

export page-bind = !-> page-binds.add ...

$ -> page-binds.process 'after'

################################
###########URL Loader###########
################################
error-div = $ '#errorDiv'
error-message = error-div.find '#message'

if LOGGED
	#@todo this currently relies on js_link() in the Character class
	#and should fucking be changed
	<- $
	window.choose-main-char = !->
		document.location = url-main-char + it
	window.main-char-selector = $ '#selectMainChar'
	$ '#charactersList' .accordion {height-style: 'content', +collapsible, -active}



export update-selectors-disabled = [UPDATE_SELECTOR ? void]



serv-info = $ '#serv-info'
pm-info = $ '#pm-info'
pm-inbox = $ '#pm-inbox'
stats = $ '#stats'
title = $ 'title'
middle = $ '#milieu'
middle-container = middle.parent!

lag = 1 #lag here is used to space refreshes, in case the user isn't doing anything
unless MAINTENANCE
	:refresh let #let here allows to directly call the function
		window.set-timeout do
			!-> $.ajax do
				url: url-misc
				success: !->
					explode-data it |> populate-content

					lag *= 2 unless lag > REFRESH_TIME #lag can't go upper than REFRESH_TIME
					refresh!
			(lag - 1 + REFRESH_TIME) * 1000.0


var javascripts
$ -> javascripts ?:= [src for {src} in $ 'script[src]']

in-ajax = false
export populate-content = !([page-title, get-path, server-state, pm-content, update-selector, content]) ->
	loader.stop?!

	unless page-title
		if DEBUG
			error-message.html page-title
			error-div.dialog 'open'
		return

	#todo we need a way to do this ! a better one
	#prolly create a update_server.js.ls script/theme ? :(
	#class toggling might be the best option
	serv-info.css 'background' status[server-state]

	pm-info.html pm-content
	pm-inbox.html inbox-html if pm-content

	if update-selector is 'null'
		$ '<div/>'
			.html content
			.append-to 'body' #in order to process js
			.remove! #remove it after
		return

	lag := 0 #reset activity

	update-selector = '' if update-selector in update-selectors-disabled

	if update-selector and $ update-selector
		#don't check if element is empty :
		# we may be following a link from the update-selector
		#so the content has to be checked only when we click
		# on the link with the data-unless-selector
		that.html content

		loader.follow-unless-selector that
		return
	

	middle.empty!remove!
	middle := $ content

	#load script and mark them loaded - they're not appended to the dom tho
	javascripts ++= for {src} in middle.filter 'script[src]'
		unless src in javascripts
			$.get-script src

			src


	middle .= filter ':not(script[src])'
	scripts = middle.filter 'script'
	middle .= filter ':not(script)'
	
	middle .= wrap-all '<div id="milieu" />' .append-to middle-container
	bind-dialogs!

	#don't mess up evaluation order ...
	#XXX this really needs to be fixed, pita to the users
	#    but I may need to do some calc based on how many scripts are loaded.
	set-timeout -> #wait a bit for scripts to load :(
		middle.append scripts
	, 200ms

	title.html page-title
	that.html (page-title / '&bull;')0.trim! if $ 'a.brand' # support brands

	page-binds.process 'after'


export update-content = !(url, popstate = false) ->
	return if in-ajax
	!:= in-ajax

	loader.start?!
	page-binds.process 'before'

	$.ajax do
		url: url
		success: !->
			[page-title, ..., update-selector,,]:data = explode-data it
			populate-content data
			in-ajax := false

			if not popstate and update-selector is '' and window.history?push-state
				history.push-state null, page-title, url
		error: !->
			in-ajax := false
			loader.html """
			An error occured during the page loading. - Une erreur s\'est produite durant le chargement de la page.
			You can try clear you cache/ directory. Vous devriez essayer de vider le dossier cache/
			<br>#{[DEBUG && it]}
			"""

explode-data = explode \<~>, _, 6
$ document .on 'click' 'a.link' loader.follow-link

#chrome bubbles popstate on first request
# firefox does not, because it follows a precedent version of the RFC
# so we ensure dom is ready before binding popstate
popped = false
initial-url = location.href
$ window .bind 'popstate' ->
	unless popped
		if location.href is initial-url
			return
		popped = true

	update-content location.href, true #in popstate