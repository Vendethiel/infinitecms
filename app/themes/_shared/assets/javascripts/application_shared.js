/**
 *= require config
 *= require variables
 *
 * Component(1) build
 *= require build
 *
 *= require jquery/dist/jquery
 * Now ... We have some hacks from the jQ 1.9 breaks
 * and the component(1) integration
 *= require jq/hacks
 */
$.uiBackCompat = false; //gotta remove it later
/**
 * This is the "core part" - gotta remove some
 *  Or simply use "skip"
 *= require jquery-ui/ui/jquery.ui.{core,widget,mouse,position,
 *=draggable,menu,tabs,tooltip,
 *=accordion,autocomplete,button,dialog}
 *
 *= require jq/{editInPlace,timers}
 *
 *= require_tree classes/
 *
 * order doesn't matter : cjs components !
 *= require_directory .
 *
 * Require()able modules, 1/action
 *= require_tree controllers/
 *
 * Scripts - these shall be moved to controllers/ scripts
 *= require_directory scripts/
 */