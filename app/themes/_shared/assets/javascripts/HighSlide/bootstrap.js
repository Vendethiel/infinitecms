hs.graphicsDir = assets_shared + 'HighSlide/';
hs.creditsText = hs.creditsTitle = '';

function resetHighSlide()
{
	$('.image:not(.highslide-onclickevent)').each(function ()
	{
		this.onclick = function ()
		{ //can't use event bubbling cause hs relies on onclick attr
			return hs.expand(this, {wrapperClassName: 'borderless floating-caption', dimmingOpacity: 0.75, align: 'center'});
		};
		this.class += ' highslide-onclickevent';
	});
}

$(function ()
{
	pageBind(resetHighSlide, 'after');
	resetHighSlide();
});