dialog-opt = {+modal, +draggable, -autoOpen, width: 600}
dialog-opt-o = dialogOpt with {+autoOpen}
calendar_opts = {+showButtonPanel, +changeMonth, +changeYear, +showOtherMonths, +selectOtherMonths, +showWeek, firstDay: 1 showAnim: "slideDown" dateFormat: "dd/mm/yy"}
tiny-mce-opt = 
	#script_url : tinyMceBaseUrl + 'tiny_mce.js',

	# General options
	theme : 'advanced'
	plugins : 'safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,iespell,spellchecker,emotions,iespell,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,fullpage,noneditable,visualchars,nonbreaking,xhtmlxtras,template,inlinepopups' ##'fullpage,save'
	theme_advanced_buttons3_add : 'fullpage'

	# templates
	skin : 'o2k7'
	skin_variant : 'silver'

	# themes opts
	theme_advanced_buttons1 : 'save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect'
	theme_advanced_buttons2 : 'cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor'
	theme_advanced_buttons3 : 'tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen'
	theme_advanced_buttons4 : 'insertlayer,moveforward,movebackward,absolute,|,styleprops,spellchecker,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak' #'blockquote,|,insertfile,insertimage',
	theme_advanced_toolbar_location : 'top'
	theme_advanced_toolbar_align : 'left'
	theme_advanced_statusbar_location : 'bottom'
	theme_advanced_resizing : yes

export dialog-opt, dialog-opt-o, calendar_opts, tiny-mce-opt