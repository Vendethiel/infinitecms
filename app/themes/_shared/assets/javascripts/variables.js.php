searchLinks = {
	accounts: "<?php echo replace_url('@account.show') ?>",
	characters: "<?php echo replace_url('@character.show') ?>",
	guilds: "<?php echo replace_url('@guild.show') ?>"
};	


var path = '<?php echo getPath() ?>',
	absPath = '<?php echo getPath(true) ?>',
	assets_shared = path + '',
	urlMisc = "<?php echo replace_url(array('controller' => 'Misc')) ?>",
	urlLogin = "<?php echo to_url(array('controller' => 'User', 'action' => 'login', 'check' => '1'), false) ?>",
	urlMainChar = "<?php echo to_url(array('controller' => 'User', 'action' => 'main', 'id' => '')) ?>",
	urlShopSearch = "<?php echo replace_url('@shop.search') ?>",
	urlSearch = "<?php echo to_url(array('controller' => 'Misc', 'action' => 'search', 'ext' => 'json')) ?>",
	urlReverseFriends = "<?php echo to_url(array('controller' => 'Account', 'action' => 'reverseFriends', 'ext' => 'json')) ?>";

searchLang = { //TODO
};

var lang = <?php echo json_encode(lang()) ?>;
lang.shop = {
	need_filter: "<?php echo lang('shop.need_filter') ?>"
};
lang.form = {
	must_specify_ps: <?php echo javascript_val(lang('acc.login.spec_login')) ?>,
	must_specify_pa: <?php echo javascript_val(lang('acc.login.spec_pass')) ?>
};
lang.search = {
	accounts: '<?php echo lang('rank.player') ?>',
	characters: '<?php echo lang('character') ?>',
	guilds: '<?php echo lang('acc.ladder.guild') ?>'
};

var form = {
	select: {
		'1 to 5':  <?php echo javascript_val(input_select_options(array(1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5))) ?> //or array_combine($ref=range(1,5),$ref)
	}
}

var inboxHtml = '&nbsp;<?php echo make_link('@pm', make_img('icons/email', EXT_PNG, lang('PrivateMessage - index', 'title')), array(), array('data-unless-selector' => '#pm')) ?>',
	errorMessages = {
		'bad': <?php echo javascript_val(lang('acc.invalid_login_action')) ?>,
		'ban': <?php echo javascript_val(lang('acc.banned')) ?>,
		'ok': <?php echo javascript_val(lang('acc.connected')) ?>
	},
	loadingMsg = <?php echo javascript_val(lang('loading')) ?> + "...";


<?php
$constants = array(
	'LOAD_NONE', 'LOAD_MDIALOG', 'LOAD_CONTENT',
	'DEBUG',
	'Member::CHAMP_PSEUDO', 'Member::CHAMP_PASS'
);
foreach ($constants as $c)
{
	if (false === strpos($c, '::'))
		$n = $c;
	else
		list(, $n) = explode('::', $c);

	echo 'var ' . $n . ' = ' . javascript_val(constant($c)) . ";\n";
}
?>

var LEVEL_ADMIN = <?php echo javascript_val(level(LEVEL_ADMIN)) ?>,
	LEVEL_LOGGED = <?php echo javascript_val(level(LEVEL_LOGGED)) ?>,
	LOAD_TYPE = <?php echo $config['LOAD_TYPE'] ?>,

	MAINTENANCE = <?php echo javascript_val(!empty($config['MAINTENANCE'])) ?>,
	REWRITE = <?php echo javascript_val(REWRITE) ?>,
	REFRESH_TIME = <?php echo $config['REFRESH_TIME'] ?>,

	status = {on: <?php echo javascript_val(asset_path('statuson' . EXT_JPG)) ?>,
		off: <?php echo javascript_val(asset_path('statusoff' . EXT_JPG)) ?>};

//global.
cleanRequire = function (a, co, ac) {
	if (!a)
		a = 'InfiniteCMS/controllers/' + controller + '/' + action;

	if (require.modules[a])
	{
		require(a);
		pageBind(function () {
			// clean after the module ran
			// I apparently thought "before" was okay ... but it wasn't ...
			require.modules[a].exports = null;
		}, 'after');
	}
}