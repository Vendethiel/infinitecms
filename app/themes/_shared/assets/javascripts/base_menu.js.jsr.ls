require! <[unique map select cookie]>

module.exports = class BaseMenu
	(selector) ->
		@show = [0]

		@menus-div = $ selector
		@menus = @find-menus!

		@ensure-cookie!

		for elem, i in @menus
			id = i + 1
			elem.dataset.i = id

			if id in @show
				@show-one elem
			else
				@toggle id, $ elem

		#live 'click' unneeded
		@titles.click !~>
			$target = $ it.target .parent!
			@toggle $target.0.dataset.i, $target

	display: !->
		@show.push +it
		@update-cookie!

	hide: !->
		@show = unique [+i for i in @show when i isnt +it]
		@update-cookie!

	ensure-cookie: !->
		if cookie 'menu' #@cookie-key = "menu_#selector
			@show = unique map that / '|' Number
			@show = select @show, Number
		else
			@update-cookie!

	update-cookie: !->
		cookie 'menu' (unique @show) * '|' #@cookie-key