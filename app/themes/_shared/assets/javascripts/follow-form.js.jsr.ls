module.exports = !->
	it.prevent-default!
		
	let @ = $ @ #avoid one-line cascade
		<- $.post @attr('action'), @serialize!
		explode-data it |> populate-content
		false