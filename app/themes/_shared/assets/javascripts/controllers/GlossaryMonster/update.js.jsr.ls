$ '#grades' .tabs!
$ 'legend'
	..find 'u'
		..css 'color' 'blue'
		..click -> $ @ .parent!next!slide-toggle!
	..next!
		..hide!

spell-template = $ '.spell-template:first'
	.clone! #clone it before it's modified

add-lock = false
$ '.add-spell' .click !->
	return if add-lock
	add-lock := true

	parent = $ @ .parent!parent! #fieldset
		i = ..data 'i'
		actual = ..find '.actual'

	id = actual.html! |> parseInt
	actual.html ++id

	$ "\#spells-#i" .show!

	elem = spell-template.clone!
		..find 'input' .attr 'name' "grade[#i][spell][#id][]"

	parent.find '.spells'
		.append elem

	add-lock := false