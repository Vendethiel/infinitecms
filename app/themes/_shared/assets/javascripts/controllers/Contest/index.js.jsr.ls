require! 'more'
list = $ '#contests' .0

return unless list?

more list
	.max 5
	.more lang.more
	.less lang.less
	.render!

page-bind !-> list := null