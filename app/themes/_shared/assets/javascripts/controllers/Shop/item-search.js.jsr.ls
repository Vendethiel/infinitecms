module.exports = class ItemSearch
	need-refresh: false

	([elem, open-link]:anchors, @url) ->
		@search-item = $ elem
			..dialog {-autoOpen, +draggable, +resizable, width: 600}

		$ open-link .click @open

	open: !~>
		it?prevent-default!
		@search-item.dialog 'open'

		unless @need-refresh
			return false

		!=@need-refresh
		@search-item.html loading-msg

		success = ~>
			@update it
			@bind-form!
		$.ajax {@url, success}

	update: !~>
		@search-item.html it
		@open! #call it with !@need-refresh for dialog

	bind-form: !~>
		event <- @search-item.find 'form' .submit
		if $ @ .find ':checked' .length is 0
			error-div
				.html lang.shop.need_filter
				.dialog 'open'
			false