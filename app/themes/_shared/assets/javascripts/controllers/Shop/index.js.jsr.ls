ItemSearch = require './item-search'

new ItemSearch <[#searchItem #open-item-search]> url-shop-search


$ '.cat' .each ->
	$this = $ this

	children-ul = $ "\#children-#{$this.attr 'id'}"
	return unless children-ul.length
	
	$this.hover do
		-> children-ul.show!
		-> children-ul.hide!

return unless selected?
load = ->
	page-binds.process
		.. 'before'
		.. 'after'

categories = $ '#categories' .tabs {selected, load}