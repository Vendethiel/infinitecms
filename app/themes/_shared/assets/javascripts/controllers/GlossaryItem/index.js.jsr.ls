categories = {}
categories-elem = $ '#itemcategories'
	..children 'li' .each ->
		categories[@getElementsByTagName 'a' .0.inner-HTML.to-lower-case!] = @

$ '#searchItem' .keyup ->
	value = @value.to-lower-case!
	list = $ '<ul/>' id: 'itemcategories'

	approximative-finds = []

	for name, category of categories
		switch name.to-lower-case!index-of value
		| -1 => continue
		| 0  => list.append category
		| _  => approximative-finds.push category

	for category in approximative-finds
		list.append category

	$ '#itemcategories' .replace-with list

page-bind !-> categories := categories-elem := null