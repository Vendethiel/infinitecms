export class Registration-Form extends Form
	fields:
		tos: []
		account: (!= '')
		pass: (!= '')
		pseudo: (!= '')
		email: (is /^[a-zA-Z0-9_-]+\@[a-zA-Z0-9_-]+\.[a-zA-Z]{1,4}$/)
		question: (!= '')
		reponse: (!= '')