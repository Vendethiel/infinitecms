require! <[debounce dom ajax position]>

set-cursor = (document.body.style.cursor =)
body = dom document.body

# BAH.
dom.List::empty = ->
	while @els.0.firstChild
		@els.0.removeChild that

	@

css = dom.List::css
dom.List::css = ->
	if typeof it is 'object'
		for k, v of it
			@set-style k, v

		@
	else css ...

module.exports = class Search
	(form-sel) ~>
		@form = dom form-sel, document
		return unless @form.length!

		@input = @form
			.on 'submit' -> it.prevent-default!
			.find 'input'
				.on 'keyup' debounce @search, 200
				.on 'focus' ~> if @input.get 0 .value then @element.css 'display' ''

		#todo use jkroso/position when previous lines are converted to DOM.
		return unless offset = position @input.get 0

		@element = dom '<div id="recherche"></div>'
		@element.css do
			display: 'none'
			position: 'absolute'
			zindex: 10999
			left: "#{offset.left}px"
			top: "#{offset.top + @input.get 0 .offset-height}px"
		body.append @element

		body.on 'click' ({target: {id}}) ~>
			unless id is 'search-term' #ugly hack - should this be currentTarget ?
				@input.get 0 .value = ''
				@element.set-style 'display' 'none'

	search: ~>
		unless id = @input.get 0 .value #skip on empty
			@element.empty!set-style 'display' 'none'
			return

		return if @last-lookup and @last-lookup is id

		@last-request?abort! #abort last request

		return if id is @last-lookup #skip on same
		@last-lookup = id

		set-cursor 'progress'
		@last-request = ajax url: url-search, data: {id}, success: @render, data-type: 'json'

	render: ~>
		return if @last-lookup isnt delete it.term #check we shouldn't avoid rendering, and remove it

		@element.empty!

		val = @input.get 0 .value

		term-regexp = //#val//i
		term-result = "<u>#val</u>"

		shown = false

		for type, records of it
			#type may be "guilds", "characters" etc
			type-link = search-links[type]

			for {name, id, level, online, icon} in records
				shown = true

				let url = "#type-link/#id"
					name-hl = name.replace term-regexp, -> "<u>#it</u>"

					div = dom '<div>'
					.add-class 'line'
					.on 'click' -> update-content url
					div.get 0 .innerHTML = "#{lang.search[type]} <b>#name-hl</b><br />
							 #{[level? and "#{lang.lv} #level "]}
							 #{if online? then "<b>#{lang[if online then 'online' else 'offline']}</b>" else ''}"
					@element.append div
					

					if icon?
						div.append do
							dom icon .css do
								height: '30px'
								float: 'left'
								margin-right: '10px'
						
		
		unless shown #todo show last search result ??
			@element
				.css 'color' 'white'
				.get 0 .innerHTML = lang.no_result

		@element.css 'display' 'block'
		set-cursor 'auto'