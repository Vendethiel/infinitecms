function call #todo change name ?
	if typeof! it is 'Function'
	then it!
	else "#it()"

reset-marks = !->
	$ '.hide-this' .hide!
	$ '.show-this' .show!

reset-dialog = !->
	dialogs = $ '.dialog-base:not(.ui-dialog-content)'
	dialogs.dialog dialog-opt

reset-tooltips = !->
	$ '[rel=tooltip]' .tooltip!

module.exports = class PageBinder
	-> @reset!

	reset: (pos) !->
		@ajax-binds = {} unless pos?
		@add-base-binds pos

	add-base-binds: (kind = 'all') !->
		if kind in <[all after]>
			@add reset-marks, 'after'
			@add reset-dialog, 'after'
			@add reset-tooltips, 'after'

	add: (fn, pos = 'before') !~>
		throw 'tried to bind an empty function' unless fn
		@ajax-binds[][pos]push fn

	process: (pos = 'before') !-> #todo prefix for ?
		try
			[call fn for fn in that] if @ajax-binds[pos]
		catch
			console.log "error executing #{fn.toString!}"
			throw e
		@reset pos