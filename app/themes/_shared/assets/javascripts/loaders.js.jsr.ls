$ = require 'jquery'
PageBinder = require './page-binder'
follow-form = require './follow-form'

is-local-uri = -> it.0 in <[? /]> or it is './'

class Loader
export class NoneLoader extends Loader
	follow-link: -> document.location = it

	follow-unless-selector: ->

	follow-link: ->

	create-page-binder: ->
		#just return an array
		reset: ->
		add: ->
		process: ->

	html: ->


export class AjaxLoader extends Loader
	follow-link: !~>
		it.prevent-default?!

		if typeof it is 'string'
			href = it
		else
			it .= current-target
			href = it.get-attribute 'href'

		if it.dataset?unless-selector? #can be a string
			$ that
				# checking html => is populated
				if ..length and $.trim ..html!
					@follow-unless-selector ..
					return
				#unless ..length
				#element not found - move along !

		if is-local-uri href
			update-content href
		else
			document.location = href

	follow-unless-selector: ->
		return false unless it.length #if it.is ':visible' or not

		if it.has-class 'ui-dialog-content' or it.parent!has-class 'ui-dialog-content'
			it.dialog 'open'
		else
			it.show!
		
		it.find 'form' .submit follow-form

		it.remove-class 'hidden'
		if it.find '#page_content'
			it.html that.html!

		return false

	create-page-binder: -> new PageBinder

export class ContentLoader extends AjaxLoader
	start: !~> middle.html loading-msg

	html: !~> middle.html it 

export class DialogLoader extends AjaxLoader
	-> super ... #bind things

	dialog: -> @modal ?= $ '#loading'

	start: !~> @dialog!dialog 'open'

	stop:  !~> @dialog!dialog 'close'

	html:  !~> @dialog!html it