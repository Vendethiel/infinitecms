<?php
$stats = array();

if ($template->statstemplate && $template->statstemplate != ',')
	IG::parseStats($template->statstemplate);

foreach (IG::getStatGroups() as $groupName => $group)
{
	$groupStats = array();
	foreach ($group as $key)
	{
		$groupStats[] = array(
			'stat[' . $key . '][from]',
			( $groupStats ? tag('br') : '' ) . lang($key, 'stat'),
			null,
			IG::getStat($key, 'min'),
			array('size' => 5),
		);
		
		$groupStats[] = array(
			'stat[' . $key . '][to]',
			' ' . lang('to') . ' ',
			null,
			IG::getStat($key, 'max'),
			array('size' => 5)
		);
	}
	$stats[lang($groupName)] = $groupStats;
}

echo make_form(array(
	( $template->exists() ? array('id', 'ID', null, $template->id, array('readonly' => true)) : '' ),
	array('name', lang('name'), null, $template->name),
	array('type', lang('type'), 'select', lang(null, 'itemcategory'), $template->type),
	array('level', lang('level'), null, $template->level),
	array('weight', lang('weight'), null, $template->pod),
	array('price', lang('cost'), null, $template->prix),
	lang('shop._stats') => $stats,
), '#', array('sep_inputs' => ''));

/*
{{ make_form({
  0: template.exists ? ['id', 'ID', null, template.id, {'readonly': true}] : '',
  1: ['name', '<br />' ~ ('name'|lang), null, template.name],
  2: ['type', '<br />' ~ ('type'|lang), 'select', [null, 'itemcategory']|lang, template.type],
  3: ['level', '<br />' ~ ('level'|lang), null, template.level],
  4: ['weight', '<br />' ~ ('weight'|lang), null, template.pod],
  5: ['price', '<br />' ~ ('cost'|lang), null, template.prix],
  ('shop._stats'|lang): stats
 }, '#', {'sep_inputs': ''}) }}
*/