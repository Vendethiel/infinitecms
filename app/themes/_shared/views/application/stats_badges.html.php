<?php
if (!isset($lis)) $lis = true;
$li = $lis ? '<li>' : '';
$end_li = $lis ? '</li>' : '';
echo $li, lang('server_state')
?> :&nbsp;
	<span class="badge badge-<?php if (server_state() == 'ON') echo 'success'; else echo 'important'; ?> "><?php echo server_state() ?></span>
<?php
echo $end_li;

if ($cache = Cache::start('layout_stats_badges_' . empty($lis), '+1 hour')):
//I don't see any reason for caching when logged? since we already loaded one table
// (the biggest memory "blow" is done at the first Doctrine_Core::getTable())

	$created = array();
	$qC = Query::create()
			->select('count(guid) as count')
				->from('Account');
	$c = $qC->fetchOne(Doctrine_Core::HYDRATE_NONE);
	$qC->free();
	$created['acc'] = $c['count'];

	$qC = Query::create()
			->select('count(guid) as count')
				->from('Character');
	$c = $qC->fetchOne(Doctrine_Core::HYDRATE_NONE);
	$qC->free();
	$created['char'] = $c['count'];
echo $li;
?>
<?php echo pluralize(lang('acc'), $created['acc']) . ' ' . pluralize(lcfirst(lang('created_')), $created['acc']) ?> :
<span class="badge"><?php echo $created['acc'] ?></span>
<?php echo $end_li . $li ?>
<?php echo pluralize(lang('character'), $created['char']) . ' ' . pluralize(lcfirst(lang('created_')), $created['char']) ?> :
<span class="badge"><?php echo $created['char'] ?></span>
<?php
echo $end_li, $li,
 make_link('@stats.game', '<span class="label label-info">' . lang('stats.all') . '</span>', array(), array('data-unless-selector' => '#stats')),
 $end_li;
 $cache->save();
endif;
?>