<?php
if (level(LEVEL_LOGGED)): //member is not logged ?
	if ($config['PM_BY_PAGE'])
		$unreads = $account->User->getUnreadPM();

	//show infos
	echo lang('pseudo') . ': ' . make_link($account) .
	 ( $config['PM_BY_PAGE'] ? tag('span', array('id' => 'pm_inbox'), count($unreads) ? '' : '&nbsp;' . make_link('@pm', make_img('icons/email', EXT_PNG, lang('PrivateMessage - index', 'title')), array(), array('data-unless-selector' => '#pm')) ) : '' ) . tag('br') .
	 ( $config['PASS']['ENABLE'] || $config['ENABLE_VOTE'] ? $account->User->getPoints() . tag('br') : '' ) .
	 lang('level') . ': ' . $account->getLevel() . tag('br') .
	 ( $config['PM_BY_PAGE'] ? tag('span', array('id' => 'pm_info'), $account->User->getNextPMNotif()) : '' ) .
	 make_link(new Account, lang('menu.acc.edit')) . tag('br') .
	 make_link('@sign_off', lang('menu.logout'), array(), array(), false) . tag( 'br' ) .
	 ( $config['URL_VOTE'] != -1 ? make_link('@vote', lang('menu.vote'), array(), array(), false) . tag( 'br' ) : '' ) .
	 ( $config['PASS']['ENABLE'] ? make_link('@credit', lang('menu.credit')) : '' ) .
	 ( !level(LEVEL_ADMIN) && ( level(LEVEL_VIP) || empty($config['COST_VIP']) ) ? '' :
	  make_link('@vip', 'VIP') );
else:
	$pseudo = $router->postVar(Member::CHAMP_PSEUDO);
	echo tag('form', array('method' => 'POST', 'action' => replace_url('@sign_in'), 'id' => 'login_form'),
	 input(Member::CHAMP_PSEUDO, null, null, $pseudo, array('class' => 'text')) .
	 input(Member::CHAMP_PASS, null, 'password', array('class' => 'pwd')) .
	 input('send_login', null, 'hidden') .
	 '<input class="ok" type="submit" value="" />' .
	 input_csrf_token());

	if ($config['ENABLE_REG']):
		echo tag('ul', tag('li', make_link(array('controller' => 'Account', 'action' => 'create'), '+ ' . lang('Account - create', 'title'))));
	endif;
endif;