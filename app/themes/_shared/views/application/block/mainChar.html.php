<div class="title" style="margin-left: 20px;">
	<?php
	$main_char = $account->getMainChar();
	echo make_link($main_char);
	if ($account->Characters->count() > 1)
		echo '&nbsp;', js_link('mainCharSelector.dialog("open");', make_img('icons/user_edit', EXT_PNG), array('controller' => 'Character', 'action' => 'main'));
	?>
</div>
<ul>
	<?php if ($main_char->relatedExists('GuildMember') && $main_char->GuildMember->relatedExists('Guild')): ?>
	<li id="guildInfo"><?php printf(lang('character._rank_of'), lang('guild.rank.' . $main_char->GuildMember->rank), make_link($main_char->GuildMember->Guild)) ?> (lv <?php echo $main_char->GuildMember->Guild->lvl ?>).<br /></li>
	<?php endif ?>
	<li><?php echo number_format($main_char->kamas, 0, '.', ' '), 'K / ', $main_char->capital, ' ', lang('statspoints'), ' / ', $main_char->spellboost, ' ', lang('spellspoints') ?></li>
	<?php if ($main_char->alignement != 0): ?>
	<li><?php echo $main_char->getAlignmentString(), ' (', strtolower($main_char->getAlignmentLevelString()), '), ',
	 $main_char->honor, 'h',
	 ( empty($main_char->deshonor) ? '' : '/' . $main_char->deshonor . 'dh' ), '.' ?></li>
	<?php endif ?>
</ul>