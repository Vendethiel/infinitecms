<?php
//jQuery UI box (loading & error)
echo tag('div', array('id' => 'note'), tag('div', '')),
 tag('div', array(
	'id' => 'loading',
	'class' => 'dialog-base',
	'style' => 'display: none;',
	'title' => lang('loading'),
	'align' => 'center'
), tag('br') . tag('b', lang('loading') . ' ...') . "\n\t\t"),
 "\n\t\t", tag('div', array(
	'id' => 'errorDiv',
	'class' => 'dialog-base',
	'style' => 'display: none;',
	'title' => lang('error'),
	'align' => 'center', //@todo => marge
), "\n\t\t\t" . tag('br') . tag('b', array('id' => 'error'), '&nbsp;') . "\n\t\t");
if (level(LEVEL_LOGGED))
{
	echo tag('div', array(
		'id' => 'firstMainChar',
		'class' => 'dialog-base',
		'style' => 'display: none;',
		'title' => lang('character.main'),
	), lang('character.main.new')),
	 tag('div', array(
		'id' => 'selectMainChar',
		'class' => 'dialog-base',
		'style' => 'display: none;',
		'title' => lang('character.main'),
	), tag('div', array('id' => 'charactersList'),
	  $account->getCharactersList(true, $account->getMainChar() ? $account->getMainChar()->guid : array(), 'chooseMainChar('))),
	 tag('div', array(
		'id' => 'pm',
//				'class' => 'dialog-base', //can not "automatically" be dialog()'d due to the if.
		'style' => 'display: none;',
		'title' => lang('PrivateMessage - index', 'title'),
	), '');
}
if (!defined('SKIP_STATS'))
{
	echo tag('div', array(
		'id' => 'stats',
		'class' => 'dialog-base',
		'style' => 'display: none;',
		'title' => lang('Stats - index', 'title'),
	), '');
}