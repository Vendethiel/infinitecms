<?php
if ($cache = Cache::start('layout_right_block_stats', '+1 hour')):
//I don't see any reason for caching when logged? since we already loaded one table
// (the biggest memory "blow" is done at the first Doctrine_Core::getTable())
	$created = array();
	$qC = Query::create()
			->select('count(guid) as count')
				->from('Account');
	$c = $qC->fetchOne(Doctrine_Core::HYDRATE_NONE);
	$qC->free();
	$created['acc'] = $c['count'];

	$qC = Query::create()
			->select('count(guid) as count')
				->from('Account')
				->where('logged = 1');
	$c = $qC->fetchOne(Doctrine_Core::HYDRATE_NONE);
	$qC->free();
	$created['logged'] = $c['count'];

	$qC = Query::create()
			->select('count(guid) as count')
				->from('Character');
	$c = $qC->fetchOne(Doctrine_Core::HYDRATE_NONE);
	$qC->free();
	$created['char'] = $c['count'];
?><ul>
	<li>
		<?php echo tag('b', $created['acc']) . ' ' . pluralize(lang('acc'), $created['acc']) . ' ' . pluralize(lcfirst(lang('created_')), $created['acc']) . "\n" ?>
	</li>
	<li>
		<?php echo tag('b', $created['logged']) . ' ' . pluralize(lang('acc'), $created['logged']) . ' ' . pluralize(lcfirst(lang('logged')), $created['char']) . "\n" ?>
	</li>
	<li>
		<?php echo tag('b', $created['char']) . ' ' . pluralize(lang('character'), $created['char']) . ' ' . pluralize(lcfirst(lang('created_')), $created['char']) . "\n" ?>
	</li>
	<li>
		<?php echo make_link('@stats.game', lang('stats.all'), array(), array('data-unless-selector' => '#stats')) ?>
	</li>
</ul>
<!-- @todo use component/counter? -->
<?php
	if (!empty($cache))
		$cache->save();
endif;
?>