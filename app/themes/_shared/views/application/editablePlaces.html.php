<?php
if (level(LEVEL_ADMIN))
{
	//@todo THIS IS FUCKED UP
	// I mean -- seriously ?
	// should use pageBind or something -_-

	$level_opt = array();
	$levels = Member::getLevels();
	unset($levels[LEVEL_GUEST]);
	foreach (array_reverse($levels, true) as $val => $text)
		$level_opt[] = '"' . floatval($val) . '": "' . $text . '"';
	$level_opt = implode(', ', $level_opt);
	echo 'var level_opts = {' . $level_opt . '};';

	$types = array('points', 'level');
	$spec = array('level' => '
	field_type: "select",
	select_text: $elem.html(),
	select_options: level_opts,');

	$controllers = array('points' => 'User', 'level' => 'Account');
	$langs = array('points' => lang('acc.user.points.updated'), 'level' => lang('acc.level.updated'));
	foreach ($types as $type)
		echo '
	function bind_' . $type . '(elem)
	{
	/**
	 * @todo refactor, in order to only use bind_f("points"); bind_f("level");
	 * little explanation here. using bind_* function instead of doing it directly makes
	 * it possible to keep the $elem instance valid. Else, javascript would see that $this has changed, and
	 *  $this in every success() call would be the last of $(".f_")
	 * jQuery also offers $.proxy for this purpose
	 * i.e., for (i in x) { var elem = x[i]; setTimeout($.proxy(fnToCall, elem)) }
	 * without that, each call made to fnToCall would be made with elem as the last element of x
	 */
		var $elem = jQuery(elem);
		if(!$elem.data("validated"))
		{
			$elem.editInPlace(
			{
				url: "' . to_url(array(
					'controller' => $controllers[$type],
					'action' => 'update',
					'header' => 0,
					'col' => $type,
					'id' => '%%$elem.data(\'id\')%%',
				), false) . '",
				success: function (newValue)
				{
					showNote("' . $langs[$type] . '".replace("%%pseudo%%", $elem.data("pseudo")));
					$(".f_' . $type . '[data-id=" + $elem.data("id") + "]").each(function()
					{
						$(this).html(newValue);
					});
				},
				' . ( isset($spec[$type]) ? $spec[$type] : '' ) . '
			});
			$elem.data("validated", true);
		}
	}
	function apply_' . $type . '()
	{
		/*cache is not usable =x*/
		jQuery(".f_' . $type . '").each(function()
		{
			bind_' . $type . '(this);
		});
		setTimeout(apply_' . $type . ', 5000);
	}
	apply_' . $type . '();';
}