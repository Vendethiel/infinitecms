<?php
if ($cache = Cache::start('block_top_' . $column . '_pandora', '+1 hour')):
//I don't see any reason for caching when logged? since we already loaded one table
// (the biggest memory "blow" is done at the first Doctrine_Core::getTable())
	$qC = Query::create()
			->from('Character')
			->orderBy($column);
	$chars = $qC->execute();
	$qC->free();

	foreach ($chars as $i => $c):
?>
	<li id="pvp"><?php echo make_link($c, tag('span', array('class' => 'top' . ($i + 1)), $c->getName())) ?></li>
<?php
	endforeach;

	if (($count = $chars->count()) < 5)
	{
		for ($i = $count; $i < 5; ++$i)
			echo '<li id="pvp"><a><span class="top' . ($i + 1) . '">' . lang('empty') . '</span></a></li>';
	}

	if (!empty($cache))
		$cache->save();
endif;
?>