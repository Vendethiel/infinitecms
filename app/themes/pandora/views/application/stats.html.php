<p id="infos1">
	<?php echo make_img(server_state(), EXT_PNG) ?>
</p>
<?php
if ($cache = Cache::start('block_stats_pandora', '+1 hour')):
//I don't see any reason for caching when logged? since we already loaded one table
// (the biggest memory "blow" is done at the first Doctrine_Core::getTable())
	$created = array();
	$qC = Query::create()
			->select('count(guid) as count')
				->from('Account');
	$c = $qC->fetchOne(Doctrine_Core::HYDRATE_NONE);
	$qC->free();
	$created['acc'] = $c['count'];

	$qC = Query::create()
			->select('count(guid) as count')
				->from('Account')
				->where('logged = 1');
	$c = $qC->fetchOne(Doctrine_Core::HYDRATE_NONE);
	$qC->free();
	$created['logged'] = $c['count'];

	$qC = Query::create()
			->select('count(guid) as count')
				->from('Character');
	$c = $qC->fetchOne(Doctrine_Core::HYDRATE_NONE);
	$qC->free();
	$created['char'] = $c['count'];
?>
<p id="infos2"><?php echo $created['logged'] ?></p>
<p id="infos3"><?php echo $created['acc'] ?></p>
<p id="infos4"><?php echo $created['char'] ?></p>
<?php
	if (!empty($cache))
		$cache->save();
endif;
?>