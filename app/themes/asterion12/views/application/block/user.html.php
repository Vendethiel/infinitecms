<div style="margin-left: 10px; margin-top: 10px;">
<?php
if ($config['PM_BY_PAGE'])
	$unreads = $account->User->getUnreadPM();

echo lang('pseudo') . ': <span class="account-link">' . make_link($account) . '</span>' .
 ( $config['PM_BY_PAGE'] ? tag('span', array('id' => 'pm_inbox'), count($unreads) ? '' : '&nbsp;' . make_link('@pm', make_img('icons/email', EXT_PNG, lang('PrivateMessage - index', 'title')), array(), array('data-unless-selector' => '#pm')) ) : '' ) . tag('br') .
 ( $config['PASS']['ENABLE'] && $config['ENABLE_SHOP'] ? $account->User->getPoints() . tag('br') : '' ) .
 lang('level') . ': ' . $account->getLevel() . tag('br') .
 ( $config['PM_BY_PAGE'] ? tag('span', array('id' => 'pm_info'), $account->User->getNextPMNotif()) : '' ) .
 make_link(new Account, lang('menu.acc.edit')) . tag('br') .
 make_link('@sign_off', lang('menu.logout'), array(), array(), false) . tag( 'br' ) .
 ( $config['URL_VOTE'] != -1 ? make_link('@vote', lang('menu.vote'), array(), array(), false) . tag( 'br' ) : '' ) .
 ( $config['PASS']['ENABLE'] ? make_link('@credit', lang('menu.credit')) : '' ) .
 ( !level(LEVEL_ADMIN) && ( level(LEVEL_VIP) || empty($config['COST_VIP']) ) ? '' :
  make_link('@vip', 'VIP') );
?>
</div>