<div class="info_1">
	<?php echo lang('part.server') . ':' . make_img(server_state(), EXT_PNG) ?>
</div>
<?php
if ($cache = Cache::start('layout_stats_asterion12', '+1 hour')):
//I don't see any reason for caching when logged? since we already loaded one table
// (the biggest memory "blow" is done at the first Doctrine_Core::getTable())

	$created = array();
	$qC = Query::create()
			->select('count(guid) as count')
				->from('Account');
	$c = $qC->fetchOne(Doctrine_Core::HYDRATE_NONE);
	$qC->free();
	$created['acc'] = $c['count'];

	$qC = Query::create()
			->select('count(guid) as count')
				->from('Account')
				->where('logged = 1');
	$c = $qC->fetchOne(Doctrine_Core::HYDRATE_NONE);
	$qC->free();
	$created['logged'] = $c['count'];
?>
<div class="info_2">
	<?php echo tag('b', $created['acc']) . ' ' . pluralize(lang('acc'), $created['acc']) . ' ' . pluralize(lcfirst(lang('created_')), $created['acc'] ) . "\n" ?>
</div>
<div class="info_3">
	<?php echo make_link('@stats.game', lang('stats.all'), array(), array('data-unless-selector' => '#stats')) ?>
</div>
<?php
	$cache->save();
endif;
?>