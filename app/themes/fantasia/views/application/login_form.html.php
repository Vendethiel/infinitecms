<?php
echo tag('form', array('method' => 'POST', 'action' => replace_url('@sign_in'), 'id' => 'login_form'),
 input(Member::CHAMP_PSEUDO, null, null, array('placeholder' => lang('account'))) .
 input(Member::CHAMP_PASS, null, 'password', array('placeholder' => lang('acc.register.password'))) .
 input('send_login', null, 'hidden') .
 tag('br') .
 tag('button', array('class' => 'btn btn-small'), lang('send')) .
 make_link(array('controller' => 'Account', 'action' => 'create'), lang('Account - create', 'title'), array(),
  array('class' => 'btn btn-small link')) .
 input_csrf_token());