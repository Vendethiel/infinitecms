<?php
if ($config['PM_BY_PAGE'])
	$unreads = $account->User->getUnreadPM();

echo make_link($account) .
 ( $config['PM_BY_PAGE'] ? tag('span', array('id' => 'pm_inbox'), count($unreads) ? '' : '&nbsp;' . make_link('@pm', make_img('icons/email', EXT_PNG, lang('PrivateMessage - index', 'title')), array(), array('data-unless-selector' => '#pm')) ) : '' ) . tag('br') .
 ( $config['PASS']['ENABLE'] && $config['ENABLE_SHOP'] ? $account->User->getPoints() : '' ) .
 ' (' . $account->getLevel() . ')';
 
if ($config['PM_BY_PAGE'])
	echo tag('span', array('id' => 'pm_info'), $account->User->getNextPMNotif());

if ($config['PASS']['ENABLE'])
	echo make_link('@credit', ucfirst(lang('acc.credit.add')), array(), array('class' => 'btn btn-small'));

if (!level(LEVEL_ADMIN) && !level(LEVEL_VIP) && !empty($config['COST_VIP']))
	echo make_link('@vip', 'VIP', array(), array('class' => 'btn btn-small'));

echo tag('br') . make_link('@sign_off', lang('acc.delog'), array(), array('class' => 'btn btn-small'), false) . tag('br');