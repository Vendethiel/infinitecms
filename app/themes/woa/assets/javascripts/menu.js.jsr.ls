BaseMenu = require './base_menu'

module.exports = class Menu extends BaseMenu
	~> super it

	find-menus: ->
		@titles = @menus-div.find '.title'
		@menus-div.find '[data-base-class]'

	show-one: ->
		$ it .add-class 'module-active'

	toggle: (i, elem) ~>
		# let's show elem one
		if elem.has-class 'module5'
			# let's show this one
			elem.remove-class 'module5'
			.add-class "module-active #{elem.data 'base-class'}"
			.children 'ul'
				.slide-toggle!

			@display i
		else
			elem.remove-class 'module-active'
			.remove-class elem.data 'base-class'
			.add-class 'module5'
			.children 'ul'
				.hide!

			@hide i