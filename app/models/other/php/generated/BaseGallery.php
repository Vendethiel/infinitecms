<?php
// Connection Component Binding
Doctrine_Manager::getInstance()->bindComponent('Gallery', 'other');

/**
 * BaseGallery
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property string $title
 * @property Doctrine_Collection $Images
 * 
 * @package    InfiniteCMS
 * @subpackage Models
 * @author     Vendethiel <vendethiel@hotmail.fr>
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseGallery extends Record
{
    public function setTableDefinition()
    {
        $this->setTableName('gallery');
        $this->hasColumn('title', 'string', null, array(
             'type' => 'string',
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasMany('GalleryImage as Images', array(
             'local' => 'id',
             'foreign' => 'gallery_id'));

        $nestedset0 = new Doctrine_Template_NestedSet();
        $this->actAs($nestedset0);
    }
}