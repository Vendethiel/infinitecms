<?php

/**
 * Character
 *
 * @package    InfiniteCMS
 * @subpackage Models
 * @author     Nami-Doc <nami.d0c.0@gmail.com>
 * @version    SVN: $Id: Personnage.php 49 2010-12-10 16:25:06Z nami.d0c.0 $
 */
class Character extends BaseCharacter
{
	/** @var $items Item[] */
	protected $items = null;
	/** @var $ordered_spells int[][] */
	protected $ordered_spells = null;
	/** @var $spellRange char[][] */
	protected $spellRange = null;

	/**
	 * returns character's Map object
	 *
	 * @return Map
	 */
	public function getMap()
	{
		return MapTable::getInstance()->find($this->map);
	}

	/**
	 * converts the character to a description string containing
	 * - name if $include_name
	 * - breed
	 * - gender
	 * - level
	 *
	 * @param bool $include_name include character's name
	 * @param bool $accordian act as accordion
	 *
	 * @return string
	 */
	public function toString($include_name = true, $accordion = false)
	{
		global $account;

		return ( $include_name ? ($accordion ? '<h3>' :
		tag('b', lang('character_name') . ': ')) . $this->getName() . tag('br')
		// ($accordion ? '</h3>' : tag('br') . $this->getOnAccountOf())
		: '' ) . ($accordion ? '<div><p>' : '') .
		tag('b', lang('acc.ladder.class') . ': ') . $this->getBreed() . tag('br') .
		tag('b', lang('acc.ladder.sex') . ': ') . $this->getGender() . tag('br') .
		tag('b', lang('level') . ': ') . $this->level . tag('br');
	}

	/**
	 * formats a description about the account owning the character
	 * the message is if the current user is owning the character, a "you own this character" message
	 *  else a link to the owner's account
	 *
	 * @XXX AVOID THIS BY ALL MEANS! This really hurts performances
	 *  because you don't have a cached instance of your account
	 *
	 * @return string
	 */
	public function getOnAccountOf()
	{
		global $account;
		if ($account && $this->Account->guid == $account->guid) //it's YOUR account
			return sprintf(lang('character.on_acc'));
		else
			return sprintf(lang('character.on_acc_of'), make_link($this->Account));
	}

	/**
	 * formats character's stats to an array (vit wis str agi cha int) 
	 *
	 * @return array
	 */
	public function getStats()
	{
		$stats = array(
			array('vitality', 'vitalite', '7d'),
			array('wisdom', 'sagesse', '7c'),
			array('strength', 'force', '76'),
			array('agility', 'agilite', '77'),
			array('chance', null, '7b'),
			array('intell', 'intelligence', '7e'),
		);

		$s = array();
		foreach ($stats as $stat)
		{
			$name = $stat[0]; //english name
			$value = $this[$stat[1] === null ? $name : $stat[1]];
			$base_value = IG::statFromCode($value);
			$add_value = IG::getStat($stat[2]);
			$s[] = array($name, $value, $base_value, $add_value);
		}

		return $s;
	}

	/**
	 * formats character's stats to HTML (vit wis str agi cha int) 
	 * @see getStats
	 *
	 * @return string the formatted HTML
	 */
	public function getStatsHTMLTable()
	{
		$tableStats = '';

		foreach ($this->getStats() as $stat)
		{
			$add = empty($stat[3]) ? '' : ' (' . IG::statFromCode($stat[3]) . ')';
			$base_value = empty($stat[2]) ? '<span style="color: orange;">0</span>' : $stat[2];
			$tableStats .= tag('tr', tag('td', tag('b', lang('shop.stat.' . $stat[0]))) .
			 tag('td', '&nbsp;&nbsp;' . $base_value . $add));
		}

		return tag('table', array('style' => 'margin-left: 10px;'), $tableStats);
	}

	/**
	 * formats character's name to be shown in event infos
	 * - if the character is owned by current user, the name is styled
	 * - if the character won, a gold medal is shown
	 *
	 * @param int $w winner character's guid
	 *
	 * @return string
	 */
	public function toEventParticipant($w)
	{
		return tag('span', array('class' => $this->isMine() ? 'myChar' : 'aChar'),
		 ($w == $this->guid ? make_img('icons/medal_gold_1', EXT_PNG, lang('winner')) : '') . make_link($this));
	}

	/**
	 * returns character's name, wrapped with <u> tag if the character is the current user's main character
	 *
	 * @return string
	 */
	public function getName()
	{
		// too costly
		#if ($this->Account && $this->Account->getMainChar()->guid == $this->guid)
		#	return tag('u', $this->name);
		/*
		if ($this->Account->banned) ?
		*/
		return $this->name;
	}

	/**
	 * sets name prefix (enclosed with `[` and `]`)
	 *
	 * @return void
	 */
	public function setPrefix($prefix)
	{
		$this->name = $this->getWithPrefix($prefix);
	}

	/**
	 * formats character's name to have the specified prefix
	 * removes the old one
	 *
	 * @param string $prefix new prefix
	 *
	 * @return string name with new prefix
	 */
	public function getWithPrefix($prefix)
	{
		$name = $this->name;
		if (( $pos = strpos($name, ']') ) !== false) //remove actual prefix
			$name = substr($name, $pos+1);
		return sprintf('[%s]%s', $prefix, $name);
	}

	/**
	 * returns an image for the character's breed and gender
	 *
	 * @return string <img> tag
	 */
	public function getBreedIcon()
	{
		return IG::getBreedIcon($this->getBreed(), $this->sexe);
	}

	/**
	 * returns the image path for the character's breed and gender
	 *
	 * @return string <img> tag
	 */
	public function getBreedIconPath()
	{
		return IG::getBreedIconPath($this->getBreed(), $this->sexe);
	}

	/**
	 * returns character's alignment as string
	 *
	 * @return string
	 */
	public function getAlignmentString()
	{
		return IG::getAlignment($this->alignement);
	}

	/**
	 * returns character's alignment level as string
	 *
	 * @return string
	 */
	public function getAlignmentLevelString()
	{
		return IG::getAlignmentLevel($this->alvl);
	}

	/**
	 * formats character's breed to string
	 *
	 * @return string
	 */
	public function getBreed()
	{
		return IG::getBreed($this->class);
	}

	/**
	 * formats character's gender to string
	 *
	 * @return string
	 */
	public function getGender()
	{
		return IG::getGender($this->sexe);
	}

	/**
	 * @see toString
	 */
	public function __toString()
	{
		return $this->toString();
	}

	/**
	 * converts the character to a <tr> tag containing <td> for
	 * - link
	 * - breed
	 * - gender
	 * - level
	 *
	 * @see getTableRowDatas
	 *
	 * @return string
	 */
	public function toTableRow()
	{
		return tag('tr', $this->getTableRowDatas());
	}

	/**
	 * converts the character to <td> tags for :
	 * - link
	 * - breed unless $simple
	 * - gender unless $simple
	 * - level unless $simple
	 *
	 * @see getTableRowDatas
	 *
	 * @param bool $simple only a <td> tag with the name
	 *
	 * @return string
	 */
	public function getTableRowDatas($simple = false)
	{
		if ($simple)
			return tag('td', $this->getLink());

		return tag('td', $this->getLink()) .
				tag('td', $this->getBreed()) .
				tag('td', $this->getGender()) .
				tag('td', $this->level);
	}

	/**
	 * initializes character
	 *
	 * @return string profil div
	 */
	protected function _init()
	{
		return Collection::charLoad($this);
	}

	/**
	 * returns character's URL parameters
	 *
	 * @return array
	 */
	public function getURL()
	{
		return array(
			'controller' => 'Character',
			'action' => 'show',
			'id' => $this
		);
	}

	/**
	 * returns character's link
	 *
	 * @return string html (remote) link
	 */
	public function getLink($text = null)
	{
		$p = $this->_init();
		return $p . js_link(sprintf('showChar(%d)', $this->guid), $text === null ? escape_html($this->name) : $text, $this->getURL());
	}

	/**
	 * formats character to an infobox with a "more" link
	 *
	 * @return string html infobox
	 */
	public function getInfoBox()
	{
		return $this . tag('br') . make_link($this->getURL(), lang('more') . ' ...', array(), array('id' => 'closeProfilBox'));
	}

	/**
	 * returns all the items of the character
	 *
	 * @return Item[]
	 */
	public function getItems()
	{
		if ('' == trim($this->objets))
			return array();

		if ($this->items === null)
		{
			$this->items = Query::create()
					->from('Item')
						->whereIn('guid', explode('|', $this->objets))
					->execute();
		}
		return $this->items;
	}

	/**
	 * counts equiped items (pos != -1) 
	 *
	 * @return int
	 */
	public function getInventoryCount()
	{
		$i = 0;

		foreach ($this->getItems() as $item)
		{
			if ($item->pos != -1)
				continue;
			++$i;
		}

		return $i;
	}

	/**
	 * orders the inventory by the inventory type of each template's type and returns it 
	 *
	 * @return array character's ordered items
	 */
	public function getOrderedInventory()
	{
		if ('' == trim($this->objets))
			return '';

		$it_table = ItemTemplateTable::getInstance();

		$templates = array();
		foreach ($this->getItems() as $item)
			$templates[] = $item->template;
		$templates = Query::create()
						->from('ItemTemplate INDEXBY id')
							->whereIn('id', array_unique($templates)) //_unique not even needed :p
						->execute(array(), Doctrine_Core::HYDRATE_ARRAY);

		$items_by_type = array();
		foreach ($this->getItems() as $item)
		{
			if ($item->pos != -1)
				continue;

			$type = $it_table->getInventoryType($templates[$item->template]['type']);

			if (!isset($items_by_type[$type]))
				$items_by_type[$type] = array();
			$items_by_type[$type][] = $item;
		}
		
		return $items_by_type;
	}

	/**
	 * returns ordered job list classified this way :
	 * - recolt jobs
	 * - craft jobs
	 * - etheral jobs
	 * NOTE : etheral jobs without their craft counterpart won't show up !
	 *
	 * @return array [[recoltjob1, ...], [craftjob1, ...], [etheraljob1, ...]]
	 */
	public function getJobs()
	{
		$jobs = array(0 => array(), 1 => array(), 2 => array());
		foreach (explode(';', $this->jobs) as $job)
		{
			if (empty($job))
				continue;

			$job = explode(',', $job);
			$job[2] = IG::getLevel($job[1], 'job');

			if (IG::isEtheralJob($job[0]))
				continue; //so yeah, you can't JUST HAVE an etheral job without having da base job. sry bro
			else if (IG::isRecoltJob($job[0]))
				$s = 0;
			else
			{
				if (IG::hasEtheralJob($job[0]) && $this->hasJob($eth = IG::getEtheralJob($job[0])))
					$jobs[2][] = array($eth, $ex = $this->getJobExp($eth), IG::getLevel($ex, 'job'));
				else
					$jobs[2][] = '';

				$s = 1;
			}

			$jobs[$s][] = $job;
		}
		return $jobs;
	}

	/**
	 * returns character's experience for the given job (xp, not level)
	 *
	 * @param int $id job's id
	 *
	 * @return int|null job's exp or null if the character doesn't have the job
	 */
	public function getJobExp($id)
	{
		foreach (explode(';', $this->jobs) as $job)
		{
			$j = explode(',', $job);

			if ($j[0] == $id)
				return $j[1];
		}

		return null;
	}

	/**
	 * returns whether the character knows the job or not
	 *
	 * @param int $id job's id
	 *
	 * @return bool
	 */
	public function hasJob($id)
	{
		return $this->getJobExp($id) !== null;
	}

	/**
	 * returns whether the character has at least one job or not
	 *
	 * @return bool
	 */
	public function hasJobs()
	{
		return !empty($this->jobs) && $this->jobs != ';';
	}

	/**
	 * returns an array containing array for each spell containg their
	 * - id
	 * - level
	 * - position
	 *
	 * @return array
	 */
	public function getSpells()
	{
		if (null === $this->ordered_spells)
		{
			if (empty($this->spells))
			{ //no spell
				$this->ordered_spells = array();
				return;
			}

			//$this->spells = spellID;spellLevel;spellPosition,ID2;lvl2;pos2,...
			$spells = explode(',', $this->spells);
			$ordered_spells = array();

			//$order_spells = [[id, level, position], ....]
			foreach ($spells as $spell)
				$ordered_spells[] = explode(';', $spell);

			$this->ordered_spells = $ordered_spells;
		}

		return $this->ordered_spells;
	}

	/**
	 * counts character's spells
	 *
	 * @return int
	 */
	public function getSpellCount()
	{
		if ('' === $this->spells || ',' === $this->spells)
			return 0;

		return count(explode(',', $this->spells));
	}

	/**
	 * returns the average level of character's spells
	 *
	 * @return integer mean
	 */
	public function getSpellAvgLevel()
	{
		$avg_level = 0;

		foreach ($this->getSpells() as $spell)
			$avg_level += $spell[2];

		return $avg_level / $this->getSpellCount();
	}

	/**
	 * returns the spell position
	 *
	 * @return string
	 */
	public function getSpellPos($p)
	{
		if ($p === '_')
			return lang('pos.no', 'spell');

		$this->_initSpellRange();

		if (in_array($p, $this->spellRange[0]))
		{
			$pos = array_search($p, $this->spellRange[0]);
			$line = lang('first');
		}
		else
		{
			$pos = array_search($p, $this->spellRange[1]);
			$line = lang('second');
		}

		return sprintf(lang('pos.line', 'spell'), $line, $pos + 1);
	}

	/**
	 * initializes spell positions
	 */
	protected function _initSpellRange()
	{
		if ($this->spellRange === null)
			$this->spellRange = array(
				range('a', 'g'),
				range('h', 'n'),
			);
	}


	/**
	 * determines whether the character is gm of a guild or not
	 * you can also pass a certain guild id to check
	 *
	 * @param null|int|array|record $guild guild's id (or object or array) to check if character's gm of a certain guild.
	 *  character must be a guild, or a character with a guild_id field
	 *
	 * @return bool
	 */
	public function isGM($guild = null)
	{
		if (!$this->isMember($guild))
			return false;
		
		return $this->GuildMember->rank == 1;
	}

	/**
	 * determines whether the character is member of a guild or not
	 * you can also pass a certain guild id to check
	 *
	 * @param null|int|Guild $guild guild's id (or object or array) to check if character's member of a certain guild
	 *
	 * @return bool
	 */
	public function isMember($guild = null)
	{
		if (!$this->relatedExists('GuildMember'))
			return false;

		if (null === $guild || -1 === $guild)
			return true;

		//convert array or object to string
		if (is_array($guild) || $guild instanceof Guild)
			$guild = $guild['id'];
		else if ($guild instanceof Doctrine_Record) //other record
			$guild = $guild['guild_id'];

		return $this->GuildMember->guild == $guild;
	}

	/**
	 * determines whether the character is owned by the current user
	 */
	public function isMine($id = null)
	{
		global $account;

		if (!level(LEVEL_LOGGED))
			return false;

		if ($id instanceof Account || is_array($id))
			$id = $id['guid'];

		if (!$id)
			$id = $account->guid;

		return $this->account == $id;
	}
}