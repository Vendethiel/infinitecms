<?php

/**
 * Item
 *
 * @package    InfiniteCMS
 * @subpackage Models
 * @author     Nami-Doc <nami.d0c.0@gmail.com>
 * @version    SVN: $Id: News.php 24 2010-10-22 11:46:07Z nami.d0c.0 $
 */
class News extends BaseNews
{
	/**
	 * formats a news after TinyMCE editing
	 *
	 * @access public
	 * @static
	 *
	 * @param string $content Content of the textarea
	 * @return string content cleared
	 */
	static public function format($content)
	{
		$content = str_replace(array('<p', '</p>'), array('<div', '</div>'), $content);
		$content = str_replace(array(
			'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">',
			'<html>',
			'<head>',
			'<title>Untitled document</title>',
			'</head>',
			'<body>',
			'</body>',
			'</html>',
				), '', $content);
		$content = preg_replace('`<div>(.*)</div>`', '$1<br />', $content);
		$content = str_replace("\n", '', $content);
		$content = str_replace('../../assets', getPath() . 'assets', $content);
		return trim($content);
	}

	/**
	 * truncates the content
	 */
	public function getTruncated($limit = 500)
	{
		$content = $this->getContent();
		if (strlen($content) <= $limit - 4)
			return $content;

		return substr($content, 0, $limit) . ' ...';
	}

	/*
	 * @return string formatted timestamp
	 */
	public function getDates()
	{
		return sprintf(lang('created'), $this->created_at) .
		 $this->updated_at && $this->updated_at != $this->created_at ?
		  sprintf(lang('last_update'), $this->updated_at) :
		  '';
	}
	
	/*
	 * @return string formatted/stripped content
	 */
	public function getContent()
	{
		return self::format($this->content);
	}

	/*
	 * embeds comment through data-unless-selector
	 * or links to them
	 */
	public function embedComments($add = array())
	{
		global $router;

		$t = pluralize(lang('news.com.title'), $this->Comments->count(), SHOW_VALUE);

		if ($router->getAction() == 'show')
			return $t;

		return make_link(array('controller' => 'Comment', 'action' => 'index', 'id' => $this, 'mode' => 'embed'),
		 $t, array(), array_merge($add, array('data-unless-selector' => '#comment-' . $this->id)));
	}

	/**
	 * updates the object
	 *
	 * @param array $values Les valeurs à mettre dans la news
	 * @return array
	 */
	public function update_attributes(array $values, $columns = null)
	{
		global $account, $errors;
		if (null === $columns)
			$columns = array('title', 'content');
		if (is_string($columns))
			$columns = explode(',', $columns);
			
		foreach ($columns as $t)
		{
			if (!$this->getTable()->hasColumn($t))
				continue;
			$t[0] = strtolower($t[0]);
			if (empty($values[$t]))
				$errors[] = sprintf(lang('must_!empty'), $t);
			else
				$this->$t = $values[$t];
		}
		if (!$this->exists())
			$this->Author = $account->User;
	}

	public function doSave()
	{
		$this->save();
	}

	public function buildLinks($showTitle = true)
	{
		global $router, $config;
		$baseAction = array('controller' => $router->getController(), 'id' => $this);
		$params = array(
			'show' => $baseAction + array('action' => 'show'),
			'update' => $baseAction + array('action' => 'update'),
			'delete' => $baseAction + array('action' => 'delete'),
		);

		$title = tag('span', array('id' => 'newsTitle'), $this['title']);
		$edit_link = make_link($params['update'], lang('act._edit'));

		return ( $showTitle ? ( $router->getAction() == 'index' ? make_link($params['show'], $title) : $title ) : '' ) .
		 ( level(LEVEL_ADMIN) ? ' ' . $edit_link . ' ' . make_link($params['delete'], lang('act.delete')) : '' );
	}

	public function getName()
	{
		global $router; //this suxx~

		if ($router->getAction() == 'index')
			return make_link('@news.show', tag('span', array('id' => 'newsTitle'), $this->title), array('id' => $this));

		return $this->title;
	}

	public function getAuthorString()
	{
		$a = make_link($this->Author->Account);
		if ($this->relatedExists('Author') && $this->Author->relatedExists('Account'))
			return sprintf(lang('by'), $a);

		return sprintf(lang('by'), '?');
	}
}