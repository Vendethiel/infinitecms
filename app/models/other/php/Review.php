<?php

/**
 * Review
 *
 * @package    InfiniteCMS
 * @subpackage Models
 * @author     Nami-Doc <nami.d0c.0@gmail.com>
 * @version    SVN: $Id$
 */
class Review extends BaseReview
{
	public function setUp()
	{
		parent::setUp();
		$this->hasMutator('comment', 'setComment');
	}

	public function setComment($comment)
	{
		if (level(LEVEL_ADMIN))
			$this->_set('comment', News::format($comment));
		else
			$this->_set('comment', escape_html($comment));
	}

	public function update_attributes(array $values)
	{
		global $errors;

		if (empty($values['comment'])) //no comment :( ?
			$errors[] = sprintf(lang('must_!empty'), 'comment');
		else //process it
			$this->comment = $values['comment'];

		if (isset($values['note']) && in_array($values['note'], $this->getTable()->getNotes()))
			$this->note = $values['note'];
		else
			$errors[] = sprintf(lang('must_!empty'), 'note');
	}
}