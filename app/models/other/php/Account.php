<?php

/**
 * Account
 * 
 * @package    InfiniteCMS
 * @subpackage Models
 * @author     Nami-Doc <nami.d0c.0@gmail.com>
 * @version    SVN: $Id: Account.php 56 2011-01-16 19:39:32Z nami.d0c.0 $
 */
class Account extends BaseAccount
{
	public static $preSend = false;
	private $profilSent = false;
	public static $profils = array();
	public $charsInit = false;
	private $amis = null,
			$rFriends = null, // reverse friends
			$otherAccounts = null,
			$inSend = false,
			$_user = null;

	static public function sendPreProfil()
	{
		if (self::$preSend)
			return '';
		self::$preSend = true;

		jQ('
var profils = {},
	profilBox = $("<div />",
{
	id: "profil",
	style: { display: "none" },
	title: "' . sprintf(lang('infos')) . '"
}).appendTo($("body"));
profilBox.dialog($.extend(dialogOpt, {"modal": false}));
function showProfil(id)
{
	profilBox
		.find("div")
		.hide();
	profilBox
		.find("#profil-" + id)
		.show();
	profilBox.dialog("open");
}
pageBind(function (undef)
{
	delete profils;
	if (profilBox != undef)
	{
		profilBox.dialog("close");
		delete profilBox;
	}
	delete showProfil;
});');
	}

	public function getLastIp()
	{
		return long2ip($this->lastip);
	}

	public function sendProfil()
	{
		if ($this->profilSent)
			return '';
		global $account, $member, $router, $acc;
		$this->profilSent = true;
		self::sendPreProfil();

		jQ('profilBox.append($("#profil-' . $this->guid . '"))'); //jQuery.fn.append
		$acc = $this;
		$div = tag('div', array(
			'id' => 'profil-' . $this->guid,
			'style' => 'display: none;',
		), require $router->getPath('Account', 'show', ASSET_HTML));
		return $div;
	}

	public function getName()
	{
		return $this->banned ? tag('strike', $this->pseudo) : $this->pseudo;
	}

	public function getAvatar()
	{
		if ($avatar = asset_path('avatar/' . $this->pseudo . EXT_PNG, true))
			return $avatar;

		if (!$char = $this->getMainChar())
			return asset_path('avatar/_none' . EXT_PNG, true);

		return $char->getBreedIconPath();
	}

	public function getLink($text = null)
	{
		if ($this->inSend)
			return make_link('@account.show', $text, array('id' => $this), array(), true, false);

		$this->inSend = true;
		$p = $this->sendProfil();
		$this->inSend = false;

		return $p . js_link('showProfil(' . $this->guid . ');', null === $text ? $this->getName() : $text, replace_url($this, true, array()));
	}

	public function getRouterParam()
	{
		return $this->guid;
	}

	public function getUnbanLink($text = null)
	{
		if (!$this->banned)
			return;

		return make_link('@unban', null === $text ? lang('ban.unban') : $text, array('id' => $this));
	}

	/**
	 * returns characters list as string, to select something
	 *
	 * @param bool $accordion format for accordion (h3 + p) ?
	 * @param array $exclude characters ID to exclude from being SELECTED (still shown, may modify this behavior later)
	 * @param string $fn the JS function to call. You must include the 1st parenthesis (that allows to call, i.e. ...(.., .., 'doWTFYaWant(1, 3, ')
	 * @param mixed $normalLink link for replacement in js_link
	 *
	 * @return string html (jq-ui-accordeon'able if $accordion) list
	 */
	public function getCharactersList($accordion = false, $exclude = array(), $fn = 'choosePerso(', $normalLink = array())
	{
		if (empty($exclude))
			$exclude = array();
		else if (!is_array($exclude))
			$exclude = array($exclude);

		if (is_array($fn) && empty($normalLink))
		{
			$normalLink = $fn;
			$fn = null;
		}

		if ($this->Characters->count())
		{
			$persos = '';
			foreach ($this->Characters as $character)
			{ //@todo refactor to allow molet-clic n such
				$persos .= $character->toString(true, $accordion) .
				 (in_array($character->guid, $exclude) ? '' : js_link($fn . $character->guid . ')', lang('choose'), to_url($normalLink).$character->guid)) .
				 ($accordion ? "</p></div>\n" : tag('br') . tag('br'));
			}
			return $persos;
		}
		return '';
	}

	/**
	 * returns friends
	 *
	 * @return Collection list of friends
	 */
	public function getFriends()
	{
		if (empty($this->friends) || $this->friends == ';')
			return array();

		if ($this->amis === null)
		{ //init
			$this->amis = $this->getTable()
								->createQuery()
									->whereIn('guid', explode(';', $this->friends))
									->andWhere('guid != ?', $this->guid)
								->execute();
		}
		return $this->amis;
	}
	/**
	 * @return Collection list of reverse friends
	 */
	public function getReverseFriends($hydrate = null)
	{
		if ($this->rFriends === null)
		{
			$this->rFriends = new Collection(__CLASS__);
			foreach ($this->getFriends() as $friend)
			{
				if (in_array($this->guid, explode(';', $friend->friends)))
					$this->rFriends[$friend['guid']] = $friend;
			}
		}
		return $this->rFriends;
	}

	public function filterFriends(array $accounts)
	{ //does not accept a callback.
		$filtered = array();
		foreach ($accounts as $acc)
		{
			if ($acc instanceof self || is_array($acc))
				$acc = $acc['guid'];

			if ($this->hasFriend($acc))
				$filtered[] = $acc;
		}
		return $filtered;
	}

	public function filterReverseFriends(array $accounts, $callback = null)
	{
		if (!is_callable($callback))
			$callback = function ($record) { return $record['guid']; };

		$reverseFriends = $this->getReverseFriends();
		$filtered = array();
		foreach ($accounts as $acc)
		{
			if ($acc instanceof self || is_array($acc))
				$acc = $acc['guid'];

			if ($acc != $this->guid && $reverseFriends->contains($acc))
				$filtered[] = $callback($reverseFriends[$acc]);
		}
		return $filtered;
	}

	/**
	 * determines if this Account has this friend
	 *
	 * @param Account $acc The account that may be the friend
	 * @return boolean true if this account has this friend, otherwise false
	 */
	public function hasFriend($acc)
	{
		if ($acc instanceof Account || is_array($acc))
			$acc = $acc['guid'];

		if ($acc == $this->guid)
			return false; //NO you can't have yourself as a friend§ umadbro?
		foreach ($this->getFriends() as $friend)
		{ /* @var $friend Account */
			if ($acc == $friend->guid)
				return true;
		}
		return false;
	}

	public function hasReverseFriend(Account $acc)
	{
		if ($acc->guid == $this->guid)
			return false;
		foreach ($this->getReverseFriends as $rFriend)
		{
			if ($acc->guid == $rFriend)
				return true;
		}
		return false;
	}

	public function getUser()
	{
		if ($this->_user)
			return $this->_user;
		if (!$this->relatedExists('User'))
			$this->User = UserTable::getInstance()->fromGuid($this);

		// prevent re-querying
		$this->User->acc = $this;
		return $this->_user = $this->User;
	}

	/*
	 * sets the private user
	 */
	public function setUser($user)
	{
		$this->_user = $user;
	}

	public function getMultiAccounts()
	{
		if (null === $this->otherAccounts)
		{
			$this->otherAccounts = $this->getTable()
					->createQuery('a')
						->leftJoin('a.User u')
					->where('guid != ?', $this->guid)
						->andWhere('lastip = ?', $this->lastip)
					->execute();
		}

		return $this->otherAccounts;
	}

	/**
	 * returns account's main character
	 *
	 * @return Character The one.
	 */
	public function getMainChar()
	{
		global $account;

		if (!$this->Characters->count())
			return null;
		if ($this->getUser()->main_char != 0 && $this->Characters->contains($this->getUser()->main_char))
			return $this->Characters[$this->getUser()->main_char];

		if ($account instanceof Account && $account->guid == $this->guid)
			jQ('$("#firstMainChar").dialog(dialogOptO);');


		$main_char = $this->Characters->getFirst();
		if ($this->Characters->count() != 1)
		{
			foreach ($this->Characters as $character)
			{
				if ($main_char instanceof Character && $character->xp > $main_char->xp)
					$main_char = $character;
			}
		}

		$this->getUser()->main_char = $main_char->guid;

		if ($account instanceof Account && $account->guid != $this->guid)
			$this->getUser()->save();

		return $main_char;
	}

	public function getLoggedState()
	{
		if ($this->logged)
			return lang('online');
		else
			return lang('offline');
	}

	public function getRolesString()
	{
		$isAdmin = level(LEVEL_ADMIN);

		$html = '<ul>';
		foreach ($this->StaffRoles as $role)
			$html .= tag('li', $role->getName() . ($isAdmin ? $role->getUpdateLink() . $role->getDeleteLink() : ''));
		if ($isAdmin)
			$html .= tag('li', $this->getNewStaffRoleLink());

		return $html . '</ul>';
	}

	public function getNewStaffRoleLink()
	{
		return make_link('@staffrole.create', lang('StaffRole - create', 'title'), array('id' => $this));
	}

	/**
	 * @return float number representation of the level
	 */
	public function getLvl()
	{
		return Member::adjustLevel($this->level, $this->vip);
	}

	/**
	 * @return string string representation of the level (html mix)
	 */
	public function getFormattedLevel()
	{
		return Member::formateLevel($this->level, $this->vip);
	}

	/**
	 * @return string html representation of the level (Edit In Place mix)
	 */
	public function getLevel($format = true)
	{
		return tag('span', array(
			'class' => 'f_level',
			'data-pseudo' => $this->pseudo,
			'data-id' => $this->guid,
		), $format ? $this->getFormattedLevel() : $this->getLvl());
	}
	
	public function isVIP()
	{
		global $config;
		if (level(LEVEL_ADMIN))
			return true;
		return empty($config['COST_VIP']) ? false : $this->vip;
	}

	public function canJudge($contest)
	{
		if ($contest->ended || !$this->isJury($contest))
			return false;

		return !$contest->Voters->contains($this->getUser()->id);
	}

	public function canJudgeHim(ContestParticipant $cp)
	{
		if (!$this->canJudge($cp->Contest))
			return false;

		return !$this->Characters->contains($cp->Character->guid);
	}

	public function isJury($contest)
	{
		if ($this->level >= $contest->level)
			return true;

		return $contest->Jurors->contains($this->getUser()->id);
	}

	public function canCompete($contest)
	{
		if ($contest->ended || !$this->getMainChar())
			return false;

		return !$this->currentlyCompete($contest);
	}

	public function currentlyCompete($contest)
	{
		if ($contest instanceof Contest || is_array($contest))
			$contest = $contest['id'];


		foreach ($this->Characters as $character)
		{
			if ($character->ContestParticipations->contains($contest))
				return true;
		}
		return false;
	}

	public function canParticipate($event)
	{
		if (!$this->getMainChar())
			return false;

		return !$this->currentlyParticipate($event);
	}

	public function currentlyParticipate($event)
	{
		if ($event instanceof Event || is_array($event))
			$event = $event['id'];

		foreach ($this->Characters as $character)
		{
			if ($character->Events->contains($event))
				return true;
		}

		return false;
	}

	public function canSetWinner(Event $e)
	{
		if ($this->level >= LEVEL_ADMIN)
			return true;
		if (!$e->relatedExists('Guild'))
			return false;

		if (!$mainChar = $this->getMainChar())
			return false;
		return $mainChar->isGM($e);
	}

	public function canVote($time)
	{
		if (!empty($this->getUser()->lastVote) && !date_passed($this->getUser()->lastVote, $time))
			return VOTE_WAIT;

		foreach ($this->getMultiAccounts() as $acc)
		{
			if (!empty($acc->getUser()->lastVote) && !date_passed($acc->getUser()->lastVote, $time))
			{ //import lastvote
				$this->getUser()->lastVote = $acc->getUser()->lastVote;
				return VOTE_ACC;
			}
		}

		return VOTE_OK;
	}

	/**
	 * executes action before insert
	 *
	 * @package Doctrine
	 * @subpackage hooks
	 *
	 * @access public
	 * @param Doctrine_Event $event Evenement déclencheur
	 * @return void
	 */
	public function preInsert($event)
	{
		global $member, $config;

		$inv = $event->getInvoker();
		$inv->lastip = $member->getIp();
		$inv->lastconnectiondate = new Doctrine_Expression('NOW()');
	}

	public function setLevel($level)
	{
		global $config;

		$level = floatval($level);
		if ($level == LEVEL_BANNED) //just put "banned", don't modify the level. But ... "LEVEL_BANNED" is not in the ranks list !
		{
			$this->banned = 1;
			$level = LEVEL_LOGGED;
		}
		else if ($this->banned)
			$this->banned = 0;

		if ($level == LEVEL_VIP)
		{
			if (empty($config['COST_VIP']))
				$level = LEVEL_LOGGED;
			else
			{
				$this->vip = 1; //@todo reset level to LOGGED ?
				return;
			}
		}
		else if ($level == LEVEL_LOGGED && $this->vip)
			$this->vip = 0; //no continue.
		else if ($level <= LEVEL_GUEST)
			$level = LEVEL_LOGGED;
		else if ($level > LEVEL_ADMIN)
			$level = LEVEL_ADMIN;

		$this->_set('level', $level);
	}

	public function addPoints($p)
	{
		global $config;

		if ($config['ACCOUNT_POINTS'])
			$this->points = $this->points + $p;
		else
			$this->getUser()->points = $this->getUser()->points + $p;
	}

	public function hasPoints($required)
	{
		return $this->getPoints() >= $required;
	}

	public function getPoints()
	{
		global $config;

		if ($config['ACCOUNT_POINTS'])
			return $this->points;
		else
			return $this->getUser()->points;
	}

	public function setTableDefinition()
	{
		global $config;
		parent::setTableDefinition();

		if ($config['ACCOUNT_POINTS'])
	        $this->hasColumn('points', 'integer', 4, array(
	             'type' => 'integer',
	             'default' => '0',
	             'length' => '4',
	             ));
	}

	public function setUp()
	{
		parent::setUp();

		$this->hasMutator('level', 'setLevel');
	}

	public function getColumns()
	{
		$columns = array('pass', 'pseudo', 'question', 'reponse');
		if (!$this->exists() || level(LEVEL_ADMIN))
		{
			array_unshift($columns, 'account');
			$columns[] = 'email';
		}
		if (level(LEVEL_ADMIN))
			$columns[] = 'level';

		return $columns;
	}

	/**
	 * updates the account
	 *
	 * @global Accounts $account
	 *
	 * @param array $values Values
	 * @param array $col -default=null Columns
	 * @param boolean $new -default=true New record ?
	 * @return array Errors
	 */
	public function update_attributes(array $values, $columns = null)
	{
		global $account, $member, $config, $errors;
		$errors = array();
		if (isset($values['email']))
			$values['email'] = strtolower($values['email']);

		if (empty($columns) || $columns === true)
			$columns = $this->getColumns(); //no, no, it's not getTable()->getColumnNames
		if (is_string($columns))
			$columns = explode(';', $columns);
		if (level(LEVEL_ADMIN))
			$values['banned'] = isset($values['banned']);

		if (!empty($values['email']))
			$values['email'] = strtolower($values['email']);

		foreach ($columns as $t)
		{
			$t[0] = is_string($t[0]) ? strtolower($t[0]) : intval($t[0]);
			if (!isset($values[$t]))
			{
				if ($t === 'banned') //WTF????
					$this->banned = 0;
				else
					$errors[] = sprintf(lang('must_!empty'), $t);
			}
			else
				$this->$t = $values[$t];
		}
		$this->lastip = ip2long($member->getIp());

		if (!filter_var($this->email, FILTER_VALIDATE_EMAIL))
			$errors['email'] = lang('acc.register.error.mail');

		if (( !isset($values['tos']) || ( $values['tos'] !== 'on' && $values['tos'] !== '' ) )
		 && !$this->exists())
			$errors['tos'] = sprintf(lang('acc.register.error.tos'), make_link('@tos', lang('cgu')));

		$this->checkExistence();

		$this->checkDuplication();
	}

	public function doSave()
	{
		$new = $this->isNew();

		$this->save();

		if ($new)
			log_act('registration');
	}

	protected function checkExistence()
	{
		global $errors, $config;

		$check = Query::create()
				->from(__CLASS__) //why do I select the account instead of a count() ? Because I need infos ...
				->where('(LOWER(account) = ? OR LOWER(pseudo) = ? OR LOWER(email) = ? or lastip = ?)', array(
					strtolower($this->account),
					strtolower($this->pseudo),
					$this->email,
					$this->lastip,
				));
		if ($this->exists())
			$check->andWhere('guid != ?', $this->guid);
		$check = $check->fetchOne();
		if ($check)
		{
			if (!$config['ALLOW_MULTI_EMAIL'] && $this->email == $check->email)
				$errors['email'] = lang('acc.register.error.mail_used');
			if ($this->account == $check->account)
				$errors['account'] = lang('acc.register.error.account_used');
			if ($this->pseudo == $check->pseudo)
				$errors['pseudo'] = lang('acc.register.error.pseudo_used');
			if ($check->banned
			 || ($this->lastip == $check->lastip && !$config['ALLOW_MULTI']))
				$errors['registered'] = lang('acc.register.error.already_created_acc');
		}
	}

	protected function checkDuplication()
	{
		//error suppression is BAD, but in my case I have no other choice
		// because php throws a warning when you use an empty delimiter with strpos.
		// JUST RETURN FALSE MORON
		if (@strpos($this->pseudo, $this->account) !== false
		 || @strpos($this->account, $this->pseudo) !== false

		 || @strpos($this->pass, $this->account) !== false
		 || @strpos($this->account, $this->pass) !== false
		 || @strpos($this->pass, $this->pseudo) !== false
		 || @strpos($this->pseudo, $this->pass) !== false

		 || @strpos($this->question, $this->pseudo) !== false
		 || @strpos($this->question, $this->account) !== false
		 || @strpos($this->question, $this->pass) !== false

		 || @strpos($this->reponse, $this->pseudo) !== false
		 || @strpos($this->reponse, $this->account) !== false
		 || @strpos($this->reponse, $this->pass) !== false)
		{ //:'). In fact : if [pseudo, account, pass].any_in([pseudo, account, pass, question, reponse]) then fail
			global $errors;
			$errors['duplicates'] = lang('acc.register.unsafe_dup'); //duplication
		}
	}
}