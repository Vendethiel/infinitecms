<?php

/**
 * USer
 *
 * @package    InfiniteCMS
 * @subpackage Models
 * @author     Nami <nami.d0c.0@gmail.com>
 * @version    SVN: $Id: User.php 53 2011-01-15 11:11:37Z nami.d0c.0 $
 */
class User extends BaseUser
{
	protected $pmUnreads = null;
	public $acc;

	/**
	 * pre-insert hook
	 *
	 * @package Doctrine
	 *
	 * @global array $config
	 * @param Doctrine_Event $event
	 * @return void
	 */
	public function preInsert($event)
	{
		global $config;

		$inv = $event->getInvoker();
		// @FIXME add some checks from proxy'n'll
		$inv->lastIP = ip2long($_SERVER['REMOTE_ADDR']);
		$inv->culture = LANG;
	}

	/**
	 * shortcut to avoid re-querying
	 * yes, Doctrine fails to remap properly :(
	 * A->B->A->B *will* trigger queries
	 */
	protected function getAccount()
	{
		if ($this->acc)
			return $this->acc;
		return $this->Account;
	}

	/**
	 * returns user's URL parameters
	 *
	 * @return array
	 */
	public function getURL()
	{
		return array(//edit link
			'controller' => __CLASS__,
			'action' => 'update',
			'id' => $this->getAccount(),
		);
	}

	/**
	 * formats user's points to a string with "point(s?) : " before
	 *
	 * @todo use Account::getPoints() ?
	 * @return string
	 */
	public function getPoints()
	{
		return pluralize(ucfirst(lang('point')), $this->points) . ': ' .
		 $this->getPointsSpan();
	}

	/**
	 * formats points to a <span> tag to be recognized by the edit in place module
	 *
	 * @return string <span> tag
	 */
	public function getPointsSpan()
	{
		// avoid re-querying account
		return tag('span', array(
			'class' => 'f_points',
			'data' => array('pseudo' => $this->getAccount()->pseudo, 'id' => $this->guid)),
		$this->points);
	}

	/**
	 * determines whether the user can vote in the given Poll
	 * NOTE : admin state NOT taken into account !
	 *
	 * @param Poll $poll
	 *
	 * @return bool
	 */
	public function canVote(Poll $poll)
	{
		foreach ($this->PollOptions as $option)
		{ /* @var $option PollOption */
			if ($option->Poll->id == $poll->id)
				return false;
		}
		return true;
	}

	/**
	 * determines whether the user can submit a review in the guestbook, true if admin or if user has never reviewed in the guestbook
	 *
	 * @return bool
	 */
	public function canReview()
	{
		return level(LEVEL_ADMIN) || !$this->relatedExists('Review'); //if this can make happy admins \o/
	}

	/**
	 * determines whether the user can comment on a news, true if admin or if user has never commented on this article already
	 *
	 * @param News $news
	 *
	 * @return bool
	 */
	public function canComment(News $news)
	{
		if (level(LEVEL_ADMIN))
			return true;

		foreach ($this->Comments as $comment)
		{
			if ($comment->News == $news)
				return false;
		}

		return true;
	}

	/**
	 * determines whether the user can purchase the given ShopItem, taking in consideration related account's admin or vip states
	 *
	 * @param ShopItem $si
	 *
	 * @return bool
	 */
	public function canPurchase(ShopItem $si)
	{
		if (!$this->getAccount()->getMainChar())
			return false;

		if ($this->getAccount()->level >= LEVEL_ADMIN)
			return true;

		if ($si->is_vip && !$this->getAccount()->vip)
			return false;

		return $this->getAccount()->hasPoints($si->getCost());
	}
	
	/**
	 * returns unread private messages
	 *
	 * @return PrivateMessageThread[]
	 */
	public function getUnreadPM()
	{
		if (null === $this->pmUnreads)
		{
			$this->pmUnreads = Query::create()
									->from('PrivateMessageThread pmt INDEXBY pmt.id')
										->leftJoin('pmt.Receivers pmr INDEXBY pmr.account_id')
									->where('pmt.id IN
									 (SELECT pmtr.thread_id FROM PrivateMessageThreadReceiver pmtr
									  WHERE pmtr.account_id = ? AND pmtr.next_page != 0 AND pmtr.present = 1)',
										 $this->guid) //the last is needed, else if you left with one PM unread you'll still see the "unread" message but you won't be able to unread it (coz you've left)
									->execute();
		}

		return $this->pmUnreads;
	}

	/**
	 * returns a link to read the first unread pm
	 *
	 * @return string html link
	 */
	public function getNextPMNotif()
	{
		$unreads = $this->getUnreadPM();

		if ($unreads->count())
		{
			$unread = $unreads->getFirst();
			$pm_url = replace_url('@pm.show', true, array('id' => $unread, 'page' => $unread['Receivers'][$this->guid]['next_page']));
			return sprintf(lang('pm.arrived' . ($unreads->count() > 1 ? 's' : '')), $pm_url, $unreads->count()) . tag('br');
		}
		else
			return '';
	}
}