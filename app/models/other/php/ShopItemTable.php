<?php

/**
 * ItemTable
 *
 * @package    InfiniteCMS
 * @subpackage Models
 * @author     Nami-Doc <nami.d0c.0@gmail.com>
 * @version    SVN: $Id: ItemTable.php 24 2010-10-22 11:46:07Z nami.d0c.0 $
 */
class ShopItemTable extends RecordTable
{
	protected $numericCols = array('cost', 'value', 'category_id'),
		$extFilters = array('name', 'cost', 'cost2', 'cat', 'is_lottery', 'is_hidden'), //extended
		$simpleFilters = array('name', 'cat', 'cost', 'cost2', 'is_lottery');

	public function getNumericCols()
	{
		global $config;
		if (empty($config['COST_VIP']))
			return $this->numericCols;
		else
			return array_merge($this->numericCols, array('cost_vip'));
	}

	public function getAllFilters()
	{
		global $config;
		if (empty($config['COST_VIP']))
			return $this->extFilters;
		else
			return array_merge($this->extFilters, array('is_vip'));
	}

	public function getFilters()
	{
		global $config;
		$filters = $this->simpleFilters;
		if (level(LEVEL_VIP) && !empty($config['COST_VIP']))
			$filters[] = 'is_vip';
		if (level(LEVEL_ADMIN))
			$filters[] = 'is_hidden';
		return $filters;
	}

	public function getProtectedFilters()
	{
		return array_diff($this->getAllFilters(), $this->getFilters());
	}

	public function getSearchBox()
	{
		global $router, $config, $categories;

		$search_val = array();
		foreach ($this->getFilters() as $filter)
			$search_val[$filter] = $router->requestVar($filter) ?: '';
		if ($search_val['cat'] === '')
			$search_val['cat'] = -1;

		$options = tag('div', array('id' => 'options')) .
					input('is_lottery', lang('shop.is_lottery'), 'checkbox', $search_val['is_lottery']);
		if (in_array('is_vip', $this->getFilters()))
			$options .= input('is_vip', lang('shop.is_vip'), 'checkbox', $search_val['is_vip']);
		if (in_array('is_hidden', $this->getFilters()))
			$options .= input('is_hidden', lang('shop.is_hidden'), 'checkbox', $search_val['is_hidden']);
		$options .= '</div>';
		jQ('$("#options").buttonset();');

		$cost_text = '&nbsp;' . sprintf(lang('shop.cost_simple'), $config['POINTS_CREDIT' . (level(LEVEL_VIP) && !empty($config['COST_VIP']) ? '_VIP' : '')], $config['POINTS_VOTE' . (level(LEVEL_VIP) ? '_VIP' : '')]) . ', ' .
			 lang('between') . '&nbsp;';

		$cats = array('records' => $categories, 'model' => 'ShopCategory');

		return tag('b', lang('shop.check_to_filter')) .
		  make_form(array(
			array('e_name', null, 'checkbox', '1', array(), false),
			array('name', '&nbsp;' . lang('shop.item.name') . '&nbsp;', null, $search_val['name']),
			array('e_cat', null, 'checkbox', '0', array(), false),
			array('cat', '&nbsp;' . lang('category'), 'record', $cats, $search_val['cat']),
			array('e_cost', null, 'checkbox', '1', array(), false),
			array('cost', $cost_text, null, intval($search_val['cost']), array(), false),
			array('cost2', '&nbsp;' . lang('and') . '&nbsp;', null, intval($search_val['cost2'])),
			$options
		), $this);
	}

	public function getSearchResults($categories)
	{
		global $router;

		$all_items_dql = Query::create()
						->from('ShopItem');
		$items_dql = Query::create()
						->from('ShopItem si')
						->where('1=1') //used for further addWhere()
							->leftJoin('si.Effects e');

		$search_params = array(); //search_params is used for pagination
		$filter_names = $this->getFilters(); //used later ^^
		foreach ($filter_names as $type)
		{ //foreach columns
			$is_check = substr($type, 0, 3) == 'is_'; //it's a check "is".
			$value = $router->requestVar($type);
			$real_type = rtrim($type, '2'); //this is the REAL type. used for e_ ! important
			if (( $router->requestVar('e_' . $real_type) !== null || $is_check )
			 && !empty($value) && $value != -1) //e = enabled
			{
				$value = $type == 'name' ? $value : ( $is_check ? strtolower($value) == 'on' : intval($value) );

				if ($real_type == 'cost' && $value == 0)
					continue; //we don't need to process that
				if ($type == 'cat' && $value < 1)
					continue; //this probably isn't an interesting category.

				$search_params['e_' . $real_type] = '1'; //used for ...
				$search_params[$type] = $value; //... pagination
				if ($type == 'cost')
				{
					$sign = '>=';
				}
				else if ($type == 'cost2')
				{ //ie : cost 200 cost2 300 (in the form) => cost > 200 & < 300
					$type = 'cost';
					$sign = '<=';
				}
				else if ($type == 'name')
				{
					$sign = 'LIKE';
					$value = '%'.str_replace('*', '%', $value).'%';
				}
				else
				{
					$sign = '=';
				}

				if ($type == 'cat')
					$type = 'category_id'; //adjust column name
				if ($type == 'cost' && (level(LEVEL_VIP) && !empty($config['COST_VIP'])))
					$type = 'cost_vip'; //check for VIP if vip.

				$all_items_dql->andWhere(sprintf('%s %s ?', $type, $sign), $value);
				$items_dql->andWhere(sprintf('si.%s %s ?', $type, $sign), $value);
			}
			if ($is_check)
			{
				$all_items_dql->addOrderBy($type . ' DESC');
				$items_dql->addOrderBy('si.' . $type . ' DESC');
			}
		}

		foreach ($this->getProtectedFilters() as $filter)
		{ //protected remaining filters (can NOT be used : VIP if you're not, ie)
			$all_items_dql->andWhere($filter . ' = 0');
			$items_dql->andWhere($filter . ' = 0');
		}

		$all_items = $all_items_dql->fetchArray();
		$allowed_cats = array();
		foreach ($all_items as $allItem)
			$allowed_cats[] = $allItem['category_id'];
		$allowed_cats = array_unique($allowed_cats);

		return array($items_dql, $allowed_cats, $search_params);
	}

	public function getType($type)
	{
		foreach (ShopItemEffectTable::getInstance()->getTypes() as $t => $k)
		{
			if ($t == $type)
				return $k;
			if (is_array($k) && isset($k[$type]))
				return $k[$type];
		}
		return null;
	}
}