<?php

/**
 * AccountTable
 * 
 * @package    InfiniteCMS
 * @subpackage Models
 * @author     Nami-Doc <nami.d0c.0@gmail.com>
 * @version    SVN: $Id: AccountTable.php 24 2010-10-22 11:46:07Z nami.d0c.0 $
 */
class AccountTable extends RecordTable
{
	public function getColumnOptions()
	{
		return array(
			'column' => array(  //name, display, searchCallback
//				array('guid', 'id'),
				'pseudo',
				'level',
				array('ip', 'lastip', 'ip2long'),
				array('points', 'u.points'),
				array(null, 'u.main_char'),
			),
			'search' => array(
				'pseudo',
				'ip',
			),
			'join' => array(
				'User' => 'u',
			),
			'path' => array(
//				array('id', '%%=guid%%'),
				array('pseudo', '%%=pseudo%%'),
				array('level'),
				array('ip', 'lastIp'),
				array('points', 'User.pointsSpan'),
				array('character', '%%MainChar%%:acc.ladder.no_character'),
			),
		);
	}

	public function getRouterParam()
	{
		return 'id';
	}
}