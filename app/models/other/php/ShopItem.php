<?php

/**
 * ShopItem
 *
 * @package    InfiniteCMS
 * @subpackage Models
 * @author     Nami-Doc <nami.d0c.0@gmail.com>
 * @version    SVN: $Id: Item.php 33 2010-10-31 20:45:35Z nami.d0c.0 $
 */
class ShopItem extends BaseShopItem
{
	/**
	 * gives the shopItem to a character, deals with lottery locks etc
	 */
	public function giveTo(Character $c, array $datas)
	{
		if ($this->is_lottery)
		{
			if (!isset($_SESSION['buy_index'][$this->id]))
				exit('cheating buy_index giveTo');

			$effectIndex = $_SESSION['buy_index'][$this->id];
			$effect = $this->Effects[$effectIndex]; //no need to use $effects since $effectIndex is based upon the good one
			$hasEffect = $effect->giveTo($c, $datas); //you can not "disable" a lottery result, that'd of course be cheating.
			unset($_SESSION['buy_index'][$this->id]); //you're again allowed to buy this item !
			return array($effect, $hasEffect);
		}
		else
		{
			$hasEffect = false; //you won't loose points if you didn't received anything
			foreach ($this->getAvailableEffectsFor($c, $datas) as $effect)
			{
				if ($effect->giveTo($c, $datas))
					$hasEffect = true;
			}
			return $hasEffect;
		}
	}

	/**
	 * checks whether we have to confirm for the character
	 */
	public function hasConfirmation(Character $c, $datas)
	{
		if ($this->is_lottery)
			throw new RuntimeException('Not even in ur deepest dreams');

		foreach ($this->getAvailableEffectsFor($c, $datas) as $effect)
		{
			if ($effect->getConfirmation($c, $datas) != null)
				return true;
		}
		return false;
	}
	
	/**
	 * builds confirmation form
	 *
	 * @param Character $c the character to receive
	 * @param array $datas datas already received
	 */
	public function getConfirmationForm(Character $c, $datas)
	{
		$form = array();
		if ($this->is_lottery)
		{
			if (!isset($_SESSION['buy_index'][$this->id]))
				exit('cheating buy_index getConfirmationForm');

			$effect = $this->Effects[$_SESSION['buy_index'][$this->id]];
			if (!$confirm = $effect->getConfirmation($c, $datas))
				return '';
			$form[$effect->render()] = array($confirm); //should never be empty !!
		}
		else
		{
			if (!$this->hasConfirmation($c, $datas))
				return '';

			foreach ($this->Effects as $effect)
			{
				$inputs = $effect->getConfirmation($c, $datas);
				if (empty($inputs))
					continue;

				$form[$effect->render()] = $inputs; 
			}
		}

		return make_form($form);
	}
	
	/**
	 * checks confirmation data and returns the state
	 * @see getConfirmationForm
	 *
	 * @param Character $c the character to receive
	 * @param array $datas datas already received
	 * @return bool is valid?
	 */
	public function confirm(Character $c, $datas)
	{
		if (!$this->is_lottery && !$this->hasConfirmation($c, $datas))
			return true;

		global $errors;

		if ($this->is_lottery)
		{
			$index = $this->getRandomIndex($c, $datas); //take one of the effect

			if (!isset($_SESSION['buy_index'][$this->id]))
			{
				if (empty($datas)) //not submitted yet
					$_SESSION['buy_index'][$this->id] = $index;
				else
				{ //I SEE YOU CHEATING
					$errors[] = lang('shop.not_buying');
					return false;
				}
			}
			if (!$this->Effects[$index]->getConfirmation($c, $datas))
				return true;
		}

		if (empty($datas)) //show up da form anyway
			return false;

		if (empty($errors)) //if errors are here ...
			$this->validateDatas($c, $datas);

		return empty($errors);
	}
	
	/**
	 * validates datas
	 * @see confirm
	 */
	public function validateDatas(Character $c, $datas)
	{
		if ($this->is_lottery)
			$this->Effects[$_SESSION['buy_index'][$this->id]]->validateDatas($c, $datas);
		else
		{
			foreach ($this->getAvailableEffectsFor($c, $datas) as $effect)
				$effect->validateDatas($c, $datas);
		}
	}
	
	/**
	 * returns available Effects for a character, based on $datas
	 *
	 * @param Character $c the character
	 * @param array $datas the datas
	 * @param bool $un Unavailable ?
	 * @return array (un?)available effects
	 */
	public function getAvailableEffectsFor(Character $c, $datas, $un = false)
	{
		$effects = array();
		foreach ($this->Effects as $i => $effect)
		{
			if ($un != $effect->isAvailableFor($c, $datas))
				$effects[$i] = $effect;
		}

		return $effects;
	}
	
	/**
	 * returns a random effect
	 *
	 * @return ShopItemEffect chosen through array_rand
	 */
	public function getRandomIndex(Character $c, $datas)
	{
		$effects = $this->getAvailableEffectsFor($c, $datas);
		return array_rand($effects);
	}

	/**
	 * returns the default allowed columns, depending on VIP state
	 *
	 * @return array column list
	 */
	protected function getDefaultColumns()
	{
		global $config;

		//WHERE vip & hidden ? for VIP-destined draw.
		$columns = array('name', 'cost', 'category_id', 'is_lottery', 'is_hidden');

		if (!empty($config['COST_VIP']))
		{
			$columns[] = 'cost_vip';
			$columns[] = 'is_vip';
		}

		return $columns;
	}

	/**
	 * updates shop item
	 *
	 * @param arrray $values values (_POST)
	 * @return array errors if errors ...
	 */
	public function update_attributes(array $values, $columns = null)
	{
		global $config, $errors;

		$columns = (array) $columns;
		if (!$columns)
			$columns = $this->getDefaultColumns();

		if (is_string($columns))
			$columns = explode(';', $columns);
		$prev = $this->exists() ? $this->toArray() : array();

		foreach ((array)$columns as $t)
		{
			$t = lcfirst($t);
			//check if the value is valid
			if (( !isset($values[$t]) || (isset($values[$t]) && trim($values[$t]) === '' ) )
			 && substr($t, 0, 3) !== 'is_')
			{
				$errors[$t] = sprintf(lang('must_!empty'), $t);
			}
			else
			{
				if (substr($t, 0, 3) == 'is_')
					$values[$t] = isset($values[$t]) && ( $values[$t] == 'on' || $values[$t] == '1' );

				if (in_array($t, $this->getTable()->getNumericCols()) && strval(intval($values[$t])) !== $values[$t])
					$errors[$t] = sprintf(lang('must_numeric'), $t);
				else
					$this->$t = $values[$t];
			}
		}
		
		if (!$this->relatedExists('Category'))
			$errors[] = sprintf(lang('must_!empty'), lang('category'));
		
		if (!empty($config['COST_VIP']) && $this->cost < $this->cost_vip)
		{
			if (empty($prev['cost_vip']))
				$this->cost_vip = $this->cost - 1; //shouldn't eq:0
			else
			{
				if ($this->cost > $prev['cost_vip'])
					$this->cost = $prev['cost'];

				$this->cost_vip = $prev['cost_vip'];
			}	
			$errors[] = lang('shop.cost_vip_lower');
		}

		if (!empty($values['type']) && !empty($values['value']))
			$this->updateEffects($values['type'], $values['value']);

		if ($this->Effects->count() < 2)
			$this->is_lottery = false; //a lottery with only A lot is stupid.
	}

	/**
	 * updates effect list : add, removes, invalidates
	 *
	 * @param array $types effect types
	 * @param array $values effect values
	 */
	protected function updateEffects($types, $values)
	{
		global $errors;

		foreach ($types as $id => $type)
		{
			if (empty($values[$id]) || $type == -1 || $type == null)
				continue;
			else if (!$this->getTable()->getType($type))
				$errors[] = sprintf(lang('must_numeric'), 'type (' . $id . ')' );
			else if ($this->Effects->contains($id))
			{
				if ($type === null || $type == -1)
					$this->Effects[$id]->delete(); //remove the effect
				else
				{
					$this->Effects[$id]->type = $type;
					if (isset($values[$id]) && $this->Effects[$id]->setValue($values[$id]))
						$this->Effects[$id]->save();
					else
						$errors['effect_' . $id] = sprintf(lang('must_!empty'), 'effect[' . $id . ']');
				}
			}
			else
			{
				$effect = new ShopItemEffect;
				$effect->type = $type;
				if ($effect->setValue($values[$id]))
				{
					$effect->save();
					$this->Effects->add($effect);
				}
				else
					$errors['effect_' . $id] = sprintf(lang('must_!empty'), 'effect[' . $id . ']');
			}
		}
	}

	/**
	 * this function is NOT used anymore in Shop
	 * but is still for Rewards
	 */
	public function toString($name = true)
	{
		global $account;
		IG::registerEffectsTooltip();

		$id = $this->getDataId();

		$effects = '';
		if ($this->Effects->count())
		{
			foreach ($this->Effects as $effect)
				$effects .= tag('li', $effect);
			$effects = tag('ul', $effects);
		}

		$desc = News::format($this->description); //:(
		if ($name)
			$name = sprintf('<b>%s:</b> %s.<br />',
			 lang('name'),
			 tag('span', $id + array('class' => 'f_name'), $this->name));

		return sprintf('
			%s
			%s<b>%s:</b><br />%s<br /><!-- desc -->
			%s<br /><!-- cost(s) -->
			%s<!-- type(s) -->
			%s<!-- Effects -->
			%s
			%s',
		 $name,
		 empty($desc) ? '<!--' : '', lang('desc'), $desc,
		 $this->getCostInfo(),
		 $this->getTypesInfo(),
		 $effects,
		 ( level(LEVEL_LOGGED) && $account->User->canPurchase($this) ? $this->getPurchaseLink() : '' ),
		 ( level(LEVEL_ADMIN) ? tag('br') . $this->getUpdateLink() . '<br />' . $this->getDeleteLink() : '' ));
	}

	/**
	 * formats instance to be displayed in a box
	 *
	 * @return string instance as html
	 */
	public function __toString()
	{
		return $this->toString();
	}
	
	/**
	 * formats instance types : VIP, lottery mode or hidden
	 *
	 * @return string types separated with a bull
	 */
	public function getTypesInfo()
	{
		global $router, $config;
		if ($router->getController() != 'Shop')
			return ''; //this is a hack, I know ._.

		$types = array();
		if ($this->is_vip && !empty($config['COST_VIP']))
			$types[] = lang('shop.is_vip');
		if ($this->is_lottery)
			$types[] = lang('shop.is_lottery');
		if ($this->is_hidden && level(LEVEL_ADMIN))
			$types[] = lang('shop.is_hidden');

		return implode(' &bull; ', $types);
	}
	
	/**
	 * returns cost for the current user, depending on his .vip state
	 *
	 * @return int real cost
	 */
	public function getCost()
	{
		global $config;
		if (level(LEVEL_VIP) && !empty($config['COST_VIP']))
			return $this->cost_vip;
		return $this->cost;
	}
	
	/**
	 * @return string name
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * return @string formatted description
	 */
	public function getDescription()
	{
		return News::format($this->description);
	}
	
	/**
	 * returns cost info, depending on the current user's vip state or admin state
	 *
	 * @return string vip cost for vip, normal cost for non-vip, both for admins (to be able to edit in place)
	 */
	public function getCostInfo()
	{
		global $config;

		$cost = '';

		if (level(LEVEL_VIP) && !empty($config['COST_VIP']))
			$cost .= tag('b', lang('cost_vip') . ' : ') . pluralize(lang('point'), $this->cost_vip, SHOW_VALUE, '%%content%%');
		if (empty($config['COST_VIP']) || !$this->is_vip)
		{
			if (level(LEVEL_ADMIN))
				$cost .= tag('br');
			if (!level(LEVEL_VIP) || level(LEVEL_ADMIN))
				$cost .= tag('b', lang('cost') . ' : ') . pluralize(lang('point'), $this->cost, SHOW_VALUE, '%%content%%');
		}
		return $cost;
	}

	public function getPurchaseLink()
	{
		return make_link('@shop.purchase', lang('act.choose'), array('id' => $this));
	}
	
	public function getUpdateLink()
	{
		return make_link('@shop.update', lang('act.edit'), array('id' => $this));
	}
	
	public function getDeleteLink()
	{
		return make_link('@shop.delete', lang('act.delete_item'), array('id' => $this));
	}
}
