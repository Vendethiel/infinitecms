<?php
/**
 * Item
 * 
 * @package    InfiniteCMS
 * @subpackage Models
 * @author     Nami-Doc <nami.d0c.0@gmail.com>
 * @version    SVN: $Id: Items.php 24 2010-10-22 11:46:07Z nami.d0c.0 $
 */
class Item extends BaseItem
{
	public function getTooltip()
	{
		return $this->getName() . '<br />' . $this->parseStats();
	}

	public function getName()
	{
		return lang($this->template, 'item', $this->template);
	}

	public function parseStats()
	{
		return IG::parseStats($this->stats, false);
	}
}