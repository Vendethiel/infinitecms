<?php

/**
 * Guilds
 * 
 * @package    InfiniteCMS
 * @subpackage Models
 * @author     Nami-Doc <nami.d0c.0@gmail.com>
 * @version    SVN: $Id: Guild.php 25 2010-10-22 12:10:44Z nami.d0c.0 $
 */
class Guild extends BaseGuild
{
	public $totalLevel = 0;
	protected $emblemInstance;

	public function getMeanLevel()
	{
		return round($this->totalLevel / $this->Members->count(), 0);
	}

	public function getLink($text = null)
	{
		if ($text === null)
			$text = $this->name;

		return $this->getEmblem() . ' ' .
		 make_link('@guild.show', $text, array('id' => $this), array('title' => $this->name));
	}

	public function getLeader()
	{
		foreach ($this->Members as $member)
		{
			if ($member->rank == 1)
				return $member->Character;
		}
	}

	public function getName()
	{
		return $this->getEmblem() . ' ' . $this->name;
	}

	public function getEmblem()
	{
		if (empty($this->emblem))
			return '';

		if (!$this->emblemInstance)
			$this->emblemInstance = new GuildEmblem($this->emblem);
		return $this->emblemInstance;
	}
}
