<?php

/**
 * ItemEffectTable
 *
 * @package    InfiniteCMS
 * @subpackage Models
 * @author     Nami-Doc <nami.d0c.0@gmail.com>
 * @version    SVN: $Id: ItemEffectTable.php 24 2010-10-22 11:46:07Z nami.d0c.0 $
 */
class ShopItemEffectTable extends RecordTable
{
	const TYPE_ADD_PREFIX = 300;
	const TYPE_CHANGE_GENDER = 301;


	/**
	 * items
	 *
	 * @var ItemTemplate[]
	 * @static
	 * @access public
	 */
	public $items = null;

	/**
	 * _initItem
	 * Initialise all items. see {@$items}
	 *
	 * @access protected
	 * @static
	 * @return void
	 */
	protected function _initItems()
	{
		if( $this->items === array() )
			$this->items = lang(null, 'item');
	}

	/**
	 * returns the list of shop item effects
	 *
	 * @return array
	 */
	public function getTypes()
	{
		return array(
			lang('shop._add') => array(
				LiveActionTable::TYPE_LEVEL_UP => lang('shop.level_up'),
				LiveActionTable::TYPE_ADD_XP => lang('shop.xp'),
				LiveActionTable::TYPE_ADD_K => lang('shop.K'),
				LiveActionTable::TYPE_ADD_CAPITAL => lang('shop.capital'),
				LiveActionTable::TYPE_ADD_SPELLPOINT => lang('shop.spellpoint'),
			),
			lang('shop._items') => array(
				LiveActionTable::TYPE_ITEM_JETS_ALEATOIRES => lang('shop.item_random'),
				LiveActionTable::TYPE_ITEM_JETS_MAX => lang('shop.item_perfect'),
			),
			lang('shop._stats') => array(
				LiveActionTable::TYPE_CARAC_FORCE => lang('shop.stat.strength'),
				LiveActionTable::TYPE_CARAC_AGILITE => lang('shop.stat.agility'),
				LiveActionTable::TYPE_CARAC_CHANCE => lang('shop.stat.chance'),
				LiveActionTable::TYPE_CARAC_SAGESSE => lang('shop.stat.wisdom'),
				LiveActionTable::TYPE_CARAC_VITALITE => lang('shop.stat.vitality'),
				LiveActionTable::TYPE_CARAC_INTELLIGENCE => lang('shop.stat.intell'),
			),
			self::TYPE_ADD_PREFIX => lang('character.prefix_name'),
		);
	}

	/**
	 * Determinates if the type is an item
	 *
	 * @param integer $type The type of the item
	 *
	 * @return boolean whether it's an item or not ?
	 */
	public function isItem($type)
	{
		if ($type instanceof ShopItemEffect || is_array($type))
			$type = $type['type'];

		return in_array(intval($type), LiveActionTable::getInstance()->getItemTypes());
	}
	public function isLiveAction($type)
	{
		if ($type instanceof ShopItemEffect || is_array($type))
			$type = $type['type'];

		return in_array(intval($type), LiveActionTable::getInstance()->getTypes());
	}

	/**
	 * doFindItem
	 * find a item name by it's ID, with a litte cache.
	 *
	 * @access public
	 * @static
	 *
	 * @param integer $id The Id of the object
	 * @return mixed boolean|error The name of the object or "Objet inconnu"
	 */
	public function doFindItem($id)
	{
		$this->_initItem();
		if ($id instanceof ItemEffect)
			return $this->doFindItemIf($id->value, $this->isItem($id));

		$id = intval($id);
		return isset($this->items[$id]) ? $this->items[$id] : sprintf(lang('shop.item.not_exists'), $id);
	}
	/**
	 * doFindByName
	 * select an object by it's name, directly into the db.
	 *
	 * @access public
	 * @static
	 * @see Table::findOne (__call: ItemTemplate::findOneByName)
	 *
	 * @param string $n The name of the item
	 * @return Doctrine_Record the ItemTemplate instance
	 */
	public function doFindByName($n)
	{
		$this->_initItems();
		return array_search($n, $this->items);
	}
	/**
	 * doFindItemIf
	 * execute a doFindItem if ...
	 *
	 * @see doFindItem
	 * @access public
	 * @static
	 *
	 * @param integer $i The item ID
	 * @param boolean $c The condition
	 * @return Doctrine_Record|$i Doctrine_Record instance if the item exists && $c, else $i
	 */
	public function doFindItemIf($i, $c) { return $c ? $this->doFindItem($i) : $i; }
	/**
	 * doFindItemUnless
	 * execute a doFindItem UNLESS ...
	 *
	 * @see doFindItem
	 * @access public
	 * @static
	 *
	 * @param integer $i The item ID
	 * @param boolean $c The condition
	 * @return Doctrine_Record|$i Doctrine_Record instance if the item exists && !$c, else $i
	 */
	public function doFindItemUnless($i, $c) { return $this->doFindItemIf($i, !$c); }


	// public because I'm too lazy to add a method for it
	public $templates = array();
	public function doFindItemTemplate($id)
	{
		if ($id instanceof ShopItemEffect || is_array($id))
			$id = $id['value'];
		if (!isset( $this->templates[$id]))
			$this->templates[$id] = ItemTemplateTable::getInstance()->find($id); //fetch the record
		return $this->templates[$id];
	}
	public function doFindItemTemplateIf($i, $c) { return $c ? $this->doFindItemTemplate($i) : $c; }
	public function doFindItemTemplateUnless($i, $c) { return $this->doFindItemTemplateIf($i, !$c); }
}