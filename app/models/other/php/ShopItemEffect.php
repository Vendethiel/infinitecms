<?php

/**
 * ItemEffect
 *
 * @package    InfiniteCMS
 * @subpackage Models
 * @author     Nami-Doc <nami.d0c.0@gmail.com>
 * @version    SVN: $Id: ItemEffect.php 40 2010-11-21 01:15:23Z nami.d0c.0 $
 */
class ShopItemEffect extends BaseShopItemEffect
{
	public function getValue()
	{
		if ($this->isItem())
			return $this->getTable()->doFindItemTemplate($this);
		return $this->value;
	}

	public function setValue($val)
	{
		if ($this->isItem()) //find the item ID
		{
			if (!$val = ItemTemplateTable::getInstance()->search($val))
				return false;
		}
		$this->value = $val;
		return true;
	}
	
	public function __toString()
	{
	try {
		if ($this->type === null || $this->type == -1 || $this->getValue() === 0)
			return ''; //null effect ?

		if ($this->isLiveAction())
			return LiveActionTable::getInstance()->render($this);
		else
			return $this->render();
	} catch (\Exception $e) {
		exit('Exception ' . get_class($e) . ' : "' . $e->getMessage() . '".<br />'. $e->getTraceAsString());
	}
	}

	public function isLiveAction()
	{
		return $this->getTable()->isLiveAction($this);
	}

	public function isItem()
	{
		return $this->getTable()->isItem($this);
	}

	public function isMaxStats()
	{
		return $this->type == LiveActionTable::TYPE_ITEM_JETS_MAX;
	}


	public function getTypeString()
	{
		return ShopItemTable::getInstance()->getType($this->type);
	}

	public function getItemTooltip()
	{
		if (!$this->isItem())
			return '';

		if (( $val = $this->getValue() ) instanceof ItemTemplate
		 && !empty($val->statstemplate))
			return str_replace('"', "'", $val->getTooltip($this->isMaxStats()));
		return ''; //no template / no stats found !
	}

	public function getDeleteLink()
	{
		return make_link('@shopitemeffect.delete', lang('act.delete'), array('id' => $this));
	}

	public function giveTo($char, $datas)
	{
		if ($this->isLiveAction())
		{
			LiveActionTable::getInstance()->give($char, $this);
			return true;
		}
		else
			return $this->process($char, $datas);
	}

	public function confirm(Character $c, $datas)
	{
		if (!$this->getConfirmation($c, array()))
			return true; //before anything.
		if (!isset($datas['proceed']))
			return false;

		global $errors;
		$this->validateDatas($c, $datas);
		return empty($errors);
	}

	public function getConfirmation(Character $c, $datas)
	{
		switch ($this->type)
		{
			case ShopItemEffectTable::TYPE_ADD_PREFIX:
				return array(
					array('confirm_' . $this->id, lang('enable') . ' ?', 'checkbox', !empty($datas['confirm_' . $this->id]))
				);
			break;
		}
		return null;
	}
	public function getConfirmationForm(Character $c, $datas, $extra = array())
	{
		return make_form(array_merge(array(
			$this->render() => $this->getConfirmation($c, $datas),
		), $extra, array(
			array('proceed', null, 'hidden', 1),
		)));
	}
	public function validateDatas(Character $c, $datas)
	{ //confirms datas passed
/*
		global $errors;

		switch ($this->type)
		{
			case ShopItemEffectTable::TYPE_ADD_PREFIX:
				$errors[] = 'test !';
			break;
		}
//*/
	}
	public function isAvailableFor(Character $c, $datas)
	{
		if ($this->isLiveAction())
			return true;

		switch ($this->type)
		{
			case ShopItemEffectTable::TYPE_ADD_PREFIX:
				return !CharacterTable::getInstance()->findOneByName($c->getWithPrefix($this->value));
			break;
		}
		return true;
	}
	/**
	 * processes the effect
	 *
	 * @param Character $char the character who's gonna receive 
	 */
	public function process(Character $char, $datas)
	{ //FOR INSTANCE : $char may not be $account->getMainChar(), i.e. the "give" form in Character/show
		switch ($this->type)
		{
			case ShopItemEffectTable::TYPE_ADD_PREFIX:
				if (empty($datas['confirm_' . $this->id]))
					return false;

				$char->setPrefix($this->value);
				$char->save();
			break;
			default:
				return false;
		}
		return true;
	}
	
	public function render()
	{
		switch ($this->type)
		{
			case ShopItemEffectTable::TYPE_ADD_PREFIX:
				return lang('character.prefix_name') . ' : ' . tag('i', $this->value);
			break;
		}
		return '';
	}
	/**@todo that way ?
	$effectLangs = array(
		ShopItemEffectTable::TYPE_ADD_PREFIX => 'Adds prefix <i>%s</i>',
	);
	//*or ...
	public function render()
	{
		return strtr(lang('shop.effect.' . $this->type), array(
			'%value%' => $this->getValue(),
			'%val%' => $this->value,
		));
	}
	*/
}