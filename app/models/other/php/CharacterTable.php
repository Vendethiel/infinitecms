<?php
/**
 * CharacterTable
 * 
 * @package    InfiniteCMS
 * @subpackage Models
 * @author     Nami-Doc <nami.d0c.0@gmail.com>
 * @version    SVN: $Id: PersonnageTable.php 45 2010-12-04 13:13:19Z nami.d0c.0 $
 */
class CharacterTable extends RecordTable
{
	public function getSearch()
	{
		//this looks like a phpBB's function header x(
		global $breed, $gender, $contest, $char, $m, $orders, $orderBy, $ordersLang;
		$hide = $gender == -1 && $breed == -1 && empty($char) && $orderBy == end($orders) && $m == 'DESC';

		$breeds = IG::getBreeds();
		$genders = IG::getGenders();

		$sBreed = isset($breeds[$breed]) ? $breed : -1; //selected breed
		$sGender = isset($genders[$gender]) ? $gender : -1; //selected gender

		jQ('
var searchForm = $("#searchDiv"),
	searchVisible = ' . ( $hide ? 'false' : 'true' ) . ';
if (!searchVisible)
	searchForm.hide();

searchLegend = $("#searchLegend").click(function ()
{
	searchForm.slideToggle();
	searchVisible = !searchVisible;
	if (searchVisible) //and just calling searchForm.is(":visible") in the browser\'s console works fine ... FUCK YOU.
		searchLegend.find("span").html(" &lt;");
	else
		searchLegend.find("span").html(" &gt;");
});');
		return tag('fieldset', array('id' => 'searchFS'), tag('legend', array('id' => 'searchLegend'), lang('character.search') . tag('span', array('class' => 'show-this'), $hide ? ' &gt;' : ' &lt;')) .
		 tag('div', array('id' => 'searchDiv'), make_form(array( //hide-this could've been used, if only I rembered it was at that time removing the el from dom
			array('character', lang('name'), null, $char),
			array('orderBy', lang('ladder.order_by'), 'select', $ordersLang, $orderBy),
			array('orderMode', lang('ladder.order_mode'), 'select',
			 array('DESC' => lang('ladder.order_mode.DESC'), 'ASC' => lang('ladder.order_mode.ASC')), $m),
			array('gender', lang('acc.ladder.sex'), 'select', $genders, $sGender),
			array('breed', lang('acc.ladder.class'), 'select', $breeds, $sBreed),
			array('contest', null, 'hidden', $contest ? $contest->id : -1),
		)))) . tag('br');
	}

	/**
	 * returns table header (with tr)
	 *
	 * @param boolean $simple Simple datas ? (only name)
	 * @return string HTML format of datas header with TR
	 */
	public function getTableHeader($simple = false)
	{
		return tag('tr', $this->getTableHeaderDatas($simple));
	}

	/**
	 * returns the datas header for a table
	 *
	 * @param boolean $simple simple datas ? (only name)
	 * @return string HTML format
	 */
	public function getTableHeaderDatas($simple = false)
	{
		$datas = tag('th', tag('b', lang('name')));
		if (!$simple)
		{
			$datas .= tag('th', tag('b', lang('acc.ladder.class'))) .
					tag('th', tag('b', lang('acc.ladder.sex'))) .
					tag('th', tag('b', lang('level')));
		}
		return $datas;
	}

	public function retrieve()
	{
		global $router;

		$col = $this->getIdentifier();
		$c = -1;
		foreach ($this->getColumnNames() as $col) //Character/show/name/...
			if (( $c = $router->requestVar($col == 'guid' ? 'id' : $col) ) !== null)
				break;
		return $this->createQuery('p')
							->leftJoin('p.GuildMember gm')
								->leftJoin('gm.Guild g')
							->where('p.' . $col . ' = ?', $c)
						->fetchOne(); //find the character and fetch his guild
	}
}
