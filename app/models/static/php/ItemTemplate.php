<?php

/**
 * ItemTemplate
 * 
 * @package    InfiniteCMS
 * @subpackage Models
 * @author     Nami-Doc <nami.d0c.0@gmail.com>
 * @version    SVN: $Id: ItemTemplate.php 24 2010-10-22 11:46:07Z nami.d0c.0 $
 */
class ItemTemplate extends BaseItemTemplate
{
	public function __toString()
	{
		return lang($this->id, 'item', $this->name);
	}

	public function getTooltip($max = false)
	{
		return (string) $this . '<br />'
		 . lang('level') . ' : ' . $this->level . '<br />'
		 . $this->parseStats($max);
	}

	public function parseStats($isMax = false)
	{
		if ($this->statstemplate)
			return IG::parseStats($this->statstemplate, $isMax);
	}

	public function parseCraft()
	{
		if (!$this->Craft)
			return;

		return IG::parseCraft($this->Craft->craft);
	}

	public function hasImage()
	{
		return asset_path('items/' . $this->id . EXT_PNG, true);
	}

	public function getName()
	{
		return (string) $this;
	}

	public function getUpdateLink()
	{
		return make_link('@glossary.item.update', lang('act._edit'), array('id' => $this));
	}

	public function update_attributes(array $values)
	{
		global $errors;

		if (empty($values['name']))
			$errors[] = sprintf(lang('must_!empty'), lang('name'));
		else
			$this->name = $values['name'];

		if (isset($values['type']) && lang($values['type'], 'itemcategory', null) !== null)
			$this->type = $values['type'];
		else
			$errors[] = sprintf(lang('must_!empty'), lang('name'));

		if (!empty($values['level']))
			$this->level = $values['level'];
		else
			$errors[] = sprintf(lang('must_!empty'), lang('level'));

		if (!empty($values['weight']))
			$this->pod = $values['weight'];
		else
			$errors[] = sprintf(lang('must_!empty'), lang('weight'));

		if (!empty($values['price']))
			$this->prix = $values['price'];
		else
			$errors[] = sprintf(lang('must_!empty'), lang('cost'));

		if (!empty($values['stat']) && is_array($values['stat']))
		{
			$stats = array();
			foreach ($values['stat'] as $key => $stat)
			{
				if (!isset($stat['from']) || !isset($stat['to']))
					exit('unable to parse from/to !');

				$from = $stat['from'];
				$to = $stat['to'];

				if ($from == 0 && $to == 0)
					continue;

				if ($from > $to)
					$from = $to + $from - ($to = $from);

				$from = dechex($from);
				$to = dechex($to);

				$stats[] = $key . '#' . $from . '#' . $to . '#0#0d0+0';
			}
			$this->statstemplate = implode(',', $stats);
		}
	}

	public function doSave()
	{
		global $member;

		$items = lang(null, 'item');
	
		if (!$this->exists())
		{
			$q = ItemTemplateTable::getInstance()->createQuery()->select('MAX(id) as max');
			$q = $q->fetchOne();
			$max = $q['max'];
			
			$flipped_items = array_flip($items);
			
			$this->id = max($max, end($flipped_items)) + rand(3, 15); //avoid any concurrency problem
		}
	
		$items[$this->id] = $this->name;
		file_put_contents('lang/' . $member->lang() . '/item' . EXT, '<?php return ' . var_export($items, true) . ';');

		$this->save();
	}
}