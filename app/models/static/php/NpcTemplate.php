<?php

/**
 * NpcTemplate
 * 
 * @package    InfiniteCMS
 * @subpackage Models
 * @author     Nami-Doc <nami.d0c.0@gmail.com>
 * @version    SVN: $Id: NpcTemplate.php 24 2010-10-22 11:46:07Z nami.d0c.0 $
 */
class NpcTemplate extends BaseNpcTemplate
{
	public function getName()
	{
		return lang($this->id, 'npc');
	}

	public function getAddMapLink()
	{
		return make_link('@glossary.npcmap.update', lang('npc.map.add'), array('id' => $this));
	}

	public function getStuffs()
	{
		if ($this->accessories == '0,0,0,0,0' || $this->accessories == '0,0,0,0')
			return array();

		/* Splitten this way :
		 * - weapon
		 * - hat
		 * - back
		 * - pet
		 * - shield
		 */
		return array_map('hexdec', explode(',', $this->accessories));
	}

	public function getSells()
	{
		if ($this->ventes == '' || $this->ventes == ',')
			return array();

		return ItemTemplateTable::getInstance()->createQuery()
			->whereIn('id', explode(',', $this->ventes))
			->execute();
	}
}