<?php

/**
 * ItemTemplateTable
 * 
 * @package    InfiniteCMS
 * @subpackage Models
 * @author     Nami-Doc <nami.d0c.0@gmail.com>
 * @version    SVN: $Id: ItemTemplateTable.php 24 2010-10-22 11:46:07Z nami.d0c.0 $
 */
class ItemTemplateTable extends RecordTable
{
	/**
	 * searches an item by it's id, it's name or it's html-escaped name
	 *
	 * @return int|false item's id or false if no item found
	 */
	public function search($id)
	{
		if (is_numeric($id)) //itemID submitted
		{
			if(lang($id, 'item', null) === null)
				return false; //item does not exists

			return $id;
		}
	
		if (false === $id = array_search($id, lang(null, 'item')))
		{
			if (false === $id = array_search(escape_html($id), lang(null, 'item')))
				return false; //item does not exists
		}

		return $id;
	}

	/**
	 * returns inventory type (stuff, usable, quest, ...) of the item type
	 *
	 * @param int $type item.type
	 */
	public function getInventoryType($type)
	{
		$fb = null;

		foreach ($this->getInventoryTypes() as $t => $types)
		{
			if ($types === true)
				$fb = $t; //fallback
			else if (in_array($type, $types))
				return $t;
		}

		return $fb;
	}

	/**
	 * returns an array with the form [(inventory type => )item categories contained, ...]
	 *
	 * @return array
	 */
	public function getInventoryTypes()
	{ //these are ItemTemplate types
		return array(
			//I'll gladly need your help !
			//Merci de m'aider si vous le pouvez !
			//102 arbal�te => ?

			//Stuff | Usable | General | Quest
			array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 16, 17, 18, 19, 20, 21, 22, 23, 81, 82, 85, 102, 113),
			array(12, 13, 27, 33, 37, 42, 43, 44, 45, 49, 69, 72, 73, 74, 75, 76, 87, 89, 100),
			true,
			array(24, 84),
		);
	}
}