<?php

class GuildEmblem
{
	static protected $generatedEmblems = array();
	protected $emblem;

	public function __construct($emblem)
	{
		$this->emblem = strtoupper($emblem);
	}

	public function __toString()
	{
		$emblemToken = rand(0, 500) + md5($this->emblem);
		$emblemCode = tag('span', array('class' => 'emblem emblem-' . $emblemToken), '');

		if (empty(self::$generatedEmblems))
		{
			include_library(LIBRARY_SWFOBJECT);
			jQ('var emblems = {}');
		}
		self::$generatedEmblems[] = $this->emblem;

		$emblem = explode(',', $this->emblem);
		//emblem's datas are in base36, so let's convert them ...
		// (if anybody have a way to do that through sprintf(), as %02x, tell me)
		$bcgSrc = str2dec($emblem[0], 36);
		$frtSrc = str2dec($emblem[2], 36);
		$bcgColor = str2dec($emblem[1], 36);
		$frtColor = str2dec($emblem[3], 36);
		jQ(sprintf('
emblems["%s"] = $(".emblem-%1$s").flash(
{
	"swf": "http://staticns.ankama.com/dofus/www/swf/pages_persos/DofusGuildes.swf",
	"expressInstaller": "http://staticns.ankama.com/global/swf/expressInstall.swf",
	"width": 30,
	"height": 30,
	"hasVersion": 10,
	"id": "logo_guilde_container_%1$s",
	"wmode": "transparent",
	"flashvars":
	{
		"bcgSrc": "%s",
		"bcgColor": "%s",
		"frtSrc": "%s",
		"frtColor": "%s",
	}
});', $emblemToken, $bcgSrc, $bcgColor, $frtSrc, $frtColor));
		return $emblemCode;
	}
}