<?php
$table = CharacterTable::getInstance();
/* @var $table CharacterTable */

$router->codeUnless(404, $charac = $table->retrieve());
/* @var $charac Character */
$router->codeUnless(404, $acc = $charac->Account); //if it's possible =°
/* @var $acc Account */

$items = $charac->getItems(); //items info
if (count($items))
	$itemsInfo = $items->process();
$evC = $charac->Events->count();
$coC = $charac->ContestParticipations->count();

echo tag('div', array('id' => 'character'), null), tag('ul', //can't use show-this here, don't ask me why ;))
//@todo fucking refactor this :D
  tag('li', tag('a', array('href' => '#general'), lang('general'))) .
  tag('li', tag('a', array('href' => '#cstats'), lang('shop._stats'))) .
  ( count($items) ? tag('li', tag('a', array('href' => '#items'), pluralize(lang('shop._items'), count($items)))) .
   ( level(LEVEL_ADMIN) && $charac->getInventoryCount() ? tag('li', tag('a', array('href' => '#inventory'), lang('inventory'))) : '' ) : '' ) .
  ( $charac->getSpellCount() ? tag('li', tag('a', array('href' => '#spells'), pluralize(ucfirst(lang('character.spell')), $charac->getSpellCount()))) : '' ) .
  ( $charac->hasJobs() ? tag('li', tag('a', array('href' => '#jobs'), pluralize(ucfirst(lang('character.job')), count($charac->getJobs())))) : '' ) .
  ( $evC || $coC ? tag('li', tag('a', array('href' => '#activity'), lang('activity'))) : '' ) .
  ( level(LEVEL_ADMIN) && $config['ENABLE_SHOP'] ? tag('li', tag('a', array('href' => '#give'), lang('character.give'))) : '' )
 ),
 tag('div', array('id' => 'general'));

echo tag('b', lang('pseudo') . ': '), $charac->name, ', ', $charac->getOnAccountOf();

echo '.', tag('br'), $charac->toString(false);

if ($charac->relatedExists('GuildMember'))
{ //show the guild info
	printf(lang('character.guild_infos'), lang('guild.rank.' . $charac->GuildMember->rank),
	 make_link($charac->GuildMember->Guild), $charac->GuildMember->Guild->lvl);
}
if (level(LEVEL_MJ) && $map = $charac->getMap()) /* @var $map Map */
	printf(lang('character.pos'), $charac->name, $charac->map, $map->getPosX(), $map->getPosY(), $charac->cell);


echo '</div>', tag('br', array('class' => 'hide-this')), //tag('h3', array('class' => 'hide-this'), lang('shop._stats')),
 tag('div', array('id' => 'cstats'), $charac->getStatsHTMLTable()), tag('br', array('class' => 'hide-this'));

if (count($items))
{
	echo tag('div', array('id' => 'items'));
	$itemCount = sprintf(lang('has'), $charac->name, count($items),
					pluralize(lang('character.item'), count($items)));

	$stuff = tag('div', array(
		'id' => 'stuff',
	 )) . make_img('stuff', EXT_PNG, array(
		'style' => 'position: absolute;',
	)); //opening of the tag ...
	$posOffset = ItemTable::getInstance()->getPosOffset();
	$usedItems = array();
	foreach ($itemsInfo as $itemInfo)
	{ //$itemInfo = [instanceof Item, stats]
		// @var $item Items 
		$item = $itemInfo[0];
		$usedItems[] = $item->guid;

		$stuff .= tag('div', array(
			'id' => 'item' . $item->template,
			'style' => 'position: absolute; ' . $posOffset[$item->pos],
		 ), make_img('items/' . $item->template, EXT_PNG,
			str_replace('"', "'", $itemInfo[1]), array(
			'class' => 'showEffects',
			'style' => array('width' => '50px', 'height' => '50px'),
			'try' => true,
		)));
	}
	echo $itemCount, $stuff, str_repeat(tag('br'), 12), '</div></div>';
	IG::registerEffectsTooltip(); //hover tooltips with item effects

	if (level(LEVEL_ADMIN) && $charac->getInventoryCount())
	{
		echo tag('div', array('id' => 'inventory')), tag('ul'); //can't use show-this here, don't ask me why ;))
		$stuff = '';
		$perLine = 11;
		$space = str_repeat('&nbsp;', 8);

		foreach ($charac->getOrderedInventory() as $type => $_items)
		{
			echo tag('li', array('href' => 'type_' . $type), lang('inventorytype.' . $type));

			$items = array();
			foreach ($_items as $item)
			{
				if (in_array($item->guid, $usedItems))
					continue;
				$items[] = $item;
			}
			unset($_items);

			//remaining cases, if you have only 13 items in your inventory
			// (and 5 cases per line), you'll have $append = 2
			$append = $perLine - (count($items) % $perLine);
			if ($append >= $perLine)
				$append = 0;
			$lines = ceil(count($items) / ( $perLine + 1 ));
			$line = 0;

			$stuff .= tag('div', array('id' => 'type_' . $type));
			$stuff .= tag('table', array('border' => 1)) . tag('tr');
			foreach ($items as $i => $item)
			{
				if (!$img = asset_path('items/' . $item->template . EXT_PNG, true))
					continue; //no image to display

				$stuff .= tag('td', tag('a', array(
					'href' => $img,
					'id' => 'item' . $item->template,
					'class' => 'showEffects image',
					'title' => $stats = str_replace('"', "'", $item->getTooltip()),
					'style' => array(
//						'border' => '1px solid black',
//						'width' => '25px',
						'height' => '40px',
					),
				 ), make_img($img, null, array('style' => array('width' => '40px', 'height' => '40px'))))
				//.$item->qua > 1 ? tag('i', $item->qua) : '&nbsp;');
				);

				if (ceil($i / $perLine) > $lines != $line + 1)
				{
					$stuff .= '</tr><tr>';
					++$line;
				}
			}
			for ($i = 0; $i < $append; ++$i)
				$stuff .= tag('td', tag('span', $space));
			$stuff .= '</tr></table></div>';
		}
		echo '</ul>', $stuff, '</div>';
	}
}

if (count($charac->getSpells()))
{ //I SEE NO REASON FOR THIS TO HAPPEN. That's why I created this if ! ofc
	$align = array(//HTML attributes
		'align' => 'center',
		'valign' => 'middle',
	);
	//spell list
	echo tag('div', array('id' => 'spells')), str_repeat(tag('br', array('class' => 'hide-this')), 2), tag('table', array('border' => '1', 'style' => 'width: 100%')), tag('thead'), tag('tr',
	   tag('td', $align + array('style' => 'width: 42%;'), tag('b', lang('character.spell'))) .
	   tag('td', $align + array('style' => 'width: 23%;'), tag('b', lang('character.spell_level'))) .
	   tag('td', $align + array('style' => 'width: 35%;'), tag('b', lang('character.spell_pos')))) .
	  '</thead>' . tag('tbody', array('style' => 'height: 550px; overflow: auto'));

	foreach ($charac->getSpells() as $spell)
	{ //build spell row
		echo tag('tr', array('data-id' => $spell[0]),
		 tag('td', make_img('spells/' . $spell[0], EXT_PNG, lang($spell[0], 'spell'), array('try' => true))) .
		 tag('td', $spell[1]) .
		 tag('td', $charac->getSpellPos($spell[2])));
	}
	echo '
		</tbody>
	</table>
</div>';
}

if ($charac->hasJobs())
{
	echo '
<div id="jobs">
	<table>';
	foreach ($charac->getJobs() as $jobs)
	{
		echo '<tr>';
		foreach ($jobs as $job)
			echo tag('td', $job == '' ? '' : make_img('jobs/' . $job[0], EXT_PNG, array('title' => lang($job[0], 'job') . ' (' . lang('lvl') . ' ' . $job[2] . ')')));
		echo '</tr>';
	}

	echo '
	</table>
</div>';
}

if ($evC || $coC)
{
	echo tag('div', array('id' => 'activity')), tag('table', array('border' => 1, 'width' => '100%')), tag('thead');

	if ($evC)
		echo tag('th', pluralize(lang('event'), $evC));
	if ($coC)
		echo tag('th', pluralize(lang('contest'), $coC));

	echo '
		</thead>
		<tbody>
			<tr>';

	if ($evC)
	{
		echo '
				<td>';
//					<ul>';
		foreach ($charac->Events as $event)
			echo $event->toParticipation($charac), '.<br />';//</li>';
		echo 
			//		</ul>
'				</td>';
	}
	if ($coC)
	{
		echo '
				<td>';
//					<ul>';
		foreach ($charac->ContestParticipations as $cp)
			echo $cp;
		echo  
					//</ul>
'				</td>';
	}

	echo '
			</tr>
		</tbody>
	</table>
</div>';
}

if (level(LEVEL_ADMIN) && $config['ENABLE_SHOP'])
{
	echo tag('br', array('class' => 'hide-this')), tag('div', array('id' => 'give'), make_form(array(
		 array('type', lang('action') . '&nbsp;', 'select', ShopItemEffectTable::getInstance()->getTypes()),
		 array('value', lang('value') . '&nbsp;'),
		 array('id', null, 'hidden', $charac->guid),
	 ), '@character.give'));
}

echo '</div>';