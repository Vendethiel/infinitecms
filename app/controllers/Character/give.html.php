<?php
if (!check_level(LEVEL_ADMIN))
	return;

$router->codeUnless(404, $character = CharacterTable::getInstance()->find($id = $router->requestVar('id', -1)));
/* @var $character Character*/

$effect = new ShopItemEffect;
$siTable = ShopItemTable::getInstance();

if (!$type = $siTable->getType($t = $router->requestVar('type', -1)))
{
	echo lang('incorrect_type');
	return;
}
$effect->type = $t;

if (!$effect->setValue($value = $router->requestVar('value')))
{ //@todo redirect to Character/give with datas ?
	echo lang('incorrect_value');
	return;
}

if (!$effect->isAvailableFor($character, $_POST))
	printf(lang('shop.empty_available_effect'), $character->name);
else if ($effect->confirm($character, $_POST))
{
	if ($effect->giveTo($character, $_POST))
		printf(lang('character.given'), $type, $effect->getValue(), make_link($character));
	else
		printf(lang('character.give_canceled'), $type, $effect->getValue(), make_link($character));
}
else
	echo $effect->getConfirmationForm($character, $_POST, array(
		array('type', null, 'hidden', $t),
		array('value', null, 'hidden', $value),
		array('id', null, 'hidden', $character->guid),
	));