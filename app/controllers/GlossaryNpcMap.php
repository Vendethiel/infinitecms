<?php
namespace Controller;

use \NpcMap;
use \NpcTemplateTable;

class GlossaryNpcMap extends Base
{
	protected function init()
	{
		$this->requireLevel(\LEVEL_ADMIN);
	}

	public function updateAction()
	{
		$this->codeUnless(404, $npc = NpcTemplateTable::getInstance()->createQuery('n')
													->leftJoin('n.Maps m')
													->where('n.id = ?', $this->getParameter('id'))
													->fetchOne());

		if (!empty($_POST))
		{
			$npcmap = new NpcMap;
			$npcmap->npcid = $npc->id;

			if ($npcmap->update($_POST)) //$save = true
				redirect($npc);
		}

																			//db fields
		return array('npc' => $npc, 'npcmap' => isset($npcmap) ? $npcmap : array());
	}

	public function deleteAction()
	{
		$this->codeUnless(404, $id = $this->getParameter('id'));
		$this->codeUnless(404, $npc = NpcTemplateTable::getInstance()->find($this->getParameter('npc')));

		$npc->unlink('Maps', array($id));
		$npc->save();

		redirect($npc);
	}
}