<?php
namespace Controller;

use \ExperienceTable;

class GlossaryExperience extends Base
{
	public function indexAction()
	{
		return array('floors' => ExperienceTable::getInstance()->createQuery()->orderBy('lvl')->execute());
	}
}