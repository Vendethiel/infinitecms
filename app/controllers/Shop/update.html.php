<?php
if (!check_level(LEVEL_ADMIN))
	return;

if (!$item = Query::create()
					->from('ShopItem i')
						->leftJoin('i.Effects e INDEXBY e.id')
					->where('i.id = ?', $router->requestVar('id', -1))
					->fetchOne())
{
	$item = new ShopItem;
	$title = lang($router->getController() . ' - create', 'title');
}

if ($sent = ( count($_POST) > 0 ))
{
	$col = $router->requestVar('col', '');
	$vals = $_POST;
	if (!$item->getTable()->hasColumn($col) || !isset($_POST['update_value']))
		$col = null;
	else
		$vals = array($col => $_POST['update_value']);
	$valid = $item->update($vals, true, $col);

	if ($col !== null)
		exit(nl2br($item[$col]));

	if ($valid)
		redirect('@shop');
}

partial('_form', array('item' => $item), PARTIAL_CONTROLLER);