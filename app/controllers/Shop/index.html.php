<?php
global $categories;
/* @var $table ItemTable */
$table = ShopItemTable::getInstance();

$categories = Query::create()
					->from('ShopCategory sc INDEXBY id')
						->where('sc.parent_id IS NULL')
					->leftJoin('sc.Children cc')
					->execute();

register_variable(compact('categories', 'table'));

if ($router->requestVar('mode') == 'search')
{ //js disabled / some errors happened
	define('TEMPLATE',  'Shop/search');
	return;
}

if (count($categories))
{
	// re-querying cause child cats are NOT going to be here
	$category = ShopCategoryTable::getInstance()->find($router->requestVar('cat')) ?: $categories->getFirst();
	list($items_dql, $allowed_cats, $search_params) = $table->getSearchResults($categories);

	//cat is not allowed and has no parent (subcats are not in allowed cats)
	if (!$category->parent_id && !in_array($category->id, $allowed_cats))
	{ //try to find a master without children
		foreach ($allowed_cats as $allowed_cat)
		{
			$category = $categories[$allowed_cat];
			if (!$category->Children->count())
				break; //we found one!
		}
	}
	register_variable(compact('allowed_cats', 'search_params', 'category'));

	global $url_ary;
	$url_ary = array_merge(array('controller' => $router->getController(), 'action' => $router->getAction()), $search_params);
	if (count($allowed_cats))
		$url_ary['cat'] = $category->id;


	if ($category !== null)
		$items_dql->andWhere('si.category_id = ?', $category->id);
	$pager = new Doctrine_Pager($items_dql, $router->requestVar('page'), $config['ITEM_LINES_BY_PAGE'] * $config['ITEMS_BY_LINE']);
	$items = $pager->execute();
	$layout = new Doctrine_Pager_Layout($pager, new Doctrine_Pager_Range_Sliding(array('chunk' => 4)), to_url($url_ary + array('page' => '')));
	$layout->setTemplate('[<a href="{%url}">{%page}</a>]');
	$layout->setSelectedTemplate('[<b>{%page}</b>]');

	if ($items->count() || count($search_params))
	{
		register_variable('items', $items);
		register_variable('pager', paginate_layout($layout));
		$i = 0; //where are we in da items foreash ?
		$items->shopDisplay();
		IG::registerEffectsTooltip();
	}
}