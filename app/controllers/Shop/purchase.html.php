<?php
if (!check_level(LEVEL_LOGGED))
	return;

$id = $router->requestVar('id', -1);
$admin = level(LEVEL_ADMIN);

$router->codeUnless(404, $shopItem = Query::create()
		->select('i.cost, e.*')
			->from('ShopItem i')
				->leftJoin('i.Effects e')
			->where('id = ?', $id)
		->fetchOne());
$return_back = '<br />' . make_link(array('controller' => 'Shop', 'action' => 'index', 'cat' => $shopItem->category_id), lang('back_to_index'));

$router->codeUnless(404, $char = $account->getMainChar());

if (!$admin)
{
	if (!$account->hasPoints($shopItem->getCost()))
	{
		printf(lang('shop.!buy_but_credit'), $shopItem->name,
		 make_link('@vote', lang('acc.vote')), make_link('@credit', lang('acc.credit.add')), $return_back);
		return;
	}

	$router->codeIf(404, $shopItem->is_hidden);
	$router->codeIf(404, $shopItem->is_vip && (empty($config['COST_VIP']) || !$account->vip));
}

if (!$shopItem->getAvailableEffectsFor($char, $_POST))
	printf(lang('shop.empty_available_effect'), $char->name);
else if ($shopItem->confirm($char, $_POST))
{
	if ($shopItem->is_lottery)
	{
		list($effect, $hasEffect) = $shopItem->giveTo($char, $_POST);
		if ($hasEffect)
			printf(lang('shop.lottery_bought'), $shopItem->name, br2nl($effect));
		else //pretty complicated name, huh
			printf(lang('shop.lottery_owned_bought' . ( $admin ? '_admin' : '' )), $shopItem->name, str_replace(tag('br'), '', $effect));

		//well that is stupid considering if shop is enabled it means we're not using cyon or fork
		$account->addPoints(-$shopItem->getCost());

		log_act('purchase', $shopItem->name);
	}
	else if ($shopItem->giveTo($char, $_POST))
	{
		$account->addPoints(-$shopItem->getCost());
		echo sprintf(lang('shop.bought'), $shopItem->name);

		log_act('purchase', $shopItem->name);
	}
	else
		printf(lang('shop.empty_buy'), $shopItem->name);
}
else
{
	if (empty($errors))
		echo sprintf(lang('shop.buying_confirm'), $shopItem->name), $shopItem->getConfirmationForm($char, $_POST);
	else
		echo render_errors();
}

echo tag('br') . $return_back;