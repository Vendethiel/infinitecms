<?php
if (!check_level(LEVEL_ADMIN))
	return;

$mode = $router->requestVar('mode', 'Item');
$router->codeUnless(404, $mode == 'Item' || $mode == 'ItemEffect');
$item = Doctrine_Core::getTable('Shop' . $mode)->find($id = $router->requestVar('id', -1));

$router->codeUnless(404, $item);

if ($mode == 'Item')
	Query::create()
		->delete('ShopItemEffect')
			->where('item_id = ?', $item->id)
		->execute();
else
	$item = $item->Item;
$item->delete();
echo lang('shop.item.deleted');

if ($mode == 'ItemEffect')
	redirect(array('controller' => $router->getController(), 'action' => 'update', 'id' => $item)); //redirect to the "edit" page