<?php
$table = ShopCategoryTable::getInstance();
$categories = $table->createQuery('c')
				->where('c.parent_id IS NULL')
				->leftJoin('c.Children cc')
				->execute();

if ($categories->count())
{
	echo '<ul>';
	foreach ($categories as $category)
	{
		echo tag('li', $category->getName() . $category->getUpdateLink() . $category->getDeleteLink());

		if ($category->Children->count())
		{
			echo '<ul>';
			foreach ($category->Children as $child)
				echo tag('li', $child->getName() . $child->getUpdateLink() . $child->getDeleteLink());
			echo '</ul>';
		}
	}
	echo '</ul>';
}

echo tag('br') . make_link(new ShopCategory);