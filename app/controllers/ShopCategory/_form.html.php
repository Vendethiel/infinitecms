<?php
echo make_form(array(
	array('name', lang('name') . tag('br'), null, $category->name),
	array('parent', lang('parent') . tag('br'), 'record', array(
		'empty' => true, 'model' => 'ShopCategory', 'where' => 'parent_id IS NULL',
	 ), $category->parent_id)
));