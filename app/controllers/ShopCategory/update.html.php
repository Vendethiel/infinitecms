<?php
$table = ShopCategoryTable::getInstance();
if (!$category = $table->find($c = intval($router->requestVar('id'))))
{
	$category = new ShopCategory;
	$title = lang($router->getController() . ' - create', 'title');
}

if (count($_POST) && $category->update($_POST))
	redirect($table);

partial('_form', array('category' => $category), PARTIAL_CONTROLLER);