<?php
if (!check_level(LEVEL_LOGGED))
	return;

if (!$config['PASS']['ENABLE'])
{
	echo lang('credit.disabled');
	return;
}

//page?
$submitted = false;
//I have to move that part ... But, where ?!
switch ($config['PASS']['TYPE'])
{
	case 'star':
	case 'starpass':
		$submitted = null != $router->requestVar('code1');
	break;

	case 'monelib':
	break;

	default:
		echo lang('credit.misconfigured');
		return;
}

$inputAttr = array();
if ($submitted)
{
	$code = '';

	//code validation
	switch ($config['PASS']['TYPE'])
	{
		case 'star':
			$code = $router->postVar('code1');
			$idp = $router->postVar('idp');
			$result = @file_get_contents('http://script.starpass.fr/check_php.php' .
			 '?ident=' . sprintf('%s;;%s', $idp, $config['PASS']['IDD']) . '&codes=' . $code);
			//Starpass uses here a totally useless (for me) "explode" ...
			// I just need "validCode?", not alot of informations ... And it's better.
			$isValidCode = $result && substr($result, 0, 3) === 'OUI';
		break;

		case 'monelib':
			require $config['PASS']['SCRIPT_PATH'];
		break;
	}

	if ($isValidCode)
	{
		//add points & count
		$key = 'POINTS_CREDIT' . (level(LEVEL_VIP) && !empty($config['COST_VIP']) ? '_VIP' : '');
		$account->addPoints($config[$key]);
		log_act('credit', $code);

		echo lang('acc.credit.credited');
	}
	else
		echo lang('acc.credit.code_invalid');
}
else
{
	echo tag('h1', ucfirst(lang('acc.credit.add')));
	//code submit
	switch ($config['PASS']['TYPE'])
	{
		case 'star':
			echo tag('div', array('id' => 'starpass_' . $config['PASS']['IDD']), '');

			//what to do with csrf token :( ?

			echo javascript_tag(replace_url('@credit.js'));

			//echo '<div style="width:380px;height:250px;font-family:Arial;font-size:11px;background-image:url(http://script.starpass.fr/images/fenetre_fond_basse.jpg);"><div style="text-align:right;padding:4px;"><a href="http://www.starpass.fr/" style="color:white;font-size:10px;text-decoration:none;">StarPass.fr - Micro paiement s&eacute;curis&eacute;</a></div><div style="margin-top:61px;margin-left:15px;color:#26637c;font-weight:bold;">Pour obtenir vos codes d\'acc&egrave;s,</div><div style="margin-left:45px;color:#ff8416;font-weight:bold;font-size:13px;">veuillez cliquer sur le drapeau de votre pays</div><div style="margin-top:16px;margin-left:40px;color:white;"><span style="font-weight:bold;">&eacute;tape 1 :</span> Votre pays - Your country</div><div style="text-align:center;margin-top:7px;"><a href="http://script.starpass.fr/numero_pays_v3.php?pays=fr&amp;id_document=' . $config['PASS']['idd'] . '" onclick="window.open(this.href,\'StarPass\',\'width=400,height=300,scrollbars=yes,resizable=yes\');return false;"><img src="http://script.starpass.fr/images/drapeaux/france.png" style="border:0px none;" alt="Micro paiement France" title="Micro paiement France" height="33" width="30"/></a> <form method="POST" action="' . replace_url('@credit') . '" style="display:inline;"><div style="margin-left:9px;margin-top:18px;color:white;"><span style="font-weight:bold;">&eacute;tape 2 : </span>Veuillez entrer votre code - Please enter your code</div><div style="text-align:center;"><input type="hidden" name="idd" value="' . $config['PASS']['idd'] . '"/><input type="hidden" name="idp" value="' . $config['PASS']['idp'] . '"/><input type="text" name="code" value="code" maxlength="8" size="4" onfocus="if (this.value.search(/code/i) >= 0) this.value=\'\';"/>' . input_csrf_token() . '<input type="submit" name="valider_code" value="OK"/></div></form></div></div>';
		break;
	}
}