<?php
$script = file_get_contents('http://script.starpass.fr/script.php?idd='. $config['PASS']['IDD'] . '&verif_en_php=1&last=1&theme=default_blue_small&datas=');

if ('error : id not found !' == trim(strtolower($script)))
{
	printf(lang('credit.not_found'), $config['PASS']['IDD']);
	exit;
}

#			$script = str_replace('</script><script type=text/javascript>', '', $script);
#			$script = str_replace("skjq(\'#starpass_109291 #sk-sms-select-operator\')", "skjq('#starpass_109291 #sk-sms-select-operator')", $script);

if (false !== $form_pos = strpos($script, $form = '<form id="sk-code-validation-form-' . $config['PASS']['IDD'] . '" method="post"'))
{
	//let's find the end of the form tag (with action="")
	$form_end = substr($script, $form_pos + strlen($form), strpos(substr($script, $form_pos + strlen($form)), '>')+1);
	//and replace it with our action + add _csrf_token field
	$script = str_replace($form_end, $c = ' id="sk-code-validation-form-' . $config['PASS']['IDD'] . '" action="' . replace_url('@credit') . '">' . input_csrf_token(), $script);
}
if (false !== $form_pos = strpos($script, $form = '<form id="sk_code_validation_' . $config['PASS']['IDD'] . '" method="post"'))
{
	//let's find the end of the form tag (with action="")
	$form_end = substr($script, $form_pos + strlen($form), strpos(substr($script, $form_pos + strlen($form)), '>')+1);
	//and replace it with our action + add _csrf_token field
	$script = str_replace($form_end, $c = ' action="' . replace_url('@credit') . '">' . input_csrf_token(), $script);
}
/*
if (false !== $form_pos = strpos($script, $form = '<form '))
{
	//the end of the form
	$form_end_pos = strpos(substr($script, $form_pos + strlen($form)), '>')+1;
	//let's find the end of the form tag (with action="")
	$form_end = substr($script, $form_pos + strlen($form), $form_end_pos);
	//and replace it with our action + add _csrf_token field
	$script = str_replace($form_end, $c = ' action="' . replace_url('@credit') . '">' . input_csrf_token(), $script);
}
*/

exit($script);