<?php
echo make_form(array(
	lang('user.points_stats') => array(
		$config['ACCOUNT_POINTS'] ? '' :
		 array('points', lang('user.points'), null, $u->points),
		array('votes', lang('user.vote'), null, $u->votes),
	),
));