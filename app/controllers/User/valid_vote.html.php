<?php
if (empty($_SESSION['user_voting']))
	exit('mdrrrr');
$_SESSION['user_voting'] = null;

if (level(LEVEL_LOGGED))
{
	//move vote URL to a config variable ? Dunno ...
	// Rpg-Paradize is the most-used Private Servers Ladder and if I change this
	// I need to change the image n stuffs ... too lazy
	if (!$account->canVote($time = '+3 hours'))
	{
		$account->User->lastVote = time();
		$account->addPoints($config['POINTS_VOTE' . (level(LEVEL_VIP) && !empty($config['COST_VIP']) ? '_VIP' : '')]);
		$account->User->votes += 1;
		log_act('vote');

		redirect($config['URL_VOTE'], 0, true);
	}
	else
		redirect(array('controller' => 'User', 'action' => vote));
}