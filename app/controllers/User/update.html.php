<?php
if (!check_level(LEVEL_ADMIN))
	return;

$id = $router->requestVar('id', $_SESSION['guid']);
$router->codeUnless(404, $acc = AccountTable::getInstance()
					->createQuery('a')
						->leftJoin('a.User u')
					->where('a.guid = ?', $id) //guid => account id
					->fetchOne());

$u = $acc->getUser();
/* @var $u User */
if (!empty($_POST))
{
	$cols = $router->requestVar('col', '');
	$vals = $_POST;
	if ($u->getTable()->hasColumn($cols) && isset($_POST['update_value']))
		$vals = array($cols => $_POST['update_value']);
	else
		$cols = array('points', 'votes');

	foreach ((array) $cols as $i => $col)
	{
		if (isset($vals[$col])) //ok I use isset() because of "0" value (level: Player, points: 0, ...)
			$u->$col = intval($vals[$col]);
		else
			$errors[$col] = sprintf(lang('must_!empty'), $col);
	}

	if (empty($errors))
		$u->save();

	if (!empty($cols) && !is_array($cols))
		exit(nl2br($u[$col])); //not if empty($errors) !

	if (empty($errors))
		redirect($acc);
}

partial('_form', array('u' => $u), PARTIAL_CONTROLLER);