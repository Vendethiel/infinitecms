<?php
$router->codeIf(404, empty($config['URL_VOTE']) || $config['URL_VOTE'] == -1);
$limit = 20;

$usersDql = Query::create()
				->from('User u')
					->innerJoin('u.Account a')
						//->leftJoin('a.Characters c')
				->orderBy('u.votes DESC')
				->limit(10);

if (!$config['LADDER_STAFF'])
	$usersDql->where('a.level = 0');


$pager = new Doctrine_Pager($usersDql, $router->requestVar('id', 1), $limit); //$config['ARTICLES_BY_PAGE'] );
$users = $pager->execute();
$layout = new Doctrine_Pager_Layout($pager, new Doctrine_Pager_Range_Sliding(array('chunk' => 4)), to_url(array('controller' => $router->getController(), 'action' => $router->getAction(), 'id' => '')));
$layout->setTemplate('[<a href="{%url}">{%page}</a>]');
$layout->setSelectedTemplate('[<b>{%page}</b>]');

foreach ($users as $user)
	$user->Account->setUser($user);

$ladder = new Ladder($users, ($layout->getPager()->getPage() - 1) * $limit);
$ladder->add('pseudo', '%%Account%%:?')
		->add('votes', 'votes:0')
		->add('character', '%%Account.MainChar%%:acc.ladder.no_character');
echo $ladder, tag('br'), paginate_layout($layout);