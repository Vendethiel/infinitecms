<?php
$router->codeIf(404, $config['URL_VOTE'] == -1);

$_SESSION['user_voting'] = true;
if (level(LEVEL_LOGGED))
{
	if ($err_code = $account->canVote($time = '+2 hours'))
	{
		if ($err_code == VOTE_ACC)
			echo lang('acc.vote.another_acc'), tag('br');

		printf(lang('acc.vote.wait_until'),
		 date('H:i', $account->User->lastVote), //last vote
		 date('H:i', strtotime($time, $account->User->lastVote)), //wait until
		 date('H:i')); //current time sprintf(lang('acc.vote.won'),
	}
	else
		printf(lang('acc.vote.will_win'),
		 $config['POINTS_VOTE' . (level(LEVEL_VIP) && !empty($config['COST_VIP']) ? '_VIP' : '')]);
}
else
{
	echo lang('acc.vote.go_login');
	redirect($config['URL_VOTE'], 10);
}

echo
 tag('br'), lang('acc.vote.be_warned'),
 '<br/><br/>',
 make_link(array('controller' => 'User', 'action' => 'valid_vote'), make_img('http://www.rpg-paradize.com/vote', EXT_GIF), false);