<?php
if (!check_level(LEVEL_ADMIN))
	return;

$router->codeUnless(404, $poll_option = PollOptionTable::getInstance()->find($id = $router->requestVar('id', -1)));
$router->codeIf(404, $poll_option->Poll->isElapsed());

$url = to_url($poll_option->Poll);
$poll_option->delete();

redirect($url);