<?php
if (!check_level(LEVEL_ADMIN))
	return;

$router->codeUnless(404, $poll = PollTable::getInstance()->find($id = $router->requestVar('id', -1)));
$router->codeIF(404, $poll->isElapsed());

$poll_option = new PollOption;
$poll_option->Poll = $poll;

if (!empty($_POST))
{
	$col = $router->requestVar('col', '');

	if ($poll_option->update($_POST))
	{
		if ($col)
			exit($poll_option[$col]);
		else
			redirect($poll);
	}
}

printf(lang('poll.option.new_for'), $poll->name); //@todo span($poll->name), bind update:form.name => update span($poll->name)
partial('_form', array('poll_option' => $poll_option), PARTIAL_CONTROLLER);