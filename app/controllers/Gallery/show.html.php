<?php
$table = GalleryTable::getInstance();
$router->codeUnless(404, $gallery = $table->find($router->requestVar('id')));
$node = $gallery->getNode();
if ($node->isRoot())
	redirect($table);

echo tag('div', array('align' => 'left')), '&gt; ' . $gallery->getBreadcrumb(), tag('br'),
 tag('h2', make_link($gallery, array(false, false, $gallery->level)));
if ($descendants = $node->getDescendants())
{
	foreach ($descendants as $descendant)
		echo make_link($descendant, array(false, true, $gallery->level - 1)), tag('br');
}
echo '</div>', tag('br'), tag('br');

$images = $gallery->Images;

if (count($images))
{
	include_library(LIBRARY_HIGHSLIDE);
	$opts = array('style' => array('width' => '110px', 'height' => '100px'));

	$i = 0;
	$count = count($images);
	foreach ($images as $image)
	{
		if (!level(LEVEL_ADMIN))
		{
			$m = $i++ % 4;
			if ($m === 0 && $i !== 1) //start a new line
				echo tag('br');
		}

		$img = getPath(true) . $image->image;
		$name = lang($image->title, null, '%%key%%');

		echo tag('a', array('class' => 'image', 'href' => $img),
		 make_img($img, null, $name, $opts)),
		 tag('div', array('class' => 'highslide-caption', 'style' => 'width: 100px;'), $name);
		
		if (level(LEVEL_ADMIN))
			echo $image->getDeleteLink() . '<br />';

		$image->free(true);
	}
}
else
	echo lang('gallery.image.empty');

if (level(LEVEL_ADMIN))
	echo tag('br'), tag('br'), make_link(array('controller' => 'GalleryImage', 'action' => 'update', 'id' => $gallery), lang('GalleryImage - create', 'title'));