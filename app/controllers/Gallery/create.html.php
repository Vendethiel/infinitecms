<?php
if (!check_level(LEVEL_ADMIN))
	return;
global $parent;

$table = GalleryTable::getInstance();
$treeObject = $table->getTree();
$gallery = $table->create();

if (!$parent = $table->find($router->requestVar('parent')))
{
	if (!$parent = $treeObject->fetchRoot())
	{
		$parent = $table->create();
		$parent->title = lang($router->getController() . ' - index', 'title');
		$parent->save();
		$treeObject->createRoot($parent);

		$ancestors = make_link($parent, array(false));
	}
}


if (empty($ancestors))
{
	if ($router->isAjax())
		$ancestors = make_link($parent, array(false));
	else
		$ancestors = $parent->getBreadcrumb(false, true) . make_link($parent, array(false));
}

echo tag('h3', tag('b', 'parent : ') . $ancestors), tag('br');

if (!empty($_POST) && $gallery->update($_POST))
	redirect($gallery);

echo make_form(array(
	array('title', lang('title'), $gallery->title),
	array('parent', null, 'hidden', $parent ? $parent->id : null),
));
define('UPDATE_SELECTOR', '#updateDialog');
jQ('pageBind(function ()
{
	$("' . UPDATE_SELECTOR . '").dialog("close");
});');