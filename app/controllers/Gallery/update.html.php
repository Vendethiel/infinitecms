<?php
if (!check_level(LEVEL_ADMIN))
	return;

$table = GalleryTable::getInstance();
$treeObject = $table->getTree();

if (!$gallery = $table->find($router->requestVar('id')))
	redirect(array('controller' => $router->getController(), 'action' => 'create'));
$node = $gallery->getNode();
if ($parent = $node->getParent())
	$parentNode = $parent->getNode();

switch ($router->requestVar('mode'))
{
	case 'up':
		$router->codeUnless(404, $node->hasPrevSibling());
		$node->moveAsPrevSiblingOf($node->getPrevSibling());
	break;
	case 'down':
		$router->codeUnless(404, $node->hasNextSibling());
		$node->moveAsNextSiblingOf($node->getNextSibling());
	break;
	case 'back':
		$router->codeIf(404, $node->isRoot());
		$router->codeIf(404, $parentNode->isRoot());
		$node->moveAsLastChildOf($parentNode->getParent());
	break;
}

redirect($table);