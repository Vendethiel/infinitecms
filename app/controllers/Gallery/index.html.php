<?php
$table = GalleryTable::getInstance();
$treeObject = $table->getTree();

if ($tree = $treeObject->fetchTree())
{
	foreach ($tree as $record)
		echo make_link($record, array(true, true)), tag('br');
}
else
	echo lang('gallery.empty');

if (level(LEVEL_ADMIN))
{
	echo tag('br'), tag('br'), make_link($table->create()),
	 tag('div', array(
		'id' => 'updateDialog',
		'class' => 'dialog-base',
		'style' => 'display: none;',
		'title' => lang('Gallery - create', 'title'),
		'align' => 'center',
	), '');
}