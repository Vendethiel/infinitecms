<?php
if (!check_level(LEVEL_ADMIN))
	return;

$table = ContestTable::getInstance();
if (!$contest = $table->retrieve())
{ //allow editing ?
	$contest = new Contest;
	$title = lang($router->getController() . ' - create', 'title');
}

if (!empty($_POST) && $contest->update($_POST))
	redirect($contest);

partial('_form', array('contest' => $contest), PARTIAL_CONTROLLER);