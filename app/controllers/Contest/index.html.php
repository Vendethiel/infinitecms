<?php
$contests = Query::create()
				->from('Contest')
					->orderBy('ended')
				->execute();

if ($contests->count())
{
	echo '<ul id="contests">';
	foreach ($contests as $contest)
		echo tag('li', '&bull; ' . make_link($contest) . ( $contest->ended ? ' (' . lang('ended') . ')' : '' ));
	echo '</ul>';
}
else
	echo lang('contest.empty');

if (level(LEVEL_ADMIN))
	echo tag('br') . tag('br') . make_link(new Contest);