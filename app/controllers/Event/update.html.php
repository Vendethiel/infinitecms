<?php
if (!check_level(LEVEL_LOGGED))
	return;

if ($event = EventTable::getInstance()->find($id = $router->requestVar('id')))
{	
	if (!check_level(LEVEL_ADMIN))
		return;
	if ($event->isElapsed())
	{
		echo lang('event.elapsed');
		return;
	}
	$event->refreshDatePassed(); //for teh next time.
}
else
	$event = new Event;

if (( $mainChar = $account->getMainChar() ) && $mainChar->isGM())
	$guildId = $mainChar->GuildMember->guild;
else if (level(LEVEL_ADMIN))
	$guildId = -1;
else
{
	define('HTTP_CODE', 404);
	return;
}

if (!empty($_POST) && $event->update($_POST) && $headers)
	redirect(array('controller' => $router->getController(), 'action' => 'index',
	 'year' => substr($event->period, 0, 4), 'month' => substr($event->period, 5, 2)));

partial('_form', array('event' => $event, 'guildId' => $guildId), PARTIAL_CONTROLLER);