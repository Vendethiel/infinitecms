<?php
if (!check_level(LEVEL_ADMIN))
	return;

$juror = new ContestJuror;
$title = lang($router->getController() . ' - create', 'title');

$router->codeUnless(404, $contest = ContestTable::getInstance()->retrieve());
$router->codeIf(404, $contest->level == LEVEL_LOGGED);

if (count($_POST))
{
	$juror->contest_id = $contest->id;

	if ($juror->update($_POST))
		redirect($contest);
}
if (empty($_POST) || count($errors))
	partial('_form', array('contest' => $contest), PARTIAL_CONTROLLER);