<?php
namespace Controller;

use \Doctrine_Pager;
use \Doctrine_Pager_Layout;
use \Doctrine_Pager_Range_Sliding;
use \Query;
use \SpellTable;

class GlossarySpell extends Base
{
	public function indexAction()
	{
		$p = intval($this->getParameter('page', 0));


		$spellsDql = Query::create()
						->from('Spell');

		if (( $search = urldecode($this->getParameter('search')) ) && $search != ' ')
			$spellsDql->where('LOWER(nom) LIKE ?', '%' . strtolower($search) . '%');
		else
			$search = ' ';

		/* @var $spellsDql Query */
		$pager = new Doctrine_Pager($spellsDql, $p, 20);
		$spells = $pager->execute();

		$link = to_url(array('controller' => 'GlossarySpell', 'action' => 'index', 'search' => $search, 'page' => ''));
		$layout = new Doctrine_Pager_Layout($pager, new Doctrine_Pager_Range_Sliding(array('chunk' => 4)), $link);
		$layout->setTemplate('[<a href="{%url}" class="link">{%page}</a>]');
		$layout->setSelectedTemplate('[<b>{%page}</b>]');

		return array('spells' => $spells, 'pager' => paginate_layout($layout), 'search' => trim($search));
	}

	public function showAction()
	{
		$this->codeUnless(404, $spell = SpellTable::getInstance()->find($this->getParameter('id')));
		return array('spell' => $spell);
	}
}