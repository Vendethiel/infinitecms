<?php
$table = StaffRoleTable::getInstance();
$staffMembers = Query::create()
				->from('Account a')
					->leftJoin('a.StaffRoles sr')
					->leftJoin('a.User u')
				->where('a.level > 0')
				->execute();

register_variable('staff_members', $staffMembers);