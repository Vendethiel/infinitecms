<?php
$router->codeUnless(404, $news = NewsTable::getInstance()
										->createQuery('n')
											->leftJoin('n.Comments c')
												->leftJoin('c.Author u') //User
											->where('n.id = ?', $router->requestVar('id'))
										->orderBy('c.created_at DESC')
										->fetchOne());

if ($router->requestVar('mode') == 'embed')
	define('UPDATE_SELECTOR', '#comment-' . $news->id);

register_variable('news', $news);