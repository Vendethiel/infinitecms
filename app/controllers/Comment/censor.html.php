<?php
if (!check_level(LEVEL_MODO))
	return;

$router->codeUnless(404, $com = CommentTable::getInstance()->find($id = $router->requestVar('id', -1)));

/* @var $com Commentaire */
$com->delete();

redirect(array(
	'controller' => 'News',
	'action' => 'show',
	'id' => $com->News,
));