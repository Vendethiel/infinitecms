<?php
//@todo merge with update (?)
if (!check_level(LEVEL_LOGGED))
	return;

$router->codeUnless(404, $news = NewsTable::getInstance()->find($id = $router->requestVar('id', -1)));
$router->codeUnless(404, $account->User->canComment($news));

/* @var $news News */
$back_msg = tag('br') . make_link($news, lang('back_to_news'));

if (!( $comment = $router->requestVar('comment') ))
{
	echo lang('news.com.need_text') . $back_msg;
	return;
}

$com = new Comment;
$com->News = $news;
$com->Author = $account->User;
$com->content = level(LEVEL_ADMIN) ? $comment : escape_html($comment);
$com->save();

if ($router->requestVar('index'))
{
	define('UPDATE_SELECTOR', '#comment-' . $news->id);
	redirect(array('controller' => 'Comment', 'action' => 'index', 'id' => $news, 'mode' => 'embed'));
}
else
	redirect($news);