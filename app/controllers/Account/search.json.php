<?php
if (!check_level(LEVEL_ADMIN))
	return;

exit((string) new Datagrid\Datatable\Handler(AccountTable::getInstance()));