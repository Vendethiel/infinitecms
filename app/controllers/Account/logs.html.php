<?php
if (!check_level(\LEVEL_ADMIN))
	return;

$router->codeUnless(404, $acc = AccountTable::getInstance()->find($id = $router->requestVar('id')));

$p = intval($router->requestVar('page', 0));

$logsDql = Query::create()
		->from('Log')
		->where('account_id = ?', $id)
		->orderBy('at');

if ($type = $router->requestVar('type'))
	$logsDql = $logsDql->where('type = ?', $type);

/* @var $logsDql Query */
$pager = new Doctrine_Pager($logsDql, $p, 5);
$logs = $pager->execute();

$link = to_url(array('controller' => $router->getController(), 'action' => $router->getAction(), 'id' => $id, 'type' => $type, 'page' => ''));
$layout = new Doctrine_Pager_Layout($pager, new Doctrine_Pager_Range_Sliding(array('chunk' => 4)), $link);
$layout->setTemplate('[<a href="{%url}" class="link">{%page}</a>]');
$layout->setSelectedTemplate('[<b>{%page}</b>]');

register_variable(array('logs' => $logs, 'pager' => paginate_layout($layout), 'type' => $type, 'acc' => $acc));