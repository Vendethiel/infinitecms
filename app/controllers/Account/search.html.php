<?php
if (!check_level(LEVEL_ADMIN))
	return;

$dg = new Datagrid(array(), array('table_id' => 'accounts')); //, array('url' => $url));
$dg
	->bind('datatable', array('dataTable' => array(
		'bProcessing' => true,
		'bServerSide' => true,
		'sAjaxSource' => to_url(array('controller' => $router->getController(), 'action' => $router->getAction(), 'ext' => 'json'), false),
	 )));
$paths = AccountTable::getInstance()->getColumnOptions();
foreach ($paths['path'] as $path)
	call_user_func_array(array($dg, 'add'), $path);
echo $dg; #, paginate_layout($layout);




/*
$ip = $router->requestVar('ip');
$pseudo = $router->requestVar('pseudo');
if (!$pseudo && !$ip)
{
	echo make_form(array(
		array('pseudo', 'Pseudo', null, ),
		array('ip', 'IP', null, ),
	));
	return;
}

$table = AccountTable::getInstance();
$q = $table->createQuery();

if ($pseudo)
	$q->andWhere('pseudo LIKE ?', '%' . str_replace('*', '%', $pseudo) . '%');
if (trim($ip))
	$q->andWhere('lastip ?', $ip);
if ('ASC' != $dir = $router->requestVar('sort_dir'))
	$dir = $_REQUEST['sort_dir'] = 'DESC';
$cols = array('pseudo', 'level');
if (!in_array($by = $router->requestVar('sort_by'), $cols))
	$by = $_REQUEST['sort_by'] = reset($cols);

$q->orderBy($by . ' ' . $dir);

*/

#$accounts = Query::create()->from();

/*
$url = array('controller' => $router->getController(), 'action' => $router->getAction(), 'pseudo' => $pseudo ?: null, 'ip' => $ip ?: null, 'sort_by' => $by, 'sort_dir' => $dir, 'id' => '');
$pager = new Doctrine_Pager($q, $router->requestVar('id', 0), 2);
$accounts = $pager->execute();
$layout = new Doctrine_Pager_Layout($pager, new Doctrine_Pager_Range_Sliding(array('chunk' => 4)), to_url($url));
$layout->setTemplate('[<a href="{%url}" class="link">{%page}</a>]');
$layout->setSelectedTemplate('[<b>{%page}</b>]');*/
#$accounts = $q->execute();