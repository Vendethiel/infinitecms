<?php
if (!check_level(LEVEL_LOGGED))
	return;
if (!level(LEVEL_ADMIN) && !check_level(LEVEL_VIP, REQUIRE_NOT))
	return;

switch ($router->requestVar('mode'))
{
	case 'valid':
		if (!check_level(LEVEL_ADMIN, REQUIRE_NOT))
			return;
		$router->codeIf(404, empty($config['COST_VIP']));

		if (!$account->hasPoints($config['COST_VIP']))
		{
			printf(lang('acc.vip.not_enough'), $config['COST_VIP'], $account->getPoints());
			return;
		}

		$account->addPoints(-$config['COST_VIP']);
		$account->vip = 1;
		echo lang('acc.vip!');
	break;

	default:
//	case 'explain':
		$f = 'lang/fr/texts/vip.txt';
		$file = file_get_contents($f);

		echo new Editable\Page($file, function ($content) use ($f) {
			file_put_contents($f, $content);
		}, array('{COST}' => $config['COST_VIP']));
		
		echo make_link('@vip!', $title);
}