<?php
$q = empty($acc->question) ? 'DELETE?' : $acc->question;
$r = empty($acc->reponse) ? 'DELETE' : $acc->reponse;
$email = empty($acc->email) ? '@hotmail.fr' : $acc->email;

$newl = tag('br');
$auth = !$acc->exists() || level(LEVEL_ADMIN);

$fields = array(
	( level(LEVEL_ADMIN) || $acc->isNew() ? '' :
		array('actual_password', lang('acc.register.password.actual') . $newl, 'password')
	),
	$newl,
	lang('acc.register.infos') => array(
		( $auth ? array('account', ( $auth ? $newl : '' ) . lang('account') . $newl, null, $acc->account) : array() ),
		array('pass', $newl . lang('acc.register.password') . $newl, 'password'),
		array('pseudo', $newl . lang('pseudo') . $newl, null, $acc->pseudo),
		array('email', $newl . lang('acc.register.mail') . $newl, null, $email),
	),
	lang('acc.register.prefs') => array(
		array('question', lang('acc.register.question') . $newl, null, $q),
		array('reponse', $newl . lang('acc.register.answer') . $newl, null, $r),
		array('guid', null, 'hidden', $acc->guid),
	),
	lang('acc.register.adv') => ( level(LEVEL_ADMIN) ? array(
		array('level', $newl . lang('level') . $newl, 'select', Member::getLevels(), $acc->level)
	) : array() )
);

if (!$acc->exists())
	array_unshift($fields, array('tos', lang('acc.register.accept_tos'), 'checkbox'));

echo make_form($fields);