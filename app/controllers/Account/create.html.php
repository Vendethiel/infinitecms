<?php
if (!check_level(LEVEL_GUEST)) //yeah, level guest is required. SO WAT ?
	return;

if (!$config['ENABLE_REG'])
{
	echo tag('b', lang('acc.register_disabled'));
	return;
}

$acc = new Account;
if (count($_POST) && $acc->update($_POST, true))
	echo lang('acc.account_created');
else
{
	if (!$config['ALLOW_MULTI'] && $acc->getTable()->findOneByLastip(ip2long($member->getIp())))
	{
		echo lang('acc.register.error.already_created_acc');
		return;
	}

	partial('_form', array('acc' => $acc), PARTIAL_CONTROLLER);
}
