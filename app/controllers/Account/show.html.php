<?php
if (isset($acc))
	$fromInclude = true; //from include (i.e. the modal box)
else
{
	$fromInclude = false;

	$router->codeUnless(404, $acc = AccountTable::getInstance()->createQuery('a')
					->leftJoin('a.User u')
					->leftJoin('a.Characters c')
					->where('a.guid = ?', $router->requestVar('id', -1))
			->fetchOne());
}


/* @var $acc Account */
$nl = tag('br');
if (!empty($acc->lastconnectiondate))
{
	$lastCnx = explode('~', $acc->lastconnectiondate);
	$lastCnx = sprintf(lang('created'), ( count($lastCnx) > 1 ) ? sprintf('%d-%d-%d %d:%d:00', $lastCnx[0], $lastCnx[1], $lastCnx[2], $lastCnx[3], $lastCnx[4]) : $lastCnx[0]);
}
else
	$lastCnx = null;
global $account; //:(
$isFriend = false;
$isFriendR = false;
if (level(LEVEL_LOGGED))
{ //explanations below
	$isFriend = $account->hasFriend($acc);
	$isFriendR = $acc->hasFriend($account);
}

/*
 * cases:
 *  1) $friend && $friendR	=> both are friends
 *  2) $friend				=> me is friend but not viewed
 *  3) $friendR				=> viewed is friend but not viewer
 *  4) none					=> both are not friends
 *
 * (ok, I admit that this one is pretty ugly ...
 */
$isAdmin = level(LEVEL_ADMIN);

$info = tag('b', lang('pseudo') . ': ') . $acc->getName() . $nl .
		$acc->getLevel() . $nl .
		( $isAdmin || $acc == $account ? ($acc ? $acc->getUser()->getPoints() : 0) . $nl : '' ) . //points if admin?
		( $acc->banned ? sprintf(lang('acc.is_banned'), $acc->pseudo) . $nl : '' ) . //banned?
		( $isFriend && $isFriendR ? sprintf(lang('acc.in_ur_friends_&'), $acc->getName()) : //parse
		 ( $isFriend ? sprintf(lang('acc.in_ur_friends'), $acc->getName()) : ( $isFriendR ? //friend
		  sprintf(lang('acc.in_ur_friends_!'), $acc->getName()) : '' ) ) ) . $nl . //(or not)
		( empty($lastCnx) ? '' : tag('b', lang('acc.last_cnx') . ': ') . $lastCnx ) . //lastConnection
		( $isAdmin ? $nl . make_link($acc->getUser()->getURL(), sprintf(lang('acc.edit'), $acc->pseudo), array(), array('class' => 'link')) . '&nbsp;&bull;&nbsp;' . make_link(array(
			'controller' => 'Account',
			'action' => 'update',
			'id' => $acc,
		), sprintf(lang('acc.edit_infos'), $acc->pseudo), array(), array('class' => 'link')) : '' );

if ($fromInclude)
{
	return $info . $nl .
	 make_link('@account.show', lang('more') . ' ...', array('id' => $acc));
}
else
{ //full page
	echo tag('div', $info);
	if ($isAdmin)
		echo tag('br'), make_link('@stats.site', lang('stats'), array('account_id' => $acc)), ' &bull; ',
		 make_link('@account.logs', lang('log'), array('id' => $acc));
	// parse friends
	$friends = '';
	foreach ($acc->getFriends() as $f)
	{
		/* @var $f Account */
		if ($f->guid != $acc->guid)
		{ //not himself his friend (yeah, sometimes it happens ... sing with me ... "I'm a poor lonesone dofus player" ...)
			$friends .= tag('li', '&bull;&nbsp;' . make_link($f));
		}
	}

	if ($friends !== '')
		echo tag('h2', pluralize(lang('acc.friend'), count($acc->getFriends()))) .
		 tag('ul', $friends);

	if ($acc->relatedExists('Characters') && $acc->Characters->count())
	{ //show characters
		echo tag('h2', ucfirst(pluralize(lang('character'), $acc->Characters->count()))),
		$acc->Characters->charactersDisplay(array(//the default params
			'controller' => $router->getController(),
			'action' => $router->getAction(),
			'id' => $acc,
		));
	}
} //end else $fromInclude