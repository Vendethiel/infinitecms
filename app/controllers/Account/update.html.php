<?php
if (!level(LEVEL_LOGGED))
	redirect(array('controller' => $router->getController(), 'action' => 'create'));

if (level(LEVEL_ADMIN) && !empty($_SESSION['referer']) && $_SESSION['referer'] === 'list')
{
	unset($_SESSION['referer']);
	define('FROM_SEARCH', true);
}
else
	define('FROM_SEARCH', false);

$sent = count($_POST);
$req_id = null; //request_id
if (level(LEVEL_ADMIN) && ( $req_id = $router->requestVar('id') ) !== null && $req_id !== $_SESSION['guid'])
	$acc = AccountTable::getInstance()->find($req_id);
else
	$acc = $account;

/* @var $acc Account */
if (!$acc)
{
	if ($req_id !== null)
		echo lang('acc.does_not_exists');
	else
		define('HTTP_CODE', 404);

	return;
}

if ($sent)
{
	$col = $router->requestVar('col', array());
	if ($col !== array() && !in_array($col, $acc->getColumns()) && !check_level(LEVEL_ADMIN))
		return; //the column does not exists? check_level at last => other conds shortcircuits
	$vals = $_POST;

	if (!in_array($col, $acc->getColumns()) || !isset($_POST['update_value']))
		$col = true; //update all
	else
		$vals = array($col => $_POST['update_value']); //only a col


	if (!level(LEVEL_ADMIN))
	{
		$actual = isset($vals['actual_password']) ? $vals['actual_password'] : null;
		if ($actual !== $acc->pass)
			$errors[] = lang('acc.register.password.not_matching');
	}

	if (empty($errors))
		$acc->update($vals, true, $col);


	if (!empty($col) && !is_array($col) && $col !== true)
	{
		$val = nl2br($acc[$col]);
		if ($col === 'level') //use propertyPath ?
			$val = $acc->getLevel();
		exit($val);
	}
	if (empty($errors))
		redirect($acc);
}

partial('_form', array('acc' => $acc), PARTIAL_CONTROLLER);