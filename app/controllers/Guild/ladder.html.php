<?php
$limit = 20;

$guildsDql = Query::create()
				->from('Guild g')
					//using innerJoin to having a gm
					->innerJoin('g.Members gm WITH gm.rank = 1')
						->leftJoin('gm.Character c')
				->orderBy('g.xp DESC');
$pager = new Doctrine_Pager($guildsDql, $router->requestVar('id', 1), $limit); //$config['ARTICLES_BY_PAGE'] );
$guilds = $pager->execute();
$layout = new Doctrine_Pager_Layout($pager, new Doctrine_Pager_Range_Sliding(array('chunk' => 4)), to_url(array('controller' => $router->getController(), 'action' => $router->getAction(), 'id' => '')));
$layout->setTemplate('[<a href="{%url}">{%page}</a>]');
$layout->setSelectedTemplate('[<b>{%page}</b>]');

if ($guilds->count())
{
	$ladder = new Ladder($guilds, ($layout->getPager()->getPage() - 1) * $limit);
	$ladder->add('name', '%%:?')
			->add('level', 'lvl', array('align' => 'left'))
			->add('guild.rank.1', '%%leader%%:guild.no_leader'); //@todo Guild::getLeader()
	echo $ladder, tag('br'), paginate_layout($layout);
}
else
	echo lang('guild.empty');