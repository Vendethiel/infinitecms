<?php
$unreads = $account->User->getUnreadPM();

$where = array();
if (empty($_REQUEST['participants']))
	$accounts = array(); //usin $_REQUEST koz I may 
else
	$accounts = $account->filterReverseFriends(explode(',', $_REQUEST['participants']),
	 function ($record) { return array('id' => $record, 'name' => $record->getName()); }); //add name for tokenInput
$accounts[] = array('id' => $account->guid); //we don't need name, this won't go in the tokenInput

//I guess there's a little explanation needed here.
//I can't do pmt.id IN (... pmtr.author_id IN ()). Why ? because it would retrieve all PM of ANY member
// (unless I use a subquery with a count)
//So instead, we use that to do multiple AND. Where each participant is present !
$threads = Query::create() //it'll be heavy (wrote at the very start)
				->from('PrivateMessageThread pmt')
				->leftJoin('pmt.Receivers pmr INDEXBY pmr.account_id')
						->leftJoin('pmr.Account pmra') //Private Message Receiver's Account
					->leftJoin('pmt.Answers pma')
						->leftJoin('pma.Author pmau') //Private Message Answer's User
							->leftJoin('pmau.Account pmaua') //Private Message Answer's User's Account
				->orderBy('pma.created_at DESC');
if (!empty($_REQUEST['favorites']))
	$threads->where('pmt.id IN (SELECT record_id FROM Favorite WHERE account_id = ? AND record_class = ?)',
	 array($account->guid, 'PrivateMessageThread'));

$_REQUEST['participants'] = '';
foreach ($accounts as $n => $acc)
{
	$i = $acc['id'];

	$threads->andWhere("pmt.id IN (SELECT pmtr{$n}.thread_id FROM PrivateMessageThreadReceiver pmtr{$n} WHERE pmtr{$n}.account_id = ? AND pmtr{$n}.present = 1)", $i);
	if ($i == $account->guid)
		unset($accounts[$n]);
	else
		$_REQUEST['participants'] .= $i . ',';
}
if ($_REQUEST['participants']) //security !
	$_REQUEST['participants'] = substr($_REQUEST['participants'], 0, -1);

$threads = $threads->execute();

echo make_link('@pm.create', lang('PrivateMessage - create', 'title'));

/*
if ($_REQUEST['participants'] || count($threads))
{ //check if we have any PM
 //or if we filtered (we may encounter no filtered result, but we still need to display the form)
	if ($accounts)
		$pre = 'prePopulate:' . json_encode($accounts) . ', ';
	else
		$pre = '';

	jQ('$(function () { $("#form_participants").tokenInput("' . getPath() . 'Account/reverseFriends.json", {' . $pre . 'theme: "facebook", preventDuplicates: true}); });');
	echo make_form(array(
		array('participants', lang('with')),
	));
}*/

if (count($threads))
{
	echo '
<ul>';
	$url = array('controller' => $router->getController(), 'action' => 'show');
	foreach ($threads as $thread)
	{
		$receivers = array();
		foreach ($thread->Receivers as $i => $receiver)
		{
			if ($receiver->account_id != $account->guid)
				$receivers[] = make_link($receiver);
		}
		$fAnswer = $thread->Answers->getFirst(); //fAnswer
		$new = $unreads->contains($thread->id) ? tag('b', tag('u', '!')) . '&nbsp;' : '';
		$_url = $url + array('id' => $thread);
		if ($thread->Receivers[$account->guid]->next_page != 0)
			$_url += array('page' => $thread->Receivers[$account->guid]->next_page);

		echo tag('li', sprintf(lang('pm.info'), $new . make_link($_url, escape_html($thread->title)), '<i>' . implode('</i>, <i>', $receivers) . '</i>') .
		 sprintf(lang('pm.last_answer_by_on'), make_link($fAnswer->Author->Account), $fAnswer->created_at));
	}
	echo '</ul>';
}
else
	echo tag('br'), lang('pm.empty');