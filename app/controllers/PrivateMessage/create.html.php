<?php
if (!empty($_POST))
{
	//might be tricky to update_attributes-ize
	$receivers = $router->postVar('receivers');
	if (empty($receivers))
		$errors[] = sprintf(lang('must_!empty'), 'receivers');
	else
	{
		if (( $receiversIds = $account->filterReverseFriends(explode(',', $receivers)) ) == array())
			$errors[] = sprintf(lang('must_!empty'), 'receivers');
		else
			$receiversIds[] = $account->guid;
	}

	$title = $router->postVar('title');
	if (empty($title))
		$errors[] = sprintf(lang('must_!empty'), 'title');

	$msg = $router->postVar('message');
	if (empty($msg))
		$errors[] = sprintf(lang('must_!empty'), 'content');


	if (empty($errors))
	{
		$thread = new PrivateMessageThread();
		$thread->title = $title;

		$reveiversId = array_unique($receiversIds);
		foreach ($receiversIds as $id)
		{
			$receiver = new PrivateMessageThreadReceiver;
			$receiver->next_page = $id == $account->guid ? 0 : 1;
			$receiver->User = UserTable::getInstance()->fromGuid($id);
			$thread->Receivers[] = $receiver;
		}

		$answer = new PrivateMessageAnswer();
		$answer->Author = $account->User;
		$answer->message = $msg;

		$thread->Answers[] = $answer;
		$thread->save();

		redirect($thread);
	}
}

include_library(LIBRARY_TOKENINPUT);

echo make_link('@pm', lang('pm.back')), tag('br'), tag('br'),
 make_form(array(
	array('receivers', lang('pm.receivers') . tag('br')),
	array('title', lang('title') . tag('br')),
	array('message', lang('rate.msg') . tag('br'), 'textarea'),
));