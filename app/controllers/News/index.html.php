<?php
$p = intval($router->requestVar('id', 0));
$newsDql = Query::create()
				->from('News n')
					->leftJoin('n.Author u')
						->leftJoin('u.Account a')
					->leftJoin('n.Comments c')
				->orderBy('n.created_at DESC');

/* @var $newsDql Query */
$pager = new Doctrine_Pager($newsDql, $p, $config['ARTICLES_BY_PAGE']);
//can not be cached because of doctrine record's complexity
$news = $pager->execute();
$layout = new Doctrine_Pager_Layout($pager, new Doctrine_Pager_Range_Sliding(array('chunk' => 4)), to_url(array('controller' => $router->getController(), 'action' => $router->getAction(), 'id' => '')));
$layout->setTemplate('[<a href="{%url}" class="link">{%page}</a>]');
$layout->setSelectedTemplate('[<b>{%page}</b>]');

register_variable('news', $news);
register_variable('pager', paginate_layout($layout));