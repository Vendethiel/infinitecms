<?php
defined('FROM_INCLUDE') || define('FROM_INCLUDE', false);
$code = make_form(array(
	array('title', lang('title') . tag('br'), null, $news['title']),
	array('content', lang('content') . tag('br'), 'textarea', $news['content']),
), $news);

if (FROM_INCLUDE)
	return $code;
else
	echo $code;