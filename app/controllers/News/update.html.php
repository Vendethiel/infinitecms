<?php
if (!check_level(LEVEL_ADMIN))
	return;

if (!($news = NewsTable::getInstance()->find($id = $router->requestVar('id', -1))))
{
	$title = lang($router->getController() . ' - create', 'title');
	$news = new News;
}
/* @var $news News */

if (!empty($_POST))
{
	$col = $router->requestVar('col', '');
	$vals = $_POST;
	if (!$news->getTable()->hasColumn($col) || empty($_POST['update_value']))
		$col = null;
	else
		$vals = array($col => $_POST['update_value']);
	$valid = $news->update($vals, true, $col);
	if (!empty($col) && !is_array($col))
		exit(nl2br($news[$col]));

	if ($valid)
		redirect($news);
}

partial('_form', array('news' => $news), PARTIAL_CONTROLLER);