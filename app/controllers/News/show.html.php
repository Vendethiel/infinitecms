<?php
$router->codeUnless(404, $id = intval($router->requestVar('id')));
$router->codeUnless(404, $news = NewsTable::getInstance()
						->createQuery('n')
						->where('n.id = ?', $id)
						->innerJoin('n.Author u')
							->innerJoin('u.Account a')
						->leftJoin('n.Comments c')
							->leftJoin('c.Author au')
								->leftJoin('au.Account ac')
						->orderBy('c.created_at DESC')
						->fetchOne());
register_variable('news', $news);

if (level(LEVEL_ADMIN))
{
	$url = to_url(array(//URL for editInPlace of the title of the news
		'controller' => $router->getController(),
		'action' => 'update',
		'col' => 'title',
		'output' => 0,
		'id' => $news,
	), false);

	jQ('
$("#newsTitle").editInPlace(
{
	url: "' . $url . '",
} );');
}