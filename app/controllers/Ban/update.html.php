<?php
if ($pseudo = $router->postVar('pseudo'))
{
	$router->codeUnless(404, $acc = AccountTable::getInstance()->findOneByPseudo($pseudo));
	$router->codeIf(301, $acc->banned); //already banned !
	$acc->banned = true;
	$acc->save();
}
else if ($ip = $router->postVar('ip'))
{
	$router->codeUnless(404, filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4));
	$router->codeIf(404, BanIpTable::getInstance()->findOne($ip));
	$router->codeIf(404, $ip == '127.0.0.1');

	$ban = new BanIp;
	$ban->ip = $ip;
	$ban->save();

	Cache::delete('ip_banned');
}
redirect('@ban');