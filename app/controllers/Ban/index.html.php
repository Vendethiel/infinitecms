<?php
echo tag('div', array('id' => 'bans')),
 tag('ul',
  tag('li', tag('a', array('href' => '#accounts'), pluralize(lang('acc'), 2))) .
  tag('li', tag('a', array('href' => '#ips'), 'IPs'))),
 tag('div', array('id' => 'accounts'));

$accounts = Query::create()
				->from('Account a')
					->leftJoin('a.Characters c')
					->leftJoin('a.User u')
				->where('banned = ?', true)
				->execute();

if (count($accounts))
{
	echo tag('ul');
	foreach ($accounts as $acc)
	{
		echo tag('li', make_link($acc) . ' - ' . $acc->getUnbanLink());
	}
	echo '</ul>';
}
else
	echo lang('ban.account.empty'), tag('br');

echo tag('br'), make_form(array(array('pseudo', lang('pseudo'))), '@ban!', array('submit_text' => lang('ban.ban')));

echo '</div>', tag('div', array('id' => 'ips'));

$ips = BanIpTable::getInstance()->findAll(Doctrine_Core::HYDRATE_NONE);
if (count($ips))
{
	echo '<ul>';
	foreach ($ips as $ip)
		echo tag('li', $ip[0] . ' - ' . make_link(array('controller' => $router->getController(), 'action' => 'delete', 'ip' => $ip[0]), lang('ban.unban')));
	echo '</ul>';
}
else
	echo lang('ban.ips.empty'), tag('br');

echo tag('br'), make_form(array(array('ip', 'IP')), '@ban!', array('submit_text' => lang('ban.ban'))),
 make_link(array('controller' => $router->getController(), 'action' => 'delete', 'ip_cache' => 1), lang('ban.ips.cc')),
 '</div></div>';