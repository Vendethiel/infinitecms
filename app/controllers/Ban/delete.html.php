<?php
if ($id = $router->requestVar('id'))
{
	$router->codeUnless(404, $acc = AccountTable::getInstance()->find($id));
	$router->codeUnless(301, $acc->banned);

	$acc->banned = false;
	$acc->save();
}
else if ($ip = $router->requestVar('ip'))
{
//	$router->codeUnless(404, filter_var($ip, FILTER_VALIDATE_IP)); //not needed, you can anyway try to delete an IP which is not one. ya know waddamine
	$router->codeUnless(404, Query::create()
								->delete()
								->from('BanIp')
								->where('ip = ?', $ip)
								->execute());

	Cache::delete('ip_banned');
}
else if ($router->requestVar('ip_cache'))
	Cache::delete('ip_banned');

redirect('@ban');