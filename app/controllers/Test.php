<?php
namespace Controller;

class Test extends Base
{
	protected function init()
	{
		$this->addAroundFilter(array($this, 'e'));
	}
	//must return updated content OR null OR array
	public function e($e) { if (is_string($e)) return $e . '!!'; else return $e;  }

	//returns string : display "as-is"
	public function indexAction()
	{
		return 'aahahehe';
	}

	//returns array : tries to load Test/show(.ext), ie assets/default/html/Test/show.html.php
	public function showAction()
	{
		return array(
			'name' => 'test',
		);
	}

	public function errorAction()
	{
		$this->codeIf(404, true);
	}

	public function permsAction()
	{
		$this->requireLevel(\LEVEL_ADMIN);
	}

	//returns null : no output
	public function doNothingAction()
	{
		return null;
	}
}