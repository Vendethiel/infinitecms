<?php
if (!check_level(LEVEL_ADMIN))
	return;

$table = GalleryImageTable::getInstance();
$router->codeUnless(404, $screen = $table->find($router->requestVar('id')));
$screen->delete(); //does not delete the file (!)
redirect(GalleryTable::getInstance());