<?php
if (!check_level(LEVEL_ADMIN))
	return;

$title = lang($router->getController() . ' - create', 'title');
$upload_path = 'app/uploads/';
$types = array('image/gif', 'image/pjpeg', 'image/jpeg', 'image/png');
$exts = array(EXT_GIF, EXT_JPG, EXT_PNG);
$size = 2000000;

$router->codeUnless(404, $gallery = GalleryTable::getInstance()->find($router->requestVar('id')));
echo tag('h3', make_link($gallery, array(false)));

if (!empty($_POST) && isset($_FILES['image']))
{
	$errors = array();

	/*
	$upl = new FileUploader($upload_path);
	$upl->setMaxSize($size);
	$upl->addAllowedTypes($types);
	$upl->addAllowedExts($exts);
	$path = $upl->validate($_FILES['image']);
	*/
	$name = strtolower($_FILES['image']['name']);

	if ($_FILES['image']['error'])
	{
		switch ($_FILES['image']['error'])
		{
			case UPLOAD_ERR_INI_SIZE:
				$errors[] = 'more than ini-defined size';
			break;
			case UPLOAD_ERR_FORM_SIZE:
				$errors[] = 'more than form-defined size';
			break;
			case UPLOAD_ERR_PARTIAL:
				$errors[] = 'file upload interrupted';
			break;
			case UPLOAD_ERR_NO_FILE:
				$errors[] = 'file looks empty';
			break;
		}
	}
	else if (!in_array($_FILES['image']['type'], $types))
		$errors[] = lang('bad file type' . escape_html($_FILES['image']['type']));
	else if ($_FILES['image']['size'] > $size)
		$errors[] = sprintf('Must not be more than %s, is %s', filesize_format($size), filesize_format($_FILES['image']['size']));
	else
	{
		$allowed_ext = false;
		foreach ($exts as $ext)
		{
			if (rstrpos($name, $ext) == 0)
			{
				$allowed_ext = true;
				break;
			}
		}

		if ($allowed_ext)
		{
			$filename = $name;
			do
			{
				$filename = rand(1, 50) . '_' . $filename;
			}
			while (file_exists($path = $upload_path . $filename));
		}
		else
			$errors[] = lang('bad_ext');
	}

	$image = new GalleryImage;
	$image->Gallery = $gallery;

	if (empty($_POST['title']))
		$errors[] = sprintf(lang('must_!empty'), lang('title'));
	else
		$image->title = $_POST['title'];

	if (empty($errors))
	{
		//$upl->process($_FILES['image'], $path);
		move_uploaded_file($_FILES['image']['tmp_name'], $path);
		$image->image = $path; //use asset_path() here ?
		$image->save();
		redirect($gallery);
	}
}

echo make_form(array(
	array('title', lang('title') . tag('br'), null, $router->postVar('title')),
	array('image', tag('br') . lang('image') . '<br />', 'file'),
));