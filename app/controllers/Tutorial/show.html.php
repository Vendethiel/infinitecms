<?php
$router->codeUnless(404, $tutorial = TutorialTable::getInstance()->find($router->requestVar('id')));
if (!in_array($lang = $router->requestVar('lang'), $config['LANGS']))
	redirect(array('controller' => $router->getController(), 'action' => $router->getAction(),
	 'id' => $tutorial, 'lang' => LANG));


echo tag('h1', $tutorial->getName($lang));
if (count($config['LANGS']) > 1)
	echo lang('langs'), ' : ', make_link($tutorial, array($lang, false)), tag('br');

$translation = $tutorial->Translation[$lang];

echo new Editable\Page($translation->content, function ($content) use ($translation, $lang) {
	$translation->content = $content;
	$translation->save();
}, array('link' => $tutorial->getUpdateURI($lang)));