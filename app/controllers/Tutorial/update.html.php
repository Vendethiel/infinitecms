<?php
if (!check_level(LEVEL_ADMIN))
	return;

if (!in_array($lang = $router->requestVar('lang'), $config['LANGS']))
{
	if ($tutorial = TutorialTable::getInstance()->find($router->requestVar('id')))
	{
		echo tag('b', lang('explain_translations')), tag('ul');
		foreach ($config['LANGS'] as $l)
		{
			echo tag('li');
			if ($tutorial->Translation->contains($l))
				echo $l;
			else
				echo make_link(array('controller' => $router->getController(), 'action' => $router->getAction(),
			 'id' => $tutorial, 'lang' => $l), $l);
			echo '</li>';
		}
		echo '</ul>';
		return;
	}
	else
	{
		$tutorial = new Tutorial;
		$tutorial->save();
		redirect(array('controller' => $router->getController(), 'action' => $router->getAction(),
		 'id' => $tutorial, 'lang' => LANG));
	}
}

//only allow this to edit title ? (which must not contain html ... or only basic HMTL ...)
//if (!$tutorial = TutorialTable::getInstance()->find($router->requestVar('id')))
$router->codeUnless(404, $tutorial = TutorialTable::getInstance()->find($router->requestVar('id')));
$title = lang($router->getController() . ' - create', 'title');

if (count($_POST))
{
	if (empty($_POST['title']))
		$errors[] = sprintf(lang('must_!empty'), lang('title'));
	else //@todo add a way to make title editable ?
		$tutorial->Translation[$lang]->title = $_POST['title'];

	if (empty($tutorial->Translation[$lang]->content))
		$tutorial->Translation[$lang]->content = 'dummy !';

	if (empty($errors))
	{
		$tutorial->save();
		redirect($tutorial->Translation[$lang]);
	}
}

echo make_form(array(
	array('title', lang('title') . ' (' . $lang . ')', null, $tutorial->Translation[$lang]->title),
	array('lang', null, 'hidden', $lang),
));