<?php
if (!check_level(LEVEL_ADMIN))
	return;
$table = TutorialTable::getInstance();
$router->codeUnless(404, $tutorial = $table->find($router->requestVar('id')));
$tutorial->delete();
redirect($table);