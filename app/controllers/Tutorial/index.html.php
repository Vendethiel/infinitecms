<?php
$tutorials = TutorialTable::getInstance()->findAll();
$avTutorials = array();
foreach ($tutorials as $tutorial)
{
	if ($tutorial->getAvailableTranslations())
		$avTutorials[] = $tutorial;
}

if (count($avTutorials))
{
	echo '<ul id="tutorials">';
	foreach ($avTutorials as $tutorial)
		echo tag('li', '&bull; ' . make_link($tutorial));
	echo '</ul>';
}
else
	echo lang('tutorial.empty');

if (level(LEVEL_ADMIN))
	echo tag('br'), make_link(new Tutorial);