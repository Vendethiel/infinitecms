<?php
namespace Controller;

use \MonsterTable;
use \LootTable;

class GlossaryLoot extends Base
{
	protected function init()
	{
		$this->requireLevel(\LEVEL_ADMIN);
	}

	public function updateAction()
	{
		$this->codeUnless(404, $monster = MonsterTable::getInstance()->createQuery('m')
													->leftJoin('m.Loots l')
													->where('m.id = ?', $this->getParameter('id'))
													->fetchOne());

		if (!empty($_POST))
		{
			$loot = LootTable::getInstance()->create();
			$monster->Loots[] = $loot;

			if ($loot->update($_POST)) //$save = true
				redirect($monster);
		}

																			//db fields
		return array('monster' => $monster, 'loot' => isset($loot) ? $loot : array('seuil' => 100, 'max' => 3, 'taux' => '50'));
	}

	public function deleteAction()
	{
		$this->codeUnless(404, $loot = LootTable::getInstance()->find($this->getParameter('id')));
		$this->codeUnless(404, $monster = MonsterTable::getInstance()->find($loot->mob));

		$loot->delete();
		redirect($monster);
	}
}