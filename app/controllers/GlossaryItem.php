<?php
namespace Controller;

use \Doctrine_Pager;
use \Doctrine_Pager_Layout;
use \Doctrine_Pager_Range_Sliding;
use \ItemTemplateTable;
use \Query;

class GlossaryItem extends Base
{
	public function indexAction()
	{
		return array('categories' => $this->getCategories());
	}

	public function categoryAction()
	{
		$p = intval($this->getParameter('page', 0));
		$this->codeUnless(404, $name = lang($id = $this->getParameter('id'), 'itemcategory', null));


		$itemsDql = Query::create()
						->from('ItemTemplate')
						->orderBy('level')
						->where('type = ?', $id);

		if (( $search = urldecode($this->getParameter('search')) ) && $search != ' ')
			$itemsDql->andWhere('name LIKE ?', '%' . $search . '%');
		else
			$search = ' ';

		/* @var $itemsDql Query */
		$pager = new Doctrine_Pager($itemsDql, $p, 20);
		$items = $pager->execute();

		$link = to_url(array('controller' => 'GlossaryItem', 'action' => 'category', 'search' => $search, 'id' => $this->getParameter('id'), 'page' => ''));
		$layout = new Doctrine_Pager_Layout($pager, new Doctrine_Pager_Range_Sliding(array('chunk' => 4)), $link);
		$layout->setTemplate('[<a href="{%url}" class="link">{%page}</a>]');
		$layout->setSelectedTemplate('[<b>{%page}</b>]');

		return array('name' => $name, 'items' => $items, 'pager' => paginate_layout($layout), 'search' => $search);
	}

	public function showAction()
	{
		$table = ItemTemplateTable::getInstance();
		$this->codeUnless(404, $template = $table
													->createQuery('it')
													->leftJoin('it.Craft c')
													->leftJoin('it.LootSources ls')
														->leftJoin('ls.Monster m')
													->where('it.id = ?', $this->getParameter('id'))
													->fetchOne());

		return array('template' => $template, 'item_template_table' => $table);
	}

	protected function updateAction()
	{
		$this->requireLevel(\LEVEL_ADMIN);
		$table = ItemTemplateTable::getInstance();

		if (!$template = $table->find($this->getParameter('id')))
		{
			global $title;
			$title = lang('GlossaryItem - create', 'title');
			$template = $table->create();
		}

		if (!empty($_POST))
		{
			if ($template->update($_POST))
				redirect($template);
		}

		return array('template' => $template);
	}

	protected function getCategories()
	{
		return lang(null, 'itemcategory');
	}
}