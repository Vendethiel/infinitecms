<?php
namespace Controller;

use \Doctrine_Pager;
use \Doctrine_Pager_Layout;
use \Doctrine_Pager_Range_Sliding;
use \Query;
use \MonsterTable;

class GlossaryMonster extends Base
{
	public function indexAction()
	{
		$p = intval($this->getParameter('page', 0));


		$monstersDql = Query::create()
						->from('Monster');

		if (( $search = urldecode($this->getParameter('search')) ) && $search != ' ')
			$monstersDql->where('LOWER(name) LIKE ?', '%' . strtolower($search) . '%');
		else
			$search = ' ';

		/* @var $monstersDql Query */
		$pager = new Doctrine_Pager($monstersDql, $p, 20);
		$monsters = $pager->execute();

		$link = to_url(array('controller' => 'GlossaryMonster', 'action' => 'index', 'search' => $search, 'page' => ''));
		$layout = new Doctrine_Pager_Layout($pager, new Doctrine_Pager_Range_Sliding(array('chunk' => 4)), $link);
		$layout->setTemplate('[<a href="{%url}" class="link">{%page}</a>]');
		$layout->setSelectedTemplate('[<b>{%page}</b>]');

		return array('monsters' => $monsters, 'pager' => paginate_layout($layout), 'search' => trim($search));
	}

	public function showAction()
	{
		$this->codeUnless(404, $monster = MonsterTable::getInstance()
												->createQuery('m')
													->leftJoin('m.Loots l')
														->leftJoin('l.ItemTemplate it')
												->where('m.id = ?', $this->getParameter('id'))
												->fetchOne());

		return array('monster' => $monster);
	}

	protected function updateAction()
	{
		$this->requireLevel(\LEVEL_ADMIN);
		$table = MonsterTable::getInstance();

		if (!$monster = $table->find($this->getParameter('id')))
		{
			global $title;
			$title = lang('GlossaryMonster - create', 'title');
			$monster = $table->create();
		}

		if (!empty($_POST) && $monster->update($_POST))
			redirect($monster);

		return array('monster' => $monster);
	}
}