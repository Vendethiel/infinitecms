<?php
if (!check_level(LEVEL_ADMIN))
	return;

$id = $router->requestVar('id', -1);
$router->codeUnless(404, $review = ReviewTable::getInstance()->find($id));

$review->delete();
redirect($review->getTable());