<?php
printf('
<div class="post review" id="review%d">
	<div class="content">
		<div class="infos">
			<div class="autre">%s %s | %s. %s</div>
		</div>
		<div align="center" class="cont">
			&nbsp;&nbsp;%s
		</div>
	</div>
</div>',
 $review->id,
 level(LEVEL_ADMIN) ? tag('div', array('class' => 'title'), make_link('@guestbook.delete', lang('act.delete'), array('id' => $review)))  : '',
 sprintf(lang('by'), $review->relatedExists('Author') && $review->Author->relatedExists('Account') ? make_link($review->Author->Account) : '' ),
 sprintf(lang('created'), $review['created_at']),
 $review->note && false /*temp disabled to remove jq.ui.slider*/ ? lang('rate.note') . ' : ' . $review->note : '',

 $review->comment);