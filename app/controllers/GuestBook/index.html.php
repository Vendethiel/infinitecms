<?php
if ($config['RATES_BY_PAGE'] == -1)
	return;

$reviewsDql = Query::create()
				->from( 'Review r' )
					->leftJoin('r.Author u')
						->leftJoin('u.Account a')
				->orderBy('r.created_at DESC');
/* @var $newsDql Query */
$pager = new Doctrine_Pager($reviewsDql, $router->requestVar('id', 0), $config['RATES_BY_PAGE']);
$reviews = $pager->execute();
$layout = new Doctrine_Pager_Layout( $pager, new Doctrine_Pager_Range_Sliding(array('chunk' => 4)), to_url(array('controller' => $router->getController(), 'action' =>  $router->getAction(), 'id' => '')));
$layout->setTemplate('[<a href="{%url}" class="link">{%page}</a>]');
$layout->setSelectedTemplate('[<b>{%page}</b>]');
if ($reviews->count())
{
	foreach ($reviews as $review)
		partial('_show', array('review' => $review), PARTIAL_CONTROLLER);
	echo paginate_layout($layout);
}
else
	echo lang('rate.empty');

if (level(LEVEL_LOGGED) && $account->User->canReview())
	echo tag('br'), make_link(new Review, lang('rate.create'));