<?php
if (!check_level(LEVEL_LOGGED) || $config['RATES_BY_PAGE'] == -1)
	return;

/* @var $account Account */
if (!$account->User->canReview())
	return;

$review = new Review;
if ($_POST)
{
	$review->Author = $account->User; //set up the author

	if ($review->update($_POST))
		redirect($review->getTable());
}

partial('_form', array('review' => $review), PARTIAL_CONTROLLER);