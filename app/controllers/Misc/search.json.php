<?php
$exact = false; //exact search ? you may want to enable that to speed up search perfs
	//note : in any way, this only matches the start to keep using db indexes

$router->codeUnless(404, $term = $router->requestVar('id'));

$records = array(
	'accounts' => Query::create()
					->from('Account')
					->where('pseudo LIKE ?', $exact ? $term : "$term%")
						->andWhere('banned IS null or banned = 0')
					->fetchArray(),
	'characters' => Query::create()
					->from('Character')
					->where('name LIKE ?', $exact ? $term : "$term%")
					->fetchArray(),
	'guilds' => Query::create()
					->from('Guild')
					->where('name LIKE ?', $exact ? $term : "$term%")
					->fetchArray(),
);

$accounts = $characters = $guilds = array();

foreach ($records['accounts'] as $acc)
{
	$accounts[] = array(
		'id' => $acc['guid'],
		'name' => $acc['pseudo'],
		'online' => level(LEVEL_ADMIN) || $acc['level'] == 0 ? $acc['logged'] : null,
	);
}

foreach ($records['characters'] as $character)
{
	$characters[] = array(
		'id' => $character['guid'],
		'name' => $character['name'],
		'level' => $character['level'],
		'icon' => IG::getBreedIcon(IG::getBreed($character['class']), $character['sexe']),
	);
}

foreach ($records['guilds'] as $guild)
{
	#guilds.push guild{id, name, level}
	$guilds[] = array(
		'id' => $guild['id'],
		'name' => $guild['name'],
		'level' => $guild['level'],
	);
}

exit(json_encode(compact('accounts', 'characters', 'guilds', 'term')));