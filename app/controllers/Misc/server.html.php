<?php
$vars = array(
	'{server.name}' => $config['SERVER_NAME'],
	'{server.corp}' => $config['SERVER_CORP'],
);
$f = 'lang/fr/texts/server.txt';
$file = file_get_contents($f);

echo new Editable\Page($file, function ($content) use ($f) { file_put_contents($f, $content); }, $vars);