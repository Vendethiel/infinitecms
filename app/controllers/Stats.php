<?php
namespace Controller;

use \Query;
use \AccountTable;
use \IG;

class Stats extends Base
{
	public function gameAction()
	{
		global $config;
		$this->codeUnless(404, $config['STATS']);
		define('SKIP_STATS', true);
		define('UPDATE_SELECTOR', '#stats');

		$counts = array(
			'guilded' => 0,

			'breed' => array(),
			'gender' => array(),

			'noBreed' => 0,
			'noGender' => 0,

			'cLevel' => 0,
			'gLevel' => 0,

			'kamas' => 0,
		);

		$characters = Query::create()
							->from('Character c')
								->leftJoin('c.GuildMember gm')
							->fetchArray();
		$genders = IG::getGenders();
		$breeds = IG::getBreeds();
		$cGuilded = 0;
		$counts['characters'] = count($characters);
		foreach ($characters as &$character)
		{
			if (isset($breeds[$character['class']]))
			{
				if (isset($counts['breed'][$character['class']]))
					++$counts['breed'][$character['class']];
				else
					$counts['breed'][$character['class']] = 1;
			}
			else
				++$counts['noBreed'];

			if (isset($genders[$character['sexe']]))
			{
				if (isset($counts['gender'][$character['sexe']]))
					++$counts['gender'][$character['sexe']];
				else
					$counts['gender'][$character['sexe']] = 1;
			}
			else
				++$counts['noGender'];

			$counts['cLevel'] += $character['level'];
			$counts['kamas'] += $character['kamas'];

			/**
			*foreach (explode(';', $character['jobs']) as $job)
			*{
			*	$jobId = substr($job, 0, strpos($job, ','));
			*	if (isset($counts['jobs'][$job]))
			*		++$counts['jobs'][$job];
			*	else
			*		$counts['jobs'][$job] = 1;
			*}
			*/

			if (isset($character['GuildMember']))
				++$counts['guilded'];

			unset($character);
		}
		unset($characters);

		$guilds = Query::create()
						->from('Guild')
						->fetchArray();
		$counts['guilds'] = count($guilds);
		foreach ($guilds as &$guild)
		{
			$counts['gLevel'] += $guild['lvl'];
			unset($guild);
		}
		unset($guilds);

		$accounts = Query::create()
						->select('COUNT(*) as size')
							->from('Account')
						->fetchOneArray();
		$counts['accounts'] = $accounts['size'];

		$items = Query::create()
						->select('COUNT(*) as size')
							->from('Item')
						->fetchOneArray();
		$counts['items'] = $items['size'];

		if ($counts['characters'])
		{
			$counts['avgCLevel'] = $counts['cLevel'] / $counts['characters'];
			$counts['avgKamas'] = $counts['kamas'] / $counts['characters'];
		}

		return array('counts' => $counts);
	}

	public function siteAction()
	{
		$this->requireLevel(\LEVEL_ADMIN);

		if ($account_id = $this->getParameter('account_id'))
		{
			if (!$acc = AccountTable::getInstance()->find($account_id))
				$account_id = null;
		}

		if (($month = (int) $this->getParameter('month'))
		 && ($year = (int) $this->getParameter('year')))
		{
			$maxYear = date('Y');
		}
		else
		{
			$month = date('n');
			$maxYear = $year = date('Y');
		}
		$minYear = $year - 5;

		$date_start = mktime(0, 0, 0, $month, 1, $year); //h min s, mo d y
		$date_end = strtotime('+1 month', $date_start);


		$points = Query::create()
					->from('Log')
					->where('at BETWEEN ? AND ?', array($date_start, $date_end))
						->andWhere('type = "vote" or type = "credit"');
		if ($account_id)
			$points->andWhere('account_id = ?', $account_id);
		$points = $points->fetchArray();

		$registrations = Query::create()
					->from('Log')
					->where('at BETWEEN ? AND ?', array($date_start, $date_end))
						->andWhere('type = "registration"');
		if ($account_id)
			$registrations->andWhere('account_id = ?', $account_id);
		$registrations = $registrations->fetchArray();

		$purchases = Query::create()
					->from('Log')
					->orderBy('at')
					->where('at BETWEEN ? AND ?', array($date_start, $date_end))
						->andWhere('type = "purchase"');
		if ($account_id)
			$purchases->andWhere('account_id = ?', $account_id);
		$purchases = $purchases->fetchArray();

		$months = array();
		foreach (range(1, 12) as $i)
			$months[$i] = lang('month.full.' . $i, 'calendar');

		$month_selector = make_form(array(
			array('month', null, 'select', $months, $month),
			array('year', null, 'select', array_combine(range($minYear, $maxYear), range($minYear, $maxYear)), $year),
			array('account_id', null, 'hidden', $account_id),
		), '#', array('sep_inputs' => ' ', 'method' => 'GET', 'add' => array('class' => 'inline')));


		return array('points' => $points, 'registrations' => $registrations, 'purchases' => $purchases,
		 'month_selector' => $month_selector, 'acc' => $account_id ? $acc : null);
	}
}