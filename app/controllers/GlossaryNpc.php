<?php
namespace Controller;

use \Doctrine_Pager;
use \Doctrine_Pager_Layout;
use \Doctrine_Pager_Range_Sliding;
use \Query;
use \NpcTemplateTable;

class GlossaryNpc extends Base
{
	public function indexAction()
	{
		$p = intval($this->getParameter('page', 0));

		$npcsDql = Query::create()
						->from('NpcTemplate');

		/* @var $npcsDql Query */
		$pager = new Doctrine_Pager($npcsDql, $p, 20);
		$npcs = $pager->execute();

		$link = to_url(array('controller' => 'GlossaryNpc', 'action' => 'index', 'page' => ''));
		$layout = new Doctrine_Pager_Layout($pager, new Doctrine_Pager_Range_Sliding(array('chunk' => 4)), $link);
		$layout->setTemplate('[<a href="{%url}" class="link">{%page}</a>]');
		$layout->setSelectedTemplate('[<b>{%page}</b>]');

		return array('npcs' => $npcs, 'pager' => paginate_layout($layout));
	}

	public function showAction()
	{
		$this->codeUnless(404, $npc = NpcTemplateTable::getInstance()
													->createQuery('n')
														->leftJoin('n.Maps m')
													->where('n.id = ?', $this->getParameter('id'))
													->fetchOne());

		return array('npc' => $npc);
	}
}