<?php
$header = $router->requestVar('header', 1);
$page = $router->requestVar('page');

$polls = Query::create()
			->from('Poll p INDEXBY p.id')
				->leftJoin('p.Options o')
					->leftJoin('o.Polleds u'); //User

if (!level(LEVEL_ADMIN))
{ //PLAEZ DO NOT ABUSE. Don't put OVER NINE THOUSANDS POLLS in the same daterange. kthx.
	$polls->where('NOW() BETWEEN p.date_start AND p.date_end'); //Doctrine_Expression does not work. I won't seek why

	$lastElapsedPoll = Query::create()
								->from('Poll p')
									->leftJoin('p.Options o')
								->where('p.date_end < NOW()')
								->limit(1)
								->fetchOne();
}
if (!empty($lastElapsedPoll))
	$polls->add($lastElapsedPoll);

$polls = $polls
			->orderBy('p.date_end DESC')
			->offset($page * $config['POLLS_BY_PAGE'])
			->limit($config['POLLS_BY_PAGE'])
			->execute();

if ($polls->count())
{
	if ($header)
		echo tag('div', array('id' => 'polls'));

	foreach ($polls as $poll)
	{ /* @var $poll Poll */
		if (!$poll->Options->count() && !level(LEVEL_ADMIN))
			continue;
		partial('_show', array('poll' => $poll), PARTIAL_CONTROLLER);
		echo '<hr />';
	}

	if ($header)
	echo '</div>';
}
else
{
	if ($header)
		echo tag('b', lang('poll.empty'));
	else
		echo '0';
}

if (!$header)
	exit;

if (level(LEVEL_ADMIN))
	echo make_link(new Poll, lang('poll.new'));