<?php
if (!check_level(LEVEL_ADMIN))
	return;

if (!$poll = PollTable::getInstance()->find($id = $router->requestVar('id')))
	$poll = new Poll;

if (!empty($_POST))
{
	$col = $router->requestVar('col', '');
	$vals = $_POST;
	if (!$poll->getTable()->hasColumn($col) || empty($_POST['update_value']))
		$col = array();
	else //replace $vals
		$vals = array($col => $_POST['update_value']);
	$valid = $poll->update($vals, true, $col);
	if (!empty($col) && !is_array($col))
	{
		$val = $poll[$col];
		if (substr($col, 0, 4) == 'date')
			$val = date_to_picker($val);
		exit(nl2br($val));
	}
	if ($valid)
		redirect($poll);
}

partial('_form', array('poll' => $poll), PARTIAL_CONTROLLER);