<?php
//define('DEBUG', false);
/**
 * InfiniteCMS
 *
 * @author Nami-D0C
 * @version $Id: index.php 56 2011-01-16 19:39:32Z nami.d0c.0 $
 *
 * J'ai mis de nombreuses heures à coder ce CMS et son CMF, le résultat est bon à mes yeux,
 *  bien documenté, conçu agréablement (changement de template rapide, gestion multi-langues, ...)
 *  et agréable à lire (fichiers assez concis, structure).
 * La plupart des CMS distribués sont sans boutique, mais je ne distribue que des choses
 *  fonctionnelles, pratiques, innovantes et surtout flexibles.
 * (écrit en 2009 !)
 *
 * Please respect my work
 * Veuillez respecter mon travail
 *
 *
 * Nami-D0C
 */
//$mem is used for my debug (shows where are memory leaks)
define('START_TIME', microtime(true));
function bench() { return '&nbsp;'.str_pad(memory_get_usage(), 8, "0", STR_PAD_LEFT) . '~' . (microtime(true) - START_TIME); }

define('FRONTEND_FILE', basename(__FILE__));
$mem = bench() . ': Avant tout <br/>';
define('ROOT', './');
define('EXT', strrchr(__FILE__, '.'));
define('DEV', false);
ini_set('include_path', implode(PATH_SEPARATOR, array(
	get_include_path(),
	ROOT . 'lib',
)));
$mem .= bench() . ': Avant fonctions.php<br />';
//load API + bootstrap


try
{
	require 'lib/bootstrap' . EXT;
}
catch (Exception $e)
{
	if (DEBUG)
	{
		if ($e->getMessage())
			exit('problems with bootstrapping : ' . $e->getMessage());
		else
			exit('problems with bootstrapping (' . get_class($e) . '), after <pre>' . $e->getTraceAsString());
	}
	else
		exit('Problems during the init. Please contact the server admin.');
}

$output = true;
$infos = $router->getInfos();

//Additional vars
$title = lang($infos['controller'] . ' - ' . $infos['action'], 'title');
$mem .= bench() . ': Avant controller+action<br />';

$controller = $router->getControllerInstance();
$front = new Controller\Front($controller);
$front->process();

__shutdown();
if (DEBUG && !$router->isAjax() && $output && $router->getExt() == $router->getDefault('ext'))
	echo '<-- DEBUG MODE ONLY --><br/>' . $mem . bench() . ': Fin<br /><-- /DEBUG MODE ONLY -->';