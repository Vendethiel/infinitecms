<?php
namespace Test;

function humanize($str) { return strtolower(preg_replace('`([A-Z])`', ' $1', $str)); }
function trim_file_path($p1, $p2)
{
	$path1 = explode(DIRECTORY_SEPARATOR, $p1);
	$path2 = explode(DIRECTORY_SEPARATOR, $p2);

	foreach ($path1 as $i => $p)
	{
		if ($path2[$i] != $p)
			return implode(DIRECTORY_SEPARATOR, array_slice($path1, $i));
	}
}

class Base
{
	protected $testName;

	final public function run()
	{
		$reflectionClass = new \ReflectionClass($c = get_called_class());
		$classMethods = $reflectionClass->getMethods(\ReflectionMethod::IS_PUBLIC);
		$methods = array();
		foreach ($classMethods as $method)
		{
			if ('test' == substr($method->name, 0, 4))
				$methods[] = $method;
		}

		$this->success = $this->fail = 0;

		echo "\t#$c\n";

		$this->runMethods($methods);

		echo "\n$this->success test(s) succeeded.\n$this->fail test(s) failed.\n\n";
		if ($this->fail)
			exit; //let the "tester" see the error(s) :)
	}

	final private function runMethods($methods)
	{
		$this->beforeRun();
		foreach ($methods as $method)
		{
			$this->beforeTest();
			$testName = substr(humanize($method->name), strlen('test '));
			$shortName = str_pad(substr($testName, 0, 40) . '', 40, ' ', STR_PAD_RIGHT);
			echo "Test $shortName";
			try {
				$method->invoke($this);
			} catch (Exception\Failed $e) {
				echo "\n\tError : " . $e->getMessage();
			}
			$this->afterTest();
			echo "\n";
		}
		$this->afterRun();
	}

	protected function beforeRun()
	{ }
	protected function afterRun()
	{ }

	protected function beforeTest()
	{ }
	protected function afterTest()
	{ }


	protected function success()
	{
		echo '.';
		++$this->success;
	}
	protected function fail($message = '')
	{
		++$this->fail;

		$backtrace = debug_backtrace();
		$fn = $backtrace[2]['function'];
		$fn = substr($fn, strlen('assert'));
		$fn = trim(strtolower(preg_replace('`([A-Z])`', ' $0', $fn)));

		throw new Exception\Failed('Test "' . $fn . '" failed on ' .
		 $backtrace[3]['class'] . // '(' . trim_file_path($backtrace[1]['file'], __FILE__) . ') ' .
		 ':' . $backtrace[2]['line'] .
		 ($message ? ' - ' . $message : ''));
	}

	private function truthy($v, $message = '')
	{
		if ($v)
			$this->success();
		else
			$this->fail($message);
	}
	private function falsy($v, $message = '')
	{
		if ($v)
			$this->fail($message);
		else
			$this->success();
	}

	protected function assertTrue($v)
	{
		$this->truthy($v);
	}
	protected function assertFalse($v)
	{
		$this->falsy($v);
	}
	protected function assertArrayHasKey($k, $a)
	{
		$this->truthy(array_key_exists($k, $a)); //, var_export($a) . " doesn't contains '$k'");
	}
	protected function assertArrayNotHasKey($k, $a)
	{
		$this->falsy(array_key_exists($k, $a)); //, var_export($a) . " doesn't contains '$k'");
	}
	protected function assertContains($v1, $v2)
	{
		$this->truthy(false !== strpos($v1, $v2)); //, "'$v1' must contain '$v2'");
	}
	protected function assertCount($c, $a)
	{
		$this->truthy($c == $ac = count($a)); //, "'\$a''s size must be $c, but is $ac");
	}
	protected function assertEmpty($v)
	{
		$this->truthy($v);
	}
	protected function assertEquals($v1, $v2)
	{
		$this->truthy($v1 == $v2); //, "'$v1' must equal '$v2'");
	}
	protected function assertNotEquals($v1, $v2)
	{
		$this->truthy($v1 !== $v2); //, "'$v1' must not equal '$v2'");
	}
	protected function assertIntGreaterThan($gt, $i)
	{
		$this->truthy($i > $gt); //, "'$i' must be greater than '$gt'");
	}
	protected function assertIntGreaterOrEqualTo($gte, $i)
	{
		$this->truthy($i >= $gte); //, "'$i' must be greater or equal to '$gt'");
	}
	protected function assertIntLesserThan($lt, $i)
	{
		$this->truthy($i < $lt); //, "'$i' must be lesser than '$lt'");
	}
	protected function assertIntLesserOrEqualTo($lt, $i)
	{
		$this->truthy($i <= $lt); //, "'$i' must be lesser or equal to '$lt'");
	}
	protected function assertNull($v)
	{
		$this->truthy(null === $v); //, gettype($v) . ' isn\'t null');
	}
	protected function assertNotNull($v)
	{
		$this->assetFalse(null === $v); //, '$v is null');
	}
	protected function assertRegexp($r, $s)
	{
		$this->truthy(preg_match($r, $a)); //, "'$a' doesn't match '$r'");
	}
	protected function assertSame($v1, $v2)
	{
		$this->truthy($v1 === $v2); //, "'$v1' must be exactly the same as '$v2'");
	}
	protected function assertNotSame($v1, $vs2)
	{
		$this->falsy($v1 === $v2); //, "'$v1' must not be exactly the same as '$v2'");
	}
	protected function assertSameLength($v1, $v2)
	{
		$this->truthy(strlen($v1) == strlen($v2)); //, "len('$v1') must equal len('$v2')");
	}
	protected function assertNotSameLength($v1, $v2)
	{
		$this->falsy(strlen($v1) == strlen($v2)); //, "len('$v1') must equal len('$v2')");
	}
	protected function assertStrStartsWith($s, $str)
	{
		$this->truthy(substr($str, 0, strlen($s)) == $s);
	}
	protected function assertStrNotStartsWith($s, $str)
	{
		$this->falsy(substr($str, 0, strlen($s)) == $s);
	}
	protected function assertStrEndsWith($e, $str)
	{
		$this->truthy(substr($str, -strlen($s)) == $s);
	}
	protected function assertStrNotEndsWith($e, $str)
	{
		$this->falsy(substr($str, -strlen($s)) == $s);
	}
}