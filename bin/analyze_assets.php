<?php
$theme = isset($argv[1]) ? $argv[1] : 'yuuki';
$filename = 'cache/dependencies_' . $theme . '.js.txt';
if (!file_exists($filename))
	exit("Theme $theme not available");

$files = array();
$_fs = explode("\n", file_get_contents($filename));
foreach ($_fs as $_f)
{
	$_f = explode(':', $_f);
	$files[]= $_f[0];
}

$_m = array();
$sizes = array();
foreach ($files as $file)
{
	$sizes[] = $e= filesize($file);
	$_m[$e] = $file;
}

sort($sizes);

foreach ($sizes as $size)
{
	$f_size = number_format($size, 0, '.', ' ');
	echo $_m[$size].": $f_size\n";
}