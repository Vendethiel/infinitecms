<?php
define('ROOT', __DIR__.'/../');
define('EXT', strrchr(__FILE__, '.'));
function bench(){}


$_SERVER['SCRIPT_NAME'] = 'index.php';
define('DEV', true);
define('LANG', 'fr');
include ROOT . 'lib/bootstrap' . EXT;


$templates = array();
foreach (glob(ROOT . 'app/themes/*') as $template)
{
	$template = explode('/', $template);
	$template = end($template);

	if ($template[0] == '_')
		continue;

	$templates[]= $template;
}

require ROOT . 'vendor/php/autoload' . EXT;
$f = file_get_contents(ROOT . 'lib/assets.json');
$router = Router::getInstance();
$v = array('router' => $router, 'config' => $config);

foreach ($templates as $template)
{
	echo "Building $template\n";
	$paths = json_decode($asset_paths = str_replace('%template%', $template, $f), true);
	if (null === $paths)
		exit("JSON error : " . json_last_error_message());

	$asset_pipeline = new Asset\Pipeline($paths, $template);

	(string)new Asset\Cache($asset_pipeline, 'js', $v, array('minify' => false));
	(string)new Asset\Cache($asset_pipeline, 'css', $v, array('minify' => false));
}

foreach(glob(ROOT . 'cache/file_*') as $filename)
{
	$cache_file = explode('/', $filename);
	$cache_file = end($cache_file);

	list(,,$end) = explode('_', $cache_file);

	$end = str_replace('.min', '', $end);

	$new_name = 'file_' . $end;
	@unlink(ROOT . 'cache/dependencies_' . $end . '.txt');
	rename($filename, str_replace($cache_file, $new_name, $filename));
}