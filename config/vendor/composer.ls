name: "Nami-Doc/InfiniteCMS"
description: "InfiniteCMS is a CMS for Ancestra and derivated"
require:
  "lexpress/doctrine1": "1.3.4"
  "twig/twig": "1.24.1"
  "Nami-Doc/sprockets-php": "2.1.4"
autoload:
  "psr-0":
    "":
      * "lib/class/"
      * "app/models/"
      * "app/models/other/php/generated"
      * "app/models/other/php/"
      * "app/models/static/php/generated"
      * "app/models/static/php"
authors:
  * name: "Vendethiel"
    email: "vendethiel@hotmail.fr"
  ...
config:
  "vendor-dir": "php"