name: "InfiniteCMS"
version: "1.3.0RC"
main: ""
dependencies:
  "jquery-ui":       "latest"
  "bootstrap":       "latest"
  "datatables":      "latest"
  "bootstrap.css":   "latest" # contains compiled CSS
  "tinymce":         "latest"      #used for the jquery adapter
  "tinymce-release": "latest" #used for the actual code