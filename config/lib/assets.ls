#same used for view paths, only replace assets/ with views/ and removing prefixes
template:
	directories:
		"app/themes/%template%/assets/"
		"app/themes/_shared/assets/"
		"lib/assets/"
		"vendor/assets/"
	prefixes:
		js: "javascripts"
		css: "stylesheets"
		img: "images"
		font: "fonts"
external:
	directories:
		"vendor/bower/"     #bower
		"vendor/components/" #component