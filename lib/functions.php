<?php
defined('ROOT') || exit;

if (!defined('DEBUG_BACKTRACE_IGNORE_ARGS'))
	define('DEBUG_BACKTRACE_IGNORE_ARGS', 0);

/**
 * Adds a debug step
 *
 * @package Debug
 */
function dbg_step($t)
{
	global $mem;

	if (!DEBUG)
		return;

	$t = strtr($t, array(
		'FROM' => '<br/>FROM', 'LEFT' => '<br/>LEFT', 'ORDER' => '<br/>ORDER', 'LIMIT' => '<br/>LIMIT', 'GROUP' => '<br/>GROUP',
		'WHERE' => '<br/>&nbsp;WHERE', 'ON' => '<br/>&nbsp;ON',
	));

	$mem .= bench() . ': ' . $t . '<br>';
}

dbg_step('Functions: before');

//glob() actually takes 5ms on my computer ._.
foreach (glob(__DIR__ . '/functions/*.php') as $r)
	include $r;

/**
 * returns the server state
 *
 * @return string ON or OFF
 */
function server_state()
{
	if (!defined('SERVER_STATE'))
		define('SERVER_STATE', @fsockopen($config['IP_SERV'], $config['PORT_SERV'], $errno, $errstr, .1) ? 'on' : 'off');

	return SERVER_STATE;
}

/**
 * logs an event to the logs
 *
 * @param string $type entry type
 * @param string $extra extra info (item name, etc)
 */
function log_act($type, $extra = '', $acc = null)
{
	global $account;

	if (null === $acc)
		$acc = $account;

	$log = new Log;
	$log->type = $type;
	$log->extra = $extra;
	$log->at = strtotime('12:00:00');
	if ($acc)
		$log->account_id = $acc->guid;

	$log->save();
}

/**
 * __shutdown
 * Function called at the end of the program
 * /!\ WARNING /!\:
 * This function cannot be registered with register_shutdown_function because PHP does not accept
 *  Exceptions to be thrown in shutdown functions
 *
 * @global Account $account Account to save
 * @global Member $member check if online
 *
 * @return void
 */
function __shutdown()
{
	global $account, $member;
	if ($member instanceof Member && level(LEVEL_LOGGED))
	{
		if ($account instanceof Account)
			$account->save();
		$account->free(true);
		unset($account);
	}
	unset($member);
}

dbg_step('Functions: after');