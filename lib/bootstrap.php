<?php
/** @todo
 * this file needs to be splitted up
 * constants, asset loading, config stuff (staying here)
 */
global $config, $errors, $metas, $types, $account, $router, $member, $guildRights, $libraries, $asset_pipeline, $view_pipeline;
if (!isset($mem)) $mem = '';

$mem .= bench() . ': go go bootstrap<br>';

include 'constants' . EXT;
include 'lib/functions' . EXT;

if (!function_exists('get_called_class'))
	exit('Your PHP version seems too old. InfiniteCMS requires PHP5.3 at least.<br />
Votre version de PHP est trop vieille. InfiniteCMS a besoin d\'au minimum PHP5.3 pour fonctionner.');

if (addslashes("'") !== "\\'")
	exit(sprintf('Pour utiliser InfiniteCMS, vous devez d&eacute;sactiver magic_quotes_sybase dans votre php.ini<br />
If you want to use InfiniteCMS, you must disable magic_quotes_sybase in your php.ini'));
//disable magic_quotes (horrible feature ...) ! @ = no error (magic_quotes: deprecated)
if (function_exists('get_magic_quotes_gpc') && @get_magic_quotes_gpc())
{
	$_POST = recursive_array_map('stripslashes', $_POST);
	$_GET = recursive_array_map('stripslashes', $_GET);
	$_REQUEST = recursive_array_map('stripslashes', $_REQUEST);
	$_COOKIE = recursive_array_map('stripslashes', $_COOKIE);
}

if (DEBUG)
	error_reporting(-1);

date_default_timezone_set(@date_default_timezone_get());


$config = include 'config' . EXT;
if (file_exists('dev_config' . EXT))
	$dev_config = include 'dev_config' . EXT;
else
	$dev_config = false;

//allows to force disabling for some reason
defined('REWRITE') || define('REWRITE', isset($_REQUEST['rewrite']));

$loader = include 'vendor/php/autoload' . EXT;
//spl_autoload_register('load');

if (defined('BASIC') && BASIC)
	return;

//config adjustment
if ($config['URL_VOTE'] != -1)
	$config['URL_VOTE'] = sprintf('http://www.rpg-paradize.com/?page=vote&vote=%d', $config['URL_VOTE']);

$config['ENABLE_GUESTBOOK'] = isset($config['RATES_BY_PAGE']) && $config['RATES_BY_PAGE'] != -1;
$config['ENABLE_VOTE'] = isset($config['URL_VOTE']) && $config['URL_VOTE'] != -1;

define('FORUM', isset($config['BOARD_URL']) ?
 ( substr($config['BOARD_URL'], 0, 4) === 'http' ? '' : getPath() ) . $config['BOARD_URL'] :
 null);

if (!isset($config['SERVER_CORP']))
	$config['SERVER_CORP'] = '';
if (!$config['JAVASCRIPT']) //shouldn't be needed
	$config['LOAD_TYPE'] = LOAD_NONE;


if (!DEV)
{
	$router = Router::getInstance();
	register_variable('router', $router);

	if (!INSTALL)
		redirect(getPath(true) . '/install' . EXT, 0, true); //force=true

	$member = Member::getInstance();

	defined('LANG') || define('LANG', $member->getLang());
}

$login = strtr('DB_TYPE://DB_USER:DB_PSWD@DB_HOST/', array_extract($config, array('DB_TYPE', 'DB_USER', 'DB_PSWD', 'DB_HOST')));
$doctrine = array(
	'other' => Doctrine_Manager::connection($login . $config['DB_OTHER'], 'other'),
	'static' => Doctrine_Manager::connection($login . $config['DB_STATIC'], 'static'),
);
unset($login, $config['DB_HOST'], $config['DB_USER'], $config['DB_PSWD']);

dbg_step('connections loaded');

$manager = Doctrine_Manager::getInstance();
$manager->setAttribute(Doctrine_Core::ATTR_QUERY_CLASS, 'Query');
$manager->setAttribute(Doctrine_Core::ATTR_TABLE_CLASS, 'RecordTable');
$manager->setAttribute(Doctrine_Core::ATTR_COLLECTION_CLASS, 'Collection');
$manager->setAttribute(Doctrine_Core::ATTR_AUTOLOAD_TABLE_CLASSES, true);
$manager->setAttribute(Doctrine_Core::ATTR_MODEL_LOADING,
 Doctrine_Core::MODEL_LOADING_CONSERVATIVE);
$manager->setAttribute(Doctrine_Core::ATTR_QUOTE_IDENTIFIER, true);
$manager->setAttribute(Doctrine_Core::ATTR_AUTO_FREE_QUERY_OBJECTS, true);
$manager->setAttribute(Doctrine_Core::ATTR_USE_DQL_CALLBACKS, true);
unset($manager);
dbg_step('Manager loaded');


if (DEV)
	return;


$errors = array();
$langs = array();
register_variable('config', $config);

if (!isset($config['LANGS']))
	$config['LANGS'] = array($config['LANG_DEFAULT']);


//@todo this is ugly
IG::setUp();

//@todo this is fugly
Cache::addReplacement('%lang%', LANG);
Cache::setDirFormat('%dir%/%lang%');
Cache::ensureDir();


session_start();
define('CSRF_TOKEN', session_id());

if ($ban_ip = Cache::start('ip_banned', -1, Cache::ACT_DEFAULT, 'ip_'))
{ //! won't auto-invalidate !
	dbg_step('Load banned IPs');

	$ips = array();
	foreach (BanIpTable::getInstance()->findAll(Doctrine_Core::HYDRATE_NONE) as $ip)
		$ips[] = $ip[0];

	$ban_ip->set('banned', $ips);
	$ban_ip->save();
}

if (in_array(Member::getIp(), $ip_banned))
	exit('umadbro');
//*/

if (!empty($_SESSION['guid']))
{ //retrieve account
	dbg_step('Before account');

	$account = Query::create()
					->from('Account a')
						->leftJoin('a.Characters c INDEXBY guid')
							->leftJoin('c.GuildMember gm')
								->leftJoin('c.Events e INDEXBY id')
#								->leftJoin('c.ContestParticipations cp INDEXBY contest_id')
#									->leftJoin('cp.Contest co')
						->leftJoin('a.User u')
#								->leftJoin('u.PollOptions po')
#									->leftJoin('po.Poll p')
#								->leftJoin('u.Review r')
					->where('guid = ?', $_SESSION['guid'])
					->fetchOne();
#		exit('Memory used by ONE Query : FROM Account WHERE guid = ? fetchOne WITHOUT ANY JOIN + Query object free\'d + unset\'d : ' . ( memory_get_usage() - $prev_mem));
#		for those asking : the result is 4 221 424 (1 try only, it's not an average)
	if ($account)
	{
		dbg_step('account\'d');
		if (!$account->relatedExists('User'))
			$account->User = UserTable::getInstance()->fromGuid($account);

		if ($config['ACCOUNT_POINTS']) //"symlinking" points
			$account->User->points = $account->points;

#		$account->User->points += $account->points;
#		$account->points = 0;

		// AWARENESS - reduces lazy queries
		$account->User->Account = $account;
		foreach ($account->Characters as $character) $character->Account = $account;

		if ($account->lastip != $member->getIp())
			$account->lastip = $member->getIp();

		register_variable('account', $account);
	}
	else
		unset($_SESSION['guid']);
}

$template = $config['THEME'];
if (!file_exists('app/themes/' . $template) || $template[0] == '_')
{
	$available_themes = array();
	foreach (glob('app/themes/*') as $theme)
	{
		$theme = substr($theme, strlen('app/themes/'));
		if ($theme[0] == '_')
			continue; // skip privates themes
		$available_themes[] = $theme;
	}
	$available_themes = implode(', ', $available_themes);

	if ($available_themes)
		exit('The selected theme does not exist. Available(s) are : ' . $available_themes . '.<br />' .
		 'Le theme selectionne n\'existe pas. Disponible(s) : ' . $available_themes . '.');
	else
		exit('No theme installed.<br />Aucun theme n\'est installé');
}

$paths = json_decode($asset_paths = str_replace('%template%', $template, file_get_contents('lib/assets.json')), true);
if (null === $paths)
	exit("JSON error : " . json_last_error_message());

$view_paths = json_decode(str_replace('assets/', 'views/', $asset_paths), true);

$view_pipeline = new Sprockets\Pipeline($view_paths, $template);

$asset_pipeline = new Sprockets\Pipeline($paths, $template);

if (!$router->isAjax())
{
	register_variable('asset_pipeline', $asset_pipeline);

	if (!defined('MINIFY'))
		define('MINIFY', !DEBUG);

	//allows to force asset files ;)
	if (!defined('JAVASCRIPT_FILE'))
	{
		if (file_exists($cache_js = 'cache/file_' . $template . '.js'))
			define('JAVASCRIPT_FILE', $cache_js);
		else
			define('JAVASCRIPT_FILE', new Sprockets\Cache($asset_pipeline, 'js', register_variable(), array('minify' => MINIFY)));
	}
	if (!defined('STYLESHEET_FILE'))
	{
		if (file_exists($cache_css = 'cache/file_' . $template . '.css'))
			define('STYLESHEET_FILE', $cache_css);
		else
			define('STYLESHEET_FILE', new Sprockets\Cache($asset_pipeline, 'css', register_variable(), array('minify' => MINIFY)));
	}

	register_variable('stylesheets', stylesheets());
}

if (!level(LEVEL_ADMIN) && $router->isPost() && empty($_SESSION['disable_csrf_token']))
{
	$request_token = $router->postVar('_csrf_token');

	if ($request_token !== session_id()) //using !== here is VERY SIGNIFICANT
	{
		if (DEBUG)
			echo '[DEBUG] invalid token<hr />'; //simple notice ...
		else
			define('HTTP_CODE', 404);
	}
}
unset($_SESSION['disable_csrf_token']);
/* @var $account Account */