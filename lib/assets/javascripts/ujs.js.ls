$body = $ document.body

$body.on 'click' 'a[data-method]' !->
	$this = $ @
	return unless allow-action $this

	target = $this.attr 'target' or ''

	method = $this.data 'method'
	return if method is 'GET'

	it
		..prevent-default!
		..stop-immediate-propagation!

	$ '<form/>' {target, method: 'POST' action: $this.attr 'href'}
		.append $ '<input/>',
			type: 'hidden'
			name: '_method'
			value: method
		.append $ '<input/>',
			type: 'hidden'
			name: '_csrf_token'
			value: csrf-token
		.hide!
		.append-to 'body'
		.submit!

$body.on 'click' 'a[data-confirm]:not([data-method])' ->
	allow-action $ @

allow-action = (message) ->
	message.=data 'confirm'
	
	if message? then confirm message else true