note-div = null

window.showNote = !-> #`export function show-note` won't work with "it" (LS bug)
	return unless note-div?

	note-div
		.fade-in 400
		.delay 3000
		.fade-out 300

		.find '#content'
			.html "<b>#it</b>"

$ ->
	note-div := $ '#note'