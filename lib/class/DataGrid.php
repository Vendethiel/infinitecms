<?php
class Datagrid
{
	protected $records,
				$td = '<td valign="center" align="center">',
				$th = '<th valign="center" align="center" class="%s">',
				$positionFunction,
				$trOptsFunction,
				$recordGetter,
				$columns = array(),
				$columnValues = array(),
				$options = array(
					'url' => array(),
					'table_id' => '',
				),
				$behaviors = array();
	
	public function __construct($records, $options = array())
	{
		$this->records = $records;
		$this->setOptions($options);

		$this->trOptsFunction = function ($record)
		{
			return array();
		};
		$this->recordGetter = function ($record)
		{
			return array('base' => $record, 'record' => $record);
		};
	}
	
	public function getOptions()
	{
		return $this->getOptions();
	}
	
	public function getOption($name, $fb = null)
	{
		if (isset($this->options[$name]))
			return $this->options[$name];
		return $fb;
	}
	
	protected function setOptions($options)
	{
		$this->options = array_merge($this->options, $options);
		if (empty($this->options['table_id']))
			$this->options['table_id'] = $this->getGridId($this->records);
	}
	
	protected function getGridId($records)
	{
		if ($records instanceof \Doctrine_Collection)
			return $records->getTable()->getClassnameToReturn();
		return rand(0, 50) . '-grid';
	}

	/**
	 * Sets the TD tag code
	 */
	public function setTd($td)
	{
		$this->td = $td;
		return $this;
	}

	/**
	 * Sets the TH tag code
	 */
	public function setTh($th)
	{
		$this->th = $th;
		return $this;
	}

	/**
	 * Sets the option to get the options for the TR tag
	 */
	public function setTrOptsFunction($fn)
	{
		if (!is_callable($fn))
			throw new InvalidArgumentException('Argument must be callable');

		$this->trOptsFunction = $fn;
		return $this;
	}

	/**
	 * sets the record getetr : basically an array with [base: ..., record: ...]
	 */
	public function setRecordGetter($fn)
	{
		if (!is_callable($fn))
			throw new InvalidArgumentException('Argument must be callable');

		$this->recordGetter = $fn;
		return $this;
	}
	
	public function add($name, $col = null, $r = 'record', $opt = array())
	{
		if (is_array($r) && $opt === array())
		{
			$opt = $r;
			$r = 'record';
		}

		$this->columns[$name] = array($col ?: $name, $r, $opt);
		return $this;
	}
	
	public function bind($name, $options = array())
	{
		if (isset($options['class']))
		{
			$class = $options['class'];
			unset($options['class']);
		}
		else
			$class = '\\' . __CLASS__ . '\\' . ucfirst($name);

		$behavior = new $class($this, $options);
		$this->behaviors[] = $behavior;
		return $this;
	}
	
	protected function behave($fn, $ret)
	{
		$fn = 'on' . ucfirst($fn);
		$args = func_get_args();
		$args = array_slice($args, 2);
		foreach ($this->behaviors as $behavior)
		{
			if (null != $r = call_user_func_array(array($behavior, $fn), array_merge(array($ret), $args)))
				$ret = $r;
		}

		return $ret;
	}

	/**
	 * Returns table headers (table tag + columns headers)
	 */
	public function getTableHeaders()
	{
		if (empty($this->columns))
			throw new Exception('Cannot process an empty ladder');

		$html = '
				<thead>
					<tr>';
		foreach ($this->columns as $key => $col)
		{
			$html .= sprintf($this->th, str_replace('.', '-', $key));
			$lang = tag('b', lang($key, null, '%%key%%'));
			$html .= $this->behave('tableHeader', $lang, $key) . '</th>';
		}
		return $html . '
					</tr>
				</thead>';
	}

	public function __toString()
	{
		try
		{
			if (empty($this->columns))
				throw new Exception('Cannot process an empty ladder');

			$html = '
				<table border="1" style="width: 95%" id="' . $this->getOption('table_id') . '">' . $this->getTableHeaders() . '<tbody>';
			foreach ($this->records as $record)
			{
				$record = call_user_func($this->recordGetter, $record);

				$html .= tag('tr', call_user_func($this->trOptsFunction, $record));
				foreach ($this->columns as $name => $c)
				{
					$rec = $record;
					list($column, $r) = $c;

					if ($column instanceof Closure)
						$text = call_user_func($column, $record, isset($this->columnValues[$name]) ? $this->columnValues[$name] : null);
					else
						$text = $rec[$r]->resolveProperty($column, isset($this->columnValues[$name]) ? $this->columnValues[$name] : null);

					$opt = isset($c[2]) ? $c[2] : array();

					$html .= tag('td', array_merge(array('align' => 'center'), $opt), $text);
				}
				$html .= '</tr>';
			}
			return $this->behave('afterRecords', $html) . '</tbody></table>';
		}
		catch (Exception $e) { vdump($e); }
	}
}