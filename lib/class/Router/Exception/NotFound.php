<?php
namespace Router\Exception;

class NotFound extends Code
{
	public function getHttpCode()
	{
		return 404;
	}
}