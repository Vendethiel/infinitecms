<?php
namespace Router\Exception;

class NotAuthorized extends Code
{
	public function getHttpCode()
	{
		return 301;
	}
}