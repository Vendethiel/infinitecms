<?php
namespace Router\Exception;

class Code extends \Exception
{
	protected $code;

	public function __construct($code = null)
	{
		$this->code = $code;
	}
	public function getHttpCode()
	{
		if (null === $this->code)
			throw new \LogicException('Cannot get the http code when it\'s not set');

		return $this->code;
	}
}