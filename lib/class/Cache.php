<?php

/**
 * Implements cache management
 *
 * @file $Id$
 */
class Cache
{
	const DIR = 'cache';

	const SKIP = 1,
		SHOW = 2,
		RET = 3, //return is a reserved keyword

		JS = true,
		NO_JS = false,

		CARET = true,
		END = false,

		ACT_LOAD = 0,
		ACT_NOTHING = 1,
		ACT_DEFAULT = null;

	static protected $cachedFiles = null,
		$actual = null,
		$replacements = array(),
		$dirFormat = '%dir%',
		/* @var string Var prefix. NOTE : if not empty, followed by an underscore ! (ie with varPrefix = '%%cache%%' and varName = 'test', will result in '%%cache%%_test' */
		$varPrefix = '';
	protected $name = null,
		$vars = array(),
		$input = '';

	/**
	 * sets dir format
	 * The dir MUST exist.
	 *
	 * @param string $format path format (dir)
	 */
	static public function setDirFormat($format)
	{
		self::$dirFormat = $format;
	}

	/**
	 * returns dir format
	 * 
	 * @return string
	 */
	static public function getDirFormat()
	{
		return self::$dirFormat;
	}
	static public function ensureDir($add = '')
	{
		ensure_directory(self::getDir() . ltrim($add, '/'));
	}
	static public function getDir()
	{
		return trim(self::_format(self::$dirFormat, array('%dir%' => self::DIR)), '/') . '/';
	}

	/**
	 * formats a string
	 *
	 * @param type $str the string to format
	 * @param type $replacements the remplacements
	 * @param type $includes add the global replacements ?
	 * 
	 * @return string
	 */
	static protected function _format($str, $replacements, $includes = true)
	{
		if ($includes)
			$replacements = array_merge(self::$replacements, $replacements);

		return strtr($str, $replacements);
	}
	static public function addReplacement($name, $value)
	{
		self::$replacements[$name] = $value;
	}

	/**
	 * sets var prefix. 
	 *
	 * @param boolean $activate whether it must be enabled or not
	 */
	static public function setVarPrefix($prefix)
	{
		self::$varPrefix = $prefix;
	}

	/**
	 * gets var prefix
	 */
	static public function getVarPrefix($prefix)
	{
		return self::$varPrefix;
	}


	/**
	 * destroys a cache file
	 *
	 * @param string $name cache's name
	 * @return void
	 */
	static public function deleteByName($name)
	{
		$filename = self::getNameFor($name);
		$path = self::getDir() . $filename;

		if (file_exists($path))
			@unlink($path);
	}
	static public function delete($name) { self::deleteByName($name); }


	/**
	 * starts caching ... Or require cached file if present & valid.
	 *
	 * @param int|string $lifeTime cachefile's lifetime (integer or strotime() arg). -1 for infinite lifetime
	 * @return Cache|false Cache instance if the cache needs to be refreshed, false if the cache is still valid
	 */
	public static function start($name, $lifeTime = -1, $act = null, $prefix = null, $whatTo = null)
	{
		if (self::$actual != null)
			throw ExceptionManager::nestingCache(self::$actual, $name);
		if ($act == null)
			$act = self::ACT_LOAD;
		if ($whatTo == null)
			$whatTo = self::SHOW;

		$path = self::getDir() . $name = self::getNameFor($name);

		if (file_exists($path)
		 && ($lifeTime == -1 || !date_passed(filemtime($path), $lifeTime)))
		{
			$__ret = false;
			if ($act == self::ACT_LOAD)
			{
				ob_start();
				require self::getDir() . $name;
				$f = ob_get_clean();

				if ($whatTo == self::SHOW)
					echo $f;
				else if ($whatTo == self::RET)
					$__ret = $f;
				

				if ($prefix == null && $prefix !== '')
					$prefix = self::_formatPrefix($name);
				
				if (isset($vars))
				{
					foreach ($vars as $vname)
						$GLOBALS[$prefix . $vname] = $$vname;
				}
			}
			return $__ret;
		}
		else
		{
			self::delete($name); //remove the cache as it's not used anymore.
			return new self($name, $prefix);
		}
	}

	/**
	 * exports variable in eval'able PHP format
	 *
	 * @param boolean $phpTags=true add PHP tags ?
	 */
	protected function _exportVars($phpTags = true)
	{
		if (empty($this->vars))
			return '';

		$code = '';

		if ($phpTags)
			$code .= '<?php ';
		$vars = array_merge($this->vars, array('vars' => array_keys($this->vars)));

		foreach ($vars as $name => $value)
			$code .= sprintf('$%s = %s; ', $name, var_export($value, true));

		return $code . ( $phpTags ? ' ?>' : '' );
	}

	/**
	 * exports javascript code in jQ() call format.
	 */
	protected static function _exportJS($phpTags = true)
	{
		$js = jQ(null, 'cache');
		jQ($js, 'cache'); //pretty stupid, yeah.
		if (!empty($js))
			return ($phpTags ? '<?php ' : '') . 'jQ(' . var_export($js, true) . '); ' . ($phpTags ? ' ?>' : '');
	}

	/**
	 * returns formatted prefix + an underscore if not empty
	 *
	 * @param string $name cache's name
	 */
	static protected function _formatPrefix($name)
	{
		if (empty(self::$varPrefix))
			return '';
		else
			return self::_format(self::$varPrefix, array('cache' => $name)) . '_';
	}

	static protected function getNameFor($name)
	{
		return strpos($name, '.') === false ? $name . EXT : $name;
	}
	public function getName()
	{
		return self::getNameFor($this->name);
	}

	/**
	 * saves cache to it's file + print it on screen. Also, it destructs the class.
	 *
	 * @param boolean $show show the content just got cached ?
	 * @param boolean $js include JS ?
	 */
	public function save($act = null, $js = null)
	{
		if ($act === null)
			$act = self::SHOW;
		if ($js === null)
			$js = self::JS;

		$content = $this->_exportVars() . ($js == self::JS ? $this->_exportJS() : '') . ob_get_contents() . ' ' . $this->input;
		file_put_contents(self::getDir() . $this->getName(), trim($content));

		switch ($act)
		{
			case self::SHOW: //if you want to nest a cache inside another WITH variables (i.e.) ...
				ob_flush();
			break;
			case self::RET:
				$ret = ob_get_contents();
			break;
		}
		ob_end_clean();

		$prefix = $this->prefix == null && $this->prefix !== '' ? self::_formatPrefix($this->getName()) : $this->prefix;
		foreach ($this->vars as $vname => $value)
			$GLOBALS[$prefix . $vname] = $value;

		unset($this);
		if (isset($ret))
			return $ret;
	}

	public function clear()
	{
		ob_end_clean();
	}

	/**
	 * adds string BUT does not show it
	 */
	public function put($str, $at = null)
	{
		if ($at == null)
			self::END;

		if ($at == self::CARET)
		{
			$this->input .= ob_end_clean() . $str;
			ob_start();
		}
		else
			$this->input .= $str;
	}

	/**
	 * sets a variable
	 *
	 * @param string $name variable's name
	 * @param mixed $value variable's value
	 */
	public function set($name, $value)
	{
		$this->vars[$name] = $value;
	}

	protected function __construct($name, $prefix)
	{
		$this->name = $name;
		$this->prefix = $prefix;
		ob_start(); //php allows nested ouput buffering. I DO NOT (or do I ?)
	}

	public function __destruct()
	{
		self::$actual = null;
	}
}