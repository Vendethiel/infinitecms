<?php
namespace Sprockets\Filter;

/**
 * JavaSCript Requirable file
 *
 * Currently bound to InfiniteCMS because of the prefix AND lookups
 * For the future:
 *  - Allow to pass options to filters (ie from a hash)
 *  - ?
 */
class Jsr extends Base
{
	public function __invoke($content, $file, $dir, $vars)
	{
		$name = $this->locator->resolveFile($file);

		if ($name == $file)
			exit('Unable to guess name for file ' . $file);

		$name = 'InfiniteCMS/' . $name;

		return "
require.register('$name', function(exports, require, module) {
	" . str_replace('%%module%%', $name, $content) . "
});";
	}
}