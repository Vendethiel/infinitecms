<?php
namespace Sprockets\Filter;

/**
 * Twig Abstraction Markup Language
 */
class Taml extends Twig
{
	public function __construct()
	{
		parent::__construct();
	}
}