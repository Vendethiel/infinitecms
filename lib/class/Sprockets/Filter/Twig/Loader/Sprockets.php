<?php
namespace Sprockets\Filter\Twig\Loader;

use \Sprockets\Filter;

use \Twig_Loader_Filesystem;
use \Twig_Loader_Error;
use \Twig_Error_Loader;

class Sprockets extends Twig_Loader_Filesystem
{
	protected $cache;

	protected function findTemplate($name)
	{
		global $view_pipeline;

        if (isset($this->cache[$name]))
            return $this->cache[$name];

        $this->validateName($name);
        $name = preg_replace('#/{2,}#', '/', strtr($name, '\\', '/'));

		try {
			$fileparts = explode('.', $name);
			$file = $view_pipeline->getLocator()->getFile($fileparts[0], isset($fileparts[1]) ? $fileparts[1] : null);
		} catch (\Sprockets\Exception\Asset $e) {
			throw new Twig_Error_Loader($e->getMessage());
		}

		return $this->cache[$name] = $file;
	}
}