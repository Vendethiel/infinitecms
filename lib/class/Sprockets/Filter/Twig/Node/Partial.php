<?php
namespace Sprockets\Filter\Twig\Node;

use \Twig_Node;
use \Twig_Node_Expression;
use \Twig_Compiler;

class Partial extends Twig_Node
{
	public function __construct(Twig_Node_Expression $file, Twig_Node_Expression $variables = null, $only = false, $ignoreMissing = false, $line, $tag = null)
	{
		parent::__construct(array('file' => $file, 'variables' => $variables), array('only' => (Boolean) $only, 'ignore_missing' => (Boolean) $ignoreMissing), $line, $tag);
	}
    /**
     * Compiles the node to PHP.
     *
     * @param Twig_Compiler A Twig_Compiler instance
     */
    public function compile(Twig_Compiler $compiler)
    {
        $compiler->addDebugInfo($this);

        if ($this->getAttribute('ignore_missing')) {
            $compiler
                ->write("try {\n")
                ->indent()
            ;
        }

        $this->addGetTemplate($compiler);

        $this->addTemplateArguments($compiler);

        $compiler->raw(");\n");

        if ($this->getAttribute('ignore_missing')) {
            $compiler
                ->outdent()
                ->write("} catch (Twig_Error_Loader \$e) {\n")
                ->indent()
                ->write("// ignore missing template\n")
                ->outdent()
                ->write("}\n\n")
            ;
        }
    }

    protected function addGetTemplate(Twig_Compiler $compiler)
    {
		$compiler
			->addDebugInfo($this)
			->write('echo new \Sprockets\File(')
			->subcompile($this->getNode('file'))
			->raw('.".". ASSET_HTML,');
    }

    protected function addTemplateArguments(Twig_Compiler $compiler)
    {
        if (false === $this->getAttribute('only')) {
            if (null === $this->getNode('variables')) {
                $compiler->raw('array_merge($context, register_variable())');
            } else {
                $compiler
                    ->raw('array_merge($context, register_variable(), ')
                    ->subcompile($this->getNode('variables'))
                    ->raw(')')
                ;
            }
        } else {
            if (null === $this->getNode('variables')) {
                $compiler->raw('array()');
            } else {
                $compiler->subcompile($this->getNode('variables'));
            }
        }
    }
}