<?php
namespace Sprockets\Filter\Twig\Token;

use \Sprockets\Filter\Twig\Node;
use \Twig_TokenParser;
use \Twig_Token;

class PartialParser extends Twig_TokenParser
{
	public function parse(Twig_Token $token)
	{
		$file = $this->parser->getExpressionParser()->parseExpression();

        list($variables, $only, $ignoreMissing) = $this->parseArguments();

		return new Node\Partial($file, $variables, $only, $ignoreMissing, $token->getLine(), $this->getTag());
	}

	protected function parseArguments()
	{
        $stream = $this->parser->getStream();

        $ignoreMissing = false;
        if ($stream->test(Twig_Token::NAME_TYPE, 'ignore')) {
            $stream->next();
            $stream->expect(Twig_Token::NAME_TYPE, 'missing');

            $ignoreMissing = true;
        }

        $variables = null;
        if ($stream->test(Twig_Token::NAME_TYPE, 'with')) {
            $stream->next();

            $variables = $this->parser->getExpressionParser()->parseExpression();
        }

        $only = false;
        if ($stream->test(Twig_Token::NAME_TYPE, 'only')) {
            $stream->next();

            $only = true;
        }

        $stream->expect(Twig_Token::BLOCK_END_TYPE);

        return array($variables, $only, $ignoreMissing);
	}

	public function getTag()
	{
		return 'partial';
	}
}