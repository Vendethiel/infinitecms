<?php
namespace Sprockets\Filter;

use \MtHaml;
use \MtHaml\Support\Twig\Loader as MtLoader;

class Twig extends Base
{
	protected $env,
			$dir,
			$dir_cache,
			$mt_env;

	public function __construct()
	{
		\ensure_directory($this->dir = \Cache::getDir() . 'asset/' . str_replace('\\', DIRECTORY_SEPARATOR, __CLASS__) . '/');
		\ensure_directory($this->dir_cache = $this->dir . 'cache');
		
		$this->initializeTwig();
		$this->register();
	}

	public function register()
	{
		$this->registerExtensions();
		$this->registerTokens();
		$this->registerFunctions();
		$this->registerFilters();
		$this->registerGlobals();
		$this->applySettings();
	}

	public function initializeTwig()
	{	
		$this->mt_env = new MtHaml\Environment('twig', array('enable_escaper' => false));

		$baseLoader = new \Twig_Loader_Filesystem(array($this->dir));
		$sprocketsLoader = new Twig\Loader\Sprockets(array($this->dir));
		$finalLoader = new Taml\Loader($this->mt_env, new \Twig_Loader_Chain(array($baseLoader, $sprocketsLoader)));

		$this->env = new \Twig_Environment($finalLoader, array(
			'cache' => $this->dir_cache,
			'auto_reload' => DEBUG,
		));
	}

	public function registerExtensions()
	{
		//if needed for later support
		$this->env->addExtension(new MtHaml\Support\Twig\Extension());
	}

	public function registerTokens()
	{
		$this->env->addTokenParser(new Twig\Token\PartialParser());
	}

	public function registerFunctions()
	{
		$functions = 'level asset_path make_form input input_select_options make_link make_img partial defined getPath' .
		' sprintf replace_url pluralize count chart stockchart input_csrf_token dbg_step server_state'.
		' getPath to_url include_library javascript_val jQ javascripts js_link'.
		' implode array_keys';
		$function_safety = array('html' => explode(' ',
		 'make_form input input_select_options make_link make_img asset_path partial chart stockchart' .
		 ' input_csrf_token jQ javascripts js_link'));
		//REMINDER : always `make cc` after adding a html-safe function, to recompile templates

		$functions = explode(' ', $functions);
		foreach ($functions as $function)
		{
			$is_safe = array();
			foreach ($function_safety as $t => $functions)
			{
				if (in_array($function, $functions))
					$is_safe[] = $t;
			}

			$this->env->addFunction($function, new \Twig_Function_Function($function,
				array('is_safe' => $is_safe)));
		}
	}

	public function registerFilters()
	{
		$this->env->addFilter('lang', new \Twig_Filter_Function('lang',
		 array('is_safe' => array('html'))));
		$this->env->addFilter('utf8', new \Twig_Filter_Function('utf8_encode',
		 array('is_safe' => array('html'))));
	}

	public function registerGlobals() 
	{
		global $config, $router, $account;

		$this->env->addGlobal('config', $config);
		$this->env->addGlobal('account', $account);
		$this->env->addGlobal('is_ajax', $router->isAjax());
		$this->env->addGlobal('layout', $router->isAjax() ? 'layouts/layout_ajax.html' : 'layouts/layout.html');
		$this->env->addGlobal('LOGGED', level(LEVEL_LOGGED));

		$constants = array(
			'FORUM',
			'LEVEL_ADMIN',
			'LEVEL_GUEST',
			'ASSET_IMAGE',
			'EXT_JPG',
			'EXT_PNG',
			'EXT_GIF',
			'PARTIAL_CONTROLLER',
			'CSRF_TOKEN',
			'LIBRARY_TINYMCE',
		);
		foreach ($constants as $constant)
			$this->env->addGlobal($constant, constant($constant));

		if (defined('UPDATE_SELECTOR'))
			$this->env->addGlobal('UPDATE_SELECTOR', UPDATE_SELECTOR);
		$this->env->addGlobal('CONTROLLER', $router->getController());
		$this->env->addGlobal('ACTION', $router->getAction());
		$this->env->addGlobal('REQUEST_URI', $_SERVER['REQUEST_URI']);
		$this->env->addGlobal('FIELD_PSEUDO', \Member::CHAMP_PSEUDO);
		$this->env->addGlobal('FIELD_PASS', \Member::CHAMP_PASS);
	}

	public function applySettings()
	{
		$this->env->getExtension('core')
			->setNumberFormat(0, '.', ' ');
	}

	public function __invoke($content, $file, $dir, $vars)
	{
		$hash = md5($content);
		if (!file_exists($p = $this->dir . ( $f = basename($file) . $hash . '.' . ASSET_PHP )))
			file_put_contents($p, $content);

		return $this->env->render($f, $vars);
	}
}