<?php
namespace Sprockets\Filter\Taml;

use \MtHaml\Environment;
use \MtHaml\Support\Twig\Loader as MtLoader;

class Loader extends MtLoader
{
    public function __construct(Environment $env, \Twig_LoaderInterface $loader)
    {
        $this->env = $env;
        $this->loader = $loader;
    }

	public function getSource($name)
	{
        global $view_pipeline;
        $source = $this->loader->getSource($name);

        //try to look for the file, without its extension (only recognizing .html here, as I do the same in other places !)
        if ($view_pipeline->getLocator()->hasFile($name = str_replace('.html', '', $name), 'html'))
            $name = $view_pipeline->getLocator()->getFile($name, 'html');

        //if it contains taml, then process it :)
        if (false !== strpos($name, '.taml')) //can't use ($name / '.')[*-1] since $name is like 'index.html.taml$hash.php'
    		return $this->env->compileString($source, $name);

        return $source;
	}
}