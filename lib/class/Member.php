<?php

/*** XXX
 * KILL
 * THIS
 */


/**
 * represents a couple of User + Account
 *
 * @file $Id: Member.php 53 2011-01-15 11:11:37Z nami.d0c.0 $
 */
class Member extends Multiton
{
	const CHAMP_LEVEL = 'level',
		CHAMP_PSEUDO = 'account',
		CHAMP_PASS = 'pass';

	public function lang()
	{
		global $config, $account;

		if (!$this->isConnected())
			return $config['LANG_DEFAULT'];

		if (empty($account->User->culture) || !in_array($account->User->culture, $config['LANGS']))
			return $config['LANG_DEFAULT'];

		return $account->User->culture;
	}

	public function pseudo($d = null)
	{
		return !empty($_POST[$this->CHAMP_PSEUDO]) ? $_POST[$this->CHAMP_PSEUDO] : $d;
	}

	public function init()
	{
		$this->lang = $this->lang();
	}

	/**
	 * logs in the user
	 *
	 * @return User
	 */
	public function log($guid = null)
	{
		if (!isset($_SESSION['guid']))
			$_SESSION['guid'] = $guid;
		else if ($guid === null)
			$guid = $_SESSION['guid'];

		return UserTable::getInstance()->fromGuid($guid);
	}

	public function disconnect()
	{
		$_SESSION['guid'] = null;
		$_SESSION = array();
		session_destroy();
	}

	public function getLevel()
	{
		global $account;
		if (level(LEVEL_LOGGED))
			return $account->level;
		return LEVEL_GUEST;
	}

	/*
	 * returns string representation of the level
	 *
	 * @param integer $lvl Niveau du membre
	 */
	static public function formateLevel($lvl, $vip)
	{
		$lvl = self::adjustLevel(intval($lvl), $vip);

		foreach (array_reverse(self::getFormattedLevels(), true) as $level => $text)
		{
			if ($lvl >= $level)
				return $text;
		}
		return $text;
	}

	static public function adjustLevel($lvl, $vip)
	{
		global $config;

		if ($lvl >= LEVEL_ADMIN)
			return LEVEL_ADMIN;
		if ($lvl == LEVEL_LOGGED && ($vip && !empty($config['COST_VIP'])))
			return LEVEL_VIP;
		if ($vip || $lvl == LEVEL_VIP)
			return empty($config['COST_VIP']) ? LEVEL_LOGGED : LEVEL_VIP;

		return $lvl;
	}

	static public function getLevels($simple = false)
	{
		$levels = array();
		if (!$simple)
		{
			$levels[LEVEL_BANNED] = lang('rank.banned');
			$levels[LEVEL_GUEST] = lang('rank.guest');
		}

		$levels += array(
			LEVEL_LOGGED => lang('rank.player'),
			'' . LEVEL_VIP => lang('rank.vip'),
			LEVEL_TEST => lang('rank.test'),
			LEVEL_MODO => lang('rank.mod'),
			LEVEL_MJ => lang('rank.gm'),
			LEVEL_ADMIN => lang('rank.admin'),
		);
		return $levels;
	}

	static public function getFormattedLevels()
	{
		return array(
			LEVEL_BANNED => '<strike>' . lang('rank.banned') . '</strike>',
			LEVEL_GUEST => lang('rank.guest'),
			LEVEL_LOGGED => '<i>' . lang('rank.player') . '</i>',
			'' . LEVEL_VIP => '<b>' . lang('rank.vip') . '</b>', //php doesn't allow float as keys
			LEVEL_TEST => '<u>' . lang('rank.test') . '</u>',
			LEVEL_MODO => '<b><i>' . lang('rank.mod') . '</i></b>',
			LEVEL_MJ => '<b><u>' . lang('rank.gm') . '</u></b>',
			LEVEL_ADMIN => '<b><i><u>' . lang('rank.admin') . '</u></i></b>',
		);
	}

	static public function getIp()
	{
		//cloudflare
		if (isset($_SERVER['HTTP_CF_CONNECTING_IP']))
			return $_SERVER['HTTP_CF_CONNECTING_IP'];
		if (isset($_SERVER['REMOTE_ADDR']))
			return $_SERVER['REMOTE_ADDR'];
		if (isset($_SERVER['HTTP_X_REQUESTED_FOR'])
		 && false !== filter_var($_SERVER['HTTP_X_REQUESTED_FOR'], FILTER_VALIDATE_IP))
			return $_SERVER['HTTP_X_REQUESTED_FOR'];
		return null;
	}
}