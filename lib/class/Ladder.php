<?php
class Ladder extends Datagrid
{
	public function __construct($records, $startAt = 0, $positionFunction = true, $options = array())
	{
		parent::__construct($records, $options);

		if ($positionFunction === true)
		{
			$this->add('acc.ladder.pos', function ($record) use ($startAt)
			{
				static $pos = null;
				if ($pos === null)
					$pos = $startAt;
				return ++$pos;
			});
		}
		else
			$this->add('acc.ladder.pos', $positionFunction);
	}
}