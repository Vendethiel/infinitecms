<?php

/**
 * adds more possibility to the basic Doctrine_Collection class
 *
 * @file $Id: Collection.php 40 2010-11-21 01:15:23Z nami.d0c.0 $
 *
 * @extends Doctrine_Collection
 */
class Collection extends Doctrine_Collection
{

	//static in case we have many collection of Character ...
	protected static $charsInit = null;

	/**
	 * adds all values to this Collection
	 *
	 * @param $values Collection|array Values to add
	 * @return void
	 */
	public function addAll($values)
	{
		foreach ($values as $id => $rec)
		/** @var $rec Record */
			$this->add($rec);
	}

	public function toValueArray($val = null)
	{
		if (method_exists($this->getTable(), 'getRouterParam'))
			$id = $this->getTable()->getRouterParam();
		else
		{
			$id = (array) $this->getTable()->getIdentifier();
			$id = $id[0];
		}

		return parent::toKeyValueArray($id, $val);
	}

	/**
	 * isEmpty
	 * determinate if this Collection contains or not records
	 *
	 * @return boolean is this Collection is empty
	 */
	public function isEmpty()
	{
		return $this->count() === 0;
	}

	/**
	 * getCleanValues
	 * keep values
	 *
	 * @return array
	 */
	public function getCleanValues()
	{
		$datas = $this->toArray();
		if (!empty($datas) && $this->getTable()->hasColumn('content'))
			foreach ($datas as &$data)
				$data['content'] = News::format($data['content']);
		return $datas;
	}
	
	public function shopDisplay()
	{
		global $config, $types, $account, $router;
		$table = ShopItemTable::getInstance();

		if ($this->isEmpty())
			return;
		
		$itemsID = array(); //BIG array of all items ID
		foreach ($this as $obj)
		{
			foreach ($obj->Effects as $ef)
				if ($ef->isItem())
					$itemsID[] = $ef->value;
		}
		if ($itemsID)
		{
			$items = Query::create()
					->from('ItemTemplate it INDEXBY id')
						->whereIn('id', $itemsID)
					->execute();
			$e_table = ShopItemEffectTable::getInstance();
			foreach ($items as $item)
				$e_table->templates[$item->id] = $item;
		}

		define('FROM_INCLUDE', true);

		IG::registerEffectsTooltip();
	}

	public function atomDisplay($title = null, $desc = null, $link = null)
	{
		global $router;

		if ($title === null)
			$title = 'RSS';
		if ($desc === null)
			$desc = $router->getControllerUse($router->getController()) . ' &bull; RSS';
		if ($link === null)
			$link = to_url(array(
						'controller' => $router->getControllerUse($router->getController()),
						'action' => 'show',
						'id' => ''
					)) . '{id}';
		echo '<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="http://rss.feedsportal.com/xsl/fr/rss.xsl"?>
<rss
	xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd"
	xmlns:dc="http://purl.org/dc/elements/1.1/"
	xmlns:taxo="http://purl.org/rss/1.0/modules/taxonomy/"
	xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" version="2.0">
	<channel>
		<title>' . $title . '</title>
		<link>' . str_replace('index' . EXT, '', $_SERVER['PHP_SELF']) . '</link>
		<description>' . $desc . '</description>';
		$table = $this->getTable();
		/* @var $table Table */
		$hasColumn = array(
			'title' => $table->hasColumn('title'),
			'content' => $table->hasColumn('content'),
			'desc' => $table->hasColumn('description'),
			'date' => $table->hasColumn('created_at'),
		);
		foreach ($this->getCleanValues() as $val)
		{
			//$pre & $post are used > 1 times
			$pre = "\n\t\t\t";
			$post = "\n\t\t";
			$title = $hasColumn['title'] ? $pre . tag('title', $val['title']) : '';
			$desc = $hasColumn['desc'] ? $val['description'] : ( $hasColumn['content'] ? $val['content'] : '' );
			$desc = $pre . tag('description', sprintf('<![CDATA[%s]]>', $desc));
			$date = $hasColumn['date'] ? $pre . tag('pubDate', $val['created_at']) : '';
			$link = str_replace('{id}', $val['id'], $link);
			echo $post . tag('item', $title . $pre . tag('link', $link) . $desc . $date . $post);
		}
		echo '
	</channel>
</rss>';
	}

	public function jsonDisplay()
	{
		echo json_encode($this->getCleanValues());
	}

	public function charactersDisplay($url)
	{
		if ($this->isEmpty())
			return;
		global $router;
		$persos = $normal = '';
		$modeNormal = $router->requestVar('mode') != 'adv';
		foreach ($this as $perso)
		{ /* @var $perso Character */
			$persos .= $perso->toTableRow();
			$normal .= tag('li', tag('b', '&bull;&nbsp;' . make_link($perso)));
		}
		$less = $normalParams = $url;
		$more = $normalParams + array(//show more (used only if JS is disabled)
			'mode' => 'adv',
		);
		$toggle = 'jQuery("#%s").slideUp(); jQuery("#%s").slideDown();';
		echo tag('div', array(
			'id' => 'normal',
			'style' => 'display: ' . ( $modeNormal ? 'block' : 'none' ) . ';',
		), tag('ul', $normal), js_link(sprintf($toggle, 'normal', 'adv'), lang('more'), to_url($more))),
		tag('div', array(
			'id' => 'adv',
			'style' => 'display: ' . ( $modeNormal ? 'none' : 'block' ) . ';'
		), tag('table', array(
			'border' => 1,
			'style' => 'width: 100%;',
		), $this->getTable()->getTableHeader() . $persos) . js_link(sprintf($toggle, 'adv', 'normal'), lang('less'), to_url($less)));
	}

	public function init()
	{
		if ($this->isEmpty())
			return;
		switch (get_class($this->getTable()))
		{
			case 'CharacterTable':
				return $this->_charsLoad();
				break;
		}
	}

	public static function charLoad($char)
	{
		$r = self::_charsInit();
		if (!in_array($char->guid, self::$charsInit))
		{
			self::$charsInit[] = $char->guid;
			jQ('cProfils[' . $char->guid . '] = \'' . $char->getInfoBox() . '\';');
		}
		return $r;
	}

	public static function _charsInit()
	{
		if (self::$charsInit !== null)
			return '';

		global $config;
		self::$charsInit = array();
		$js = '';
		$r = tag('div', array(
			'id' => 'c_profil',
			'style' => 'display: none;',
			'title' => lang('infos'),
		), '');

		jQ('cleanRequire("InfiniteCMS/controllers/Character/_collection");');
		return $r;
	}

	protected function _charsLoad()
	{
		foreach ($this as $perso)
		{
			/* @var $perso Character */
			if (!isset($r))
				$r = self::charLoad($perso);
			else
				self::charLoad($perso);
		}
		return $r;
	}

	public function process()
	{
		if ($this->isEmpty())
			return array();

		//armory:
		$itemsInfo = array(); //stock all stats of equiped items
		$pos = array_keys($this->getTable()->getPosOffset());
		foreach ($this as $item)
		{ //put in a array all items equiped, by the pos.
			/* @var $item Item */
			if (in_array($item->pos, $pos))
				$itemsInfo[$item->pos] = array($item, $item->getTooltip());
		}
		return $itemsInfo;
	}
}
