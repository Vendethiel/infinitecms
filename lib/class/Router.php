<?php

define('ROOT_CONTROLLERS', ROOT . 'app/controllers/');

/**
 * makes all the route business and all logic
 *
 * @file $Id: Router.php 53 2011-01-15 11:11:37Z nami.d0c.0 $
 */

//I wanna throw all this into my trash.
class Router extends Multiton
{
	//no prefix since they're the only constants !
	const GET = 'GET',
		POST = 'POST',
		PUT = 'PUT',
		PATCH = 'PATCH',
		DELETE = 'DELETE';

	static protected $methodAliases = array(
		self::PATCH            => self::PUT,
	),
		$verbs = array(
			true => array( //id is present
				self::PUT	   => 'update',
				self::DELETE   => 'delete', 
				self::GET	   => 'show',	
			),
			false => array(    //id is missing
				self::POST	   => 'update',
				self::GET	   => 'index',
			),
		),
		$mimes = array(
			'application/json' => 'json',
			'text/javascript'  => 'js',
		);

	protected $def_controller = 'Index',
	$def_ext = ASSET_HTML,
	$replaces = array(
		'ext' => array(
			'/' => EXT, //fallback value
		),
	),
	$_routes = array(),
	$_replacements = array(),
	$_controller = false,
	$_action = false,
	$_ext = false,
	$_use = array(),
	$_alias = null,
	$_stockR = null,
	$_isRoute = false,
	$_method = self::GET;

	protected function parseFile($file)
	{
		//caching for around 8ms
		$f = 'cache/routes.php';
		if (!file_exists('cache'))
			mkdir('cache');
		if (file_exists($f))
		{
			$values = include $f;
			list($this->_use, $this->_alias, $this->_replacements) = $values;
			return;
		}

		try
		{
			/** Personnal note:
			 * If you want to customize the CMS, and if your skills are not good as Rasmus Ledorf's,
			 *  I recommend you to skip this module and to go in any other file.
			 * SimpleXMElement is the biggest SHIT I've ever seen in my entire dev life
			 * between bugs, bugged comportement and unnexistant/undocumented functions ... You can't do anything
			 *  without losing at least 1h. Believe me, you should go further.
			 * Oh, and, BTW. If you want to do ANY THING WITH IT. NEVER FORGET TO TYPECAST ! (ie (string) $x)
			 */
			//wanna ask "why is it located under lib/ ?" Well, I dunno. at all
			//I SAID I WON'T TOUCH THAT CRAP ANYMORE.
			$file = new SimpleXMLElement($file, null, true); //open a file
		}
		catch (Exception $e)
		{
			exit(sprintf('Router error: %s', $e->getMessage()));
		}

		//register rewrites
		foreach ($file->rewrites[0] as $k => $types)
		{ //foreach all rewrite rules
			foreach ($types as $t => $type)
			{
				if (!isset($this->_use[$t]))
					$this->_use[$t] = $this->_alias[$t . 's'] = array(); //new type

				$attributes = $type->attributes();
				$name = (string) $attributes['name'];
				foreach ($type as $n => $value)
				{ //all aliases for this type
					$attrs = $value->attributes();
					$value = (string) $value;

					if ((bool) $attrs['use'])
						$this->_use[$t][$name] = $value; //default?

					$this->_alias[$t . 's'][$value] = $name;
				}
			}
		}

		//register replacements 
		foreach ($file->replacements[0] as $repl)
		{
			$attributes = $repl->attributes();
			$name = (string) $attributes->name;
			$name_parts = explode('.', $name);
			$values = $default = array();

			foreach ($repl as $k => $v)
			{
				if ($k == '__default')
					throw new RuntimeException('Trying to use reserved keyname : __default');

				$attrs = $v->attributes();
				if ($attrs->default)
					$default[$k] = (string) $attrs->default;

				$e = $v = (string) $v;
				if (strpos($v, '{') !== false)
				{ // process {0}, {1}, ... {n}
					$v = str_replace('{0}', $name, $v);
					foreach ($name_parts as $i => $part)
						$v = str_replace('{' . ($i + 1) . '}', $part, $v);
				}

				$values[$k] = $v;
			}
			$values['__default'] = $default;
			$this->_replacements[$name] = $values;
		}

		file_put_contents($f, '<?php return ' . var_export(array($this->_use, $this->_alias, $this->_replacements), true) . ';');
	}

	protected function init()
	{
		global $config;

		if ($this->_stockR === null)
			$this->_stockR = $_REQUEST; //we stock a copy

		$this->parseFile('lib/rewrite.xml');

		//next step ..
		//method
		$this->resolveMethod();

		if (isset($_SERVER['REQUEST_URI']))
			$this->resolveURI();
		
		if (empty($_REQUEST['action']))
			$this->resolveAction();

		$types = array('controller', 'action', 'ext');
		foreach ($types as $type)
		{
			$t = $this->requestVar($type, $this->{'def_' . $type});
			if (empty($t) || false !== strpos($t, '/'))
				$t = $this->{'def_' . $type};
			if (isset($this->replaces[$type][$t]))
				$t = $this->replaces[$type][$t];
			if (isset($this->_alias[$type . 's'][$t]))
				$t = $this->_alias[$type . 's'][$t];
			$this->{'_' . $type} = $t;
		}

		if (substr($this->getAction(), 0, 1) == '_')
			$this->_action = null;

		if (!isset($_REQUEST['ext']))
			$this->resolveExt();

		$this->resolveController();
	}

	protected function resolveURI()
	{
		//create the URL from the request
		$dir = dirname($_SERVER['SCRIPT_NAME']);
		$file = basename($_SERVER['SCRIPT_NAME']);

		$uri = ltrim(substr($_SERVER['REQUEST_URI'], strlen($dir)), '/');
		if (substr($uri, 0, strlen($file)) == $file)
			$uri = ltrim(substr($uri, strlen($file)), '/');


		$real_uri = explode('?', $uri);
		$real_uri = $real_uri[0];

		if (false !== $last_dot_pos = strrpos($real_uri, '.'))
		{
			$_REQUEST['ext'] = $ext = substr($real_uri, $last_dot_pos + 1);
			$real_uri = substr($real_uri, 0, $last_dot_pos); //-1 for leading dot
		}

		$params = explode('/', trim($real_uri, '/'));
		$key = null;
		$paramsCount = count($params);

		foreach ($params as $i => $part)
		{
			if ($part === '')
				continue;

			if ($i === 0) //first
				$_REQUEST['controller'] = $part;
			else if ($i === 1) //second
			{
				if (is_numeric($part) && ( isset($params[2]) ? !is_numeric($params[2]) : true ))
					$_REQUEST['id'] = $part;
				else
					$_REQUEST['action'] = $part;
			}
			else
			{
				if ($i === 2 && isset($_REQUEST['id']) && !is_numeric($part))
					$_REQUEST['action'] = $part;
				else if ($i === 2 && is_numeric($part) && $paramsCount < 5) //controller, action, id[, mode]
					$_REQUEST['id'] = $part;
				else if (isset($_REQUEST['id']) && $i === 3 && $paramsCount == 4)
					$_REQUEST['mode'] = $part;
				else if ($key === null)
					$key = $part; //next key
				else
				{ //pair key=value
					$_REQUEST[$key] = $part;
					$key = null;
				}
			}
		}
	}

	protected function resolveMethod()
	{
		//XXX: this should be simpler
		if (!empty($_POST['_method']) && ( $m = strtoupper($_POST['_method']) )
		 && $m != self::GET && preg_match('`[A-Z]+`', $m) && defined('self::' . $m))
			$this->_method = constant('self::' . $m);
		else if (!empty($_SERVER['REQUEST_METHOD']) && ( $m = $_SERVER['REQUEST_METHOD'] )
		 && $m != self::GET && preg_match('`[A-Z]+`', $m) && defined('self::' . $m))
			$this->_method = constant('self::' . $m);
		else
			$this->_method = self::GET;

		if (isset(self::$methodAliases[$this->_method]))
			$this->_method = self::$methodAliases[$this->_method];
	}
	
	protected function resolveAction()
	{
		$has = isset($_REQUEST['id']);
		$_REQUEST['action'] = self::getActionForVerb(isset($_REQUEST['id']), $this->getMethod());
	}

	protected function resolveExt()
	{
		if (!isset($_SERVER['HTTP_ACCEPT']))
			return;

		$accepts = explode(',', $_SERVER['HTTP_ACCEPT']);
		foreach ($accepts as $accept)
		{
			if (false !== $p = strpos($accept, ';'))
				$accept = substr($accept, 0, $p);
			$accept = trim($accept);

			if (isset(self::$mimes[$accept]))
			{
				$this->_ext = $_REQUEST['ext'] = self::$mimes[$accept];
				return;
			}
		}
	}

	public function resolveController()
	{ //forbids passing parameters 
		$this->controller = $this->_resolveController();
	}

	protected function _resolveController($c=null, $a=null, $e=null)
	{
		if (null === $c) $c = $this->_controller;
		if (null === $a) $a = $this->_action;
		if (null === $e) $e = $this->_ext;

		try
		{
			$instance = $this->createController($c,$a,$e);

			if (!$instance->isAccessible())
				throw new Router\Exception\NotFound();

			return $instance;
		}
		catch (Router\Exception\NotFound $e)
		{
			return new Controller\Type\NotFound(array());
		}
	}

	protected function createController($c,$a,$e)
	{
		$params = array($c,$a,$e);

		if (file_exists(ROOT_CONTROLLERS . $c . EXT))
			return new Controller\Type\Controller($params);
		else if (file_exists(ROOT_CONTROLLERS . $c . DS . $a . '.' . $e . EXT))
			return new Controller\Type\Action($params);

		throw new Router\Exception\NotFound();
	}

	public function getControllerInstance()
	{
		return $this->controller;
	}


	static public function getActionForVerb($has, $verb = '')
	{
		return isset(self::$verbs[$has][$verb]) ? self::$verbs[$has][$verb] : self::$verbs[$has][self::GET];
	}

	/**
	 * isAjax
	 *
	 * @return boolean Request from AJaX ?
	 */
	public function isAjax()
	{
		return isset($_SERVER['HTTP_X_REQUESTED_WITH'])
		&& strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
	}

	public function isRoute()
	{
		return $this->_isRoute;
	}

	/**
	 * postVar
	 * return a key from _POST
	 *
	 * @param string $n name of the key
	 * @param mixed $d Default value
	 * @return mixed value of the key or $d
	 *
	 * @static
	 * @access public
	 */
	public function postVar($n, $d=null)
	{
		return isset($_POST[$n]) ? $_POST[$n] : $d;
	}

	/**
	 * postVars
	 * return an array with keys from _POST
	 *
	 * @param string [func_get_args] keys
	 * @return array Associative array.
	 */
	public function postVars()
	{
		$args = func_get_args();
		$values = array();
		foreach ($args as $arg)
		{
			$values[$arg] = $this->postVar($arg);
		}
		return $values;
	}

	/**
	 * getVar
	 * return a key from _GET
	 *
	 * @param string $n name of the key
	 * @param mixed $d Default value
	 * @return mixed value of the key or $d
	 *
	 * @access public
	 */
	public function getVar($n, $d=null)
	{
		return isset($_GET[$n]) ? $_GET[$n] : $d;
	}

	/**
	 * getVars
	 * return an array with keys from _GET
	 *
	 * @param string [func_get_args] keys
	 * @return array Associative array.
	 */
	public function getVars()
	{
		$args = func_get_args();
		$values = array();
		foreach ($args as $arg)
		{
			$values[$arg] = $this->getVar($arg);
		}
		return $values;
	}

	/**
	 * requestVar
	 * return a key from _REQUEST
	 *
	 * @param string $n name of the key
	 * @param mixed $d Default value
	 * @return mixed value of the key or $d
	 *
	 * @access public
	 */
	public function requestVar($n, $d=null)
	{
		return isset($_REQUEST[$n]) ? $_REQUEST[$n] : $d;
	}

	public function requestVars()
	{
		$args = func_get_args();
		$values = array();
		foreach ($args as $arg)
			$values[$arg] = $this->requestVar($arg);
		return $values;
	}

	//little bit useless
	public function getDefault($for)
	{
		return $this->{'def_' . strtolower(preg_replace('`([A-Z])`', '_$1', lcfirst($for)))};
	}

	public function getDefaults()
	{
		$defaults = array();
		
		foreach (array('controller', 'action', 'ext') as $type)
			$defaults[] = $this->getDefault($type);

		return $defaults;
	}

	public function getController()
	{
		return $this->_controller;
	}
	public function getAction()
	{
		return $this->_action;
	}
	public function getExt()
	{
		return $this->_ext;
	}

	public function getControllerUse($c)
	{
		if (isset($this->_use['controller'][$c]))
			return $this->_use['controller'][$c];
		return $c;
	}
	public function getActionUse($a)
	{
		if (isset($this->_use['action'][$a]))
			return $this->_use['action'][$a];
		return $a;
	}
	public function getExtUse($e)
	{
		if (isset($this->_use['ext'][$e]))
			return $this->_use['ext'][$e];
		return $e;
	}

	public function getControllerAlias($c)
	{
		if (isset($this->_alias['controllers'][$c]))
			return $this->_alias['controllers'][$c];
		return $c;
	}
	public function getActionAlias($a)
	{
		if (isset($this->_alias['actions'][$a]))
			return $this->_alias['actions'][$a];
		return $a;
	}
	public function getExtAlias($e)
	{
		if (isset($this->_alias['exts'][$e]))
			return $this->_alias['exts'][$e];
		return $e;
	}

	public function formateExt($ext)
	{
		if (isset($ext[0]) && $ext[0] === '.')
			return substr($ext, 1);
		else
			return $ext;
	}

	public function getInfos()
	{
		return array(
			'controller' => $this->getController(),
			'action' => $this->getAction(),
			'ext' => $this->getExt(),
		);
	}

	public function getPath($c = null, $a = null, $e = null, $action = true)
	{
		$c = null === $c ? $this->getController() : $c;
		$a = null === $a ? $this->getAction() : $a;
		$e = null === $e && false !== $e ? $this->getExt() : $e;
		return ( $action ? ROOT_CONTROLLERS : getPath() ) . $c . DS . $a . ( false === $e ? '' : '.' . $e ) . EXT;
	}

	public function getReplacement($name)
	{
		if (isset($this->_replacements[$name]))
			return $this->_replacements[$name];
		return null;
	}
	public function getRoute($name)
	{
		if (isset($this->_routes[$name]))
			return $this->_routes[$name];
		return null;
	}


	public function getMethod()
	{
		return $this->_method;
	}
	public function isMethod($method)
	{
		if (isset(self::$methodAliases[$method]))
			$method = self::$methodAliases[$method];

		return $this->_method == $method;
	}
	public function isGet()
	{
		return $this->isMethod(self::GET);
	}
	public function isPost()
	{
		return $this->isMethod(self::POST);
	}
	public function isPut()
	{
		return $this->isMethod(self::PUT) || $this->isMethod(self::PATCH);
	}
	public function isPatch()
	{
		return $this->isPut();
	}
	public function isDelete()
	{
		return $this->isMethod(self::Delete);
	}

	public function code($code)
	{
		throw new Router\Exception\Code($code);
	}
	public function codeIf($code, $if)
	{
		if ($if)
			$this->code($code);
	}
	public function codeUnless($code, $unless)
	{
		return $this->codeIf($code, !$unless);
	}
}

			/*
			//register routes
			foreach ($file->routes[0] as $route)
			{ //foreach all routes
				$attributes = $route->attributes();
				$name = (string) $attributes->name;

				$a = $e = $c = null;

				$replace = $route->replace[0];
				foreach ($replace as $k => $v)
					$r[$k] = (string) $v;

				if (( $find = $route->find[0] ) && ( $c = $this->getControllerAlias((string) $find->controller) ) != '')
				{
					if (isset($alias['controllers'][$c]))
						$c = $alias['controllers'][$c];
					$f = $c . '|';

					if (isset($find->action))
					{
						$a = $this->getActionAlias((string) $find->action);
						$f .= $a . '|';
						if (isset($find->ext)) //can only spec. a ext if a action is spec.
						{
							$e = $this->getExtAlias((string) $find->ext);
							$f .= $e;
						}
						else
							$f .= '-1';
					}
					else
						$f .= '-1|-1';

					//default or submitted ?
					$r['controller'] = $c === null ? $this->getControlerUse($this->def_controller) : $e;
					$r['action'] = $a === null ? $this->getActionUse($this->def_action) : $a;
					$r['ext'] = $e === null ? $this->getExtUse($this->def_ext) : $e;

					$this->_routes[$f] = $r;
				}
				if (!empty($name))
					$this->_routes[$name] = $r;
			}
			*/