<?php

class FileUploader
{
	protected $path,
				$maxSize = 0,
				$allowedTypes = array(),
				$allowedExts;

	public function __construct($path)
	{
		$this->path = $path;
	}
	public function setMaxSize($size)
	{
		$this->maxSize = $size;
	}
	public function addAllowedTypes($types)
	{
		$this->allowedTypes = array_unique(array_merge($this->allowedTypes, $types));
	}
	public function addAllowedExts($exts)
	{
		$this->allowedExts = array_unique(array_merge($this->allowedExts, $exts));
	}
	protected function processError($error)
	{
		global $errors;
		switch ($error)
		{ //@todo lang()
			case UPLOAD_ERR_INI_SIZE:
				$errors[] = 'more than ini-defined size';
			break;
			case UPLOAD_ERR_FORM_SIZE:
				$errors[] = 'more than form-defined size';
			break;
			case UPLOAD_ERR_PARTIAL:
				$errors[] = 'file upload interrupted';
			break;
			case UPLOAD_ERR_NO_FILE:
				$errors[] = 'file looks empty';
			break;
		}
	}
	public function validate(array $file)
	{
		global $errors;

		if ($file['error'])
			$this->processError($file['error']);
		else if (!in_array($file['type'], $this->allowedTypes))
			$errors[] = lang('bad file type' . escape_html($file['type']));
		else if ($file['size'] > $this->maxSize)
			$errors[] = sprintf('Must not be more than %s, is %s', filesize_format($size), filesize_format($file['size']));
		else
		{
			$name = strtolower($file['name']);

			$allowed_ext = false;
			foreach ($this->allowedExts as $ext)
			{
				if (substr($name, -strlen($ext)) == $ext)
				{
					$allowed_ext = true;
					break;
				}
			}

			if ($allowed_ext)
			{
				$filename = str_replace('.', '_', $name);
				do
				{
					$filename = rand(1, 50) . $filename;
				}
				while (file_exists($path = $this->path . $filename));
			}
			else
				$errors[] = lang('bad_ext');
		}

		if (empty($errors))
			move_uploaded_file($file['tmp_name'], $path);

		return $path;
	}
}