<?php
/**
 * adds more possibility to the basic Doctrine_Record class
 *
 * @file $Id: Record.php 20 2010-09-24 08:51:22Z nami.d0c.0 $
 *
 * @extends Doctrine_Record
 */
class Record extends Doctrine_Record
{
	public function update(array $values, $save = true, $extra = null)
	{
		global $errors;
		$this->update_attributes($values, $extra);

		if (empty($errors) && $save)
			$this->doSave();

		return empty($errors);
	}

	public function doSave()
	{ //saving strategy may change (NestedSet and suchs)
		$this->save();
	}

	public function isNew()
	{
		return !$this->exists();
	}

	/**
	 * Processes a column for the record
	 *
	 * @param mixed $column The column : callable or string. Use "x:default". Use "%%" to make a link, you can use both : "%%record%%:Anonymous"
	 * @param mixed $value @see $this->columnValues
	 * @return string
	 */
	public function resolveProperty($column, $cValue = null)
	{
		$attribute = explode(':', $column);
		$column = $attribute[0];
		$fallback = isset($attribute[1]) ? lang($attribute[1], null, '%%key%%') : '';

		$method = '';
		$args = array();
		if (substr($column, 0, 2) == $apply = substr($column, -2))
		{
			switch ($apply)
			{
				case '%%':
					$method = 'make_link';
					$args = array('ret', 'text');
				break;
			}

			$column = substr($column, 2, -2);
		}

		$column = explode('=', $column);
		if (isset($column[1]))
			$text = $column[1];
		else
			$text = null;

		$column = $column[0];

		if ($ret = $this->resolvePropertyPath($column, $fallback))
		{
			if ($method)
			{
				foreach ($args as &$arg)
				{
					switch ($arg)
					{
						case 'record':
							$arg = $this;
						break;
						case 'ret':
							$arg = $ret;
						break;
						case 'text':
							$arg = $text ? $this->resolvePropertyPath($text) : $text;
						break;
					}
				}
				return call_user_func_array($method, $args);
			}
			else
				return $ret;
		}
		return $fallback;
	}

	/**
	 * sub-routine for processColumnFor()
	 *
	 * @param mixed $attribute closure or string : may be like "record.Account.pseudo"
	 *  The CMS will try to look for a "get..." method, but you can force it to not do so by appending ! :
	 *  "record.Account.MainChar" => record->Account->getMainChar();
	 *  "record.GuildMember.rights!" => record->GuildMember->rights;
	 *  "record.GuildMember.rights" => record->GuildMember->getRights()
	 * @param array $record [base: ..., record: ...]
	 * @param mixed $fallback default return value
	 */
	protected function resolvePropertyPath($attribute)
	{ //link ->add('=b')
		if ($attribute == '')
			return $this;

		$parts = explode('.', $attribute);
		$rec = $actual = $this;
		foreach ($parts as $part)
		{
			$columnNames = $actual->getTable()->getColumnNames();
			if (!in_array($part, $columnNames) && $actual->hasRelation($part) && !$actual->relatedExists($part))
				return null;
			else
				$actual = $actual->resolveGetter($part);
		}
		return $actual;
	}

	protected function resolveGetter($column)
	{
		return method_exists($this, $getter = 'get'. ucfirst($column)) ?
		 $this->$getter() : $this[substr($column, -1) == '!' ? substr($column, 0, -1) : $column];
	}
}