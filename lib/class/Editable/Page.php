<?php
namespace Editable;

/*
 * fully editable page through MercuryEditor
 */
class Page
{
	private $content,
			$saveCallback,
			$vars,
			$link;

	/**
	 * @var string $content current content
	 * @var callable $saveCallback callback to call on save
	 * @var array $vars
	 *  * link: edit link
	 *  * ...others: variables to be replaced in the display
	 */
	public function __construct($content, $saveCallback, array $vars = array())
	{
		global $router;

		$this->content = $content;
		$this->saveCallback = $saveCallback;

		if (null === $this->link) // no @vars for new PHP strtr()  ...
			$this->link = array('controller' => $router->getController(), 'action' => $router->getAction(), 'update' => true);
		if (isset($vars['link'])) { // extract link out ...
			$this->link = $vars['link'];
			unset($vars['link']); // just in case ...
		}
		$this->vars = $vars;
	}

	public function __toString()
	{
		global $router;

		$update = false;
		if (level(LEVEL_ADMIN) && $router->requestVar('update'))
		{
			if ($router->isPost() && $c = $router->postVar('content'))
			{
				$this->content = $c;
				call_user_func($this->saveCallback, $c); //redirect ? works great like that
			}
			else
			{
				$update = true;

				if ($this->vars)
				{
					echo tag('div', array('align' => 'left')) . tag('h3', lang('variables')), tag('ul');
					foreach ($this->vars as $var => $value)
						echo tag('li', tag('b', $var) . ' => ' . tag('i', $value));
					echo '</ul></div>';
				}
			}
		}

		return $update ?
			make_form(array(array('content', lang('content'), 'textarea', $this->content)), $this->link) :
			(level(LEVEL_ADMIN) ? make_link($this->link, lang('act._edit'), array(), array(), false) : '') .
			 strtr($this->content, $this->vars);
	}
}