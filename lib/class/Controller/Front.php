<?php
namespace Controller;

class Front
{
	private $controller;

	public function __construct($controller)
	{
		$this->controller = $controller;
	}
	public function getController()
	{
		return $this->controller;
	}

	private function processHeaders()
	{
		if (defined('HEADERS_SENT')) //self-defined headers
			return;

		header('Content-type: text/html; charset=UTF-8'); //UTF-8 is our MASTER §
	}

	private function processRender($title, $data, $headers)
	{
		global $router, $config;

		register_variable('title_basic', $title);
		$title = strtr(str_replace('{page}', $title, $config['TITLE']), array(
			'{server.name}' => $config['SERVER_NAME'],
			'{server.corp}' => $config['SERVER_CORP'],
		));
		register_variable('title', $title);
		register_variable('data', $data);

		$ext = $this->controller->getTemplateExt();
		if ($ext != 'html' && $ext != 'xml')
			exit($data);

		if ($router->isAjax())
			$this->processTemplateAjax($title);
		else
			$this->processTemplate($title);
	}

	private function processTemplate($title)
	{
		global $mem, $router, $view_pipeline, $view_pipeline;
		dbg_step('before layout');
		
		register_variable('stylesheets', stylesheets());

		$minify = function ($str) {
			return preg_replace('`\s+`', ' ', str_replace("\n", "", $str));
		};

		$content = $view_pipeline($this->controller->getTemplateExt(), $this->controller->getTemplate(), register_variable());
		echo MINIFY ? $minify($content) : $content;

		dbg_step('after layout');
	}

	private function processTemplateAjax($title)
	{
		global $member, $config, $account, $router, $view_pipeline;

		echo implode('<~>', array($title, getPath(), server_state(),
		 ($member->isConnected() ? $account->User->getNextPMNotif() : ''), defined('UPDATE_SELECTOR') ? UPDATE_SELECTOR : '', ''));

		echo trim($view_pipeline($this->controller->getTemplateExt(), $this->controller->getTemplate(), register_variable()));

		if ($config['JAVASCRIPT'])
		{
			echo javascript_extras();

			$jQ = trim(jQ());
			echo tag_js('
			setTimeout(function () {
				' . $jQ . '
				controller = "' . $router->getController() . '";
				action = "' . $router->getAction() . '";
				cleanRequire();
			});');
		}
	}

	public function process()
	{
		global $output, $router;

		if (!isset($output))
			$output = (bool) $router->requestVar('output', 1);
		$headers = !$router->requestVar('raw', 0); //false to only display content

		try
		{
			list($title, $data) = $this->processController();

			//returns false => no layout
			if (!$this->controller->getTemplate())
				$headers = false;

			if ($output)
			{
				$this->processHeaders();

				dbg_step('Render: before');

				$this->processRender($title, $data, $headers);

				dbg_step('Render: after');
			}
		}
		catch (Exception $e)
		{
			if (DEBUG)
				exit('Problems with page loading : ' . $e->getMessage() . '<br />' .
				 $e->getTraceAsString());
			else
				exit('Problems during page loading. Please contact the server admin.');
		}
	}

	protected function processController()
	{
		global $router, $account, $config, $view_pipeline,
			$title, $mem, $output, $headers, $errors;

		if (!empty($config['MAINTENANCE']))
		{
			return array(
				lang('maintenance'),
				tag('h3', array('style' => array('color' => 'red')), lang('maintenance', 'user'))
			);
		}

		if (level(LEVEL_LOGGED) && $account->banned)
			exit('no way u bant.');

		//we can access to the page (/actions/Module/_include.php) and the accunt is not banned
		try
		{
			dbg_step('before controller:process (if accessible)');
			
			if ($this->controller->isAccessible()) //"access allowed"
			{
				$content = (string) $this->controller->process(); // may change $title
				return array($title, $content);
			}
			else
				throw new \Router\Exception\NotFound;

			dbg_step('after controller:process');
		}
		catch (\Router\Exception\Code $e)
		{
			switch ($e->getHttpCode())
			{
				case 404:
					return array(
						lang('error.404'),
						tag('br') .
						 tag('p', tag('h1', array('align' => 'center', 'style' => 'color: red;'), lang('error.404')))
					);
				break;

				case 301:
					return array(
						sprintf('(%s)', lang('unknow')),
						lang('auth.not_needed_level') . make_link('@root', lang('back_to_index'))
					);
				break;
			}
		}
	}
}