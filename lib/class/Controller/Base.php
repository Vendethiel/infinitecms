<?php
namespace Controller;

use Closure;
use Router\Exception;

abstract class Base
{
	protected $ext,
		$filters = array(),
		$state = 0;

	public function __construct($ext)
	{
		$this->ext = $ext;

		$this->init();
	}

	//use it to bind filters
	protected function init()
	{ }

	public function dispatch($action)
	{
		$method = $action . 'Action';

		$this->state = 1;

		$this->processBeforeFilters();
		$result = $this->processAroundFilters($this->$method());
		$this->processAfterFilters();

		$this->state = 2;

		return $result;
	}

	protected function getParameter($v,$d=0)
	{ //@todo inject router
		global $router;
		return $router->requestVar($v,$d);
	}

	//should be protected (can't because of $this closure workaround)
	public function requireLevel($level, $lt = false)
	{
		if ($this->state == 0)
		{
			$that = $this; //php < 5.4 workaround
			$this->addBeforeFilter(function () use ($level, $lt, $that) { $that->requireLevel($level, $lt); });
			return;
		}

		if (!\check_level($level, $lt, true))
			$this->code(301);
	}

	protected function code($code)
	{
		if ($this->state == 0)
		{
			$this->addBeforeFilter(function ()  { $this->code($code); });
			return;
		}

		global $router;
		$router->code($code);
	}
	protected function codeIf($code, $cond)		{ if ($cond) $this->code($code); }
	protected function codeUnless($code, $cond)	{ if (!$cond) $this->code($code); }

	private function addFilter($name, $type)
	{
		$this->filters[$type][] = $name;
	}
	protected function addBeforeFilter($name)	{ $this->addFilter($name, 'before'); }
	protected function addAroundFilter($name)	{ $this->addFilter($name, 'around'); }
	protected function addAfterFilter($name)	{ $this->addFilter($name, 'after'); }

	private function processBeforeFilters()
	{
		if (empty($this->filters['before']))
			return;

		foreach ($this->filters['before'] as $filter)
		{
			if ($filter instanceof Closure)
				$filter();
			else
				call_user_func(array($this, $filter));
		}
	}
	private function processAroundFilters($text)
	{
		if (empty($this->filters['around']))
			return $text;

		foreach ($this->filters['around'] as $filter)
		{
			if ($filter instanceof Closure)
				$filter($text);
			else
				$text = call_user_func(array($this, $filter), $text);
		}

		return $text;
	}
	private function processAfterFilters()
	{
		if (empty($this->filters['after']))
			return;

		foreach ($this->filters['after'] as $filter)
		{
			if ($filter instanceof Closure)
				$filter();
			else
				call_user_func(array($this, $filter));
		}
	}

	public function getExt()
	{
		return $this->ext;
	}
}