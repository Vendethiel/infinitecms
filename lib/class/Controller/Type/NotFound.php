<?php
namespace Controller\Type;

class NotFound extends Base
{
	public function isAccessible()
	{
		return false;
	}

	public function __construct(array $params)
	{
		$this->params = array('', '', '');
	}

	public function getTemplate()
	{
		return $this->getLayout();
	}
}