<?php
namespace Controller\Type;

class Controller extends Base
{
	private $instance;

	public function __construct(array $params)
	{
		$this->setParams($params);

		global $router;

		$class = 'Controller\\' . $this->getController();

		include ROOT_CONTROLLERS . $this->getController() . EXT;
		$this->instance = new $class($params[1]);
	}

	public function isAccessible()
	{
		if (!parent::isAccessible())
			return false;

		if (!method_exists($this->instance, $this->getAction() . 'Action'))
			return false;

		return true;
	}

	public function getResult()
	{
		return $this->instance->dispatch($this->getAction());
	}

	public function process()
	{
		global $router;
		$return = null;

		switch (strtolower(gettype($result = $this->getResult())))
		{
			case 'array':
				foreach ($result as $k => $v)
					register_variable($k, $v);
			break;
			
			case 'string':
				$return = $result;
			break;

			case 'null':
				$this->template = false;

				global $output; //blank page
				$output = false;
			break;

			default:
				throw new InvalidArgumentException('Controller action must return array OR null OR string OR boolean');
		}

		return $return;
	}
}