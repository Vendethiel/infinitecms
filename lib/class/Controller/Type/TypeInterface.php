<?php
namespace Controller\Type;

interface TypeInterface
{
	public function __construct(array $params);
	public function isAccessible();
	public function process();
}