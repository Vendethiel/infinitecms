<?php
namespace Controller\Type;

class Action extends Base
{
	public function process()
	{
		$this->processIncludes();
		dbg_step('after process includes');
		return $this->processAction();
	}

	public function getPath()
	{
		return ROOT_CONTROLLERS . $this->getController() . DS . $this->getAction() . '.' . $this->getExt() . EXT;
	}

	private function processIncludes()
	{
		global $router, $config, $account;
		
		$file_include = $router->getPath(null, '_shared', false);
		if (file_exists($file_include) && substr($file_include, -1) != DS)
			require_once $file_include;

		foreach (glob(ROOT . 'app/controllers/*/_each' . EXT) as $each)
			include $each;
	}

	protected function getTemplateWithBaseExtension()
	{
		return false;
	}

	protected function getLayout()
	{
		global $view_pipeline;

		$layout = parent::getLayout();

		if ($view_pipeline->getLocator()->hasFile($layout, $this->getTemplateExt()))
			return $layout;
		else
			return false;
	}

	private function processAction()
	{
		//here are the variables that must be available inside the actions
		global $router, $account, $config, $member, $title, $mem, $headers, $errors;

		ob_start();

		jQ(false, 'cache'); //enter cache mode
		require $this->getPath();
		jQ(false, 'main'); //go back to main mode
		if (( $jQcache = trim(jQ(null, 'cache')) ) != '')
			jQ('/*cache:begin*/' . $jQcache . '/*cache:end*/'); //add remaining "cache"[JS] to main

		return ob_get_clean();
	}
}