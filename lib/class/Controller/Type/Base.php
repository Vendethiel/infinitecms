<?php
namespace Controller\Type;

abstract class Base implements TypeInterface
{
	private $params, $template, $template_ext;

	protected function setParams($p)	{ $this->params = $p; }
	protected function getParams()		{ return $this->params; }
	protected function getParam($i)		{ return $this->params[$i]; }

	public function getController()	{ return $this->params[0]; }
	public function getAction()		{ return $this->params[1]; }
	public function getExt()		{ return $this->params[2]; }

	public function __construct(array $params)
	{
		$this->params = $params;
	}

	public function isAccessible()
	{
		global $router;

		if (defined('HTTP_CODE') && HTTP_CODE != 200)
			return false;
		if (defined('LEVEL_FALLBACK'))
			return false;

		if (null === $this->params[0]
		 || null === $this->params[1]
		 || null === $this->params[2])
			return false;
		return true;
	}

	protected function getTemplateWithBaseExtension()
	{
		global $view_pipeline;

		if ($view_pipeline->getLocator()->hasFile($this->getTemplateBasePath(), ASSET_HTML))
		{
			$this->setTemplateExt(ASSET_HTML);
			return $this->getTemplateBasePath();
		}
		return false;
	}

	protected function getLayout()
	{
		global $router;

		return 'layouts/layout' . ($router->isAjax() ? '_ajax' : '');
	}

	public function getTemplate()
	{
		global $view_pipeline;

		if (null !== $this->template)
			return $this->template;
		if (defined('TEMPLATE'))
			return $this->template = TEMPLATE;

		$ext = $this->getTemplateExt();
		if ($view_pipeline->getLocator()->hasFile($this->getTemplateBasePath(), $ext))
			return $this->template = $this->getTemplateBasePath();

		if ($this->getTemplateWithBaseExtension())
			return $this->template = $this->getTemplateWithBaseExtension();


		return $this->template = $this->getLayout();
	}

	public function getTemplateExt()
	{
		if ($this->template_ext)
			return $this->template_ext;
		
		if (defined('TEMPLATE_EXT'))
			return TEMPLATE_EXT;

		$ext = $this->getExt() ? $this->getExt() : ASSET_HTML;

		global $view_pipeline;
		$view_pipeline->getLocator()->setDefaultExtension($ext);

		return $ext;
	}

	protected function setTemplate($template)
	{
		$this->template = $template;
	}

	protected function setTemplateExt($template_ext)
	{
		$this->template_ext = $template_ext;
	}

	protected function getTemplateBasePath($ext = false)
	{
		if ($ext)
			return $this->getController() . '/' . $this->getAction() . '.' . $this->getExt();
		else
			return $this->getController() . '/' . $this->getAction();
	}

	public function process()
	{ }
}