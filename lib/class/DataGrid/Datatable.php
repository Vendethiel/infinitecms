<?php
namespace Datagrid;

class Datatable extends Behavior
{
	protected $options = array(
		'dataTable' => array(
			'bJQueryUI' => 1,
		),
	);

	public function onAfterRecords($key)
	{
		include_library(LIBRARY_DATATABLES);
		jQ('
jQuery.fn.dataTableExt.oSort["points-asc"] = function (x, y)
{
	x = parseInt(x.innerHTML);
	y = parseInt(y.innerHTML);
	return (x < y) ? -1 : ((x > y) ?  1 : 0);
}
jQuery.fn.dataTableExt.oSort["points-desc"] = function (x, y)
{
	x = parseInt(x.innerHTML);
	y = parseInt(y.innerHTML);
	return (x < y) ?  1 : ((x > y) ? -1 : 0);
}');
		jQ('$("#' . $this->dg->getOption('table_id') . '").dataTable(' .
		 json_encode($this->options['dataTable']) . ');');
	}
}