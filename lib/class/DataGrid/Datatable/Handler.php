<?php
namespace Datagrid\Datatable;

class Handler
{
	protected $table;

	public function __construct(\RecordTable $t)
	{
		$this->table = $t;
	}
	public function __toString()
	{ try {
		global $router;

		$options = $this->table->getColumnOptions();
		$c = $this->table->getClassnameToReturn();
		$alias = strtolower($c[0]);

		$q = $this->table
					->createQuery($alias);

		if (!$pk = (array) $this->table->getIdentifier())
			throw new Exception('You must have at least one identifier in your table !');
		$pk = $pk[0];

		$wouldQ = \Query::create()
						->select('COUNT(' . $pk . ') as count')
							->from($c . ' ' . $alias);

		if (!empty($options['join']))
		{
			foreach ($options['join'] as $k => $v)
			{
				$join = ( strpos($k, '.') === false ? $alias . '.' . $k : $k ) . ' ' . $v;
				$q->leftJoin($join);
				$wouldQ->leftJoin($join);
			}
		}


		$total = \Query::create()
						->select('COUNT(' . $pk . ') as count')
							->from($c)
						->fetchArray();
		$total = $total[0]['count'];

		$columns = $indexes = $realNames = $transforms = array();
		foreach ($options['column'] as $column)
		{
			$column = (array) $column;

			if (null != $column[0])
				$columns[] = $column[0];

			if (null == $column[0] && empty($column[1]))
				continue; //IGNORE MEH !
			
			if (empty($column[1]))
				$i = $indexes[] = $alias . '.' . $column[0];
			else
				$i = $indexes[] = $column[1];

			$realNames[$column[0]] = $i;

			if (isset($column[2]))
				$transforms[$column[0]] = $column[2];
			else
				$transforms[$column[0]] = '';
		}

		if (isset($_GET['iSortCol_0']) && !empty($_GET['iSortingCols']))
		{
			for ($i = 0, $l = intval($_GET['iSortingCols']); $i < $l; ++$i)
			{
				if ($router->requestVar('bSortable_' . $router->getVar('iSortCol_' . $i), 'f') == 'true'
				 && isset($indexes[$_GET['iSortCol_' . $i]]) && isset($_GET['sSortDir_' . $i]))
				{
					$order = $indexes[$_GET['iSortCol_' . $i]] . ' ' .
					 ( strtoupper($_GET['sSortDir_' . $i]) == 'ASC' ? 'ASC' : 'DESC' );
					$q->addOrderBy($order);
					$wouldQ->addOrderBy($order);
				}
			}
		}

		$where = $whereValues = array();
		if (!empty($_GET['sSearch']) && !empty($options['search']))
		{
			foreach ($options['search'] as $col)
			{
				$column = isset($realNames[$col]) ? $realNames[$col] : $col;

				$cond = $_GET['sSearch'];
				if ($transforms[$col])
				{
					$type = '=';
					$cond = call_user_func($transforms[$col], $cond);
				}
				else
				{
					$type = ' LIKE';
					$cond = '%' . $cond . '%';
				}

				$where[] = $column . $type . ' ?';
				$whereValues[] = $cond;
			}
		}
		if ($where)
		{
			$where = implode(' OR ', $where);

			$q->where($where, $whereValues);
			$wouldQ->where($where, $whereValues);
		}

		$where = $whereValues = array();
		foreach ($indexes as $i => $column)
		{
			if (isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == 'true'
			 && !empty($_GET['sSearch_'.$i]))
			{
				$where[] = $column . ' = ?';
				$whereValues[] = $_GET['sSearch_'.$i];
			}
		}
		if ($where)
		{
			$where = implode(' OR ', $where);

			$wouldQ->andWhere($where, $whereValues);
			$q->andWhere($where, $whereValues);
		}

		$would = $wouldQ->fetchArray();
		$would = $would[0]['count'];

		if (isset($_GET['iDisplayStart']) && isset($_GET['iDisplayLength']) && $_GET['iDisplayLength'] != '-1')
		{
			if (!empty($_GET['iDisplayStart']))
				$q->offset($_GET['iDisplayStart']);

			$q->limit($_GET['iDisplayLength']);
		}
		else
			$q->limit(20); //default max

		$return = array(
			'sEcho' => intval($_GET['sEcho']),
			'iTotalRecords' => $total,
			'iTotalDisplayRecords' => $would,
			'aaData' => array(),
		);

		$paths = $options['path'];
		foreach ($q->execute() as $record)
		{
			$data = array();
			foreach ($paths as $path)
				$data[] = $record->resolveProperty(isset($path[1]) ? $path[1] : $path[0]);
			$return['aaData'][] = $data;
		}

		/*
		if ($return['aaData'])
			$return['aaData'][0][0] .= '_holder_';
		*/
		} catch( \Exception $e) { vdump($e); } //:(

		return json_encode($return);
	}
}