<?php
namespace Datagrid;

abstract class Behavior
{
	protected $dg,
		$options = array();

	public function __construct(\Datagrid $dg, array $options = array())
	{
		$this->dg = $dg;
		$this->options = \Doctrine_Lib::arrayDeepMerge($this->options, $options);
	}

	public function onTableHeader($lang, $key)
	{
		return $lang;
	}
	public function onAfterRecords($html)
	{
		return $html;
	}
}