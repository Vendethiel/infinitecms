<?php
namespace Datagrid;

class Sortable extends Behavior
{
	protected $options = array(
		'key_column' => 'sort_by',
		'key_dir' => 'sort_dir',
	);

	public function onTableHeader($lang, $key)
	{
		global $router;

		if ($this->options['columns'] === true
		 || ( is_array($this->options['columns']) && in_array($key, $this->options['columns']) ))
		{
			$n = encode_url_param($key);

			if ($router->requestVar($this->options['key_column']) == $n)
			{
				$dir = $router->requestVar($this->options['key_dir']);
				$lang .= make_link(to_url(array_merge($this->dg->getOption('url'),
				 array($this->options['key_dir'] => $dir == 'ASC' ? 'DESC' : 'ASC'))),
				 ' ' . ( $dir == 'ASC' ? 'up' : 'down' ));
			}
			else
				$lang = make_link(to_url(array_merge($this->dg->getOption('url'),
				 array($this->options['key_column'] => $n, $this->options['key_dir'] => 'DESC'))), $key);
		}
		return $lang;
	}
}