<?php
$array_inputs = array('select', 'record', 'slider');

/**
 * generates an input
 *
 * @package Helper
 * @subpackage HTML
 *
 * @param string $name Name of input
 * @param string $label Label of input (treated as $value if $type == 'label')
 * @param string $type Type of input
 * @param string $value Value of input
 * @param array $add Attributes for input
 *
 * @return string label, input and it's attributes
 */
function input($name, $label = null, $type = null, $value = '', $add = array())
{
	global $array_inputs;

	$act_label = false;
	$args = func_get_args();
	$params = array(); //contains everything for the input
	$type = strtolower($type); //convention

	if (is_array($value) && $add === array()
	 && $type !== 'select' && $type !== 'date_range' && $type !== 'record')
	{ //replace $add && $value IF it's not a (select || date_range) 
		$add = $value;
		$value = '';
	}
	if (!$type)
	{ //by default
		$type = 'text';
	}
	if ($type === 'submit' && null !== $label)
	{ //If it's a submit and we have $value instead of $label
		$value = $label;
		$label = null;
	}

	if (in_array($type, $array_inputs))
	{
		if (!is_array($add))
		{ //if add is not array, then add is the default select's value. Then, try to take $add from $args[0] (the arg after $add)
			$selected = $add;
			$add = isset($args[5]) ? $args[5] : array();
		}
		if (isset($args[5]))
		{ //act as a label ?
			$act_label = (bool) $args[5];
		}
	}

	if (is_string($value))
		$value = str_replace('</', '&lt;/', $value); // remove special chars from $value

	//@todo move dat to switch
	if ($type === 'textarea')
	{
		if (!defined('FORM_TEXTAREA'))
		{
			/*
			 * hours spent trying to understand why I have to nest this $(function() { ... }); in the jQ() one :
			 *  ACTUALLY MORE THAN 5 HOURS (a lot more.)
			 * This bug has existed since the very start of the CMS, I tried to resolve it for hours and hours,
			 *  and I just figured out that this silly solution is working.
			 * You may also try to only put this JS at the very end (like jQ('tinymce blah'); echo jQ();),
			 *  it wouldn't work. Ok anyway, it's resolved now, I won't touch that crap anymore.
			 *
			 * NOTE : this was probably related to shared HTML IDs ...
			 */
			if (level(LEVEL_ADMIN))
				jQ('$("textarea").tinymce(tinyMceOpt);');
			define('FORM_TEXTAREA', true);
		}

		//rand used for tinyMCE
		$type = tag('textarea', $add + array(
			'name' => $name,
			'id' => 'form_' . $name . '_' . rand(1, 150)
		), str_replace('</textarea', '', $value));
	}
	else if ($type === 'select' || $type == 'record')
	{
		if ($type == 'record')
			$value = input_select_record($value);

		if (!isset($selected))
			$selected = null;
		$type = tag('select', array('name' => $name, 'id' => 'form_' . $name), input_select_options($value, $selected));
	}
	else
	{
		if (is_string($value) && false !== strpos($value, '<'))
			$value = str_replace('"', '', $value); 

		$pre_html = $post_html = '';
		$params = array('type' => $type, 'name' => $name, 'value' => $value);

		if (function_exists($fn = 'input_' . $params['type']))
		{
			$params['label'] = $label;
			$fn($params);
			$label = $params['label'];
		}

		foreach (array('post_html', 'pre_html', 'label') as $f)
		{
			if (isset($params[$f]))
				$$f = $params[$f];
			unset($params[$f]);
		}

		$params['id'] = 'form_' . $params['name'];
		$name = $params['name'];
		if (isset($params['value']))
			$params['value'] = (string) $params['value'];
		$type = $pre_html . tag('input', array_merge($params, $add)) . $post_html;
	}

	$opts_label = array('for' => 'form_' . $name);
	if ($act_label && is_array($add))
		$opts_label = array_merge($opts_label, $add);

	return tag('span', array('id' => 'form_couple_' . $name), (empty($label) ? '' : tag('label', $opts_label, $label)) . $type);
}

/**
 * generates an <input for a jQuery.button()'d checkbox
 */
function input_jcheckbox(&$params)
{
	jQ('$("form_' . $params['name'] . '").button();');
	$params['type'] = 'checkbox';
	input_checkbox($params);
}

/**
 * generates an <input for a checkbox
 */
function input_checkbox(&$params)
{
	if ((bool) $params['value'])
	{
		$params['checked'] = 'checked';
	}
	$params['value'] = 'on';
}

/**
 * generates an <input for a date
 */
function input_date(&$params)
{	
	if (is_numeric($params['value']))
		$params['value'] = date('d/m/Y', $params['value']);
	$params['type'] = 'text';
	jQ(sprintf('$("#form_%s").datepicker(calendar_opts);', $params['name']));
}

/**
 * generates an <input for a slider
 *@todo jQuery.UI.slider is currently removed - need to convert to component
function input_slider(&$params)
{
	global $add;

	$conf = $params['value'];

	$opts = array();

	$opts['min'] = isset($conf['min']) ? $conf['min'] : 0;
	$opts['max'] = $conf['max'];
	$opts['step'] = isset($conf['step']) ? $conf['step'] : 1;
	$opts['value'] = isset($selected) ? $selected : $opts['max'];
	$params['type'] = 'text';
	unset($params['value']);
	$add['class'] = 'hide-this';
	$params['pre_html'] = tag('span', array('id' => 'holder_form_' . $params['name']), $opts['value']) .
	 tag('div', array('id' => 'form_div_' . $params['name']), '');

	jQ(sprintf('
$("#form_%1$s").hide();
$("#form_div_%s").slider($.extend(%s,
{
	slide: function (event, ui)
	{
		$("input#form_%1$s").val(ui.value);
		$("span#holder_form_%1$s").html(ui.value);
	}
}));', $params['name'], json_encode($opts)));
}
*/

/**
 * generates an <input for a datetime field
 */
function input_datetime(&$params)
{
	if (!empty($params['value']))
		$params['value'] = datetime_to_picker($params['value']);
	$params['type'] = 'text';
	$_opts = array();
	if (isset($add['__restrict']))
	{
		$restrict = $add['__restrict'];
		if (!is_array($restrict))
			$res = explode(';', $restrict);

		$restrict = array();
		if (!empty($res[0]))
			$restrict['minDate'] = $res[0];
		if (!empty($res[1]))
			$restrict['minDate'] = $res[1];
		foreach ($restrict as $t => &$res)
		{
			if ($res == '@today')
				$res = date('j') . '/' . date('m') . '/' . date('Y');
			if ($res == '@today+')
				$res = ( date('j') + 1 ) . '/' . date('m') . '/' . date('Y');
		}

		$_opts += $restrict;
		unset($add['restrict']);
	}

	include_library(LIBRARY_TIMEPICKER);
	jQ(sprintf('$("#form_%s").datetimepicker($.extend(calendar_opts, %s));', $params['name'], json_encode($_opts)));
}

/**
 * generates an <input for a date range
 */
function input_date_range(&$params)
{
	if (is_numeric($params['value']))
		$params['value'] = date('d/m/Y', $params['value']);
	static $date_range_i = -1; //multiple dateranges on the same page.
	if ($date_range_i == -1)
	{ //first date range
		jQ('dates = {};'); //create the date ranges array
	}
	$params['type'] = 'text';
	if (is_array($params['name']))
	{
		$to_name = empty($params['name'][1]) ? $params['name'][0] . '2' : $params['name'][1];
		$params['name'] = $params['name'][0];
	}
	else
		$to_name = $name . '2';

	include_library(LIBRARY_DATEPICKER);
	jQ(strtr('
var updateDateRanges = function (selectedDate)
{
	var $t = $(this);

	if (typeof selectedDate != "string")
		selectedDate = $t.val();

	var date,
		option = this.name == "%%name%%" ? "minDate" : "maxDate",
		instance = $t.data("datepicker");

	selectedDate = selectedDate
		.replace(/(^|\/)([0-9])\//, "$10$2/");

	if (selectedDate.length < 10)
		return;

	try {
		date = $.datepicker.parseDate(
			instance.settings.dateFormat ||
			$.datepicker._defaults.dateFormat,
		selectedDate, instance.settings);

		dates[%%id%%]
			.not(this)
			.datepicker("option", option, date);
	} catch (e) {}
};

dates[%%id%%] = jQuery("#form_%%to_name%%, #form_%%name%%")
	.datepicker($.extend(calendar_opts,
	{
		changeMonth: true,
		onSelect: updateDateRanges
	}))
	.keyup(updateDateRanges);
', array('%%id%%' => ++$date_range_i, '%%to_name%%' => $to_name, '%%name%%' => $params['name'])));

	if (is_array($params['label']))
	{
		$to_label = empty($params['label'][1]) ? lang('date_to') : $params['label'][1];
		$params['label'] = $params['label'][0]; //replace
	}
	else
		$to_label = lang('date_to');

	if (is_array($params['value']))
	{
		$to_value = empty($params['value'][1]) ? '' : date_to_picker($params['value'][1]);
		$params['value'] = $params['value'][0];
	}
	else
		$to_value = '';
	
	$params['post_html'] = '&nbsp;' . input($to_name, $to_label, null, $to_value);

	if (!empty($params['value']))
	{
		if (!( $params['value'] = @date('d/m/Y', $params['value']) ))
		{
			$params['value'] = false;
		}
	}
}

/**
 * generates a <select input to select a record
 */
function input_select_record($value)
{
	if ($add_empty = !empty($value['empty']))
		$empty_value = empty($value['empty_value']) ? null : $value['empty_value'];

	if (!isset($value['displayMethod']))
		$value['displayMethod'] = null;
	if (!isset($value['column']) && empty($value['displayMethod']))
		$value['column'] = 'name'; //-> getName()

	if (empty($value['parent']))
	{
		if (method_exists($value['model'], $d_method = 'get'.Doctrine_Inflector::classify($value['column']))
		 && (null === $value['displayMethod'] || true === $value['displayMethod']))
			$value['displayMethod'] = $d_method;

		$t = Doctrine_Core::getTable($value['model']);
		$identifier = $t->getIdentifier();

		if (isset($value['records']))
			$records = $value['records'];
		else if (isset($value['query']))
			$records = $value['query']->execute();
		else
		{
			if (isset($value['where']))
			{
				$records = $t
						->createQuery()
							->where($value['where'])
						->execute();
			}
			else
				$records = $t->findAll();
		}

		$exclude = isset($value['exclude']) ? (array) $value['exclude'] : array();

		if (empty($value['displayMethod']))
			$value = $records->toValueArray($value['column']);
		else
		{
			$method = $value['displayMethod'];
			$value = array();
			foreach ($records as $record)
				$value[$record[$identifier]] = $record->$method();
		}

		if (count($exclude))
		{
			foreach ($exclude as $exc)
				unset($value[$exc]);
		}
	}
	else
	{
		$t = Doctrine_Core::getTable($value['parent']);
		$parent_records = $t->createQuery('p')
					->leftJoin('p.' . $value['model'] . ' c')
					->execute();
		if ($parent_records->count())
		{
			$records = array();
			if (method_exists(get_class($parent_records[0]), $pCol = 'get'.Doctrine_Inflector::classify($value['pColumn'])))
				$value['pDisplayMethod'] = $pCol;
			foreach ($parent_records as $parent_record)
			{
				$val = array();
				foreach ($parent_record[$value['model']] as $cRecord)
					$val[$cRecord[$cRecord->getTable()->getIdentifier()]] = empty($value['displayMethod']) ? $cRecord[$value['column']] : $cRecord->{$value['displayMethod']}();
				$records[empty($value['pDisplayMethod']) ? $parent_record[$value['pColumn']] : $parent_record->{$value['pDisplayMethod']}()] = $val; 
			}
			$value = $records;
		}
		else
			$value = array();
	}

	if ($add_empty)
		$value[$empty_value ?: '-1'] = lang('empty');

	return $value;
}

/**
 * generates options for a select tag
 *
 * @package Helper
 * @subpackage HTML
 *
 * @param array $value values
 * @param mixed $selected the actual selection
 *
 * @return string HTML for this select
 */
function input_select_options($value, $selected = null)
{
	if (null == $selected)
		$selected = reset($value);
	$options = '';

	/*
	$in_optgroup = false;
	$optgroup_label = $cache = $actual = $options = '';
	foreach ($value as $val => $title)
	{
		if ($val < -1) //allowing -1 as blank :p
		{ //add an OptGroup
			if ($in_optgroup)
			{
				$options .= tag('optgroup', array('label' => $optgroup_label), $cache);
				$cache = '';
			}
			$in_optgroup = true;
			$optgroup_label = $title;
			continue;
		}

		$o = array('value' => str_replace('\\', '', $val));
		if ($val == $selected)
		{ //the actual val.
			$o += array('selected' => 'selected');
		}
		$actual = tag('option', $o, $title);

		if ($in_optgroup)
			$cache .= $actual;
		else
			$options .= $actual;
	}
	if ($in_optgroup) //fix if we have just 1 optgroup
	{
		$options .= tag('optgroup', array('label' => $optgroup_label), $cache);
		$cache = '';
	}
	//*/
	//*
	foreach ($value as $val => $title)
	{
		if (is_string($val) && is_array($title))
		{
			$optgroup = '';
			foreach ($title as $opt => $name)
				$optgroup .= tag('option', array('value' => str_replace('\\', '', $opt)) +
				 ($opt == $selected ? array('selected' => 'selected') : array()), $name);
			$options .= tag('optgroup', array('label' => $val), $optgroup);
		}
		else
		{
			$options .= tag('option', array('value' => str_replace('\\', '', $val)) +
				 ($val == $selected ? array('selected' => 'selected') : array()), $title);
		}
	}
	//*/
	return $options;
}

function input_csrf_token($on = true)
{
	return input('_csrf_token', null, 'hidden', CSRF_TOKEN);
}