<?php
/**
 * formats a string PHP => JS
 *
 * @param string $str HTML value
 * @return string JS Value
 */
function javascript_string($str, $quote = "'")
{
	$str = str_replace($quote, '\\' . $quote, $str);
	return str_replace(array("\n", "\r", "\t", "\r\n"), '', $str);
}

/**
 * formats a mixed val to it's JavaScript value
 *
 * @param mixed $val the value to convert
 * @return string $val in JS format
 */
function javascript_val($val)
{
	if (is_integer($val))
		return $val;
	if (is_bool($val))
		return $val ? 'true' : 'false';
	if (is_string($val)
	 || ( is_object($val) && method_exists($val, '__toString') ))
		return '"' . javascript_string('' . $val, '"') . '"'; //force string conversion
	if (is_array($val))
	{
		$js = array();
		foreach ($val as $k => $v)
			$js[] = "'" . javascript_string($k) . "': " . javascript_val($v);
		return '{' . implode(', ', $js) . '}';
	}
	else //object, callable, anything else you want ...
		return 'null';
}

/**
 * creates a HighChart pie chart
 *
 * @param string $title the pie's name
 * @param array $datas {'name' => count}
 * @param string name_prefix name prefix for each 'name' (in $datas)
 */
function chart($title, array $datas, $namespace = '', $name_prefix = '')
{
	$total = 0;
	foreach ($datas as $c)
		$total += $c;

	$series = array();
	foreach ($datas as $k => $count)
		$series[] = array(
			'name' => lang($name_prefix . $k, $namespace, '%%key%%'),
			'y' => round(($count * 100) / $total, 2),
			'count' => $count,
		);

	$series = json_encode($series);

	$render_to = 'container-' . reset($datas) . $total;

	include_library(LIBRARY_HIGHCHARTS);
	jQ("
chart = new Highcharts.Chart({
	chart:
	{
		renderTo: '{$render_to}',
		plotBackgroundColor: null,
		plotBorderWidth: null,
		plotShadow: true
	},
	title:
	{
		text: '{$title}'
	},
	tooltip:
	{
		formatter: function()
		{
			return '<b>'+ this.point.name +'</b>: '+ this.y +' % (' + this.point.count + ')';
		}
	},
	plotOptions:
	{
		pie:
		{
			allowPointSelect: true,
			cursor: 'pointer',
			dataLabels:
			{
			   enabled: true
			},
			showInLegend: true
		}
	},
	series:
	[{
		type: 'pie',
		data: {$series}
	}]
});");

	return tag('div', array('id' => $render_to), '');
}

/**
 * creates a HighChart stock chart
 */
function stockchart($title, array $datas, $namespace = null, array $cols = array())
{
	static $i = 0;

	if (is_array($namespace) && $cols == array())
	{
		$cols = $namespace;
		$namespace = null;
	}

	$t = isset($cols['type']) ? $cols['type'] : 'type';
	$at = isset($cols['at']) ? $cols['at'] : 'at';

	$render_to = 'stockchart_' . ++$i;

	$counts = array();

	//[type: vote, at: 123; type: vote, at: 456; type: vote, at: 456]
	//to
	//[vote: {456: 2, 123: 1}]
	foreach ($datas as $data)
	{
		if (!isset($counts[$data[$t]]))
			$counts[$data[$t]] = array();

		if (isset($counts[$data[$t]][$data[$at]]))
			++$counts[$data[$t]][$data[$at]];
		else
			$counts[$data[$t]][$data[$at]] = 1;
	}

	$series = array();

	foreach ($counts as $name => $data)
	{
		$d = array();
		foreach ($data as $k => $v)
			$d[] = array($k * 1000, $v);

		$series[] = json_encode(array('name' => lang(utf8_encode($name), $namespace, '%%key%%'), 'data' => $d));
	}

	$series = implode(', ', $series);

	include_library(LIBRARY_HIGHCHARTS);
	jQ("
var piechart_$i = new Highcharts.Chart({
    chart: {
        renderTo: '$render_to',
        type: 'line'
    },
    title: {
        text: '$title'
    },
    xAxis: {
    	allowDecimals: false,
    	type: 'datetime'
    },
    yAxis: {
    	title: {
    		text: '',
    	}
    },
    series: [$series]
});");

	return tag('div', array('id' => $render_to), '');
}

/**
 * saves jQuery Code or creates a slot for it (ob)
 *
 * @package Helper
 * @subpackage JS
 *
 * @param string $put jQuery code to put
 * @return string actual jQuery code
 */
function jQ($put = null, $in = false)
{
	static $_jQs = array(),
		$_starteds = array(),
		$actual_in = 'main';

	if (false === $in)
		$in = $actual_in;

	if (empty($_jQs[$in]))
	{
		$_jQs[$in] = '';
		if (is_string($put))
			$_starteds[$in] = false;
	}

	if (true === $put)
	{
		$_starteds[$in] = true;
		ob_start();
		return;
	}
	else if (null === $put)
	{
		if (empty($_starteds[$in]))
		{
			$js = $_jQs[$in];
			$_jQs[$in] = '';
			return $js;
		}
		else
		{
			$_starteds[$in] = false;
			$put = ob_get_clean();
			$put = str_replace(array('<script type="text/javascript">', '<script>', '</script>'), '', $put);
		}
	}
	else if (false === $put)
	{ //default in.
		$actual_in = $in;
		return;
	}
	if (!empty($put) && substr($put = trim($put), -1) != ';')
		$put .= ';';
	if (is_string($put))
		$_jQs[$in] .= "\n" . $put;
	return $_jQs[$in];
}

/**
 * creates a javascript link
 *
 * @package Helper
 * @subpackage HTML
 *
 * @param string $js JS
 * @param string $text Text of the link
 * @param string $link The link unless JS.active?
 * @param array $add HTML properties
 * @param string $event Event (@example click)
 * @return string the JS Link
 */
function js_link($js, $text, $link = '#', $add = array(), $event = null)
{
	if (is_array($js))
	{
		$in = $js[1];
		$js = $js[0];
	}
	else
		$in = false;

	if (null === $js) //wtf ?
		return make_link($link, $text, array(), $add);

	$link = replace_url($link);

	if (!$event)
		$event = 'click';
	$event = (array) $event;
	
	do
	{
		$id = $mem_id = rand(0, 10000);
	}
	while (isset($JSs[$id]));

	$js = ';' == substr($js, 0, -1) ? $js : $js . ';';
	$js_code = '
	var action' . $id . ' = function (event)
	{
		' . $js . '
		event.preventDefault();
		event.stopPropagation();
		return false;
	}';
	jQ('
var loc' . $id . ' = $("#link_' . $id . '");
' . $js_code, $in);

	foreach ($event as $ev)
	{
		$ev = strtolower($ev);
		$js_code = 'loc' . $mem_id . '.attr("href", "#")'; //yeah, this might be slow, but it's the best to keep it nojs-compatible
		jQ('locations["link_' . $mem_id . '"] = ' . javascript_val($link) . ';', $in);
		$js_code .= sprintf('.on( "%s", action%d )', $ev, $id);
		jQ($js_code, $in);
	}
	return tag('a', $add + array('href' => $link, 'class' => 'js-link', 'id' => 'link_' . $id), $text);
}