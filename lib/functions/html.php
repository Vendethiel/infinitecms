<?php
/**
 * renders array to HTML attributes
 *
 * @package Helper
 * @subpackage HTML
 *
 * @param array $opt attributes to render (array)
 * @return string the attributes rendered (HTML)
 */
function attributes(array $opt = array())
{
	if (!$opt) //empty array, null, false ...
		return '';

	$opts = '';
	$second = array();
	foreach ($opt as $key => $value)
	{
		if (is_array($value) && !is_string($key))
		{ //adding new values
			$second += $value;
			continue;
		}
		else
		{
			if (is_array($value))
			{
				if ($key == 'style')
					$value = array_to_style($value);
				if ($key == 'data')
				{
					$datas = array();
					foreach ($value as $k => $v)
					{
						$datas['data-' . $k] = $v;
					}
					$opts .= attributes($datas);
					continue;
				}
			}
			$opts .= sprintf(' %s="%s"', $key, $value);
		}
	}
	$opts .= attributes($second); //values added on-the-fly
	return $opts;
}

/**
 * formats an array to a style declaration
 *
 * @package Helper
 * @subpackage HTML
 *
 * @param $ary CSS params
 *
 * @return string css-valid declaration(s)
 */
function array_to_style(array $ary)
{
	$style = '';
	foreach ($ary as $k => $v)
		$style .= sprintf('%s: %s; ', $k, $v);
	return $style;
}
 
/**
 * generates a HTML tag
 *
 * @package Helper
 * @subpackage HTML
 *
 * @param string $n Name of the tag, as "a"
 * @param array $opt HTML attributes for the tag
 * @param string $c -default:null Content of the tag
 * @return string HTML rendered
 */
function tag($name, $opt = array(), $content = null)
{
	if (null === $content && !is_array($opt))
	{
		$content = $opt;
		$opt = array();
	}

	$end = null === $content ? ( in_array($name, array('img', 'br', 'link')) ? ' />' : '>' ) :
			'>' . $content . '</' . $name . '>';
	return tag_open($name, $opt) . $end;
}

/**
 * creates the beginning of a HTML tag (<{name} {opts}>)*
 *
 * @param string $name tag name
 * @param array $opts HTML options
 * @return string opening tag
 */
function tag_open($name, $opts = array())
{
	return '<' . $name . attributes($opts);
}

/**
 * generates style tag to include an external style
 *
 * @param string $href css's file href
 * @return string <style> tag
 */
function stylesheet_tag($href, $rel = 'stylesheet')
{
	return tag('link', array('rel' => $rel, 'type' => 'text/css', 'href' => $href));
}

/**
 * creates javascript tag and CDATA plus puts JS in
 *
 * @package Helper
 * @subpackage HTML
 *
 * @param string $c JS Content
 * @param array $opt Attributes to add to the tag
 * @return string script{type:text/javascript} tag with the content
 */
function tag_js($c = null, $opt = array())
{
	static $opened = false;
	$type = array('type' => 'text/javascript');
	if (null === $c)
	{
		if ($opened)
			$html = "\n</script>";
		else
			$html = '<script' . attributes($type) . ">\n";
		$opened = !$opened;
		return $html;
	}
	else
		return tag('script', $opt + $type, $c);
}

/**
 * creates script tag plus puts CSS in
 *
 * @package Helper
 * @subpackage HTML
 *
 * @param string $c CSS Content
 * @param array $opt Attributes to add to the tag
 * @return string style{type:text/css} tag with the content
 */
function tag_css($c, $opt = array())
{
	return tag('style', array_merge($opt, array('type' => 'text/css')), $c);
}

/**
 * generates script tag to include an external script
 *
 * @param string $src javascript's file SRC
 * @return string <script> tag
 */
function javascript_tag($src, $type = 'javascript')
{
	//really giving me too much headaches
	#'async' => 'async', 
	return tag('script', array('type' => 'text/' . $type, 'src' => $src), '');
}

/**
 * creates an image
 *
 * @package Helper
 * @subpackage HTML
 *
 * @param string $url URL of image
 * @param string $title attributes.title
 * @param string $alt attributes.alt
 * @param array $style attributes.style.join " "
 * @return string the image
 */
function make_img($url, $ext = EXT_JPG, $title = null, $alt = null, $add = array())
{
	global $asset_pipeline;

	if (is_array($title))
	{ //title = add
		$add = $title;
		$title = null;
	}
	else if (is_array($alt))
	{ //alt = add
		$add = $alt;
		$alt = '';
	}

	if (isset($add['try']))
	{
		$try = $add['try'];
		unset($add['try']);
	}
	else
		$try = false;

	if (is_string($title) && empty($alt))
		$alt = $title;

	$img = $url . $ext;
	$gPath = getPath();
	if (strpos($img, 'http') !== 0 && substr($img, 0, strlen($gPath)) != $gPath)
		$img = asset_path($img, $try);

	return tag('img', array_merge($add, array('src' => $img, 'title' => $title, 'alt' => $alt)));
}

/**
 * paginates
 *
 * @param int $page the actual page
 * @param int $last_page pager's last page
 * @param array $range pages to show
 * @param string $uri base URL. Page will be appended
 * @param string $sep the separator between each link
 * @return string the pagination links
 */
function paginate($page, $last_page, $range = array(), $uri = '', $sep = ' ')
{
	if (count($range) < 2)
		return '';

	$navigation = '';

	if ($page != 1)
	{
		$navigation .= make_link($uri.'1', make_img('first', EXT_PNG, array('align' => 'absmiddle')));
		if ($page != 2)
		{ //if it's the second page, this link is useless ...
			$navigation .= make_link($uri.($page - 1), make_img('previous', EXT_PNG, array('align' => 'absmiddle')));
		}
	}

	// Pages one by one
	$links = array();
	foreach ($range as $p)
	{
		if ($page == $p)
			$links[] = tag('b', $page);
		else
			$links[] = make_link($uri.$p, $p);
	}
	$navigation .= implode($sep, $links);

	if ($page != $last_page)
	{
		if ($last_page != $page + 1)
		{ //the "last" & "next" page are the same
			$navigation .= $sep.make_link($uri.($page + 1), make_img('next', EXT_PNG, array('align' => 'absmiddle')));
		}
		$navigation .= make_link($uri.$last_page, make_img('last', EXT_PNG, array('align' => 'absmiddle')));
	}

	return $navigation;
}

/**
 * paginates a layout
 *
 * @package Helper
 * @subpackage HTML
 *
 * @param Doctrine_Pager_Layout $pager layout to display
 * @return string the pagination links
 */
function paginate_layout(Doctrine_Pager_Layout $layout, $sep = ' ')
{
	$pager = $layout->getPager();

	if (!$pager->haveToPaginate())
		return '';

	return paginate($pager->getPage(), $pager->getLastPage(), $layout->getPagerRange()->rangeAroundPage(), $layout->getUrlMask(), $sep);
}

/**
 * makes a list from an array
 *
 * @package Helper
 * @subpackage HTML
 *
 * @param array $list List of Options
 * @param array $opt_ul HTML options for the ul tag
 * @param array $opt_li HTML options for the li tag
 *
 * @return the list rendered
 */
function to_list(array $list, $opt_ul = array(), $opt_li = array())
{
	$list_html = '';
	foreach ($list as $item)
		$list_html .= tag('li', $opt_li, $item);
	return tag('ul', $opt_ul, $list_html);
}