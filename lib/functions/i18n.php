<?php
/**
 * returns a lang key
 *
 * @param string $key The key to search
 * @param string $namespace the namespace containing the string, as
 *  /langs/[LANG]/[NAMESPACE].php
 * @param string $default the default value if the key doesn't exist
 * @return string The key translated or "Untranslated"
 */
function lang($key = null, $namespace = null, $default = '"%%ns%%":"%%key%%" (Untranslated)')
{
	global $langs;
	static $lang = null;

	if (is_array($key))
	{
		if (isset($key[1]))
			$namespace = $key[1];
		if (isset($key[2]))
			$default = $key[2];

		$key = $key[0];
	}

	if (null === $namespace)
		$namespace = 'common';

	if (null === $lang)
	{
		if (!isset($langs[LANG]))
			//common is needed everywhere. This should save around 7-15ms
			//'common' => include 'lang/' . LANG . '/common' . EXT);
			//^ makes install fail
			$langs[LANG] = array();
		$lang = $langs[LANG];
	}

	$l = $lang;
	if (!isset($lang[$namespace]))
	{
		if (file_exists($php_file = 'lang/' . LANG . '/' . $namespace . EXT))
			$lang[$namespace] = include $php_file;
		else if (file_exists($txt_file = 'lang/' . LANG . '/' . $namespace . EXT_TXT))
		{
//			$lang[$namespace] = parse_ini_file($txt_file);
			$lang[$namespace] = array();
			foreach (file($txt_file) as $line)
			{
				list($k, $v) = explode('=', $line);
				$lang[$namespace][$k] = trim($v);
			}
		}
		else
			throw new InvalidArgumentException(sprintf('Unable to find namespace %s (lang %s).', $namespace, LANG));
	}
	$l = $lang[$namespace];

	if (null === $key)
		return $l;

	return isset($l[$key]) ? $l[$key] :
	( is_string($default) ? str_replace(array('%%key%%', '%%ns%%'), array($key, $namespace), $default) : $default );
}

/**
 * pluralizes a string
 *
 * @param string $word the word to pluralize
 * @param integer $i count
 * @param boolean $show_i show $i?
 * @return string string pluralized (or not)
 */
function pluralize($word, $i = 0, $show_i = false, $add = '%%content%% ')
{
	return ( $show_i ? str_replace('%%content%%', $i, $add) : '' ) . $word . ( $i > 1 ? 's' : '' );
}

/**
 * Formats filesize
 *
 * @param int $size file actual size
 * @return string filesize formatted, i.e. "5 KB"
 */
function filesize_format($size)
{
	$ranks = array('', 'KB', 'MB', 'GB', 'TB');
	$rank = 0;
	while ($size > 1001)
	{
		++$rank;
		$size /= 1000;
	}
	return round($size, 1) . ' ' . $ranks[$rank];
}