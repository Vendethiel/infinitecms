<?php
if (!function_exists('ucfirst'))
{
	/**
	 * ucfirst
	 *
	 * @see ucfirst
	 * @ignore
	 */
	function ucfirst($str)
	{
		$str[0] = strtolower($str[0]);
		return $str;
	}

}
if (!function_exists('lcfirst'))
{
	/**
	 * lcfirst
	 *
	 * @see lcfirst
	 * @ignore
	 */
	function lcfirst($str)
	{
		$str[0] = strtolower($str[0]);
		return $str;
	}
}

if (!function_exists('cal_days_in_month'))
{
	function cal_days_in_month($calendar, $month, $year)
	{
		return date('t', mktime(0, 0, 0, $month, 1, $year));
	}
}
if (!defined('CAL_GREGORIAN'))
	define('CAL_GREGORIAN', 1); 