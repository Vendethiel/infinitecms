<?php
/**
 * determinates whether a user is authorized or not (>=)
 *
 * @package Helper
 * @subpackage Auth
 *
 * @param integer $required -default=LEVEL_LOGGED lvl needed
 * @param boolean $reverse lower than ?
 * @return boolean authorized?
 */
function level($required = LEVEL_LOGGED, $reverse = false)
{
	global $member, $account, $config;
	$logged = isset($_SESSION['guid']);

	if ($required == LEVEL_GUEST)
		return $reverse ? $logged : !$logged; //return true if !connected (without reverse)
	
	if ($required == LEVEL_LOGGED)
		return $reverse ? !$logged : $logged; //return true if connected (without reverse)

	if ($required == LEVEL_VIP)
	{
		if (empty($config['COST_VIP']) || !isset($_SESSION['guid']))
			return false;

		return $reverse ? !$account->isVIP() : $account->isVIP();
	}

	$lvl = isset($_SESSION['guid']) ? $account->getLvl() : LEVEL_GUEST;
	return $reverse ? $lvl < $required : $lvl >= $required;
}

/**
 * checks the level of a member for accessing to a page
 *
 * @package Helper
 * @subpackage Auth
 *
 * @param integer $lvl required level
 * @param boolean $lt Lower than ? (greater than(false) by default)
 * @return boolean level's result
 */
function check_level($lvl = LEVEL_ADMIN, $lt = false, $silent = false)
{
	$check = level($lvl, $lt);
	if (!$check)
	{
		define('LEVEL_FALLBACK', true);
		throw new Router\Exception\NotAuthorized;
	}
	return $check;
}