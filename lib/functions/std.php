<?php
function json_last_error_message()
{
	return json_error_message(json_last_error());
}

function json_error_message($id)
{
	switch ($id)
	{
		case JSON_ERROR_NONE:
			return "no error";
		case JSON_ERROR_DEPTH:
			return "stack too deep";
		case JSON_ERROR_STATE_MISMATCH:
			return "malformed / invalid";
		case JSON_ERROR_CTRL_CHAR:
			return "invalid encoding";
		case JSON_ERROR_SYNTAX:
			return "syntax error";
		case JSON_ERROR_UTF8:
			return "invalid utf8";
	}
}


/**
 * reverses nl2br()
 *
 * @see nl2br
 * @param string $str string to convert
 * @return string converted string
 */
function br2nl($string)
{
	return str_replace(array('<br>', '<br />', '<br/>'), "\n", $string);
}


/**
 * applies recursively array_map (useful when dealing with nested arrays)
 * note : only accepts one array
 *
 * @param callable|closure $callback the callback to apply
 * @param array $array the array to process
 * @return array the (nested ?) array, with the callback applied
 */
function recursive_array_map($callback, $array)
{
	$fn = __FUNCTION__;
	if (!is_callable($callback))
		throw new InvalidArgumentException('Can not apply recursive_array_map : "' . $callback . '" is not a valid callback');
	if (!is_array($array))
		throw new InvalidArgumentException('Can not apply recursive_array_map : you must provide an array to map');

	//this can be achieved by closure + ... => too lazy
	foreach ($array as &$v)
	{
		if (is_array($v))
			$v = $fn($callback, $v);
		else
			$v = $callback($v);
	}
	return $array;
}

/**
 * same as strrpos(), but returns the pos from the end (0 = end, strlen($haystack) = start)
 *
 * @param string $haystack the string you're gonna search in
 * @param string $needle the string you're (not) looking for
 * @return int position
 *
 * @example rstrpos('a.gif', 'gif') => 0
 */
function rstrpos($haystack, $needle)
{
	return strpos(strrev($haystack), strrev($needle));
}

/**
 * converts a string from the base $base to a decimal
 *
 * @param string $string base-{$base} encoded string
 * @param integer $base the base (from 2 to 36, except 10)
 * @return integer
 */
function str2dec($string, $base)
{
	$decimal = 0;
	$base = (int) $base;
	if (10 == $base)
		return $string; //for those who don't know that, base 10 is the "common" base ...
	if ($base < 2 || $base > 36)
		return '$base must be between 2..36.';
	$charset = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$charset = substr($charset, 0, $base);
	$string = trim($string);
	if (empty($string))
		return 0;

	do
	{
		$char = $string[0];
		$string = substr($string, 1);
		$pos = strpos($charset, $char);
		if (false === $pos = strpos($charset, $char))
			return sprintf('Illegal character (%s) in $string', $char);
		$decimal = ( $decimal * $base ) + $pos;
	}
	while (null != $string);

	return $decimal;
}

/**
 * ensures a directory exist, by creating it if it does no & it's possible to
 *
 * @param string $dir directory
 */
function ensure_directory($dir)
{
	if (file_exists($dir))
	{
		if (!is_dir($dir))
			throw ExceptionManager::invalidDir($dir);
	}
	else
	{
		$dir = explode('/', str_replace(DIRECTORY_SEPARATOR, '/', $dir));
		$cDir = ''; //complete dir

		foreach ($dir as $d)
		{
			$cDir .= $d . '/';

			if (!file_exists($cDir) && !@mkdir($cDir))
				throw ExceptionManager::cantCreate($cDir);
		}
	}
}

// A.K.A. UpperCamelCase
function camelize($s)
{
	$s = str_replace('_', ' ', $s);
	$s = ucwords($s);
	return str_replace(' ', '', $s);
}

// A.K.A. lowerCamelCase
function pascalize($s)
{
	return lcfirst(camelize($s));
}