<?php
/**
 * returns the path depending on rewrite on/off
 *
 * @package Router
 * @subpackage Rewrite
 *
 * @param string $url the URL
 * @param bool $absolute Absolute url (http://) or not ?
 * @return string script's path
 */
function getPath($absolute = false)
{
	global $router;
	static $path = null,
		$absolute_path = null;
	if (null === $path)
	{
		if (isset($_SERVER['SCRIPT_NAME']))
			$path = '.' === ( $d = dirname($_SERVER['SCRIPT_NAME']) ) ? '' : $d;
		else
			$path = '';

		if ($path == '\\' || $path == '/')
			$path = '';

		$absolute_path = (isset($_SERVER['HTTP_HOST']) ?
		 'http://' . $_SERVER['HTTP_HOST'] . '/' : '') .
		 $path;
	}

	return $absolute ? $absolute_path : $path . DS;
}

/**
 * redirects to a page
 *
 * @package Router
 * @subpackage Location
 *
 * @param string|null $loc the url to redirect
 * @param double $wait how many seconds to wait before redirect ? 0 for instant redirect
 * @return void
 */
function redirect($loc = null, $wait = 0, $force = false)
{
	global $router;

	$internal = is_string($loc) ? substr($loc, 0, 7) != 'http://' : false;
	if (null === $loc)
		$loc = '@root';
	if (is_array($loc))
		$loc = to_url($loc, false);

	$loc = replace_url($loc, false, array('getName', 'getURL', 'getUpdateURL', 'getParam'));
	$wait = doubleval($wait);

	if (!$internal && $router->isAjax())
	{
		$js = 'title<~>' . getPath() . '<~>' . server_state() . '<~>nextPMNotif<~>' . ( defined('UPDATE_SELECTOR') ? UPDATE_SELECTOR : 'null' ) . '<~>';
		$force = $force ? 'document.location = "%s"' : 'followLink("%s")';
		$js .= tag_js(sprintf('window.setTimeout(function () { ' . $force . '; }, %d);', $loc, $wait == 0.0 ? 0 : $wait * 1000));
		if ($wait)
			echo $js;
		else
		{
			__shutdown();
			exit($js);
		}
	}
	else
	{
		if ($wait)
			echo tag_js(sprintf('document.location = "%s";', $loc));
		else
			header('Location: ' . $loc);
	}
}

/**
 * encodes params plus formates the URL
 *
 * @global Router $router
 *
 * @param array $params Params to encode
 * @param boolean $strict Strict encoding ? (&amp;) or no ?
 * @return string URL
 */
function to_url($params, $strict = true)
{
	global $router;
	if (empty($params))
		return '';
	if (is_string($params))
		return $params;
	$url = encode_url_params($params, $strict);
	return getPath() . $url;
}

/**
 * replaces special urls
 *
 * @package Helper
 * @subpackage Route
 *
 * @example echo replace_url('@root')
 * @example echo replace_url(array('controller' => 'Misc'))
 * @example echo replace_url(new Account)
 * @example echo replace_url(AccountTable::getInstance()->find(5))
 * @example echo replace_url(array('controller' => 'News', 'id' => instanceof Record));
 *
 * @param mixed $url can be
 *  - a string :
 *   - if routes[$url]
 *    - routes to the URL replacement
 *   - else
 *    - routes to $url
 *  - an array : routes to key/value pairs
 *  - a Doctrine_Table object :
 *   - routes to [table.recordName]
 *  - a Doctrine_Record object :
 *   - unless $url.exists? :
 *    - if opts[:rest]
 *     - routes to [$url.class.to_s]
 *    - else
 *     - routes to [$url.class.to_s]/update
 *   - else :
 *    - if $url.respond_to?(:getLink) && false !== $record_name
 *     - return $url.getLink($record_name)
 *    - else
 *     - unless $record_name.is_a String
 *      - $record_name = $url.getName()
 *     - routes to [$url.class.to_s]/show/[$url.[$url.table.identifier]]
 *  - anything else : returns it.
 * @param bool $strict &amp; if true
 * @param array|bool $opts true if you wanna use default optiosn, else pass it
 *
 * @return string URL modified
 */
function replace_url($url, $strict = true, $opts = true)
{
	global $router;

	if (is_array($strict))
	{
		$opts = $strict;
		$strict = true;
	}

	if (!is_string($url))
	{
		if ($url instanceof Doctrine_Record)
		{
			if (true === $opts)
				$opts = array('getLink', 'getName', 'getURL', 'getUpdateURL', 'getParam');
			$opts = (array) $opts;

			global $record_name, $magic;
			if ($url->exists())
			{ //no way to edit_path, I know. fuck you :3
			 //@todo could use $opts ?
				if (method_exists($url, 'getLink') && $magic && in_array('getLink', $opts))
					return call_user_func_array(array($url, 'getLink'), (array) $record_name);
				else
				{
					if (!is_string($record_name) && in_array('getName', $opts))
					{
						$record_name = $url->getName($record_name);
					}

					if (method_exists($url, 'getURL') && in_array('getURL', $opts))
						$ary = $url->getURL();
					else
					{
						$ary = array('controller' => get_class($url), 'action' => 'show');

						$idents = (array) $url->getTable()->getIdentifier();
						$k = $p = $idents[0];

						if (method_exists($url->getTable(), 'getRouterParam'))
							$p = $url->getTable()->getRouterParam();
						
						
						$ary[$p] = $url[$k];
					}
					return to_url($ary);
				}
			}
			else
			{
				if (empty($record_name))
					$record_name = lang(get_class($url) . ' - create', 'title');
				$rest = !empty($opts['rest']); //restful => omit action (using HTTP verbs)

				return to_url(method_exists($url, 'getUpdateURL') && in_array('getUpdateURL', $opts) ? $url->getUpdateURL() :
				 array('controller' => get_class($url), 'action' => $rest ? '' : 'update')); //Account#update redirects to #create ;)
			}
		}
		else if ($url instanceof Doctrine_Table)
			return to_url(array('controller' => substr(get_class($url), 0, -5), 'action' => 'index'));
		else if (is_array($url))
			return to_url($url);
		else
			return $url;
	}
	
	$_url = substr($url, 1);

	if (is_string($url))
	{
		if ($url[0] === '@')
			$url = (array) $router->getReplacement($_url) ?: array();
		else if ($url[0] === '!')
			$url = (array) $router->getRoute($_url, $opts) ?: array();

		if (is_array($opts) && is_array($url))
		{ //may as well be true
			foreach ($opts as $k => $v)
			{
				if (is_object($v) && $v instanceof Record)
				{
					$i = (array) $v->getTable()->getIdentifier();
					$opts[$k] = $v[$i[0]];
				}
			}

			foreach ($opts as $find => $repl)
			{
				/*should never happen
				if ($find == '__default')
					throw new RuntimeException('Trying to use reserved keyname : __default');
				//*/
				foreach ($url as $k => $v)
				{
					if ($k == '__default')
						continue;

					$url[$k] = str_replace('{' . $find . '}', $repl, $v);
				}
			}
		}
		if (is_array($url) && isset($url['__default']))
		{
			foreach ($url['__default'] as $find => $repl)
			{
				foreach ($url as $k => $v)
					$url[$k] = str_replace('{' . $find . '}', $repl, $v);
			}
			unset($url['__default']);
		}
	}

	if (is_array($url))
		$url = to_url($url, $strict);

	return $url;
}

/**
 * creates a link
 *
 * @package Helper
 * @subpackage HTML
 *
 * @param string $url(|$opt) Url of the link
 * @param string $n Text for the link
 * @param array $opt URL Params
 * @param boolean $js active JS link ?
 * @param boolean $magic Use magic getLink()?
 * @return string formatted link
 */
function make_link($_url, $n = null, $opt = array(), $add = array(), $js = true, $_magic = true)
{
	global $config, $router, $magic;
	if ($config['LOAD_TYPE'] == LOAD_NONE)
		$js = false; //forces JS to be off

	$magic = $_magic;
	if (false === $n)
		$magic = false;

	if ($_url instanceof Doctrine_Record && method_exists($_url, 'getLink') && $_url->exists()
	 && $magic)
		return call_user_func_array(array($_url, 'getLink'), (array) $n);

	if ((is_array($_url) || $_url instanceof Doctrine_Record || $_url instanceof Doctrine_Table)
	 && !empty($opt) && empty($add))
	{
		$add = $opt;
		$opt = array();
	}

	if ($_url instanceof Doctrine_Record)
	{
		global $record_name;
		$record_name = $n;
		$url = replace_url($_url);
		if (!$n)
		{
			$n = $record_name;
			unset($record_name);
		}
	}
	else if ($_url instanceof Doctrine_Table)
	{
		$url = replace_url($_url);
		if (empty($n))
			$n = '%s';
		$n = sprintf($n, lang($router->getControllerAlias(substr(get_class($_url), 0, -5)) . ' - index', 'title'));
	}
	else
		$url = replace_url($_url, true, $opt);

	if (is_string($_url) && isset($_url[0])
	 && ( $_url[0] == '!' || $_url[0] == '@' ))
		$opt = array();

	if (is_array($url) && ( array() === $opt || null === $opt ))
	{
		$opt = $url;
		$url = '';
	}

	if ($js)
		$link = array('class' => 'link');
	else
		$link = array();

	return tag('a',
	 array_merge($link, $add, array('href' => $url . ( empty($opt) ? '' : to_url($opt, true, false) ))),
	 $n);
}

/**
 * formats an url
 *
 * @param string $url adress of page
 * @return string the URL
 */
function make_url($url, $params, $strict = true)
{
	if (is_array($url))
	{
		$params = $url;
		$url = '';
	}

	return $url . ( empty($params) ? '' : '?' ) . encode_url_params($params, $strict);
}

/**
 * encodes an url param
 *
 * @param string $val The param to encode
 * @return string urlencode(param) unless param startsWith and endsWith '%%'
 */
function encode_url_param($val)
{
	if (is_object($val) && $val instanceof Record)
	{
		$k = (array) $val->getTable()->getIdentifier();
		$val = $val[$k[0]];
	}

	if (!$val)
		return '';

	if ('%%' == substr($val, 0, 2) && '%%' == substr($val, -2) ) //hack for js ...
		return '" + ' . substr($val, 2, -2) . ' + "';
	else
		return urlencode($val);
}

/**
 * encodes the params to URL
 *
 * @see make_link
 * @see redirect
 *
 * @package Helper
 * @subpacekage Route
 *
 * @param array $params The params to convert to URL
 * @param boolean $strict &amp; or just & ?
 * @return string parameters as string
 */
function encode_url_params(array $params = array(), $strict = true, $external = false)
{
	global $router, $config;

	$url = REWRITE && !$external ? '' : (defined('FRONTEND_FILE') ? FRONTEND_FILE : '') . '/';

	if (empty($params))
		return getPath();

	if (!isset($params['controller']) && isset($params['action']))
		$params['controller'] = $router->getDefault('controller');

	$ext = '';
	
	if (!empty($params['ext']))
	{ //empty ext forces ... an empty ext
		$ext = '.' . $router->getExtAlias($params['ext']);
		unset($params['ext']);
	}

	foreach ($params as $key => $param) //rewrite all params, rewriting or not.
	{
		if (null === $key
		 || null === $param)
			unset($params[$key]);
		else
			$params[$key] = encode_url_param($param);
	}

	if ($external)
	{
		$and = $strict ? '&amp;' : '&';
		foreach ($params as $param => $val)
			$url .= $param . '=' . $val . $and;
		return substr($url, 0, -( strlen($and) ));
	}

	$keys = array_keys($params);
	sort($keys); //to compare, later

	if (isset($params['controller']))
		$url .= $params['controller'];

	if ($keys == array('controller') || ($keys == array('action', 'controller') && empty($params['action'])))
		return $url . $ext;
//I can't use this because this URL might be used for some non-GET actions
// and in this case, we must force action
#	 || ( $keys == array('action', 'controller') && Router::getActionForVerb(false) == $params['action'] ))

	if (isset($params['id']) && $params['id'] != '')
		$url .= DS . $params['id'];
	if ($keys == array('controller', 'id'))
		return $url . $ext;

	if ($keys == array('action', 'controller', 'id')
	 && ( empty($params['action']) ? true : Router::getActionForVerb(true) == $params['action'] ))
		return $url . $ext;

	if (isset($params['action']))
		$url .= DS . $params['action'];
	if ($keys == array('action', 'controller') || $keys == array('action', 'controller', 'id'))
		return $url . (isset($params['id']) && empty($params['id']) ? DS : '') . $ext;

	if ($keys == array('action', 'controller', 'id', 'mode'))
		return $url . DS . $params['mode'] . $ext;

	unset($params['controller'], $params['action']);
	if (!empty($params['id'])) //id=0 shouldn't happen anyway
		unset($params['id']); //@todo can't I just delete it with the others ??

	$last_key = array_keys($params);
	$last_key = $last_key[count($last_key)-1];

	foreach ($params as $k => $v)
	{ //don't append empty values, unless it's the latest key
		if ($v !== '' || $k == $last_key)
			$url .= DS . $k . DS . $v;
	}

	if (!$ext && false !== strpos($url, '.')
	 && (isset($params['ext']) ? $params['ext'] != '' : true))
		$ext = '.' . $router->getExt();

	return $url . $ext;
}