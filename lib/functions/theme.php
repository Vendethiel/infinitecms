<?php
/**
 * return *the* cache file for css.
 */
function stylesheets()
{
	$html = '';
	
	$stylesheets = array_merge(array_unique(extra_asset('css')), (array) STYLESHEET_FILE);
	
	foreach ($stylesheets as $stylesheet)
		$html .= stylesheet_tag(getPath() . $stylesheet);
	
	return $html;
}

/**
 * @return string The javascript tag(s)
 */
function javascripts()
{
	return javascript_tag(getPath() . JAVASCRIPT_FILE) . javascript_extras();
}

function javascript_extras()
{
	$html = '';

	foreach (array_unique(extra_asset('js')) as $extra)
		$html .= "\n" . javascript_tag(getPath() . $extra);

	return $html . "\n";
}

function asset_path($file, $try = false)
{
	global $asset_pipeline;
	
	$file = explode('.', $file);
	if (!isset($file[1]))
	{
		debug_print_backtrace();
		vardump(__FUNCTION__, $file);
	}

	if ($try && !$asset_pipeline->getLocator()->hasFile($file[0], $file[1]))
		return false;

	return getPath() . $asset_pipeline->getLocator()->getFile($file[0], $file[1]);
}

/**
 * applies an asset type for a file
 *
 * @param string $file
 * @param string $type ASSET_* asset type
 * @return void
 */
function apply_asset_type($file, $type)
{
	switch ($type)
	{
		case ASSET_STYLESHEET:
		case ASSET_JAVASCRIPT:
			extra_asset($type, $file);
		break;

		case 'libraries':
			include_library($file);
		break;

		case EXT:
		//case ASSET_PHP:
			require $file;
		break;
		
		default:
			throw new InvalidArgumentException('Asset type ' . $type . ' not found/unsupported');
	}
}

/**
 * includes a library
 *
 * @param string $type LIBRARY_* library type
 * @return void
 */
function include_library($name)
{
	global $config, $libraries;

	if (!isset($libraries[$name]))
		throw new InvalidArgumentException('Library ' . $name . ' cannot be found !');

	$library = $libraries[$name];
	if (isset($library['if']))
	{
		if (!$library['if'])
			return; //not allowed =(

		unset($library['if']); //else ...
	}
	
	foreach ($library as $type => $files)
	{
		foreach ($files as $file)
			apply_asset_type($file, $type);
	}
}

function extra_asset($type, $file = null)
{
	global $asset_pipeline;
	static $extras = array();
	
	if (!isset($extras[$type]))
		$extras[$type] = array();
	
	if (null === $file)
		return $extras[$type];

	$extras[$type][] = $asset_pipeline->getLocator()->getFile($file, $type);
}

/**
 * renders a controller partial (old way namespacing)
 *
 * @package Helper
 * @subpackage Template
 *
 * @param string $name Name (location) of the partial
 * @param array $sandbox Variables on the partial
 * @return void
 */
function partial($name, $sandbox = PARTIAL_FULL)
{
	global $router, $member, $account, $view_pipeline;

	$e = null;
	if (false !== strpos($name, '/'))
	{
		list($c, $name) = explode('/', $name);

		if (false !== strpos($name, '.'))
			list($name, $e) = explode('.', $name);
	}
	else
		$c = $router->getController();

	if (is_integer($sandbox))
	{
		if ($sandbox >= PARTIAL_SEMI)
			global $layout;
		$sandbox = array();
	}
	global $config;


	$restaure = array();
	foreach ((array) $sandbox as $k => $v)
	{
		if (is_integer($k))
			global $$v;
		else
		{ //['comments' => news.Comments]
			global $$k;
			if (isset($$k))
				$restaure[$k] = $$k;

			$$k = $v;
		}
	}


	require $router->getPath($c, $name, $e);

	foreach ($restaure as $k => $v)
		$k = $v;
}

/**
 * registers a variable
 *
 * @param string|null $name variable's name.	| If both $name and $value are null,
 * @param string|null $value variable's value.	| $variables are returned and cleared
 * @param void|array $variables if null == $name == $value else void
 */
function register_variable($name = null, $value = null)
{
	static $variables = array();

	if (null === $name && null === $value)
		return $variables;

	if (is_array($name))
	{
		foreach ($name as $k => $v)
			$variables[$k] = $v;
	}
	else
		$variables[$name] = $value;
}

/**
 * securizes for show (who said "uglyyyyy" :x ?!)
 *
 * @package Helper
 * @subpackage HTML
 *
 * @param string $str String to securise
 * @return string Securised string
 */
function escape_html($str)
{
	return htmlentities($str, ENT_QUOTES, 'UTF-8');
}