<?php
function array_extract(array $ary, array $cols) {
	$res = array();
	foreach ($cols as $col)
		$res[$col] = $ary[$col];
	return $res;
}
