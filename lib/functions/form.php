<?php
/**
 * renders error
 *
 * @package Helper
 * @subpackage Error
 *
 * @param string|null $for Namespace
 *
 * @return string Error, to show
 */
function render_errors($for = null)
{
	global $errors;
	$str = '';
	$err = $errors;
	if (null !== $for)
		$err = $err[$for];
	if (!empty($errors))
	{
		$str = "<ul>\n";

		foreach ($errors as $error)
			$str .= "\t<li>\n\t\t<span style=\"color: red;\">\n\t\t\t&bull;&nbsp;" . $error . "\n\t\t</span>\n\t</li>\n";
		
		$str .= '</ul>';
	}

	//clean errors
	$errors = array();
	return $str;
}

/**
 * creates a form
 * @todo rename
 *
 * @package Helper
 * @subpackage HTML
 *
 * @param array|string $columns columns
 * @param string|array $loc URL (will be formatted)
 * @param array $opts options
 *
 * @return string the form rendered
 */
function make_form($columns, $loc = '#', $opts = array())
{
	global $router;

	if (!isset($opts['method']))
		$opts['method'] = 'POST';
	if (!isset($opts['sep_inputs']))
		$opts['sep_inputs'] = tag('br');
	if (!isset($opts['append_form_tag']))
		$opts['append_form_tag'] = true;
	if (!isset($opts['submit_text']))
		$opts['submit_text'] = lang('send');
	if (!isset($opts['submit_hide-this']))
		$opts['submit_hide-this'] = false;
	if (!isset($opts['submit_add']))
		$opts['submit_add'] = array();
	if (!isset($opts['add']))
		$opts['add'] = array();
	if (isset($opts['submit_add']['class']))
	{
		if (is_array($opts['submit_add']['class']))
			$opts['submit_add']['class'] = implode(' ', $opts['submit_add']['class']);
	}
	else
		$opts['submit_add']['class'] = '';
	if ($opts['submit_hide-this'])
		$opts['submit_add']['class'] .= ' hide-this';

	if ($loc === '#')
	{
		$params = array(
			'controller' => $router->getController(),
			'action' => $router->getAction(),
		);
		if (null != $id = $router->requestVar('id'))
			$params['id'] = $id;
		$loc = to_url($params);
	}
	else if ($loc instanceof \Record)
	{
		$opts['method'] = $loc->exists() ? Router::PUT : Router::POST;

		$loc = replace_url($loc, array('rest' => true));
	}
	else
		$loc = replace_url($loc);

	$first_col = '';
	$str = render_errors();

	if (is_array($columns))
		$str .= form_columns($columns, $opts);
	else
		$str .= $columns;

	//highly experimental
	if (empty($opts['disable_csrf']) && $opts['method'] == Router::POST)
		$str .= input_csrf_token();
	if ($opts['method'] != Router::GET && $opts['method'] != Router::POST)
	{
		$str .= input('_method', null, 'hidden', $opts['method']);
		$opts['method'] = Router::POST;
	}

	if (null !== $opts['submit_text'])
		$str .= input('send', $opts['submit_text'], 'submit', null, $opts['submit_add']);

	if (false !== strpos($str, ' type="file"')) //if there's a file
		$enctype = 'multipart/form-data';
	else
		$enctype = '';

	//we want the close tag if !isset
	$close_tag = isset($opts['close_tag']) && empty($opts['close_tag']) ? '' : '</form>';
	$form_opts = array_merge($opts['add'], array('method' => $opts['method'], 'action' => $loc, 'id' => 'form', 'enctype' => $enctype));

	return $opts['append_form_tag'] ?
	 tag('form', $form_opts) . $str . $close_tag :
	 $str;
}

/**
 * creates form's body : inputs !
 * (subroutine for make_form)
 */
function form_columns($columns, $opts)
{
	static $randKeys = array();
	static $nesting = 0;
	++$nesting;
	$str = '';

	foreach ($columns as $i => $column)
	{
		if (empty($column))
			continue;

		if (is_integer($i))
		{
			if (is_array($column))
			{
				if (empty($first_col))
					$first_col = $column[0];

				if (!isset($column[2]))
					$column[2] = null;

				$br = isset($column[5]) ? $column[5] : true;
				if (empty($column[3]))
					$column[3] = '';
				if (empty($column[4]))
					$column[4] = array();

				$str .= input($column[0], $column[1], $column[2], $column[3], $column[4])
				 . ( $br && $column[2] != 'hidden' ? $opts['sep_inputs'] : '' ); //hidden hardcoded fu-
			}
			else
				$str .= $column;
		}
		else
		{
			$temp = '';
			if (is_array($column))
			{ //$columns = ['e' => [xx]]
				//fieldSet
				$count = count($column) - 1;
				do
				{
					$randKey = rand(0, 5000);
				}
				while (in_array($randKey, $randKeys));

				$randKeys[] = $randKey;

				$temp .= form_columns($column, $opts);
			}
			else
				$temp = $column;

			//@todo: a way to return the fs key(s) ? =|.
			$str .= tag('fieldset', array('id' => 'fs_' . $randKey),
					 tag('legend', tag('span', array('name' => $i, 'class' => 'slideMenu'),
					  js_link('$("#fs-div-' . $randKey . '").slideToggle()', $i))) .
					 tag('div', array('class' => 'inputs', 'id' => 'fs-div-' . $randKey), $temp));

			if ($nesting > 1)
				jQ('$("#fs-div-' . $randKey . '").toggle();');
		}
	}
	--$nesting;
	return $str;
}