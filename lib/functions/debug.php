<?php
/**
 * Prints the backtrace and stops execution
 *
 * @package Debug
 */
function d_p_b()
{
	ob_start();
	debug_print_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);

	exit(nl2br(ob_get_clean()));
}

/**
 * dumps a variable then exit();
 *
 * @package Debug
 */
function vdump()
{
	debug_print_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 1);
	$args = func_get_args();
	call_user_func_array('vardump', $args);
	exit;
}

/**
 * like var_dump but with "pre" formatting
 *
 * @package Debug
 *
 * @param mixed $v Variable to dump
 * @return void
 */
function vardump()
{
	$args = func_get_args();
	//move <pre> here ?
	foreach ($args as $arg)
	{
		echo '<pre>';
		var_dump($arg);
		echo '</pre>';
	}
	//move </pre> here ?
}

/**
 * like print_r but with <pre> formatting
 *
 * @subpackage Debug
 *
 * @param array $a Array to dump
 * @param boolean $s -default=null Stop?
 * @return void
 */
function printr($a, $s = true)
{
	if (DEBUG)
	{
		tag('pre', print_r($a, true));
		if ($s) exit;
	}
	else
	{
		echo '<!-- Still in debug mode ! - %s-->';
		debug_print_backtrace(); //trace
	}
}