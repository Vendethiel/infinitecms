<?php
/**
 * determinates whether time() is passed or not
 *
 * @return boolean @see time_passed
 */
function date_passed()
{
	$args = func_get_args();
	return call_user_func_array('time_passed', array_merge(array(time()), $args));
}

/**
 * determinates whether a date is passed or not
 *
 * @param integer the date
 * @return boolean passed or not
 */
function time_passed($from)
{
	$time = 0;
	foreach(array_slice(func_get_args(), 1)  as $arg)
		$time += is_numeric($arg) ? $arg : strtotime($arg, 0);

	return $from > $time;
}

/**
 * generates a date from a datepicker
 *
 * @param string $date date to parse
 * @return DateTime the datetime object
 */
function date_from_picker($date)
{
	$date = implode('-', array_reverse(explode('/', $date)));
	if (strlen($date) !== 10)
		return false;
	return date_create($date);
}

/**
 * generates a date for a datepicker
 *
 * @param string $date date to parse
 * @return string the new date
 */
function date_to_picker($date)
{
	if ($date instanceof DateTime)
		$date = $date->format('Y-m-d');
	$date = implode('/', array_reverse(explode('-', $date)));
	if (strlen($date) !== 10)
		return false;
	return $date;
}

/**
 * generates a datetime from a datetimepicker
 *
 * @param string $date date+time to parse
 * @return DateTime the datetime object
 */
function datetime_from_picker($date)
{
	list($date, $time) = explode(' ', $date);
	$time = explode(':', $time);
	if (count($time) < 2)
		return false;
	if (!$date = date_from_picker($date))
		return false;

	$date = $date->modify('+' . $time[0] . ' hours');
	$date = $date->modify('+' . $time[1] . ' minutes');
	return $date;
}

/**
 * generates a datetime for a datepicker
 *
 * @param string $date datetime to parse
 * @return string the new datetime
 */
function datetime_to_picker($datetime)
{
	if ($datetime instanceof DateTime)
		$datetime = $date->format('Y-m-d H:i');
	list($date, $time) = explode(' ', $datetime);
	$date = implode('/', array_reverse(explode('-', $date)));
	if (strlen($date) !== 10)
		return false;
	$time = explode(':', $time);
	if (count($time) < 2)
		return false;
	$time = implode(':', array($time[0], $time[1]));
	return $date . ' ' . $time;
}

/**
 * @package time
 */
function beginning_of_month($m = null, $y = null)
{
	if (null === $m)
		$m = date('m');
	if (null === $y)
		$y = date('Y');

	return strtotime("1/$m/$y");
}

function end_of_month($m = null, $y = null)
{
	if (null === $m)
		$m = date('m');
	if (null === $y)
		$y = date('Y');

	return mktime(0, 0, 0, $m + 1, 0, $y);
}