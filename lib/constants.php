<?php
//Directory Separator (DIRECTORY_SEPARATOR)
define('DS', '/');


//I created this constants because I hate "ghost parameters"

/* * Reverse level */
define('REQUIRE_NOT', true);

//@see parse_hour
/* * A second */
define('TYPE_DATE_SECOND', 1);
/* * A minute */
define('TYPE_DATE_MINUTE', TYPE_DATE_SECOND * 60);
/* * A hour */
define('TYPE_DATE_HOUR', TYPE_DATE_MINUTE * 60);
/* * A day */
define('TYPE_DATE_DAY', TYPE_DATE_HOUR * 24);
/* * A week */
define('TYPE_DATE_WEEK', TYPE_DATE_DAY * 7);

//@see partial
/* * Mini-authorized partial */
define('PARTIAL_SANDBOXED', 0);
/* * Semi-authorized partial */
define('PARTIAL_SEMI', 1);
/* * Full-authorized partial */
define('PARTIAL_FULL', 2);

/* * A TPL */
define('PARTIAL_TPL', false);
/* * A controller */
define('PARTIAL_CONTROLLER', true);

//vote errors
define('VOTE_OK', 0);
define('VOTE_WAIT', 1);
define('VOTE_ACC', 2);

//@see pluralize
/* * Show the pluralized value */
define('SHOW_VALUE', true);
define('HIDE_VALUE', true);

defined('INSTALL') || define('INSTALL', file_exists('install') || file_exists('install.txt'));

defined('DEBUG') || define('DEBUG', ( isset($_SERVER['SESSIONNAME']) && $_SERVER['SESSIONNAME'] === 'Console' )
				 || file_exists('debug') || file_exists('debug.txt') //let's allow debug.txt here ...
				 || $_SERVER['REMOTE_ADDR'] === '127.0.0.1'); //@todo remove that ? confusing noobs

//CMS Version
define('VERSION', '1.3RC9');

//Asset types
//@todo I don't think they're needed anymore since sprockets-php
define('ASSET_PHP',			'php');
define('ASSET_HTML',		'html');
define('ASSET_CFG',			'cfg');
define('ASSET_JAVASCRIPT',	'js');
define('ASSET_STYLESHEET',	'css');
define('ASSET_TXT',			'txt');
define('ASSET_GIF',			'gif');
define('ASSET_JPG',			'jpg');
define('ASSET_PNG',			'png');
define('ASSET_IMAGE',		'image');
define('ASSET_GIMAGE',		'global_image');

//Some extensions
define('EXT_GIF', '.' . ASSET_GIF);
define('EXT_JPG', '.' . ASSET_JPG);
define('EXT_PNG', '.' . ASSET_PNG);
define('EXT_TXT', '.' . ASSET_TXT);

//Asset config
define('ASSET_DEPTH', 2);

//Asset pipeline modes
define('MODE_CONTENT', 1);
define('MODE_FILENAME', 2);
define('MODE_FILTERS', 3);

//Page-load type
define('LOAD_NONE', -1);   //no-AJaX
define('LOAD_NOTHING', 0); //nothing
define('LOAD_CONTENT', 1); //change content
define('LOAD_MDIALOG', 2); //modal dialog

//For BugTracker (No real enums in PHP)
define('STATE_SUBMITTED', 0);
define('STATE_RESOLVING', 1);
define('STATE_RESOLVED', 2);

//User levels
//WARNING : you must change the list in Member.php (getLevels and getFormattedLevels)
define('LEVEL_BANNED', -2.0);
define('LEVEL_GUEST', -1.0);
define('LEVEL_LOGGED', 0.0);
define('LEVEL_VIP', 0.5); //[...] type them oll
define('LEVEL_TEST', 1.0);
define('LEVEL_MODO', 2.0);
define('LEVEL_MJ', 3.0);
define('LEVEL_ADMIN', 4.0);

//Libraries
define('LIBRARY_TINYMCE', 'TinyMCE');
define('LIBRARY_HIGHSLIDE', 'HighSlide');
define('LIBRARY_HIGHCHARTS', 'HighCharts');
define('LIBRARY_DATATABLES', 'DataTables');
define('LIBRARY_TOKENINPUT', 'TokenInput');
define('LIBRARY_DATEPICKER', 'UI_DatePicker');
define('LIBRARY_TIMEPICKER', 'UI_TimePicker');
define('LIBRARY_SWFOBJECT', 'SwfObject');

$libraries = array( //php html cfg js css img
	LIBRARY_TINYMCE => array(
		ASSET_JAVASCRIPT => array(
			'tinymce-release/tiny_mce',
			'tinymce/jscripts/tiny_mce/classes/adapter/jquery/jquery.tinymce',
			'tinymce/jscripts/tiny_mce/classes/adapter/jquery/adapter',
		),
	),
	LIBRARY_HIGHSLIDE => array(
		ASSET_JAVASCRIPT => array('HighSlide/core', 'HighSlide/bootstrap'),
		ASSET_STYLESHEET => array('HighSlide'),
	),
	LIBRARY_HIGHCHARTS => array(
		ASSET_JAVASCRIPT => array('jq/highCharts'),
	),
	LIBRARY_DATATABLES => array(
		ASSET_JAVASCRIPT => array('datatables/media/js/jquery.dataTables'),
	),
	LIBRARY_TOKENINPUT => array(
		ASSET_JAVASCRIPT => array('jq/tokenInput'),
	),	
	LIBRARY_DATEPICKER => array(
		ASSET_JAVASCRIPT => array('jquery-ui/ui/jquery.ui.datepicker'),
	),
	LIBRARY_TIMEPICKER => array(
		'libraries' => array(LIBRARY_DATEPICKER),
		ASSET_JAVASCRIPT => array('jquery-ui/ui/jquery.ui.slider', 'jq/UI/timepicker'),
	),
	LIBRARY_SWFOBJECT => array(
		ASSET_JAVASCRIPT => array('jq/SWFObject'),
	),
);