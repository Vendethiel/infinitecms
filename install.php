<?php
define('INSTALL', file_exists('install') || file_exists('install.txt'));
define('EXT', strrchr(__FILE__, '.'));
define('ROOT', './');
define('DEV', true);
define('DB_DRIVER', 'mysql');
header('Content-Type: text/html; charset=UTF-8');

function bench(){}

$langs = glob('lang/*');
$firstLang = substr(reset($langs), 5); //remove lang/

set_include_path(implode(PATH_SEPARATOR, array(
	ROOT, //see #0.3.2a
	'lib/class/', //local libs > global libs
#	get_include_path(),
)));

defined('DEBUG') || define('DEBUG', ( isset($_SERVER['SESSIONNAME']) && $_SERVER['SESSIONNAME'] === 'Console' )
				 || $_SERVER['REMOTE_ADDR'] === '127.0.0.1'
				 || file_exists('debug') || file_exists('debug.txt')); //let's allow debug.txt here ...

define('LANG', isset($_REQUEST['lang']) && strlen($_REQUEST['lang']) == 2 && file_exists('lang/' . $_REQUEST['lang']) ?
 $_REQUEST['lang'] : $firstLang);

require 'lib/constants' . EXT;
require 'lib/functions' . EXT;
if (file_exists('config' . EXT))
	@require 'config' . EXT; //this is just required for "defined? INSTALL"
include 'vendor/php/autoload' . EXT;

if (INSTALL)
	exit(lang('already', 'install'));

if (!DEBUG)
	exit(lang('debug', 'install'));

$inputs = array(
	array('lang', null, LANG),
	array('IP_SERV', lang('server.ip', 'install'), '127.0.0.1', FILTER_VALIDATE_IP),
	array('PORT_SERV', lang('server.port', 'install'), '443', FILTER_VALIDATE_INT),
	array('VOTE_ID', 'RPG Paradize ID (off = -1)', '-1', FILTER_VALIDATE_INT),
	array('BOARD_URL', lang('board.url', 'install'), './forum/'),
	'<br />',
	array('DB_HOST', lang('db.host', 'install'), '127.0.0.1'),
	array('DB_USER', lang('db.user', 'install'), 'root'),
	array('DB_PSWD', lang('db.pswd', 'install'), ''),
	array('DB_STATIC', lang('db.name.static', 'install'), 'ancestra_static'),
	array('DB_OTHER', lang('db.name.other', 'install'), 'ancestra_other'),
	'<br />',
	array('PASS_TYPE', lang('pass.type', 'install'), '', array('', 'star'), function ($input, $table) { $table['%%PASS_ENABLE%%'] = empty($input) ? 'false' : 'true'; return $table; }),
	array('PASS_IDD', lang('pass.star.idd', 'install'), null, FILTER_VALIDATE_INT),
	array('PASS_CC', lang('pass.webo.cc', 'install'), null, FILTER_VALIDATE_INT),
	array('PASS_DOC', lang('pass.webo.document', 'install')),
);

$errors = array();
if (!empty($_POST))
{
	$table = array();
	foreach ($inputs as $input)
	{
		if (!is_array($input))
			continue;

		$k = $input[0];
		$key = '%%' . $k . '%%';


		if (!isset($_POST[$k]))
		{
			$errors[] = 'must not be empty/ne doit pas être vide : ' . $k;
			continue;
		}

		if (isset($input[3]))
		{
			if (is_array($input[3]))
			{
				if (isset($input[3][$_POST[$k]])) //eh eh
					$value = $input[3][$_POST[$k]];
				else
				{
					$errors[] = 'invalid array value : ' . $k;
					continue;
				}
			}
			else
			{
				if (false === ($value = filter_var($_POST[$k], $input[3])))
				{
					$errors[] = 'invalid : ' . $k;
					continue;
				}
			}
		}
		else
			$value = $_POST[$k];

		if (isset($input[4]))
			$table = call_user_func($input[4], $value, $table);

		$table[$key] = $value;
	}

	$template = "<?php
//auto-installed

return array(
# BASICS
#At each installation, you'll edit this part
#Pour chaque installation, vous devrez modifier cette partie
	//Informations on the owner
	//Informations sur les possesseurs du serveurs
	'SERVER_NAME' => 'Myserver',
	'SERVER_EMAIL' => 'contact@monserver.com',
	//you can delete the key below if your server has not corporation
	// (by adding a sharp before the line, #'SERVER_CORP' => ...)
	//vous pouvez supprimer cette entrée si votre serveur n'appartient pas à une entreprise
	// (en ajoutant un dièse avant la ligne, #'SERVER_CORP' => ...)
	'SERVER_CORP' => 'MyServerCorporation',

	//Title of a page. Vars you can use : {title} (the default title, as 'Index')
	// {server.name}, {server.corp}
	//Titre de la page. Variables utilisables : {title} (titre normal, comme 'Index')
	// {server.name}, {server.corp}
	'TITLE' => '{page} &bull; {server.name}',


	//Is the shop enabled ?
	//La boutique est-elle activée ?
	'ENABLE_SHOP' => true,
	//Your RPG Paradize ID (don't put http://rpg-paradize.com/...)
	// IF YOU WANT TO DISABLE VOTE, PUT -1 !
	//Votre ID RPG Paradize (ne mettez pas http://rpg-paradize.com/...)
	// SI VOUS VOULEZ DÉSACTIVER LE VOTE, METTEZ -1 !
	'URL_VOTE' => -1,
	//Points given per vote
	//Points donnés pour un vote
	'POINTS_VOTE' => 1,
	//Points given for one pass
	//Points donnés pour un pass
	'POINTS_CREDIT' => 100,
	//Points given for a vote from a VIP
	//Points donnés pour un vote PAR UN VIP
	'POINTS_VOTE_VIP' => 4,
	//Points given for one pass from a VIP
	//Points donnés pour un pass PAR UN VIP
	'POINTS_CREDIT_VIP' => 140,
	//Points to become VIP. To disable VIP mode, set to 0
	//Points à payer pour être VIP. Pour désactiver le mode VIP, mettez 0
	'COST_VIP' => 5,

	//URL of board (if it's an external url, the url _MUST_ begin with http://)
	// (if you don't have a board, put # before the line: #'BOARD_URL' => ...)
	//Adresse du forum (si c'est une adresse externe, elle _DOIT_ commencer avec http://)
	// (si vous n'avez pas de forum, ajoutez # au début de la ligne : #'BOARD_URL' => ...)
	'BOARD_URL' => './forum/',

	//Used for 'server online?'
	//Utilisé pour 'serveur en ligne ?'
	'IP_SERV' => '%%IP_SERV%%',
	'PORT_SERV' => %%PORT_SERV%%,

	//Downloads link (Misc/join)
	//Liens de téléchargement (Misc/join)
	'DOWNLOAD' => array(
		//URL for download 1.29.1 client
		//Adresse pour télécharger le client 1.29.1
		'CLIENT' => 'http://sourceforge.net/projects/dof129/files/DofusInstaller_v1_29_0.exe/download',
		//URL of the launcher (32 bits)
		//Adresse pour télécharger le launcher (32 bits)
		'LAUNCHER.32' => './client/launcher_setup.exe',
		//URL of the launcher (64 bits)
		//Adresse pour télécharger le launcher (64 bits)
		'LAUNCHER.64' => './client/launcher_setup.exe',
		//URL of the config file
		//Adresse du fichier de configuration
		'CONFIG' => './config.xml',
	),

# DB CONFIGURATION
#You won't modify that unless you did some 'tricks' to your DB.
#Vous ne devriez pas avoir à modifier ça, sauf si vous avez touché à votre DB
	//TYPE of DB Server (as mysql, mssql)
	//TYPE du serveur BDD (comme mysql/mssql ... J'ai crée ce CMS de manière
	// à ce que ce paramètre n'ai pas de répercussion)
	'DB_TYPE' => '" . DB_DRIVER . "',
	//HOST of DB Server
	//Hébergeur du serveur BDD
	'DB_HOST' => '%%DB_HOST%%',
	//User of DB Server
	//Utilisateur pour la base de donn&eacute;es
	'DB_USER' => '%%DB_USER%%',
	//Password of the user of the DB Server
	//Mot de passe pour se connecter à l'utilisateur dans la base de données
	'DB_PSWD' => '%%DB_PSWD%%',
	//Database name static
	//Nom de la base de données statique
	'DB_STATIC' => '%%DB_STATIC%%',
	//Database name other
	//Nom de la base de données other
	'DB_OTHER' => '%%DB_OTHER%%',

# PASS CONFIGURATION
#You must edit that if the shop is enabled
#Vous devez modifier ça si la boutique est activée
	'PASS' => array(
		//Is the pass enabled ?
		//Le système de créditage est-il actif ?
		'ENABLE' => %%PASS_ENABLE%%,

		//different possible type: webo, star (case sensitive)
		//IT MUST BE FORMATTED THIS WAY : (without sharp (#) at the line start)
#		'TYPE' => 'star',
		//différent types possibles: webo, star (sensible à la casse !)
		// LE FORMAT DOIT ÊTRE LE SUIVANT : (sans dièse (#) au début de la ligne)
#		'TYPE' => 'star / webo',
		'TYPE' => '%%PASS_TYPE%%',

		//If it's webopass ('webo' in type)
		//Si c'est webopass ('webo' dans type)
		'CC' => '%%PASS_CC%%',
		'DOCUMENT' => '%%PASS_DOC%%',

		//If it's starpass ('star' in type)
		//Si c'est starpass ('star' dans type)
		'IDD' => %%PASS_IDD%%,

		//Else: not implemented yet
		//Sinon: pas encore disponible
	),

# TEAMSPEAK
//Okay I'm gonna remove that, TS sucks x(
	'TEAMSPEAK' => array(
		//Is the TS server is opened?
		//Le serveur TS est-il ouvert ?
		'ENABLE' => false,
		//TS server address
		//Adresse du serveur TS
		'SERVER' => 'mon serveur',
		//Port of the TS Server
		//Port du serveur TS
		'PORT' => 'mon port',
		//Password, comment this line if your TS serv don't need a pass
		// (by adding a sharp before the line, #'pass' => ...)
		//Mot de passe, commentez cette ligne si votre serveur TS n'en a pas
		// (en ajoutant un dièse avant la ligne, #'pass' => ...)
		'PASS' => 'mon mot de passe',
	),

# SETTINGS
#Configuration basics
#Configuration basique
	//Registration enabled ?
	//Inscription activée ?
	'ENABLE_REG' => true,
	//Allow multi-account ?
	//Autoriser le multi-compte ?
	'ALLOW_MULTI' => true,
	//Allow multiple usage of the same e-mail adress ?
	//Autoriser la réutilisation d'adresses e-mail ?
	'ALLOW_MULTI_EMAIL' => true,

	//YES IT IS \"PER\", fu-
	//Number of bugs by pages on the BugTracker
	//Nombre de bugs à montrer par page sur le bugTracker
#	'BUGS_BY_PAGE' => 30, //commented: not finished functionnality | commenté: fonctionnalité non terminée
	//Number of articles by pages on the index
	//Nombre d'articles à montrer par page sur l'index
	'ARTICLES_BY_PAGE' => 3,
	//Number of rate by page in the GuestBook.
	// To disable the GuestBook, put -1
	//Nombre de commentaires par page dans le Livre d'Or
	// Pour désactiver le livre d'or, mettez -1
	'RATES_BY_PAGE' => 5,
	//Number of Private Messages by page to show (in private message index)
	//Nombre de messages privés à montrer (dans la boîte de réception)
	'PM_BY_PAGE' => 10,
	//Number of Private Message Answers by page to show (in private message view)
	//Nombre de messages aux messages privés à montrer (dans la vue d'un message privé)
	'PMA_BY_PAGE' => 3,
	//Number of items to show by line
	//Nombre d'objets par ligne dans la boutique
	'ITEMS_BY_LINE' => 2,
	//Number of lines by page on shop
	//Nombre de lignes d'objets par page dans la boutique
	'ITEM_LINES_BY_PAGE' => 2,
	//Number of screenshots by page
	//Nombre de captures d'écran par page
	'SCREENSHOTS_BY_PAGE' => 3,
	//Internal polling
	'POLLS_BY_PAGE' => 5,
	//Number of character by page on ladder
	//Nombre de personnages par page dans les ladders
	'LADDER_LIMIT' => 10,
	//Show staff on ladder ?
	//Afficher le staff sur le ladder ?
	'LADDER_STAFF' => true,

	//STATS: Show (or not) 'Accounts created', 'Characters created' and 'accounts logged on'
	// It costs 3 SQL Queries (1h cache). It enables also the Misc/stats page (showing all stats, 6h cache)
	//STATS: Montrer (ou pas) 'Comptes créés', 'Personnages créés' et 'Comptes connectés'
	// Cela coûte 3 requêtes SQL, il y a un cache d'une heure. Cela active aussi la page Misc/stats (toutes les stats, cache de 6h)
	'STATS' => true,

	//Time between 2 refresh of : server state / next PM notif
	// (this is triggered when you stay on the same page) (in seconds)
	//Temps entre 2 rafraichissiments de : l'état du serveur / vérification des nouveaux messages privés
	// (ceci est lancé quand vous restez sur la même page) (en secondes)
	'REFRESH_TIME' => 30,

	//Is the server actually in maintenance ?
	//Est-ce que le serveur est en maintenance ?
	// true / false
	'MAINTENANCE' => false,

	//Theme (located under /app/themes/)
	//Thème (situés dans /app/themes/)
	// * woa
	// * yuuki
	// * fantasia
	// * asterion12
	// * pandora
	// * scylla
	'THEME' => 'yuuki',

	//CyonEmu etc =>
	//Points table : uncomment if on points is stored on account
	//Table des points : true si votre émulateur
	// stocke ses points dans la table `accounts` (pour une boutique en ligne par exemple)
	// pensez à désactiver la boutique plus haut (ENABLE_SHOP)
	'ACCOUNT_POINTS' => false,

	//default lang
	//langue de base
	'LANG_DEFAULT' => 'fr',
	//Langs to load (if commented, the CMS will only load :LANG_DEFAULT)
	// to uncomment, remove the sharp at the beginning of the line
	//Langues à charger (si commenté, la seule langue chargée sera celle d'use_lang)
	// Pour décommenter, enlever le dièse avant la ligne
	//!\Format/!\: 'langs' => array( '1', '2', '3', '4', '5' ),
#	'LANGS' => array( 'fr', 'en' ),

	//Type of AJaX-Load (no-refresh)
	//Type de chargement AJaX (pas de rechargement)
	/*
	 * LOAD_NOTHING => nothing
	 *			rien
	 * LOAD_CONTENT => page content changed with loading message
	 *			contenu de la page remplacé avec le message de chargement
	 * LOAD_MDIALOG => modal dialog box with loading message
	 *			Fenêtre avec le message de chargement
	 * LOAD_NONE	=> no AJaX (by default if JAVASCRIPT => false)
	 *			désactive l'AJaX (par défaut si JAVASCRIPT => false)
	 */
	'LOAD_TYPE' => LOAD_MDIALOG,

	//Enable the JavaScript (widgets: modal box, accordeon, AJaX, Edit In Place, ...), it forces LOAD_TYPE to be NONE
	//Activer le JavaScript (modules: boîte de dialogues, accordeon, AJaX, édition rapide, ...) force LOAD_TYPE à NONE
	'JAVASCRIPT' => true,
);";

	if (empty($errors))
	{
/*
		$cnt = new Pdo('');
		$sql = explode(';', file_get_contents('lib/models/other/sql/InfiniteCMS patch.sql'));
		foreach ($sql as $part)
			$cnt->query($part);
		$cnt->close();
//*/
		@touch('install');
		@unlink('install.php');
		file_put_contents('config' . EXT, strtr($template, $table));

		exit(lang('processed', 'install') . '<br />' . sprintf(lang('process_db', 'install'), $table['%%DB_OTHER%%'], $table['%%DB_STATIC%%']) . '<br />' .
		 sprintf(lang('back_to_index', 'install'), getPath() . 'index' . EXT));
	}
}

$i = array();
foreach ($inputs as $input)
{
	if (!is_array($input))
	{
		$i[] = $input;
		continue;
	}

	$id = $input[0];
	$value = isset($_POST[$id]) ? $_POST[$id] : ( empty($input[2]) ? '' : $input[2] );
	$type = null;
	$add = array();
	if (isset($input[3]))
	{
		if (is_array($input[3]))
		{
			if ($value == '')
				$value = 0;

			$type = 'select';
			$add = isset($input[3][$value]) ? $value : ( array_search($value, $input[3]) ?: 0 );
			$value = $input[3];
		}
		else if (is_string($input[3]))
			$type = $input[3];
		else
			$value = $value == '' && $input[3] == FILTER_VALIDATE_INT ? '0' : $value;
	}
	if ($input[1] === null)
		$type = 'hidden';

	$i[] = array($id, $input[1], $type, $value, $add);
}
echo make_form($i, null, array('submit_text' => 'Go !', 'disable_csrf' => true));