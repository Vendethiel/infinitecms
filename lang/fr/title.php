<?php
return array(
	//Pages titles
	//Titres des pages
	'News - index' => 'Index',
	'News - show' => 'Vue d\'une news',
	'News - create' => 'Création d\'une news',								#fake
	'News - update' => 'Édition d\'une news',
	'News - delete' => 'Suppression d\'une news',
	'News - comment' => 'Commenter',

	'Comment - index' => 'Commentaires',

	'Account - join' => 'Nous rejoindre',
	'Account - show' => 'Compte',
	'Account - create' => 'Inscription',
	'Account - update' => 'Gestion de compte',
	'Account - vip' => 'Devenir VIP',
	'Account - search' => 'Recherche de compte',							#admin
	'Account - logs' => 'Journal',												#admin

	'User - ladder_vote' => 'Classement vote',
	'User - login' => 'Connexion',
	'User - delog' => 'Déconnexion',
	'User - credit' => 'Créditer son compte',
	'User - vote' => 'Vote',
	'User - main' => 'Personnage principal',
	'User - update' => 'Gestion des informations de compte',				#admin

	'Misc - index' => '',
	'Misc - tos' => 'Conditions générales d\'utilisation',
	'Misc - tos_serv' => 'Conditions d\'utilisations du serveur',
	'Misc - ts' => 'Informations sur TeamSpeak',
	'Misc - server' => 'Informations Serveur',
	'Misc - join' => 'Nous rejoindre',
	'Misc - mass_mail' => 'E-mail de masse',								#admin

	'Shop - index' => 'Boutique',
	'Shop - purchase' => 'Achat d\'un objet',
	'Shop - create' => 'Création d\'un objet boutique',						#fake
	'Shop - update' => 'Édition d\'un objet boutique',
	'Shop - delete' => 'Suppression d\'un objet boutique',
	'Shop - search' => 'Recherche d\'objet boutique',

	'ShopCategory - index' => 'Gérer les catégories d\'objet',
	'ShopCategory - create' => 'Création d\'une catégorie d\'objet',		#fake
	'ShopCategory - update' => 'Édition d\'une catégorie d\'objet',
	'ShopCategory - delete' => 'Suppression d\'une catégorie d\'objet',

	'Character - ladder' => 'Classement PvM',
	'Character - search' => 'Recherche de personnage',
	'Character - show' => 'Vue de personnage',
	'Character - give' => 'Faire un don',
	'Character - warp' => 'Téléporter le personnage principal',

	'Guild - show' => 'Informations sur la guilde %s',
	'Guild - ladder' => 'Classement des guildes',

	'GuestBook - index' => 'Livre d\'Or',
	'GuestBook - delete' => 'Suppression d\'un commentaire',
	'GuestBook - update' => 'Ajout d\'un commentaire',

	'Poll - index' => 'Sondages',
	'Poll - show' => 'Vue d\'un sondage',
	'Poll - update' => 'Édition d\'un sondage',
	'Poll - delete' => 'Suppression d\'un sondage',

	'PollOption - update' => 'Nouvelle option',
	'PollOption - delete' => 'Suppression d\'une option',
	'PollOption - vote' => 'Voter',

	'PrivateMessage - index' => 'Messagerie',
	'PrivateMessage - show' => 'Vue d\'un message privé',
	'PrivateMessage - create' => 'Création d\'un message privé',

	'Event - index' => 'Évènements',
	'Event - update' => 'Édition d\'évènement',
	'Event - create' => 'Création d\'un évènement',
	'Event - win' => 'Définir le gagnant',									#admin

	'StaffRole - index' => 'Équipe du serveur',
	'StaffRole - create' => 'Création d\'un rôle',							#fake
	'StaffRole - update' => 'Édition d\'un rôle',
	'StaffRole - delete' => 'Suppression d\'un rôle',

	'Contest - index' => 'Concours',
	'Contest - create' => 'Création d\'un concours',						#fake
#	'Contest - update' => 'Édition d\'un concours',
#	'Contest - delete' => 'Suppression d\'un concours',

	'ContestJuror - create' => 'Ajout d\'un juré',

	'ContestParticipant - vote' => 'Voter pour un participant',

	'Ban - index' => 'Bannissements',

	'ItemTemplate - index' => 'Modèles d\'objet',
	'ItemTemplate - create' => 'Création d\'un objet',						#fake
	'ItemTemplate - update' => 'Édition d\'un objet',
	'ItemTemplate - delete' => 'Suppression d\'un objet',

	'Tutorial - index' => 'Tutoriels',
	'Tutorial - show' => 'Vue d\'un tutoriel',
	'Tutorial - create' => 'Création d\'un tutoriel',
	'Tutorial - update' => 'Édition d\'un tutoriel',
	'Tutorial - delete' => 'Suppression d\'un tutoriel',

	'Gallery - index' => 'Galeries',
	'Gallery - show' => 'Captures d\'écran',
	'Gallery - create' => 'Création d\'une galerie',						#fake

	'GalleryImage - create' => 'Ajout d\'une capture d\'écran',				#fake
	'GalleryImage - delete' => 'Suppression d\'une capture d\'écran',		#fake

	'GlossaryExperience - index' => 'Paliers d\'expérience',	

	'GlossaryItem - index' => 'Catégories',
	'GlossaryItem - category' => 'Catégorie',
	'GlossaryItem - show' => 'Vue d\'objet',
	'GlossaryItem - create' => 'Création d\'un objet',						#fake
	'GlossaryItem - update' => 'Édition d\'un objet',

	'GlossaryMonster - index' => 'Monstres',
	'GlossaryMonster - show' => 'Vue de monstre',
	'GlossaryMonster - create' => 'Création d\'un monstre',					#fake
	'GlossaryMonster - update' => 'Édition d\'un monstre',

	'GlossaryLoot - update' => 'Ajouter un drop', #should probably be "create"

	'GlossarySpell - index' => 'Sorts',
	'GlossarySpell - show' => 'Vue de sort',

	'GlossaryNpc - index' => 'PNJs',
	'GlossaryNpc - show' => 'Vue de PNJ',

	'GlossaryNpcMap - update' => 'Placer un PNJ',

	'Stats - game' => 'Statistiques serveur',
	'Stats - site' => 'Statistiques site',									#admin
);
