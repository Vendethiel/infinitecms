<?php
return array(
	'debug' => 'Vous devez être en mode DEBUG pour utiliser le module d\'installation.<br />Pour ce faire, vous devez créer un fichier "debug" ou "debug.txt" dans le dossier racine du CMS.',
	'already' => 'Le CMS est déjà installé !',
	'processed' => 'La configuration a été mise à jour !', //%s = link to Index
	'process_db' => 'N\'oubliez pas d\'exécuter le fichier <i>app/models/other/sql/InfiniteCMS patch.sql</i> dans la base de données <i>%s</i>, ainsi que le fichier <i>app/models/static/sql/InfiniteCMS patch.sql</i> dans la base de données <i>%s</i>.', //%s = other db name
	'back_to_index' => 'Cliquez <a href="%s">Ici</a> pour revenir à l\'Index.',

	'server.ip' => 'Adresse IP du serveur',
	'server.port' => 'Port du serveur',

	'board.url' => 'Adresse du forum',

	'db.host' => 'Hôte de la base de données',
	'db.user' => 'Utilisateur de la base de données',
	'db.pswd' => 'Mot de passe de la base de données',
	'db.name.static' => 'Nom de la base de données "static"',
	'db.name.other' => 'Nom de la base de données "other"',

	'pass.type' => 'Type de payement (vide pour désactiver)',
	'pass.star.idd' => 'Starpass : code IDD',
	'pass.webo.cc' => 'Webopass : code CC',
	'pass.webo.document' => 'Webopass : code Document',
);