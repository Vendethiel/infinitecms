<?php
//user-defined
return array(
	'maintenance' => 'Une maintenance est en cours sur nos serveurs, veuillez repasser plus tard.',
	'join' => '<h1>Téléchargement</h1>
<b>Vous n\'avez pas le client DOFUS en 1.29 ?</b><br />
Téléchargez-le <a href="%client%">ici</a> !<br />
<b>Vous avez déjà le client DOFUS en 1.29 ?</b><br />
Téléchargez le launcher <a href="%launcher.32%">ici</a> !<br /><br /><hr style="width: 50%;" />
Ou téléchargez le launcher pour un ordinateur 64bits <a href="%launcher.64%">ici</a><br />
Note: Il faut avoir téléchargé la configuration (Voir ci dessous).
<br /><hr style="width: 50%%" />
Et téléchargez directement la configuration <a href="%config%">ici</a> et installez-le en double cliquant dessus.
<br /><br /><br />{log}<!--{/log}{register}
<h1>Inscription</h1>
Vous pouvez vous inscrire %register%.{/register}{log}-->{/log}', //{log} = if logged. {/log} = end if logged. %register% => "here"
	'tos_serv' => '<div class="black"><p>- Les insultes sont interdites. Tout racisme, xénophobie, vulgaritée conduira à des sanctions.</p></div>
		<div class="white"><p>- Le flood est interdit. Pour garder la bonne ambiance dans le jeu, les modérateurs expulseront les personnes répétants des messages trop de fois, des messages hors-charte, de publicité ou autre.</p></div>
		<div class="black"><p>- L\'utilisationde bugs est interdit. Puisque rien n\'est jamais parfait, il peut subsister quelques bugs. Tous ceux qui utiliseront ces bugs verront leurs personnages supprimés et leur compte suspendu.</p></div>',
	'tos' => '<h3 class="sub_tit">CONDITIONS D\'UTILISATION</h3>
	<div>
		Ce site internet n\'est pas le site officiel de DOFUS, ce site internet n\'est en AUCUN CAS affilié à Ankama, ou tout autre fournisseur du jeu DOFUS.
		En acceptant cet accord, vous devenez responsable de toute action que vous entreprenez sur ce site internet. Si votre action est en violation avec les lois de votre ètat, nous ne pourrions &ecirc;tre tenu responsables. Vous pouvez utiliser ce site internet dans le cadre des conditions d\'utilisation, sans aucune garantie ou promesse de service. Toute violation de ces conditions résultera en une action en justice. Aucune condition n\'est actuellement justifiable, mais les lois de votre pays s\'appliquent tout de m&ecirc;me. Nous nous réservons le droit de vous refuser l\'accès à ce site internet et vous vous engagez à accepter toutes les conditions concernant l\'utilisation de ce site internet. Le fait de ne pas suivre ces règles se traduirait par une perte de service.
	</div>

	<h3 class="sub_tit">AVIS D\'UTILISATION</h3>
	<div>
		Ce site internet est susceptible de posséder un contenu dont vous n\'&ecirc;tes pas en accord ou que vous ne pouvez pas comprendre. Si vous persistez à naviguer sur ce site internet, sachez que toute personne ou société qui concerne, implicitement ou explicitement, Ankama, n\'est autorisée à voir notre contenu. Toute personne qui révélerait des informations privées de ce site internet à une société/entreprise ou autre entitée (y compris Ankama, ou autre fournisseur du jeu DOFUS) n\'est pas autorisée à afficher le contenu de ce site internet.
		Ce domaine est une propriétée privée et exploitée, sans aucune application commerciale ou à but lucratif. Si vous utilisez nos liens, nos téléchargements ou que vous vous inscrivez, vous confirmer &ecirc;tre pleinement d\'accord avec toutes les conditions d\'utilisation de ce site internet. En acceptant les conditions d\'utilisation, vous acceptez également que les règles, les avis et le contenu puissent &ecirc;tres modifiés à tout moment.
	</div>

	<h3 class="sub_tit">AVIS DE LEGALIT&Eacute;</h3>
	<div>
		Si une information de ce site internet venait à &ecirc;tre divulguée à une société/entreprise ou autre entitée lié explicitement ou implicitement à Ankama, cela violerait les termes et conditions de ce site internet.
		Les informations ainsi obtenues seraient illégales et donc non recevable devant un tribunal. Si ce site internet et/ou ses serveurs sont forcés de fermer, nous ne pourrions &ecirc;tre tenus pour responsable de toute perte qui en découlerait.
	</div>

	<h3 class="sub_tit">AVIS SUR LES DONATIONS</h3>
	<div>
		En faisant don au serveur, vous acceptez les règles et conditions d\'utilisation du service indiquées ci-dessous. Nous nous réservons le droit d\'interdire, de supprimer ou d\'effectuer toute action nécessaire si l\'un de vos personnages transgresse implicitement ou explicitement les règles. Si vous souhaitez effectuer un don, mais ne voulez
		rien en retour, veuillez nous le préciser par email à %2$s. Veuillez également ne pas envoyez d\'argent aux membres de l\'équipe, mais utilisez uniquement notre site internet, c\'est ici le seul endroit o&ugrave; votre argent pourra aider au développement de notre communauté. Nous vous informons que les crédits re&ccedil;us en jeu suite à vos donations sont valables sans durée limite.
	</div>

	<h3 class="sub_tit">AVIS SUR LA SéCURIT&Eacute;</h3>
	<div>
		En créant un compte sur %1$s, vous acceptez que la sécurité de celui-ci soit sous votre responsabilitée et que vous serez donc tenu pour responsable de toutes les actions qui sont effectuée à destination ou en provenance de votre compte. Vous reconnaissez également &ecirc;tre conscient que le fait de donner votre mot de passe peut compromettre la sécurité de votre compte, et qu\'en donnant votre mot de passe à une personne tierce, vous en acceptez pleinement toutes les conséquences. Vous acceptez de reconna&ecirc;tre que si vous recevez un fichier de n\'importe qui, sauf du site internet officiel %1$s, il se pourrait qu\'il contienne un logiciel malveillant connu sous le nom de "Key Logger", et que %1$s ne peut &ecirc;tre tenu responsable des conséquences qui surviendraient suite au lancement de ce logiciel à partir de sources extérieures. Vous reconnaissez que %1$s a pris le plus de mesures possibles pour protéger votre compte sur nos serveurs, et que le fait d\'&ecirc;tre piraté, par définition, est presque impossible et très improbable, vous vous engagez donc à&ecirc;tre pleinement responsable si nous ne pouvons confirmer qu\'il s\'agissait d\'une faille venant de notre part.<br><br>
	</div>

	<h3 class="sub_tit">AVIS SUR LES COMPTES ET LA SECURITé</h3>
	<div>
		Comme le compte n\'est pas votre propriété, vous ne pouvez le vendre ni l\'échanger quelles que soient les circonstances. Les "Escroqueries" qui pourraient avoir lieu dans le cas d\'un commerce ou d\'une vente ne seront pas remboursées, et les comptes qui se trouvent avoir été vendus ou échangés seront suspendus indéfiniment et toute les personnes concernées seront bannies de notre réseau.
	</div>

	<h3 class="sub_tit">AVIS SUR LA PROTECTION DE LA VIE PRIVéE</h3>
	<div>
		Toutes les informations recueillies sur nos serveurs ne seront divulguées à aucune personne tierce. Nous respectons votre vie privée et nous nous engageons à ne pas consulter vos conversations ni vos transactions sauf en cas de conflits concernants votre compte ou votre personnage.
	</div>',
);