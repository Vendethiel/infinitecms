```
__________  ____    ___  ___________  __________ _____   ___ __________  ___________  ___________
|___  ____| |   \   | |  |  _______| |____  ___| |    \  | | |____  ___| |____  ____| |  _______|
    | |     | |\ \  | |  |  |______      |  |    | |\  \ | |     | |         |  |     |  |____
    | |     | | \ \ | |  |  _______|     |  |    | | \  \| |     | |         |  |     |  _____|
____| |___  | |  \ \| |  |  |        ____|  |___ | |  \    | ____| |___      |  |     |  |______
|_________| |_|   \___|  |__|        |_________| |_|   \___| |_________|     |__|     |_________|

___________ ____    ___  ___________
| _________||   \  /   | |  _______|
| |         | |\ \/ /| | |  |______
| |         | | \__/ | | |_______  |
| |________ | |      | |  ______|  |
|__________||_|      |_| |_________|
```

Note : la version stylisée du document est sur le GitHub

### LE CMS ~ De vos Rêves


 - Vous ne pouvez pas vendre ce CMS
You can't sell this CMS

 - Vous pouvez distribuer vos thèmes
You can release your templates
 - Vous ne pouvez pas vendre vos thèmes (sauf avec accord)
You can not sell your templates
 - Vous pouvez distribuer vos traductions
You can release your translations
 - Vous pouvez modifier ce CMS, mais vous ne devez pas le distribuer à votre nom
You can modify the CMS, but you can't release your modifications under your name

## Do not upload / Ne pas envoyer:
 - `changelog.txt`
 - `readme`
 - `bin/`
 - `config/`

## Configuration
Veuillez lire de haut en bas le fichier `config.php`.
Il y a de nombreuses options pour personnaliser le CMS, comme par exemple pour CyonEmu.

## Installation
## Si vous pouvez installer Node.JS
*(i.e. serveur dédié)*

Télécharger Node.JS depuis `npmjs.org` et installer.
Si vous obtenez un message comme quoi node n'est toujours pas installé, ouvrez `config.php` et ajoutez après `<?php ` :
```perl
define('NODE_BINARY', 'mon_chemin_node');
# pour Windows - NE PAS INSTALLER DANS "Program Files" ! Les noms avec des espaces peuvent poser problème.
define('NODE_BINARY', 'C:/nodejs/node');
#ou alors
define('NODE_BINARY', '/usr/bin/node');
```


## Si vous ne pouvez pas installer node.js
*(i.e. hébergeur mutualisé)*

### Si vous voulez lancer le CMS depuis la racine de votre site
*(i.e. http://monsite.fr/)*
Il n'est pas nécessaire, dans ce cas, de faire une modification.
Les fichiers que vous avez téléchargés contiennent un dossier cache/ pré-rempli pour marcher à la racine

### Sinon
*(i.e. http://monsite.fr/cms/)*
Dans ce cas, le fichier peut vous servir de démarrage mais il empêchera certaines fonctionnalités de marcher.

Vous devez commencer par vider votre dossier `cache/`.

Lancez d'abord le CMS sur votre machine locale (avec WampServer par exemple), avec le même dossier d'accès :
Si vous voulez plus tard accéder au CMS via `monsite.fr/`, alors installez Infinite directement à la racine de votre serveur.
Si vous le voulez dans `monsite.fr/cms/`, il doit être accédé via `localhost/cms/`.

Ce premier lancement permettra à Node.JS (installé sur votre machine) de générer les fichiers cache.
4 fichier seront alors générés dans `cache/` :
 - `dependencies_yuuki.css.txt`
 - `dependencies_yuuki.js.txt`
 - `file_779d50d10aa1d4d50d7354587ca953dc_yuuki.css`
 - `file_4284e8847d3e444231cc2a8d9cd59cf2_yuuki.js`

Si les deux premiers fichiers ne nous intéressent pas, les deux autres sont les fichiers de style à renommer.
Retirez dans les deux la partie du milieu, nommée "hash", pour garder "file_" et la fin (nom du thème et extension).
Le premier (`file_779d50d10aa1d4d50d7354587ca953dc_yuuki.css`) deviendra `file_yuuki.css` et le second `file_yuuki.js`.
(bien sûr, si vous utilisez un autre thème que yuuki, ces valeurs seront différentes).


## Note
4 thèmes sont distribués avec Infinite :
 - woa (thème original de base)
 - yuuki (thème par Kitty, codé par Keldrael)
 - fantasia (thème par Kitty, codé par Keldrael)
 - asterion12 (thème de Nicow, pour asterion V12 / N'cms 3)
 - pandora (thème glace par BlackStar)


# Problème courants
## Par quoi commencer ?
## Erreur serveur
Si vous avez une `Internal server error` (erreur 500), supprimez le fichier `.htaccess`.
## Tout d'abord, vous devriez normalement avoir un de ces messages :

`Problems during the init. Please contact the server admin.`

`Problems during page loading. Please contact the server admin.`

Il faut activer le mode debug. Ce mode est activé par défaut si vous accédez au CMS via `127.0.0.1` ou `localhost`.
Si vous ne pouvez pas, vous pouvez le forcer.
Pour ceci, vous devez créer un fichier nommé `debug` à la racine du CMS.
Vous devriez avoir l'erreur. Si vous ne comprenez pas l'erreur, vous pouvez toujours vous référer à google.
Note : une erreur sur une table signifie que la table n'existe pas OU que ce n'est pas la bonne (venant d'un autre CMS par exemple) : vous devez la supprimer et la refaire.
Note : une erreur `hydrate ...` signifie souvent que vous avez 2 objets (dans `item_templates`) avec le même `id`.



## Modification du thème
### Ajouter un fichier dans le thème
Dans le thème, les fichiers sont gérés par l'**asset pipeline**, ce qui permet d'avoir des fichiers comme  `style.css.less.php`.
(le système utilisé est [Sprockets-PHP](http://github.com/Nami-Doc/Sprockets-PHP))

Le système de l'"asset pipeline" (gestionnaire de fichiers thèmes) permet d'utiliser les extensions suivantes (optionnelles) :

(note : les extensions en **gras** sont celles utilisées/conseillées. Actuallement, LiveScript, Twig HAML et Stylus sont utilisés)

### Javascript :
 - .coffee : [CoffeeScript](http://coffeescript.org)
 - **.ls**     : [LiveScript](http://livescript.net)

### CSS
 - .less : [LessCSS](http://lesscss.org)
 - .sass : [SASS](http://sass-lang.com)
 - .scss : [SASSy CSS](http://sass-lang.com)
 - **.styl** : [Stylus](http://learnboost.github.com/stylus/)

### HTML :
 - .haml : [HAML](http://haml-lang.com) (via [MtHaml:PHP](https://github.com/arnaud-lb/MtHaml))
 - .mustache : [Mustache](http://mustache.github.com/)
 - .php : [PHP](http://php.net)
 - .talus : [Talus'TPL](http://www.talus-works.net/)
 - .twig : [Twig](http://twig-project.org)
 - **.taml** : [MtHaml:Twig](https://github.com/arnaud-lb/MtHaml)

Chaque action peut avoir un thème, qui doit se nommer :controller/:action et qui doit être dans app/themes/[theme]/views

Par exemple : app/themes/woa/views/News/index.html(.*filtres).

Les filtres utilisables sont les mêmes que pour les autres fichiers.


# Twig tags
### `partial`
Same as `include`, but uses the asset pipeline (warning : can hurt performances)


# Hackish
editInPlace for select

z-index in jQ.tokenInput
